# Morley debugger

[![VS Marketplace](https://img.shields.io/visual-studio-marketplace/v/serokell-io.michelson-debugger?style=flat&label=VS%20Marketplace&logo=visual-studio-code)](https://marketplace.visualstudio.com/items?itemName=serokell-io.michelson-debugger)
[![Open VSX](https://img.shields.io/open-vsx/v/serokell-io/michelson-debugger?color=green)](https://open-vsx.org/extension/serokell-io/michelson-debugger)

Morley debugger allows running a Tezos smart contract and tracking its execution.

There are two versions: simple console tool and fully fledged VSCode extension.

## VSCode extension

The extension is located in [morley-debugger-vscode](./morley-debugger-vscode),
to obtain more information read the `README.md` there.

## Console debugger

The console debugger is a simplified version of the VS Code extension with a
minimal feature set and it is located in the current directory.

### Built from source

To build the console debugger from source you'll need the [Haskell Tool Stack](https://docs.haskellstack.org/)
installed on your system.

You can build and run a given command at once with `stack exec`, use:
```sh
stack exec -- morley-debugger-console --help
```
for a help message, the `demo` command
```sh
stack exec -- morley-debugger-console demo
```
for a demo launch or the `run` command for a normal launch, for example:
```sh
stack exec -- morley-debugger-console run --contract contracts/add1.tz --storage 1 --parameter 1
```

### Using docker

As an alternative to building morley-debugger from scratch, you can use
[`morley-debugger.sh`](https://gitlab.com/morley-framework/morley-debugger/-/blob/master/scripts/morley-debugger.sh) script that wraps docker image
with morley-debugger-console executable.

Note: if you are using MacOS you will need to have `coreutils` installed,
in order to do that run:
```sh
brew install coreutils
```

Execute:
```sh
./scripts/morley-debugger.sh
```
to see the help message.

An example of launching it is:
```sh
./scripts/morley-debugger.sh run --contract contracts/add1.tz --storage 1 --parameter 1
```
