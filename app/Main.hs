-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Simple terminal application which demostrates debugger capabilities.
module Main
  ( main
  ) where

import Data.Version (showVersion)
import Named ((!))
import Options.Applicative (execParser, fullDesc, header, helper, info, progDesc)
import Options.Applicative qualified as Opt
import Paths_morley_debugger (version)

import Morley.CLI
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Runtime (prepareContract)
import Morley.Michelson.Runtime.Dummy (dummyContractEnv)
import Morley.Michelson.TypeCheck (typeCheckContract, typeVerifyParameter, typeVerifyStorage)
import Morley.Michelson.Typed (Contract, Contract'(..), SomeContract(..))
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Util.Main (wrapMain)

import DebuggerDemo
import InputProcessor
import Morley.Debugger.Core.Common

data DbgCmd
  = Run DbgRunOptions
  | Demo

data DbgRunOptions = DbgRunOptions
  { groContractFile :: FilePath
  , groStorageValue :: U.Value
  , groParameterValue :: U.Value
  , groEntrypoint :: U.EpName
  }

cmdParser :: Opt.Parser DbgCmd
cmdParser = Opt.hsubparser $ mconcat
  [ runSubCmd
  , demoSubCmd
  ]
  where
    runSubCmd = Opt.command "run" $ info
      (Run <$> runOptions)
      (progDesc "Debug given contract")

    demoSubCmd = Opt.command "demo" $ info
      (pure Demo)
      (progDesc "Debug demo contract")

    runOptions = do
      groContractFile <- contractFileOption
      groStorageValue <-
        valueOption Nothing
        ! #name "storage"
        ! #help "Storage of a running contract"
      groParameterValue <-
        valueOption Nothing
        ! #name "parameter"
        ! #help "Parameter for a contract run"
      groEntrypoint <- entrypointOption
        ! #name "entrypoint"
        ! #help "Call specific entrypoint"
      return DbgRunOptions{..}

programInfo :: Opt.ParserInfo DbgCmd
programInfo = info (helper <*> versionOption <*> cmdParser) $
  mconcat
  [ fullDesc
  , progDesc "Morley-debugger: terminal app with minimal debugger functionality"
  , header "Morley tools"
  ]
  where
    versionOption = Opt.infoOption ("morley-debugger-" <> showVersion version)
      (Opt.long "version" <> Opt.help "Show version.")

main :: IO ()
main = wrapMain $ do
  cmd <- execParser programInfo

  case cmd of
    Run DbgRunOptions{..} -> do
      uContract <- prepareContract (Just groContractFile)
      SomeContract (contract@Contract{} :: Contract cp st) <-
        either error pure $
          typeCheckingForDebugger $ typeCheckContract uContract
      epcRes <-
        maybe (error "Specified entrypoint not found") pure $
        T.mkEntrypointCall groEntrypoint (cParamNotes contract)
      case epcRes of
        T.MkEntrypointCallRes (_ :: T.Notes arg) epc -> do
          param <-
            either error pure $
            typeCheckingForDebugger $
            typeVerifyParameter @arg mempty groParameterValue
          storage <-
            either error pure $
            typeCheckingForDebugger $
            typeVerifyStorage groStorageValue
          runDebugger (MSFile groContractFile) contract epc param storage dummyContractEnv
    Demo ->
      debuggerDemo
