-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Morley.Util.Main (wrapMain)

import Protocol.DAP (compileHandlers)
import Protocol.DAP.Serve.IO
  (ServingRequestsOptions(..), defaultRequestsServingOptions, servingRequestsIO)

import Morley.Debugger.DAP.Handlers (safenHandler)
import Morley.Debugger.DAP.RIO (logMessage)
import Morley.Debugger.DAP.Types (RIO, newRioContext)
import Morley.Debugger.DAP.Types.Morley (Morley, morleyHandlers)


main :: IO ()
main = wrapMain $ do
  ctx <- newRioContext
  usingReaderT ctx $ servingRequestsIO defaultRequestsServingOptions
    { log = usingReaderT ctx . logMessage
    }
    \stopAdapter -> compileHandlers @(RIO Morley) (safenHandler <$> morleyHandlers stopAdapter)
