-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module InputProcessor
  ( runDebugger
  ) where

import Control.Lens (makeLenses, zoom, (.=))
import Data.Text qualified as T
import "morley-prelude" Fmt (build, fmt, listF, pretty)
import System.Console.Haskeline

import Morley.Debugger.Core
import Morley.Debugger.DAP.Types.Morley (mkMorleyDebuggerState)
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Typed qualified as T

data ConsoleDebuggerState = ConsoleDebuggerState
  { _cdsDebuggerState :: DebuggerState InterpretSnapshot
  , _cdsPreviousStack :: [ SomeStackElem ]
  }

makeLenses ''ConsoleDebuggerState

mkConsoleDebuggerState
  :: MichelsonSource
  -> InterpretHistory InterpretSnapshot
  -> T.ContractCode cp st
  -> ConsoleDebuggerState
mkConsoleDebuggerState source his cCode =
  let
    ds = mkMorleyDebuggerState source his cCode
  in
    ConsoleDebuggerState ds (stackElemsFromDebuggerState ds)

availableCommands :: [Text]
availableCommands =
  [ "exit"
  , "status"
  , "next"
  , "next:<steps num>"
  , "prev"
  , "prev:<steps num>"
  , "break:<line num>:<column num>"
  , "breakpoints"
  , "continue:<line num>:<column num>"
  , "reverse:<line num>:<column num>"
  ]

-- | 'InputT' does not have mtl instances, so it's not possible to lift it nicer than this.
--
-- 'outputStrLn' advertises "cross-platform output of text that may contain Unicode characters",
-- so we use it instead of 'putTextLn'.
outputTextLn :: Text -> MaybeT (StateT (ConsoleDebuggerState) (InputT IO)) ()
outputTextLn = lift . lift . outputStrLn . toString

-- | Execute debugger app.
runDebugger
  :: MichelsonSource
  -> T.Contract cp st
  -> T.EntrypointCallT cp arg
  -> T.Value arg
  -> T.Value st
  -> ContractEnv
  -> IO ()
runDebugger source contract@T.Contract{cCode} epc arg st env = do
  let his = collectInterpretSnapshots source contract epc arg st env
  putTextLn $
    "Debug started. Available commands:" <>
    mconcat (map ("\n * " <>) availableCommands)
  putTextLn ""

  void . runInputT defaultSettings . evaluatingStateT (mkConsoleDebuggerState source his cCode) . runMaybeT . forever $ do
    -- Nothing from getInputLine means end of input, e.g. ^D.
    cmd <- (lift $ lift $ getInputLine "morley> ") >>= maybe mzero (return . fromString)
    case cmd of
      "" -> pass
      "exit" -> mzero

      "status" -> do
        ConsoleDebuggerState{..} <- get
        let DebuggerState{..} = _cdsDebuggerState
        is@InterpretSnapshot{..} <- zoom cdsDebuggerState (frozen curSnapshot)
        let SnapshotNo spNo = issSnapNo
        outputTextLn $ "Current state: #" <> pretty spNo <> " / " <> case issStatus of
          InterpretRunning _ -> "running"
          InterpretTerminatedOk _ -> "execution finished"
          InterpretFailed InterpretFailedArg{..} -> "failed: " <> pretty ifaError

        outputTextLn "-----------------------------------"
        outputTextLn "Current stack: "
        let stacks = stackToElems $ issStack is
        let stackLength = length stacks
        forM_ (zip [0..] stacks) $ \((i :: Int), SomeStackElem v) ->
          putTextLn ("* " <> pretty (stackLength - i - 1) <> ": " <> (fmt $ debugBuild DpmNormal v))

        outputTextLn "-----------------------------------"
        let stackDiffs = getStackDiff _cdsPreviousStack $ stackElemsFromSnapshot is
        putTextLn $ "Stack diff: " <> buildStackDiffWithIndex stackDiffs

        outputTextLn "-----------------------------------"
        loc <- zoom cdsDebuggerState (frozen getExecutedPosition)
        outputTextLn $ "Current location: " <> pretty (maybe "<Nothing>" build loc)
        outputTextLn "-----------------------------------"
        outputTextLn $ "Breakpoints: " <> prettyBreakpoints _dsSources
        outputTextLn ""

      "next" -> do
        setPreviousStack
        result <- zoom cdsDebuggerState $ moveRaw Forward
        case result of
          HitBoundary -> outputTextLn "Already at end\n"
          MovedToException exception -> outputTextLn $ "Reached exception: " <> pretty exception
          MovedSuccessfully _ -> pass
          ReachedTerminatedOk _ -> pass

      (T.stripPrefix "next:" -> Just (readMaybe @Int . toString -> Just num)) -> do
        setPreviousStack
        results <- replicateM num $ zoom cdsDebuggerState $ moveRaw Forward
        case findStopped results of
          HitBoundary -> outputTextLn "Reached end\n"
          MovedToException exception -> outputTextLn $ "Reached exception: " <> pretty exception
          MovedSuccessfully _ -> pass
          ReachedTerminatedOk _ -> pass

      "prev" -> do
        setPreviousStack
        result <- zoom cdsDebuggerState $ moveRaw Backward
        case result of
          HitBoundary -> outputTextLn "Already at beginning\n"
          _ -> pass

      (T.stripPrefix "prev:" -> Just (readMaybe @Int . toString -> Just num)) -> do
        setPreviousStack
        results <- replicateM num $ zoom cdsDebuggerState $ moveRaw Backward
        case findStopped results of
          HitBoundary -> outputTextLn "Reached beginning\n"
          _ -> pass

      "breakpoints" -> do
        breakpoints <- _dsSources <$> use cdsDebuggerState
        outputTextLn $ prettyBreakpoints breakpoints

      (parseCmdWithSrcPos 0 "break" -> Just mPos)
        | Just (SourceLocation _ startPos _) <- mPos -> do
            r <- zoom cdsDebuggerState $ switchBreakpoint source startPos
            case r of
              Nothing ->
                outputTextLn "Couldn't find appropriate location for the breakpoint"
              Just (nextInstrPos, Nothing) ->
                outputTextLn $ "Breakpoint at " <> pretty nextInstrPos <> " removed"
              Just (nextInstrPos, Just bId) ->
                outputTextLn $ "Breakpoint at " <> pretty nextInstrPos <> " set up with " <> pretty bId
        | otherwise -> outputTextLn "Please specify line (and optionally column) to switch breakpoint"

      (parseCmdWithSrcPos 0 "continue" -> Just mPos) ->
        void $ runWriterT $ zoom cdsDebuggerState $ continueUntilBreakpoint $ breakpointSelectorFromMaybe mPos
      (parseCmdWithSrcPos maxBound "reverse" -> Just mPos) ->
        void $ runWriterT $ zoom cdsDebuggerState $ reverseContinue $ breakpointSelectorFromMaybe mPos

      -- NOTE: When adding new commands, don't forget to update 'availableCommands'

      other -> outputTextLn $ "Unknown command: " <> pretty other
  where
    parseCmdWithSrcPos :: Word -> Text -> Text -> Maybe (Maybe SourceLocation)
    parseCmdWithSrcPos defCol cmd inp = do
      suffix <- T.stripPrefix cmd inp
      case T.split (== ':') suffix of
        ["", num] -> do
          ln <- readMaybe @Word (toString num)
          pure $ Just $ pointSourceLocation source (SrcLoc ln defCol)
        ["", num1, num2] -> do
          ln  <- readMaybe @Word (toString num1)
          col <- readMaybe @Word (toString num2)
          pure $ Just $ pointSourceLocation source (SrcLoc ln col)
        [""] -> pure Nothing
        _ -> Nothing

    prettyBreakpoints :: Map MichelsonSource DebugSource -> Text
    prettyBreakpoints = pretty . listF . concatMap (toList . _dsBreakpoints . snd) . toPairs

    -- Find whether the result of moving reached a boundary or an exception. If
    -- so, stop at the first occurrence. Otherwise return @MovedSuccessfully@.
    findStopped :: [MovementResult ()] -> MovementResult ()
    findStopped = fromMaybe (MovedSuccessfully ()) . find (not . moveSucceeded)

    -- Set the previous stack for diff.
    setPreviousStack :: MonadState ConsoleDebuggerState m => m ()
    setPreviousStack = do
      debuggerState <- use cdsDebuggerState
      cdsPreviousStack .= stackElemsFromDebuggerState debuggerState


    -- Pretty print the stack including the index.
    buildStackDiffWithIndex :: [StackDiffWithIndex] -> Text
    buildStackDiffWithIndex = \case
      [] -> "none."
      stackDiffs -> "\n" <>
        ( stackDiffs
          <&> (\(i, stackDiffType) -> case stackDiffType of
                  StackDiffAdd v ->
                    "* Added #" <> pretty i <> ": " <> (fmt @Text $ debugBuild DpmNormal v)
                  StackDiffRemove v ->
                    "* Removed #" <> pretty i <> ": " <> (fmt @Text $ debugBuild DpmNormal v)
                  StackDiffModify o n ->
                    "* Modified #" <> pretty i <> ": " <> (fmt @Text $ debugBuild DpmNormal o)
                    <> " -> " <> (fmt @Text $ debugBuild DpmNormal n)
              )
          &  reverse -- We want to display the latest change first.
          &  foldWithNewLine
        )

    foldWithNewLine :: [Text] -> Text
    foldWithNewLine input = foldl' (\acc el ->
        if (acc == "")
        then el
        else acc <> "\n" <> el
      ) "" input
