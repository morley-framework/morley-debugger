-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module DebuggerDemo
  ( debuggerDemo
  ) where

import Lorentz ((#))
import Lorentz qualified as L
import Morley.Michelson.Runtime.Dummy
import Morley.Michelson.Typed
  (ForbidOr, ParameterScope, StorageScope, ToT, defaultContract, epcPrimitive)

import InputProcessor
import Morley.Debugger.Core.Snapshots (ContractEnv, lorentzSource)

demoContract :: L.ContractCode (Integer, Natural) Integer
demoContract = L.mkContractCode $
  L.unpair # L.unpair # L.add # L.add #
  L.nil # L.pair

runDebuggerSimpleL
  :: forall cp st.
     (L.NiceParameterFull cp, L.NiceStorage st, ForbidOr (ToT cp), ParameterScope (ToT cp), StorageScope (ToT st))
  => L.ContractCode cp st -> cp -> st -> ContractEnv -> IO ()
runDebuggerSimpleL (L.unContractCode -> contract) arg st env =
  runDebugger lorentzSource (defaultContract (L.compileLorentz contract)) epcPrimitive
    (L.toVal arg) (L.toVal st) env

debuggerDemo :: IO ()
debuggerDemo = runDebuggerSimpleL demoContract (3, 3) 5 dummyContractEnv
