# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

require_relative 'helpers'

def check_changelogs
  # Changelog check
  gitfiles = git.modified_files + git.added_files
  appendMarker = /<!-- Unreleased: append new entries here -->/
  iid = githost.mr_json['iid']
  changelog_head = "* [!#{iid}](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/#{iid})"
  changelog_title = "  #{githost.mr_title_payload.sub(/^\[[^\]]+\] */, '')}"
  git.diff.each do |file|
    Pathname.new(file.path).dirname().ascend do |dir|
      changelogpath = dir + 'CHANGES.md'
      if File.file?(changelogpath) then
        # Changelog wasn't updated
        if not gitfiles.include?(changelogpath.to_s) then
          lines = File.foreach(changelogpath)
          # Check if we do update the changelog (i.e. if appendMarker is there)
          if lines.grep(appendMarker).any? then
            markdown(
              ":warning: You didn't update the changelog `#{changelogpath.to_s}`:\n\n"\
              "Maybe something like this will work?\n\n"\
              "```markdown\n"\
              "#{changelog_head}\n"\
              "#{changelog_title}\n"\
              "```"
            )
          end
        else #Changelog was updated
          state = 0
          File.foreach(changelogpath).with_index do |line, i|
            if state == 0 then
              if appendMarker.match?(line) then
                # Found append marker
                state = 1
                next
              end
            elsif state == 1 then
              if not /^ *$/.match?(line) then
                # Found first nonblank line after append marker
                state = 2
                # Check changelog header
                if line != "#{changelog_head}\n" then
                  markdown(
                    ":warning: Incorrect changelog header:\n\n"\
                    "```suggestion:-0+0\n"\
                    "#{changelog_head}\n"\
                    "```", file: changelogpath.to_s, line: i+1
                  )
                end
                next
              end
            else
              # look at the next nonblank line (supposedly title)
              if /^\* */.match?(line) then
                markdown(
                  ":warning: Changelog title line is apparently missing.\n\n"\
                  "```suggestion:-0+0\n"\
                  "#{changelog_head}\n"\
                  "#{changelog_title}\n"\
                  "```", file: changelogpath.to_s, line: i
                )
              elsif /^ *[+*\-] */.match?(line) then
                markdown(
                  ":warning: Changelog title line starts with a list.\n\n"\
                  "Perhaps you intended to write the title?\n\n"\
                  "```suggestion:-0+0\n"\
                  "#{line.gsub(/^ *[+*\-] */, '  ')}"\
                  "```", file: changelogpath.to_s, line: i + 1
                )
              end
              break
            end
          end
        end
      end
    end
  end
end
