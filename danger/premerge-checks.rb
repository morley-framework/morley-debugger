# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# Checks that are fine to fail during development, but must be fixed before merging.

require_relative 'helpers'

# Fixup commits
if git.commits.any? &:fixup?
  fail "Some fixup commits are still there."
end

# Work-in-progress commits
if git.commits.any? &:wip?
  fail "WIP commits are still there."
end

# Unresolved TODOs
def scan_todos

  def wrap_brackets(regexp)
    return Regexp.new('\[' + regexp.source + '\]')
  end

  def prepend_todo(regexp)
    return Regexp.new('TODO\s*' + regexp.source)
  end

  def any_issue_regexp(with_brackets = false)
    # example: #123
    gitlab_issue_regexp = /#\d+/
    # example: TM-381
    youtrack_issue_regexp = /\w+-\d+/
    if with_brackets
      return Regexp.union(
        wrap_brackets(gitlab_issue_regexp),
        wrap_brackets(youtrack_issue_regexp)
      )
    else
      return Regexp.union(
        gitlab_issue_regexp,
        youtrack_issue_regexp
      )
    end
  end

  # Get list of resolved issues from the MR body
  def get_resolved_issues(mr_body)
    resolves_regexp = /(?<resolves_line>Resolves(.*))$/
    if resolves_match = mr_body.match(resolves_regexp)
      resolves = resolves_match[:resolves_line]
      return resolves.scan(any_issue_regexp())
    end
    return []
  end

  def build_todo_regexp(mr_body)
    # TODOs related to the current MR
    todo_this_mr_regexp = /TODO\s*\[\s*this\s+MR\s*\]/

    resolved_issues = get_resolved_issues(mr_body)

    # Build a regexp that represents a union of all resolved issues
    # and '[this MR]'
    unresolved_todo_regexp = todo_this_mr_regexp
    resolved_issues.each { |issue_id|
      issue_regexp = /TODO\s*\[#{issue_id}\]/
      unresolved_todo_regexp = Regexp.union(unresolved_todo_regexp, issue_regexp)
    }

    return unresolved_todo_regexp
  end

  unresolved_todo_regexp = build_todo_regexp(githost.mr_body)
  # example: TODO [#123]: blah blah blah
  todo_any_issue_regexp = prepend_todo(any_issue_regexp(with_brackets = true))
  ignored_files =
    [ 'premerge-checks.rb',
      'code-style.md',
      'CHANGES.md',
      'morley-debugger-vscode/images/'
    ]
  tracked_files = %x[git ls-files]
  tracked_files
  .lines
  .reject { |path| ignored_files.any? { |ignored_file| path.include?(ignored_file) } }
  .each { |path|
    file = path.rstrip
    expanded_path = File.expand_path(file)
    File.foreach(expanded_path).with_index do |line, index|
      if line.match?(unresolved_todo_regexp)
        warn("Found an unresolved TODO.", file: file, line: index + 1)
      elsif line.include?("TODO") && !line.match?(todo_any_issue_regexp)
        todo_section_link = "https://gitlab.com/morley-framework/morley/-/blob/master/docs/code-style.md#todos"
        failure(
          "Found a TODO without issue id. \
          See the [TODOs section](#{todo_section_link}) in the style guide.",
          file: file,
          line: index + 1
        )
      end
    end
  }
end

scan_todos()
