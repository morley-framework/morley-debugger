-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Provides Haskell interface for
-- [Debug Adapter Protocol](https://microsoft.github.io/debug-adapter-protocol/specification)
-- from Microsoft.
module Protocol.DAP
    ( module M
    ) where

import Protocol.DAP.EventBase as M
  (EventSubmitIO, EventSubmitPure, IsEvent(..), MonadEventSubmit(..), submitEvent)
import Protocol.DAP.EventTypes as M
import Protocol.DAP.HelperTypes as M
import Protocol.DAP.Message as M
import Protocol.DAP.Mk as M
import Protocol.DAP.RequestBase as M
  (Handler, Handler'(..), HandlerBody, HandlersSet, IsRequest(..), IsRequestToAdapter,
  IsRequestToClient, ResponseAndLaterActions, compileHandlers, embedHandler, inappropriateDapState,
  ixHandler, mapHandler, mapHandler', mapRunHandler, mkHandler, respond, respondAndAlso)
import Protocol.DAP.RequestTypes as M
import Protocol.DAP.ReverseRequestTypes as M
import Protocol.DAP.Serve.Types as M
import Protocol.DAP.Types as M
import Protocol.DAP.Util as M (ClosedEnum, OpenEnum, dapAesonOptions, dapDeriveMkOptions)
