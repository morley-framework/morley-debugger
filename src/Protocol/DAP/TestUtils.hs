-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utilities for writing tests.
module Protocol.DAP.TestUtils
  ( areHandlersExhaustive
  , ExhaustivenessError (..)
  ) where

import Data.List qualified as List
import "fmt" Fmt (Buildable(..), blockListF)

import Protocol.DAP.RequestBase
import Protocol.DAP.RequestTypes
import Protocol.DAP.Types

data ExhaustivenessError = ExhaustivenessError
  { eeMissingCommands :: NonEmpty Text
  } deriving stock (Show, Eq)

instance Buildable ExhaustivenessError where
  build (ExhaustivenessError missing) =
    "Some requests remain unhandled, their commands:\n" <> blockListF missing

-- | Check whether the supplied set of handlers contains all the handlers
-- expected from the adapter.
--
-- This depends on the declared 'Capabilities', since there you claim support
-- for certain requests.
areHandlersExhaustive :: Capabilities -> [Handler' m n] -> Either ExhaustivenessError ()
areHandlersExhaustive Capabilities{..} handlers =
  let
    presentCommandHandlers = map handlerCommand handlers
    missingCommandHandlers = expectedToHandleRequests List.\\ presentCommandHandlers
  in maybe pass (Left . ExhaustivenessError) $ nonEmpty missingCommandHandlers
  where
    expectedToHandleRequests = catMaybes
      [ requestCommand @CancelRequest
          <$ guard (supportsCancelRequest ?: False)
      , requestCommand @InitializeRequest
          & Just
      , requestCommand @ConfigurationDoneRequest
          <$ guard (supportsConfigurationDoneRequest ?: False)
      , requestCommand @LaunchRequest
          & Just
      , requestCommand @AttachRequest
          & Just
      , requestCommand @RestartRequest
          <$ guard (supportsRestartRequest ?: False)
      , requestCommand @DisconnectRequest
          & Just
      , requestCommand @TerminateRequest
          <$ guard (supportsTerminateRequest ?: False)
      , requestCommand @BreakpointLocationsRequest
          <$ guard (supportsBreakpointLocationsRequest ?: False)
      , requestCommand @SetBreakpointsRequest
          & Just
      , requestCommand @SetFunctionBreakpointsRequest
          <$ guard (supportsFunctionBreakpoints ?: False)
      , requestCommand @SetExceptionBreakpointsRequest
          <$ guard (not $ null (exceptionBreakpointFilters ?: []))
      , requestCommand @DataBreakpointInfoRequest
          <$ guard (supportsDataBreakpoints ?: False)
      , requestCommand @SetDataBreakpointsRequest
          <$ guard (supportsDataBreakpoints ?: False)
      , requestCommand @SetInstructionBreakpointsRequest
          <$ guard (supportsInstructionBreakpoints ?: False)
      , requestCommand @ContinueRequest
          & Just
      , requestCommand @NextRequest
          & Just
      , requestCommand @StepInRequest
          & Just
      , requestCommand @StepOutRequest
          & Just
      , requestCommand @StepBackRequest
          <$ guard (supportsStepBack ?: False)
      , requestCommand @ReverseContinueRequest
          <$ guard (supportsStepBack ?: False)
      , requestCommand @RestartFrameRequest
          <$ guard (supportsRestartFrame ?: False)
      , requestCommand @GotoRequest
          <$ guard (supportsGotoTargetsRequest ?: False)
      , requestCommand @PauseRequest
          & Just
      , requestCommand @StackTraceRequest
          & Just
      , requestCommand @ScopesRequest
          & Just
      , requestCommand @VariablesRequest
          & Just
      , requestCommand @SetVariableRequest
          <$ guard (supportsSetVariable ?: False)
      , requestCommand @SourceRequest
          & Just
      , requestCommand @ThreadsRequest
          & Just
      , requestCommand @TerminateThreadsRequest
          <$ guard (supportsTerminateThreadsRequest ?: False)
      , requestCommand @ModulesRequest
          <$ guard (supportsModulesRequest ?: False)
      , requestCommand @LoadedSourcesRequest
          <$ guard (supportsLoadedSourcesRequest ?: False)
      , requestCommand @EvaluateRequest
          & Just
      , requestCommand @SetExpressionRequest
          <$ guard (supportsSetExpression ?: False)
      , requestCommand @StepInTargetsRequest
          <$ guard (supportsStepInTargetsRequest ?: False)
      , requestCommand @GotoRequest
          <$ guard (supportsGotoTargetsRequest ?: False)
      , requestCommand @CompletionsRequest
          <$ guard (supportsCompletionsRequest ?: False)
      , requestCommand @ExceptionInfoRequest
          <$ guard (supportsExceptionInfoRequest ?: False)
      , requestCommand @ReadMemoryRequest
          <$ guard (supportsReadMemoryRequest ?: False)
      , requestCommand @WriteMemoryRequest
          <$ guard (supportsWriteMemoryRequest ?: False)
      , requestCommand @DisassembleRequest
          <$ guard (supportsDisassembleRequest ?: False)
      ]
