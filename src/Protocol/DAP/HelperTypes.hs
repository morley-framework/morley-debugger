-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Smart helper types.
--
-- They do not appear in DAP spec, rather introduced to be more idiomatic in
-- our Haskell implementation of the interface.
module Protocol.DAP.HelperTypes where

import Prelude hiding (id)

import Data.Aeson (FromJSON(..), ToJSON(..))
import Data.Aeson qualified as Aeson
import Data.Aeson.Types qualified as Aeson
import "fmt" Fmt (Buildable(..))

----------------------------------------------------------------------------
-- Simple newtype wrappers
----------------------------------------------------------------------------

newtype SourceId = SourceId { unSourceId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype ThreadId = ThreadId { unThreadId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype StackFrameId = StackFrameId { unStackFrameId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

-- | In single-threaded languages enumeration makes sense.
deriving newtype instance Enum StackFrameId

newtype VariableId = VariableId { unVariableId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype BreakpointId = BreakpointId { unBreakpointId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype StepInTargetId = StepInTargetId { unStepInTargetId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype GotoTargetId = GotoTargetId { unGotoTargetId :: Int }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype ClientId = ClientId { unClientId :: Text }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype AdapterId = AdapterId { unAdapterId :: Text }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype ExceptionId = ExceptionId { unExceptionId :: Text }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype ExceptionFilterId = ExceptionFilterId { unExceptionFilterId :: Text }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

newtype ProgressId = ProgressId { unProgressId :: Text }
  deriving newtype (Eq, Ord, Show, FromJSON, ToJSON)

pattern NoVariableId :: VariableId
pattern NoVariableId = VariableId 0

-- Buildable instances
----------------------------------------------------------------------------

instance Buildable SourceId where
  build (SourceId x) = "source #" <> build x

instance Buildable ThreadId where
  build (ThreadId x) = "thread #" <> build x

instance Buildable StackFrameId where
  build (StackFrameId x) = "stack frame #" <> build x

instance Buildable VariableId where
  build (VariableId x) = "variable #" <> build x

instance Buildable BreakpointId where
  build (BreakpointId x) = "breakpoint #" <> build x

instance Buildable StepInTargetId where
  build (StepInTargetId x) = "step in target #" <> build x

instance Buildable GotoTargetId where
  build (GotoTargetId x) = "goto target #" <> build x

instance Buildable ClientId where
  build (ClientId x) = "client `" <> build x <> "`"

instance Buildable AdapterId where
  build (AdapterId x) = "adapter `" <> build x <> "`"

instance Buildable ExceptionId where
  build (ExceptionId x) = "exception `" <> build x <> "`"

instance Buildable ExceptionFilterId where
  build (ExceptionFilterId x) = "exception filter `" <> build x <> "`"

instance Buildable ProgressId where
  build (ProgressId x) = "progress `" <> build x <> "`"

----------------------------------------------------------------------------
-- More complex types
----------------------------------------------------------------------------

-- | Module identifier.
data ModuleId
  = TextModuleId Text
  | NumModuleId Word
  deriving stock (Show, Eq, Generic)

instance Buildable ModuleId where
  build = \case
    TextModuleId t -> "\"" <> build t <> "\""
    NumModuleId n -> "#" <> build n

instance ToJSON ModuleId where
  toJSON = \case
    TextModuleId t -> toJSON t
    NumModuleId n -> toJSON n
instance FromJSON ModuleId where
  parseJSON v = case v of
    Aeson.String{} -> TextModuleId <$> parseJSON v
    Aeson.Number{} -> NumModuleId <$> parseJSON v
    other -> Aeson.unexpected other

-- | Line or column number.
type Pos = Word

-- | Keeps a number between 0 and 100.
newtype Percent = UnsafePercent { unPercent :: Double }
  deriving stock (Show, Eq, Ord)
  deriving newtype (ToJSON)

instance Buildable Percent where
  build (UnsafePercent p) =
    build (round p :: Int) <> "%"

-- | Safe constructor for 'Percent'.
mkPercent :: Double -> Percent
mkPercent = UnsafePercent . min 100 . max 0

-- | Construct percent from a fractional number.
--
-- import Data.Ratio
-- >>> pretty $ percentFromReal (1 % 5) :: Text
-- "20%"
percentFromReal :: Real f => f -> Percent
percentFromReal = mkPercent . realToFrac . (* 100)
