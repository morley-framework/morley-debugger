-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE TypeFamilyDependencies #-}

-- | Convenient constructors for large product types.
module Protocol.DAP.Mk
  ( Mk (..)
  , module Protocol.DAP.Mk.TH
  ) where

import Protocol.DAP.Mk.Class
import Protocol.DAP.Mk.TH
