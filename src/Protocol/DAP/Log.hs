-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Trivial logging capability.
module Protocol.DAP.Log
  ( MonadLog (..)

  , NoLog
  , NoLogT (..)
  ) where

import Control.Monad.Writer.Strict qualified as W
import "fmt" Fmt (Builder)
import UnliftIO (MonadUnliftIO)

class Monad m => MonadLog m where
  log :: Builder -> m ()

-- | Discards logs.
newtype NoLogT m a = NoLog { runNoLog :: m a }
  deriving newtype ( Functor, Applicative, Monad, MonadIO, MonadUnliftIO
                   , MonadReader r, MonadState s, W.MonadWriter w
                   , MonadThrow, MonadCatch, MonadMask )

type NoLog = NoLogT Identity

instance Monad m => MonadLog (NoLogT m) where
  log _ = pass
