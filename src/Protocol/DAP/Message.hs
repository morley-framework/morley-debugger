-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Generic messages like @Request@, @Response@, e.t.c.
--
-- This is approximately the same list that is provided in
-- [@Base Protocol@ section of the specification](https://microsoft.github.io/debug-adapter-protocol/specification#Base_Protocol).
module Protocol.DAP.Message
  ( MessageDirection (..)
  , RequestMessage (..)
  , ResponseMessage (..)
  , EventMessage (..)
  , MessageToClient (..)

  , SeqId (..)
  , initSeqId
  , nextSeqId
  , ErrorDetails (..)
  , Error (..)
  , customError
  ) where

import Prelude hiding (id, seq)

import Data.Aeson (FromJSON, ToJSON(..), object, withObject, (.!=), (.:), (.:?), (.=))
import Data.Aeson qualified as Aeson
import Data.Aeson.TH (deriveToJSON)
import "fmt" Fmt (Buildable(..), genericF, indentF)
import "fmt" Fmt.Internal.Core (FromBuilder(..))

import Protocol.DAP.Util

data MessageDirection
  -- | Message from the client to the adapter
  = MessageToAdapter
  -- | Message from the adapter to the client
  | MessageToClient

type family ReverseMessageDirection d where
  ReverseMessageDirection 'MessageToAdapter = 'MessageToClient
  ReverseMessageDirection 'MessageToClient = 'MessageToAdapter

-- | Sequence number appearing in messages.
newtype SeqId (d :: MessageDirection) = SeqId { unSeqId :: Word }
  deriving newtype (Show, Eq, Ord, ToJSON, FromJSON)

instance Buildable (SeqId d) where
  build (SeqId i) = "#" <> build i

-- | Initial sequence number.
initSeqId :: SeqId d
initSeqId = SeqId 1

-- | Get next sequence number, assuming we keep an appropriate state.
nextSeqId :: MonadState (SeqId d) m => m (SeqId d)
nextSeqId = state \(SeqId i) -> (SeqId i, SeqId (i + 1))

-- | General request message.
--
-- Most of requests have 'MessageToAdapter' direction, and reverse requests
-- carry 'MessageToClient' direction.
data RequestMessage (d :: MessageDirection) = RequestMessage
  { seq :: SeqId d
  , command :: Text
  , args :: Aeson.Value
  } deriving stock (Show, Eq)

instance Buildable (RequestMessage d) where
  build RequestMessage{..} =
    "Request " <> build command <> " " <> build seq <> ":\n" <> indentF 2 (build (AnyType args))

instance FromJSON (RequestMessage d) where
  parseJSON = withObject "request message" \o -> do
    o .: "type" >>= \case
      "request" -> pass
      other -> fail $ "Unexpected message type, wanted request, got '" <> other <> "'"
    command <- o .: "command"
    seq <- o .: "seq"
    args <- o .:? "arguments"
      -- nullary objects encode to @[]@ in aeson
      .!= Aeson.Array mempty
    return RequestMessage{..}

-- | A message describing an error.
--
-- This is the [Message](https://microsoft.github.io/debug-adapter-protocol/specification#Types_Message)
-- type from the DAP spec, the original name of which we find misleading.
--
-- We suggest several ways of constructing a value of this type:
--
-- 1. From string literal with human-readable text:
--    @"Things went wrong" :: ErrorDetails@.
--    This is a quick way suitable for custom messages that won't require
--    any handling at the client side. You may not worry about other fields,
--    or fill them immediately with a record update.
-- 2. From a constant or function: declare @myMessage :: ErrorDetails@ constant,
--    using constructor or as in (1), define @id@, @format@, @url@ and other
--    fields fixed, and use this constant.
--    In case your error assumes filling @variables@ field, turn the constant
--    into function.
--    @unhandledCommandError@ is a good example of such @ErrorDetails@ or @Error@.
data ErrorDetails = ErrorDetails
  { id :: Integer
  , format :: Text
  , variables :: Map Text Text
  , sendTelemetry :: Bool
  , showUser :: Bool
  , url :: Maybe Text
  , urlLabel :: Maybe Text
  } deriving stock (Eq, Show, Generic)

instance Buildable ErrorDetails where
  build = genericF

instance FromBuilder ErrorDetails where
  fromBuilder txt = ErrorDetails
    { id = 0
    , format = fromBuilder txt
    , variables = mempty
    , sendTelemetry = False
    , showUser = True
    , url = Nothing
    , urlLabel = Nothing
    }

instance IsString ErrorDetails where
  fromString = fromBuilder . build

deriveToJSON Aeson.defaultOptions ''ErrorDetails

-- | All the information that is sent in case of error.
data Error = Error
  { message :: Maybe Text
  , body :: Maybe ErrorDetails
  } deriving stock (Eq, Show, Generic)

instance Buildable Error where
  build (Error msg body) = build msg <> " / " <> build body

-- | Create a custom 'Error' from a 'ErrorDetails', in assumption that the message
-- contains all the necessary information.
customError :: ErrorDetails -> Error
customError = Error Nothing . Just

instance FromBuilder Error where
  fromBuilder txt =
    customError (fromBuilder txt)

instance IsString Error where
  fromString = fromBuilder . build

-- | General response message.
--
-- Requests of 'MessageToAdapter' direction will have response with
-- 'MessageToClient' direction, and vice versa.
data ResponseMessage (d :: MessageDirection) = ResponseMessage
  { seq :: SeqId d
    -- ^ Sequence number of the message itself.
  , requestSeq :: SeqId (ReverseMessageDirection d)
    -- ^ Sequence number of the corresponding request.
  , command :: Text
  , outcome :: Either Error Aeson.Value
  } deriving stock (Eq, Show, Generic)

instance Buildable (ResponseMessage d) where
  build ResponseMessage{..} =
    "Response " <> build command <> " " <> build seq <> " on " <>
    build requestSeq <> "\n" <> indentF 2 do
      case outcome of
        Left err -> "Error " <> build err
        Right res -> build (AnyType res)

instance ToJSON (ResponseMessage d) where
  toJSON respMsg = do
    let
      encodedTop = object
        [ "seq" .= respMsg.seq
        , "request_seq" .= respMsg.requestSeq
        , "type" .= ("response" :: Text)
        , "command" .= respMsg.command
        , "success" .= isRight respMsg.outcome
        ]
    encodedTop `mergeObjects`
      case respMsg.outcome of
        Right body -> object
          [ "body" .= body
          ]
        Left Error{..} -> object $ catMaybes
          [ message <&> \msg -> "message" .= msg
          , body <&> \bd -> "body" .= object
              [ "error" .= bd
              ]
          ]

-- | General event message.
data EventMessage = EventMessage
  { seq :: SeqId 'MessageToClient
  , event :: Text
  , body :: Aeson.Value
  } deriving stock (Show, Eq)

instance Buildable EventMessage where
  build EventMessage{..} =
    "Event " <> build event <> " " <> build seq <> ":\n" <> indentF 2 (build $ AnyType body)

instance ToJSON EventMessage where
  toJSON ev = object
    [ "type" .= ("event" :: Text)
    , "event" .= ev.event
    , "seq" .= ev.seq
    , "body" .= ev.body
    ]

-- | Some message sent from adapter to client.
data MessageToClient
  = ResponseMessageToClient (ResponseMessage 'MessageToClient)
  | EventMessageToClient EventMessage
  deriving stock (Show, Eq)

instance Buildable MessageToClient where
  build = \case
    ResponseMessageToClient resp -> build resp
    EventMessageToClient ev -> build ev

instance ToJSON MessageToClient where
  toJSON = \case
    ResponseMessageToClient resp -> toJSON resp
    EventMessageToClient ev -> toJSON ev
