-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | All types from the [Events section of the DAP spec](https://microsoft.github.io/debug-adapter-protocol/specification#Events).
module Protocol.DAP.EventTypes where

import Prelude hiding (group)

import Control.Lens (ix)
import Data.Aeson.TH (deriveFromJSON, deriveToJSON)
import Data.Char qualified as Char
import "fmt" Fmt (pretty)
import "fmt" Fmt.Internal.Core (FromBuilder(..))
import Language.Haskell.TH qualified as TH

import Protocol.DAP.EventBase
import Protocol.DAP.HelperTypes
import Protocol.DAP.Message
import Protocol.DAP.Mk
import Protocol.DAP.Types
import Protocol.DAP.Util

-- | @Initialized@ Event
--
-- This event indicates that the debug adapter is ready to accept configuration
-- requests (e.g. @setBreakpoints@, @setExceptionBreakpoints@).
--
-- A debug adapter is expected to send this event when it is ready to accept
-- configuration requests (but not before the initialize request has finished).
--
-- The sequence of events/requests is as follows:
--
-- * adapters sends initialized event (after the initialize request has
--   returned)
-- * client sends zero or more @setBreakpoints@ requests
-- * client sends one @setFunctionBreakpoints@ request (if corresponding
--   capability `supportsFunctionBreakpoints` is true)
-- * client sends a @setExceptionBreakpoints@ request if one or more
--   exceptionBreakpointFilters have been defined (or if
--   @supportsConfigurationDoneRequest@ is not true)
-- * client sends other future configuration requests
-- * client sends one @configurationDone@ request to indicate the end of the
--   configuration.
data InitializedEvent = InitializedEvent

-- | @Stopped@ Event
--
-- The event indicates that the execution of the debuggee has stopped due to
-- some condition.
--
-- This can be caused by a breakpoint previously set, a stepping request has
-- completed, by executing a debugger statement etc.
data StoppedEvent = StoppedEvent
  { {- |
      The reason for the event.

      For backward compatibility this string is shown in the UI if the
      @description@ attribute is missing (but it must not be translated).
    -}
    reason :: OpenEnum "stopReason"
      [ "step", "breakpoint", "exception", "pause", "entry", "goto"
      , "function breakpoint", "data breakpoint", "instruction breakpoint"
      ]

    {- |
      The full reason for the event, e.g. @Paused on exception@. This string is
      shown in the UI as is and can be translated.
    -}
  , description :: Maybe Text

    {- |
      The thread which was stopped.
    -}
  , threadId :: Maybe ThreadId

    {- |
      A value of true hints to the client that this event should not change the
      focus.
    -}
  , preserveFocusHint :: Maybe Bool

    {- |
      Additional information. E.g. if reason is @exception@, text contains the
      exception name. This string is shown in the UI.
    -}
  , text :: Maybe Text

    {- |
      If `allThreadsStopped` is true, a debug adapter can announce that all
      threads have stopped.

      * The client should use this information to enable that all threads can
      be expanded to access their stacktraces.
      * If the attribute is missing or false, only the thread with the given
      @threadId@ can be expanded.
    -}
  , allThreadsStopped :: Maybe Bool

    {- |
      Ids of the breakpoints that triggered the event. In most cases there is
      only a single breakpoint but here are some examples for multiple
      breakpoints:

      * Different types of breakpoints map to the same location.
      * Multiple source breakpoints get collapsed to the same instruction by
      the compiler/runtime.
      * Multiple function breakpoints with different function names map to the
      same location.
    -}
  , hitBreakpointIds :: Maybe (NonEmpty BreakpointId)
  }

-- | @Continued@ Event
--
-- The event indicates that the execution of the debuggee has continued.
--
-- Please note: a debug adapter is not expected to send this event in response
-- to a request that implies that execution continues, e.g. @launch@ or
-- @continue@.
--
-- It is only necessary to send a continued event if there was no previous
-- request that implied this.
data ContinuedEvent = ContinuedEvent
  { {- |
      The thread which was continued.
    -}
    threadId :: ThreadId

    {- |
      If @allThreadsContinued@ is true, a debug adapter can announce that all
      threads have continued.
    -}
  , allThreadsContinued :: Maybe Bool
  }

-- | @Exited@ Event
--
-- The event indicates that the debuggee has exited and returns its exit code.
data ExitedEvent = ExitedEvent
  { {- |
      The exit code returned from the debuggee.
    -}
    exitCode :: Int
  }

-- | @Terminated@ Event
--
-- The event indicates that debugging of the debuggee has terminated. This does
-- not mean that the debuggee itself has exited.
type TerminatedEvent = Maybe TerminatedEventBody

data TerminatedEventBody = TerminatedEvent
  { {- |
      A debug adapter may set `restart` to true (or to an arbitrary object) to
      request that the client restarts the session.

      The value is not interpreted by the client and passed unmodified as an
      attribute `__restart` to the `launch` and `attach` requests.
    -}
    restart :: Maybe AnyType
  }

-- | @Thread@ Event
--
-- The event indicates that a thread has started or exited.
data ThreadEvent = ThreadEvent
  { {- |
      The reason for the event.
    -}
    reason :: OpenEnum "threadEventReason" ["started", "exited"]

    {- |
      The identifier of the thread.
    -}
  , threadId :: ThreadId
  }

-- | @Output@ Event
--
-- The event indicates that the target has produced some output.
data OutputEvent = OutputEvent
  { {- |
      The output category. If not specified or if the category is not
      understood by the client, `console` is assumed.

      Values:

      * @console@: Show the output in the client's default message UI, e.g. a
      @debug console@. This category should only be used for informational
      output from the debugger (as opposed to the debuggee).
      * @important@: A hint for the client to show the output in the client's UI
      for important and highly visible information, e.g. as a popup
      notification. This category should only be used for important messages
      from the debugger (as opposed to the debuggee). Since this category value
      is a hint, clients might ignore the hint and assume the `console`
      category.
      * @stdout@: Show the output as normal program output from the debuggee.
      * @stderr@: Show the output as error program output from the debuggee.
      * @telemetry@: Send the output to telemetry instead of showing it to the
      user.

      etc.
    -}
    category :: Maybe $ OpenEnum "outputEventCategory"
      ["console", "important", "stdout", "stderr", "telemetry"]

    {- |
      The output to report.
    -}
  , output :: Text

    {- |
      Support for keeping an output log organized by grouping related messages.
      Values:

      * @start@: Start a new group in expanded mode. Subsequent output events are
      members of the group and should be shown indented.
      The @output@ attribute becomes the name of the group and is not indented.
      * @startCollapsed@: Start a new group in collapsed mode. Subsequent output
      events are members of the group and should be shown indented (as soon as
      the group is expanded).
      The @output@ attribute becomes the name of the group and is not indented.
      * @end@: End the current group and decrease the indentation of subsequent
      output events.
      A non-empty `output` attribute is shown as the unindented end of the
      group.
    -}
  , group :: Maybe (ClosedEnum ["start", "startCollapsed", "end"])

    {- |
      If an attribute `variablesReference` exists and its value is > 0, the
      output contains objects which can be retrieved by passing
      `variablesReference` to the `variables` request as long as execution
      remains suspended. See @Lifetime of Object References@ in the Overview
      section for details.
    -}
  , variablesReference :: Maybe VariableId

    {- |
      The source location where the output was produced.
    -}
  , source :: Maybe Source

    {- |
      The source location's line where the output was produced.
    -}
  , line :: Maybe Pos

    {- |
      The position in `line` where the output was produced. It is measured in
      UTF-16 code units and the client capability `columnsStartAt1` determines
      whether it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      Additional data to report. For the `telemetry` category the data is sent
      to telemetry, for the other categories the data is shown in JSON format.
    -}
  , data_ :: Maybe AnyType
  }

instance FromBuilder OutputEvent where
  fromBuilder msg = OutputEvent
    { category = Nothing
    , output = fromBuilder msg
    , group = Nothing
    , variablesReference = Nothing
    , source = Nothing
    , line = Nothing
    , column = Nothing
    , data_ = Nothing
    }

instance IsString OutputEvent where
  fromString = fromBuilder . pretty

-- | @Breakpoint@ Event
--
-- The event indicates that some information about a breakpoint has changed.
data BreakpointEvent = BreakpointEvent
  { {- |
      The reason for the event.
    -}
    reason :: OpenEnum "breakpointEventReason" ["changed", "new", "removed"]

    {- |
      The `id` attribute is used to find the target breakpoint, the other
      attributes are used as the new values.
    -}
  , breakpoint :: Breakpoint
  }

-- | @Module@ Event
--
-- The event indicates that some information about a module has changed.
data ModuleEvent = ModuleEvent
  { {- |
      The reason for the event.
    -}
    reason :: ClosedEnum ["new", "changed", "removed"]

    {- |
      The new, changed, or removed module. In case of `removed` only the module
      id is used.
    -}
  , module_ :: Module
  }

-- | @LoadedSource@ Event
--
-- The event indicates that some source has been added, changed, or removed from
-- the set of all loaded sources.
data LoadedSourceEvent = LoadedSourceEvent
  { {- |
      The reason for the event.
    -}
    reason :: ClosedEnum ["new", "changed", "removed"]

    {- |
      The new, changed, or removed source.
    -}
  , source :: Source
  }

-- | @Process@ Event
--
-- The event indicates that the debugger has begun debugging a new process.
-- Either one that it has launched, or one that it has attached to.
data ProcessEvent = ProcessEvent
  { {- |
      The logical name of the process. This is usually the full path to
      process's executable file. Example: @/home/example/myproj/program.js@.
    -}
    name :: Text

    {- |
      The system process id of the debugged process. This property is missing
      for non-system processes.
    -}
  , systemProcessId :: Maybe Int32

    {- |
      If true, the process is running on the same computer as the debug
      adapter.
    -}
  , isLocalProcess :: Maybe Bool

    {- |
      Describes how the debug engine started debugging this process.

      Values:

      * @launch@: Process was launched under the debugger.
      * @attach@: Debugger attached to an existing process.
      * @attachForSuspendedLaunch@: A project launcher component has launched a
      new process in a suspended state and then asked the debugger to attach.
    -}
  , startMethod :: Maybe $ ClosedEnum
      ["launch", "attach", "attachForSuspendedLaunch"]

    {- |
      The size of a pointer or address for this process, in bits. This value
      may be used by clients when formatting addresses for display.
    -}
  , pointerSize :: Maybe Word
  }

-- | @Capabilities@ Event
--
-- The event indicates that one or more capabilities have changed.
--
-- Since the capabilities are dependent on the client and its UI, it might not
-- be possible to change that at random times (or too late).
--
-- Consequently this event has a hint characteristic: a client can only be
-- expected to make a ‘best effort’ in honoring individual capabilities but
-- there are no guarantees.
--
-- Only changed capabilities need to be included, all other capabilities keep
-- their values.
data CapabilitiesEvent = CapabilitiesEvent
  { {- |
      The set of updated capabilities.
    -}
    capabilities :: Capabilities
  }

-- | @ProgressStart@ Event
--
-- The event signals that a long running operation is about to start and
-- provides additional information for the client to set up a corresponding
-- progress and cancellation UI.
--
-- The client is free to delay the showing of the UI in order to reduce flicker.
--
-- This event should only be sent if the corresponding capability
-- `supportsProgressReporting` is true.
data ProgressStartEvent = ProgressStartEvent
  { {- |
      An ID that can be used in subsequent @progressUpdate@ and @progressEnd@
      events to make them refer to the same progress reporting.

      IDs must be unique within a debug session.
    -}
    progressId :: ProgressId

    {- |
      Short title of the progress reporting. Shown in the UI to describe the
      long running operation.
    -}
  , title :: Text

    {- |
      The request ID that this progress report is related to. If specified a
      debug adapter is expected to emit progress events for the long running
      request until the request has been either completed or cancelled.

      If the request ID is omitted, the progress report is assumed to be
      related to some general activity of the debug adapter.
    -}
  , requestId :: Maybe (SeqId 'MessageToAdapter)

    {- |
      If true, the request that reports progress may be cancelled with a
      @cancel@ request.

      So this property basically controls whether the client should use UX that
      supports cancellation.

      Clients that don't support cancellation are allowed to ignore the
      setting.
    -}
  , cancellable :: Maybe Bool

    {- |
      More detailed progress message.
    -}
  , message :: Maybe Text

    {- |
      Progress percentage to display (value range: 0 to 100). If omitted no
      percentage is shown.
    -}
  , percentage :: Maybe Percent
  }

-- | @ProgressUpdate@ Event
--
-- The event signals that the progress reporting needs to be updated with a new
-- message and/or percentage.
--
-- The client does not have to update the UI immediately, but the clients needs
-- to keep track of the message and/or percentage values.
--
-- This event should only be sent if the corresponding capability
-- `supportsProgressReporting` is true.
data ProgressUpdateEvent = ProgressUpdateEvent
  { {- |
      The ID that was introduced in the initial @progressStart@ event.
    -}
    progressId :: ProgressId

    {- |
      More detailed progress message. If omitted, the previous message (if any)
      is used.
    -}
  , message :: Maybe Text

    {- |
      Progress percentage to display (value range: 0 to 100). If omitted no
      percentage is shown.
    -}
  , percentage :: Maybe Percent
  }

-- | @ProgressEnd@ Event
--
-- The event signals the end of the progress reporting with a final message.
--
-- This event should only be sent if the corresponding capability
-- `supportsProgressReporting` is true.
data ProgressEndEvent = ProgressEndEvent
  { {- |
      The ID that was introduced in the initial `ProgressStartEvent`.
    -}
    progressId :: ProgressId

    {- |
      More detailed progress message. If omitted, the previous message (if any)
      is used.
    -}
  , message :: Maybe Text
  }

-- | @Invalidated@ Event
--
-- This event signals that some state in the debug adapter has changed and
-- requires that the client needs to re-render the data snapshot previously
-- requested.
--
-- Debug adapters do not have to emit this event for runtime changes like
-- stopped or thread events because in that case the client refetches the new
-- state anyway. But the event can be used for example to refresh the UI after
-- rendering formatting has changed in the debug adapter.
--
-- This event should only be sent if the corresponding capability
-- `supportsInvalidatedEvent` is true.
data InvalidatedEvent = InvalidatedEvent
  { {- |
      Set of logical areas that got invalidated. This property has a hint
      characteristic: a client can only be expected to make a best effort in
      honoring the areas but there are no guarantees. If this property is
      missing, empty, or if values are not understood, the client should assume
      a single value @all@.
    -}
    areas :: Maybe [InvalidatedAreas]

    {- |
      If specified, the client only needs to refetch data related to this
      thread.
    -}
  , threadId :: Maybe ThreadId

    {- |
      If specified, the client only needs to refetch data related to this stack
      frame (and the `threadId` is ignored).
    -}
  , stackFrameId :: Maybe StackFrameId
  }

-- | @Memory@ Event
--
-- This event indicates that some memory range has been updated. It should only
-- be sent if the corresponding capability `supportsMemoryEvent` is true.
--
-- Clients typically react to the event by re-issuing a readMemory request if
-- they show the memory identified by the memoryReference and if the updated
-- memory range overlaps the displayed range. Clients should not make
-- assumptions how individual memory references relate to each other, so they
-- should not assume that they are part of a single continuous address range and
-- might overlap.
--
-- Debug adapters can use this event to indicate that the contents of a memory
-- range has changed due to some other request like @setVariable@ or
-- @setExpression@. Debug adapters are not expected to emit this event for each
-- and every memory change of a running program, because that information is
-- typically not available from debuggers and it would flood clients with too
-- many events.
data MemoryEvent = MemoryEvent
  { {- |
      Memory reference of a memory range that has been updated.
    -}
    memoryReference :: Text

    {- |
      Starting offset in bytes where memory has been updated. Can be negative.
    -}
  , offset :: Int

    {- |
      Number of bytes updated.
    -}
  , count :: Word
  }


-- TH Instances
----------------------------------------------------------------------------

-- To avoid boilerplate we generate the instances automatically with TH for all
-- datatypes.
--
-- Note that you still can define instances manually next to your types,
-- the TH generator below will notice that.

$( do
  let
    allEvents =
      [ "Breakpoint"
      , "Capabilities"
      , "Continued"
      , "Exited"
      , "Initialized"
      , "Invalidated"
      , "LoadedSource"
      , "Memory"
      , "Module"
      , "Output"
      , "Process"
      , "ProgressEnd"
      , "ProgressStart"
      , "ProgressUpdate"
      , "Stopped"
      , "Terminated"
      , "Thread"
      ]

  let nulifyDeclsWhen decls cond = if cond then pure [] else decls

  concatForM allEvents \event -> do
    eventTyName <- TH.lookupTypeName (event <> "Event")
      >>= maybe (fail $ "Such event type is not declared: " <> event) pure
    let eventTy = TH.conT eventTyName

    -- some events have separate bodies
    mEventBodyTyName <- TH.lookupTypeName (event <> "EventBody")

    isEventTypeAlias <- TH.reify eventTyName <&> \case
      TH.TyConI TH.TySynD{} -> True
      _ -> False

    dropExtraInstances $ concat <$> sequence
      [ -- IsEvent instance
        [d| instance IsEvent $eventTy where
              type EventName $eventTy =
                $(TH.litT $ TH.strTyLit $ ix 0 %~ Char.toLower $ event)
          |]

        -- A couple of basic instances
      , [d| deriving stock instance Eq $eventTy |]
          `nulifyDeclsWhen` isEventTypeAlias
      , [d| deriving stock instance Show $eventTy |]
          `nulifyDeclsWhen` isEventTypeAlias
      , deriveMkWith dapDeriveMkOptions eventTyName
          `nulifyDeclsWhen` isEventTypeAlias

        -- JSON instance
      , deriveToJSON dapAesonOptions eventTyName
          `nulifyDeclsWhen` isEventTypeAlias
      , deriveFromJSON dapAesonOptions eventTyName
          `nulifyDeclsWhen` (isEventTypeAlias || eventTyName `elem`
              [ -- some fields lack FromJSON instance
                ''ProgressStartEvent
              , ''ProgressUpdateEvent
              , ''CapabilitiesEvent
              ])

      , case mEventBodyTyName of
          Just eventBodyTyName -> concat <$> sequence
            [ [d| deriving stock instance Eq $(TH.conT eventBodyTyName) |]
            , [d| deriving stock instance Show $(TH.conT eventBodyTyName) |]
            , deriveToJSON dapAesonOptions eventBodyTyName
            , deriveMkWith dapDeriveMkOptions eventBodyTyName
            ]
          Nothing -> pure []
      ]
  )
