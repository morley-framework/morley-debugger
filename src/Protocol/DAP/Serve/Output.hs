-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Protocol.DAP.Serve.Output
  ( outputWriter
  ) where

import Control.Concurrent.STM.TBChan (TBChan, readTBChan)
import Data.Aeson (ToJSON(..), encode)
import Data.ByteString qualified as BS
import Data.ByteString.Lazy.Char8 qualified as BSL
import "fmt" Fmt (pretty)
import System.IO (hFlush)

separator :: BSL.ByteString
separator = "\r\n\r\n"

outputWriter
  :: (ToJSON a)
  => TBChan a -> Handle -> IO Void
outputWriter chan handle = do
  forever $ do
    join $ atomically $ do
      msg <- readTBChan chan
      return $
        liftIO $ writeDAPMessage handle msg

writeDAPMessage :: ToJSON a => Handle -> a -> IO ()
writeDAPMessage handle resp = do
  let encoded = encode resp
  let encLength = BSL.length encoded
  let msg = "Content-Length: " <> pretty encLength
        <> separator
        <> encoded
        <> separator

  BS.hPutStr handle $ BSL.toStrict msg
  hFlush handle
