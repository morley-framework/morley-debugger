{-# LANGUAGE OverloadedRecordDot #-}

-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
module Protocol.DAP.Serve.IO
  ( servingRequestsIO
  , ServingRequestsOptions (..)
  , defaultRequestsServingOptions
  , StopAdapter (..)
  ) where

import Control.Concurrent.STM.TBChan (newTBChanIO, writeTBChan)
import Control.Concurrent.STM.TBMChan (newTBMChanIO, readTBMChan)
import System.IO (BufferMode(..), hSetBuffering)
import UnliftIO (MonadUnliftIO)
import UnliftIO.Async (concurrently_, race)

import Protocol.DAP.Serve.Input (inputReader)
import Protocol.DAP.Serve.Output (outputWriter)
import Protocol.DAP.Serve.Types (RequestsProcessor(..))

-- HLint wrongfully thinks we are not using these things from universum,
-- perhaps because of morley-prelude.

{-# ANN module ("HLint: ignore Use 'stdin' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'stdout' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'withFile' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'WriteMode' from Universum" :: Text) #-}

data ServingRequestsOptions = ServingRequestsOptions
  { log :: Text -> IO ()
    -- ^ Logging function
  , inBufferLimit :: Word
    -- ^ Limit on the number of messages in the buffer for incoming messages;
    -- stops processing the messages until the free space in the buffer appears.
  , outBufferLimit :: Word
    -- ^ Limit on the number of messages in the buffer for outgoing messages.
    -- In case of overflow the requests processing will be blocked.
  , inHandle :: Handle
    -- ^ The handle from which the requests are read.
  , outHandle :: Handle
    -- ^ The handle to which responses are written.
  }

defaultRequestsServingOptions :: ServingRequestsOptions
defaultRequestsServingOptions = ServingRequestsOptions
  { log = const pass
  , inBufferLimit = 100
  , outBufferLimit = 100
  , inHandle = stdin
  , outHandle = stdout
  }

-- | Says that the adapter should stop after processing the current request.
newtype StopAdapter = StopAdapter { doStopAdapter :: IO () }

-- | Start serving DAP requests.
--
-- On side-effects, this will write to / consume some input from the handles
-- supplied in options and will update their buffering mode.
servingRequestsIO
  :: forall m. MonadUnliftIO m
  => ServingRequestsOptions -> (StopAdapter -> IO (RequestsProcessor m)) -> m ()
servingRequestsIO opts mkReqsProc = do
  -- DAP messages (both input and output) are not newline-terminated, so if
  -- we don't disable buffering, some messages may be not delivered in time.
  liftIO $ hSetBuffering opts.inHandle NoBuffering
  liftIO $ hSetBuffering opts.outHandle NoBuffering

  inChan <- liftIO (newTBMChanIO 100)
  outChan <- liftIO (newTBChanIO 100)

  shouldStopRef <- newIORef False
  let markShouldStopVar = StopAdapter $ writeIORef shouldStopRef True

  reqsProc <- liftIO $ mkReqsProc markShouldStopVar

  let withChans :: m () -> m ()
      withChans action =
        concurrently_ @_ @() @() (liftIO $ inputReader inChan opts.inHandle) $
          fmap (either absurd id) $ race (liftIO $ outputWriter outChan opts.outHandle) $
          action

  withChans $
    fmap (maybe () absurd) . runMaybeT . forever $ do
      cmd <- atomically $ readTBMChan inChan
      case cmd of
        Nothing -> do
          liftIO $ opts.log "Input pipe was closed, finishing debug adapter"
          mzero
        Just val -> do
          case val of
            Left err -> do
              liftIO $ opts.log $ "Internal error in communication with adapter: " <> toText err
            Right req -> do
              lift $ runRequestsProcessor reqsProc (atomically . writeTBChan outChan) req
              readIORef shouldStopRef >>= guard . not
