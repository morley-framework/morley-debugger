-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA
--
-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-FileCopyrightText: 2016 Alan Zimmerman
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
-- SPDX-License-Identifier: LicenseRef-MIT-alanz

module Protocol.DAP.Serve.Input
  ( inputReader
  ) where

import Control.Concurrent.STM.TBMChan (TBMChan, closeTBMChan, writeTBMChan)
import Data.Aeson (FromJSON, eitherDecode')
import Data.Attoparsec.ByteString qualified as A
import Data.Attoparsec.ByteString.Char8 qualified as A
import Data.ByteString.Builder.Extra (defaultChunkSize)
import Data.ByteString.Char8 qualified as BS
import Data.ByteString.Lazy qualified as BSL
import "fmt" Fmt (fmt, listF, pretty)

-- Based on
-- https://github.com/alanz/haskell-lsp/blob/47d7270c9644941b37a8023bef2a0ddc198a2648/src/Language/Haskell/LSP/Control.hs#L118

separator :: ByteString
separator = "\r\n\r\n"

-- | A parser for one DAP input message.
--
-- It is one header, @Content-Length@ with a numeric value, followed by
-- 'separator' and then that many bytes.
dapParser :: A.Parser ByteString
dapParser = do
  void $ A.string "Content-Length: "
  len <- A.decimal
  void $ A.string separator
  A.take len

-- | Read DAP messages from the given handler and write them to a channel.
--
-- When there are no more messages or an unrecoverable error occurs,
-- the channel is closed.
inputReader :: FromJSON a => TBMChan (Either String a) -> Handle -> IO ()
inputReader chan handle =
  go (A.parse dapParser "") >> atomically (closeTBMChan chan)
  where
    go = \case
      A.Fail _ ctxt err -> do
        atomically $ writeTBMChan chan $ Left $
          fmt $ "Could not parse input: " <> pretty err <> " in " <> listF ctxt
      A.Partial cont -> do
        inp <- BS.hGetSome handle defaultChunkSize
        if null inp
           then pass
           else go $ cont inp
      A.Done rest msg -> do
        let msg' = BSL.fromStrict msg
        atomically $ writeTBMChan chan $ eitherDecode' msg'
        go $ A.parse dapParser rest
