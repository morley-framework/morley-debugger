-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
module Protocol.DAP.Serve.Types
  ( RequestsProcessor (..)
  ) where

import Protocol.DAP.Message

-- Note: this declaration requires a separate module because record-dot-preprocessor
-- produces invalid code here.
--
-- This issue might be fixed by
-- https://github.com/ndmitchell/record-dot-preprocessor/pull/55
-- and is likely to not be a problem with the built-in @RecordDotSyntax@.

-- | General thing that handles incoming requests.
--
-- It is allowed to send several events and one response (the latter is
-- not expressed in types since this part of interface is quite low-level).
data RequestsProcessor m = RequestsProcessor
  { runRequestsProcessor
      :: (MonadIO m)
      => (MessageToClient -> IO ()) -> RequestMessage 'MessageToAdapter -> m ()
  }
