-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | All the reverse request types.
--
-- Those that are mentioned
-- [in the Requests section of the spec](https://microsoft.github.io/debug-adapter-protocol/specification#Reverse_Requests).
module Protocol.DAP.ReverseRequestTypes where

import Prelude hiding (filter, lines)

import Control.Lens (ix)
import Data.Aeson.TH (deriveFromJSON, deriveToJSON)
import Data.Char qualified as Char
import "fmt" Fmt (Buildable(..), genericF)
import Language.Haskell.TH qualified as TH

import Protocol.DAP.Message
import Protocol.DAP.Mk (deriveMkWith)
import Protocol.DAP.RequestBase
import Protocol.DAP.Util

-- | @RunInTerminal@ Request
--
-- This request is sent from the debug adapter to the client to run a command in
-- a terminal.
--
-- This is typically used to launch the debuggee in a terminal provided by the
-- client.
--
-- This request should only be called if the corresponding client capability
-- `supportsRunInTerminalRequest` is true.
--
-- Client implementations of @runInTerminal@ are free to run the command however
-- they choose including issuing the command to a command line interpreter (aka
-- ‘shell’). Argument strings passed to the runInTerminal request must arrive
-- verbatim in the command to be run. As a consequence, clients which use a
-- shell are responsible for escaping any special shell characters in the
-- argument strings to prevent them from being interpreted (and modified) by the
-- shell.
--
-- Some users may wish to take advantage of shell processing in the argument
-- strings. For clients which implement @runInTerminal@ using an intermediary
-- shell, the 'argsCanBeInterpretedByShell' property can be set to true. In this
-- case the client is requested not to escape any special shell characters in
-- the argument strings.
data RunInTerminalRequest = RunInTerminalRequest
  { {- |
      What kind of terminal to launch. Defaults to `integrated` if not specified.
    -}
    kind :: Maybe (ClosedEnum ["integrated", "external"])

    {- |
      Title of the terminal.
    -}
  , title :: Maybe Text

    {- |
      Working directory for the command. For non-empty, valid paths this
      typically results in execution of a change directory command.
    -}
  , cwd :: FilePath

    {- |
      List of arguments. The first argument is the command to run.
    -}
  , args :: [Text]

    {- |
      Environment key-value pairs that are added to or removed from the default
      environment.
    -}
  , env :: Maybe (Map Text (Maybe Text))

    {- |
      This property should only be set if the corresponding capability
      `supportsArgsCanBeInterpretedByShell` is true. If the client uses an
      intermediary shell to launch the application, then the client must not
      attempt to escape characters with special meanings for the shell. The user
      is fully responsible for escaping as needed and that arguments using
      special characters may not be portable across shells.
    -}
  , argsCanBeInterpretedByShell :: Maybe Bool
  }

-- | Response to runInTerminal request.
data RunInTerminalResponse = RunInTerminalResponse
  { {- |
     * The process ID. The value should be less than or equal to 2147483647
     * (2^31-1).
    -}
    processId :: Maybe Int32

    {- |
     * The process ID of the terminal shell. The value should be less than or
     * equal to 2147483647 (2^31-1).
    -}
  , shellProcessId :: Maybe Int32
}

-- | @StartDebugging@ Request
--
-- This request is sent from the debug adapter to the client to start a new
-- debug session of the same type as the caller.
--
-- This request should only be sent if the corresponding client capability
-- `supportsStartDebuggingRequest` is true.
--
-- A client implementation of startDebugging should start a new debug session
-- (of the same type as the caller) in the same way that the caller’s session
-- was started. If the client supports hierarchical debug sessions, the newly
-- created session can be treated as a child of the caller session.
data StartDebuggingRequest = StartDebuggingRequest
  { {- |
      Arguments passed to the new debug session. The arguments must only contain
      properties understood by the `launch` or `attach` requests of the debug
      adapter and they must not contain any client-specific properties (e.g.
      @type@) or client-specific features (e.g. substitutable @variables@).
    -}
    configuration :: Map Text AnyType

    {- |
    * Indicates whether the new debug session should be started with a @launch@
    * or @attach@ request.
    -}
  , request :: ClosedEnum ["launch", "attach"]
  }

-- | Response to @startDebugging@ request. This is just an acknowledgement, so no
-- body field is required.
type StartDebuggingResponse = ()


-- TH Instances
----------------------------------------------------------------------------

-- To avoid boilerplate we generate the instances automatically with TH for all
-- datatypes.
--
-- Note that you still can define instances manually next to your types,
-- the TH generator below will notice that.

$( do
  let
    allCommands =
      [ "RunInTerminal"
      , "StartDebugging"
      ]

  concatForM allCommands \command -> do
    let requestTyName = TH.mkName (command <> "Request")
    let responseTyName = TH.mkName (command <> "Response")

    let requestTy = TH.conT requestTyName
    let responseTy = TH.conT responseTyName

    let nulifyDeclsWhen decls cond = if cond then pure [] else decls

    isResponseTypeAlias <- TH.reify responseTyName <&> \case
      TH.TyConI TH.TySynD{} -> True
      _ -> False

    dropExtraInstances $ concat <$> sequence
      [ -- IsRequest instance
        [d| instance IsRequest $requestTy where
              type DirectionFor $requestTy = 'MessageToClient
              type ResponseFor $requestTy = $responseTy
              type CommandFor $requestTy =
                $(TH.litT $ TH.strTyLit $ ix 0 %~ Char.toLower $ command)
          |]

        -- A couple of basic instances
      , [d| deriving stock instance Generic $requestTy |]
      , [d| deriving stock instance Generic $responseTy |]
          `nulifyDeclsWhen` isResponseTypeAlias
      , [d| instance Buildable $requestTy where build = genericF |]
      , [d| instance Buildable $responseTy where build = genericF |]
          `nulifyDeclsWhen` isResponseTypeAlias
      , deriveMkWith dapDeriveMkOptions requestTyName
      , deriveMkWith dapDeriveMkOptions responseTyName
          `nulifyDeclsWhen` isResponseTypeAlias

        -- JSON instances
      , deriveFromJSON dapAesonOptions requestTyName
      , deriveToJSON dapAesonOptions responseTyName
          `nulifyDeclsWhen` isResponseTypeAlias
      ]
  )
