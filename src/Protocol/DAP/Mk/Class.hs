-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE TypeFamilyDependencies #-}

module Protocol.DAP.Mk.Class
  ( Mk (..)
  ) where

import Named (arg, argF, defaults, (!), (:!), (:?))

{- | Defines a constructor for a product type declared with means of @named@
package.

This helps to easily create values of a type that mixes a few mandatory fields
with multiple optional fields.

@

data MyType = MyType
  { a :: Int
  , b :: Maybe Word
  }

instance Mk MyType where
  type MkSig MyType
    =  "a" :! Int
    -> "b" :? Word
    -> MyType
  mk (arg #a -> a) (argF #b -> b) = MyType{..}

myType1 :: MyType
myType1 = mk @MyType ! #a 1 ! #b 5

myType2 :: MyType
myType2 = mk @MyType ! #a 1 ! defaults
@

If the record field name is made not so pretty to avoid name collisions,
in this typeclass still prefer using the original neat name.

-}
class Mk r where
  type MkSig r = (f :: Type) | f -> r
  mk :: MkSig r


-- Example
----------------------------------------------------------------------------

data MyType = MyType
  { a :: Int
  , b :: Maybe Word
  , c :: Maybe String
  }

instance Mk MyType where
  type MkSig MyType
    =  "a" :! Int
    -> "b" :? Word
    -> "c" :? String
    -> MyType
  mk (arg #a -> a) (argF #b -> b) (argF #c -> c) = MyType{..}

_myType1 :: MyType
_myType1 = mk @MyType ! #a 1 ! #b 5 ! #c "a"

_myType2 :: MyType
_myType2 = mk @MyType ! #a 1 ! defaults
