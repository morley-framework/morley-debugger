-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Protocol.DAP.Mk.TH
  ( deriveMk

  , deriveMkWith
  , DeriveMkOptions(..)
  ) where

import Data.Default (Default(..))
import Language.Haskell.TH hiding (Type)
import Language.Haskell.TH qualified as TH
import Named (arg, argF, (:!), (:?))

import Protocol.DAP.Mk.Class

-- | Derivation options.
data DeriveMkOptions = DeriveMkOptions
  { dmoMapFieldName :: String -> String
    -- ^ How to generate type-level names from record field names.
  }

defaultDeriveMkOptions :: DeriveMkOptions
defaultDeriveMkOptions = DeriveMkOptions
  { dmoMapFieldName = id
  }

instance Default DeriveMkOptions where
  def = defaultDeriveMkOptions

-- | Generate an 'Mk' instance for the type.
deriveMk :: Name -> Q [Dec]
deriveMk = deriveMkWith defaultDeriveMkOptions

deriveMkWith :: DeriveMkOptions -> Name -> Q [Dec]
deriveMkWith opts mainTyName = do
  ConT dollarOp <- [t|($)|]

  -- In case a type stands for an optional - extract the inner type
  let
    isOptional :: TH.Type -> Maybe TH.Type
    isOptional = \case
      ConT appTy `AppT` ty
        | appTy == ''Maybe -> pure ty
      ConT dol `AppT` ConT appTy `AppT` ty
        | dol == dollarOp && appTy == ''Maybe -> pure ty
      _ -> Nothing

  mainTy <- reify mainTyName
  -- reportError $ Debug.show mainTy
  (ctx, tyBinds, con) <- case mainTy of
    TyConI (DataD ctx _tyName binds kind [con] _derivs) -> do
      unless (isNothing kind || kind == Just (ConT ''Type)) $
        fail "Datatype is not of kind Type"
      return (ctx, binds, con)
    _ -> fail $ nameBase mainTyName <> " is not a product type"

  -- may have to be used if polymorphic datatypes appear
  let _ = tyBinds

  (conName, fieldInfos) <- case con of
    RecC conName fields -> pure (conName, fields <&> \(name, _bang, ty) -> (name, ty))
    NormalC conName [] -> pure (conName, [])
    _ -> fail $ nameBase mainTyName <> " is not a plain datatype with records"

  ConT argTyOp <- [t|(:!)|]
  ConT argOptTyOp <- [t|(:?)|]

  -- Construct `MkSig` from right to left
  (tyArgs, pats, fieldAssigns) <- fmap unzip3 $ forM fieldInfos \(fName, fTy) -> do
    -- the name assigned to field from @named@ package's point of view
    let assignedName = dmoMapFieldName opts $ nameBase fName
    -- "name" at type level
    let tyArgName = LitT $ StrTyLit assignedName
    -- variable for the field
    var <- newName (nameBase fName)

    case isOptional fTy of
      Nothing ->
        return
          ( -- "name" :! ty
            InfixT tyArgName argTyOp fTy
            -- (arg #name -> var) pattern
          , (VarE 'arg `AppE` LabelE assignedName) `ViewP` VarP var
            -- field = var
          , (fName, VarE var)
          )
      Just fOptInnerTy ->
        return
          ( -- "name" :? ty'
            InfixT tyArgName argOptTyOp fOptInnerTy
            -- (argF #name -> var)
          , (VarE 'argF `AppE` LabelE assignedName) `ViewP` VarP var
            -- field = var
          , (fName, VarE var)
          )

  return . one $
    -- instance Mk MainTy where
    InstanceD Nothing ctx (ConT ''Mk `AppT` ConT mainTyName)
    [ -- type MainTy =
      TySynInstD $ TySynEqn Nothing (ConT ''MkSig `AppT` ConT mainTyName) $
        -- ("a" :! Int) -> ("b" :? Word) -> ... -> MainTy
        foldr (\tyArg acc -> ArrowT `AppT` tyArg `AppT` acc) (TH.ConT mainTyName) tyArgs
    , -- mk ... = MyType{ a = 1, b = 2, ... }
      FunD 'mk [Clause pats (NormalB $ RecConE conName fieldAssigns) []]
    ]
