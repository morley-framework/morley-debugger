-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Machinery for declaring & processing requests and submitting responses.
module Protocol.DAP.RequestBase
  ( IsRequest (..)
  , IsRequestToAdapter
  , IsRequestToClient
  , requestCommand

    -- * Requests processing via handlers
  , Handler' (..)
  , Handler
  , HandlersSet
  , mkHandler
  , ResponseAndLaterActions (..)
  , respond
  , respondAndAlso
  , HandlerBody
  , handlerCommand
  , embedHandler
  , mapHandler
  , mapHandler'
  , mapRunHandler
  , ixHandler
  , compileHandlers
  , unhandledCommandError
  , messageDecodeError
  , inappropriateDapState

    -- * Encoding requests and parsing responses
  , wrapRequest
  , wantResponseFor
  , wantError
  ) where

import Prelude hiding (id, seq)
import Prelude qualified as P

import Control.Lens qualified as L
import Control.Monad.Except (throwError)
import Control.Monad.Morph (hoist)
import Control.Parallel.Strategies qualified as S
import Data.Aeson (FromJSON, ToJSON(..))
import Data.Aeson qualified as Aeson
import Data.Map qualified as Map
import "fmt" Fmt ((+|), (|+))
import Morley.Util.TypeLits (KnownSymbol, Symbol, symbolValT')
import UnliftIO (MonadUnliftIO, UnliftIO(..), askUnliftIO)

import Protocol.DAP.EventBase
import Protocol.DAP.Log
import Protocol.DAP.Message
import Protocol.DAP.Serve.Types
import Protocol.DAP.Util

class (FromJSON r, KnownSymbol (CommandFor r), ToJSON (ResponseFor r)) => IsRequest r where
  type CommandFor r :: Symbol
  type ResponseFor r :: Type
  type DirectionFor r :: MessageDirection
  type DirectionFor _ = 'MessageToAdapter

type IsRequestToAdapter r = (IsRequest r, DirectionFor r ~ 'MessageToAdapter)
type IsRequestToClient r = (IsRequest r, DirectionFor r ~ 'MessageToClient)

-- | Command associated with the request type.
requestCommand :: forall r. IsRequest r => Text
requestCommand = symbolValT' @(CommandFor r)

-- Handlers
----------------------------------------------------------------------------

-- | A handler for one particular request type.
--
-- It is polymorphic over two monads:
--
-- 1. @m@ is for the main body of the handler.
-- 2. @n@ is for the actions performed after the response is submitted.
data Handler' m n = forall r. (IsRequestToAdapter r) => Handler
  { handlerRequestType :: Proxy r
    -- ^ If we remember the request type it would be simpler to extract it later,
    -- but use 'Handler' constructor if the type is not needed.

  , handlerDeclCallStack :: CallStack
    -- ^ Callstack of the position where the handler is declared.

  , runHandler :: r -> m (ResponseAndLaterActions n (ResponseFor r))
    -- ^ Execute the handler.
  }

-- | A helper type to represent the handler's result.
data ResponseAndLaterActions m resp = ResponseAndLaterActions
  { response :: resp
    -- ^ The response value.
  , laterActions :: m ()
    -- ^ Later actions, e.g. containing events that should be submitted.
  } deriving stock (Functor)

-- | Just send a response. Put this at the end of your handler's logic.
respond :: (Applicative m, Applicative n) => resp -> m (ResponseAndLaterActions n resp)
respond resp = pure $ ResponseAndLaterActions resp pass

-- | Send a response, and then do some further actions.
respondAndAlso :: (Applicative m) => resp -> n () -> m (ResponseAndLaterActions n resp)
respondAndAlso resp later = pure $ ResponseAndLaterActions resp later

type HandlerBody r m =
  r -> ExceptT Error (EventSubmitIO m) $ ResponseAndLaterActions (EventSubmitIO m) (ResponseFor r)

-- | Simple 'Handler'' that is used in most of the cases.
type Handler m = Handler' (ExceptT Error $ EventSubmitIO m) (EventSubmitIO m)

-- | A set of handlers that you put to compilation.
type HandlersSet m = [Handler m]

-- | Construct a handler.
mkHandler
  :: (HasCallStack, IsRequestToAdapter r)
  => (r -> m (ResponseAndLaterActions n (ResponseFor r))) -> Handler' m n
mkHandler run = Handler Proxy callStack run

-- | The command the handler catches.
handlerCommand :: Handler' m n -> Text
handlerCommand (Handler (_ :: Proxy r) _ _) =
  requestCommand @r

-- | Update the body of the handler.
--
-- This is a big hammer that you'd likely not want to use in many situations,
-- consider using 'mapHandler' or 'embedHandler' instead.
mapRunHandler
  :: (forall r. IsRequest r
      => (r -> m1 (ResponseAndLaterActions n1 (ResponseFor r)))
      -> r -> m2 (ResponseAndLaterActions n2 (ResponseFor r))
    )
     -- ^ The action applied to the part after response.
  -> Handler' m1 n1 -> Handler' m2 n2
mapRunHandler f (Handler t cs run) =
  Handler t cs (f run)

-- | Change the monad within a handler.
mapHandler'
  :: (Functor m2)
  => (forall a. m1 a -> m2 a)
     -- ^ The action applied to the part before response.
  -> (n1 () -> n2 ())
     -- ^ The action applied to the part after response.
  -> Handler' m1 n1 -> Handler' m2 n2
mapHandler' f fPost = mapRunHandler \run ->
  fmap
    (\r -> ResponseAndLaterActions
      { response = r.response, laterActions = fPost (laterActions r) }
    )
  . f . run

-- | Change the monad within a simple handler.
--
-- Note: the mapper will be applied twice, before and after responding.
-- This may have undesired effects, e.g. @mapHandler (runningStateT 0)@ will
-- add a 'StateT' layer that resets to 0 at the moment of response.
mapHandler
  :: (Monad m, Functor n)
  => (forall a. m a -> n a)
  -> Handler m -> Handler n
mapHandler f = mapHandler' (hoist $ hoist f) (hoist f)

-- | Add a given action to execute alongside the handler.
--
-- For more control over when (before or after the handler) the action will
-- be executed use 'mapHandler''.
--
-- Any exceptions thrown by the embedded code will be caught and reported
-- via 'MonadLog' instance. If your monad does not support exceptions throwing,
-- use 'embedHandlerNoThrow'.
embedHandler
  :: (MonadLog m, MonadCatch m, HasCallStack)
  => m () -> Handler m -> Handler m
embedHandler action = embedHandlerNoThrow $ action `catchAny` catcher
  where
    catcher e = log $
      "Handler embed threw an exception " +| displayException e |+ "\n\
      \Embedded at: " +| prettyCallStack callStack |+ ""

-- | Variation of 'embedHandler' that does not catch exceptions from the
-- embedded code.
embedHandlerNoThrow :: MonadLog m => m () -> Handler m -> Handler m
embedHandlerNoThrow action = mapHandler' P.id (<* lift action)

-- | Focus on handler(s) that has the same command as given request.
--
-- This is only a valid traversal if you do not change the command
-- of the 'Handler'.
--
-- Use this in conjunction with 'L.singular' to make sure that your update
-- doesn't apply to zero handlers.
ixHandler :: forall r m. IsRequest r => Traversal' (HandlersSet m) (Handler m)
ixHandler = L.traversed . L.filtered ((== requestCommand @r) . handlerCommand)

-- | Turn a pack of 'Handler's into a large handler for an entire
-- 'RequestMessage`, performing handler selection by the command in request.
--
-- The handlers are provided error throwing capabilities, produced errors will
-- be included into the response.
--
-- Event submission capabilities are also provided, use 'submitEvent'.

-- This implementation does no try to use or account for the sequence numbers,
-- so it's suitable only for strictly sequential messages processing.
--
-- No subsequent request will be processed until the current handler completes.
-- This applies to actions put to 'respondAndAlso' - those shouldn't take long,
-- otherwise the processing of the next request will be delayed.
--
--
-- The function expects all the passed handlers to have distinct commands
-- ('CommandFor'), otherwise an error will be thrown.
-- Force computation of the @compileHandlers handlers@ part to trigger that
-- check.
compileHandlers
  :: forall m. (MonadLog m, MonadUnliftIO m, HasCallStack)
  => HandlersSet m
  -> IO (RequestsProcessor m)
compileHandlers handlers = do
  seqIdRef <- newIORef initSeqId
  return $ RequestsProcessor \sendToClient req -> do
    UnliftIO unliftIO <- askUnliftIO
    let takeSeqId = atomicUpdateIORef seqIdRef nextSeqId

    log $ "Handling " +| req |+ ""

    let sendEvent :: PartialEventMessage -> IO ()
        sendEvent ev = do
          seq <- takeSeqId
          let ev' = completeEventMessage ev seq
          unliftIO . log $ "Submitting " +| ev' |+ ""
          liftIO . sendToClient $ EventMessageToClient ev'

    curSeqId <- liftIO takeSeqId

    eAfterHandler <- runEventSubmitIO sendEvent $ runExceptT do
      Handler (_ :: Proxy r) _ runHandler <-
        case Map.lookup req.command handlersMap of
          Just handler -> pure handler
          Nothing -> do
            lift . log $ "Got a request with unknown command: " +| req.command |+ ""
            throwError (unhandledCommandError req.command)
      args <-
        case Aeson.fromJSON @r req.args of
          Aeson.Success args -> pure args
          Aeson.Error e -> throwError $ messageDecodeError (toText e)

      res <- runHandler args
      return (Aeson.toJSON <$> res)

    let resp = ResponseMessage
          { seq = curSeqId
          , requestSeq = req.seq
          , command = req.command
          , outcome = response <$> eAfterHandler
          }
    log $ "Sending " +| resp |+ ""
    liftIO . sendToClient $ ResponseMessageToClient resp

    whenRight (laterActions <$> eAfterHandler) \postAction -> do
      runEventSubmitIO sendEvent postAction

  where
    !handlersMap = Map.fromListWith failAndReportHandlers
      [ (handlerCommand handler, handler) | handler <- handlers ]
      -- immediately force evaluation of values in the Map,
      -- we want to know about errors due to conflicts as early as possible
      `S.using` traverse @(Map _) S.rseq

    failAndReportHandlers handler2 handler1 =
      error $ mconcat
        [ "Two conflicting handlers are in the set:\n\n"
        , "For handler 1 "
        , toText $ prettyCallStack (handlerDeclCallStack handler1)
        , "\n\nvs\n\n"
        , "For handler 2 "
        , toText $ prettyCallStack (handlerDeclCallStack handler2)
        , "\n"
        ]

unhandledCommandError :: Text -> Error
unhandledCommandError command = customError
  "Unhandled command {command}"
  { id = 10000
  , variables = Map.fromList [("command", command)]
  , showUser = False
  }

messageDecodeError :: Text -> Error
messageDecodeError errMsg = customError
  "Failed to decode message: {errMsg}"
  { id = 10001
  , variables = Map.fromList [("errMsg", errMsg)]
  , showUser = False
  }

-- | This request is not expected at this stage of communication according
-- to the DAP spec.
--
-- For instance, getting @StackFramesRequest@ before @LaunchRequest@ may
-- raise this error.
inappropriateDapState :: Text -> Error
inappropriateDapState stateDesc = customError
  "Inappropriate state for handling this request: {stateDescription}"
  { id = 10002
  , variables = Map.fromList [("stateDescription", stateDesc)]
  , showUser = False
  }

-- Encoding requests and parsing responses
----------------------------------------------------------------------------

-- | Turn request into 'RequestMessage'.
wrapRequest :: forall r d. (IsRequest r, ToJSON r) => SeqId d -> r -> RequestMessage d
wrapRequest seq req = RequestMessage
  { seq
  , command = requestCommand @r
  , args = toJSON req
  }

-- | Extract an encoded response back. Returns @Nothing@ on response type mismatch.
wantResponseFor
  :: forall r. (IsRequest r, FromJSON (ResponseFor r))
  => MessageToClient -> Maybe (ResponseFor r)
wantResponseFor msg = do
  ResponseMessageToClient respMsg <- pure msg
  guard (requestCommand @r == respMsg.command)
  Right res <- pure respMsg.outcome
  Aeson.Success okRes <- pure $ Aeson.fromJSON res
  return okRes

-- | Extract an encoded error back. Returns @Nothing@ if the response is different.
wantError :: MessageToClient -> Maybe Error
wantError msg = do
  ResponseMessageToClient respMsg <- pure msg
  Left err <- pure respMsg.outcome
  return err

-- Sending requests
----------------------------------------------------------------------------

-- Not implemented yet
