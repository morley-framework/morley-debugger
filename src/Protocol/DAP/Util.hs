-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Utilities for implementing the interface.
module Protocol.DAP.Util
  ( -- * JSON helpers
    mergeObjects
  , dapAesonOptions

  , AnyType (..)

    -- * TH
  , dropExtraInstances

    -- * TypeScript-like enums
  , ClosedEnum (..)
  , OpenEnum (..)
  , (?->)
  , emptied
  , IsElem
  , CheckIsElem

    -- * Misc
  , dapDeriveMkOptions
  , atomicUpdateIORef
  ) where

import Debug qualified

import Data.Aeson (FromJSON, ToJSON)
import Data.Aeson qualified as Aeson
import Data.Aeson.Key qualified as Aeson.Key
import Data.List qualified as List
import Data.Type.Bool (If)
import "fmt" Fmt (Buildable(..), blockMapF')
import GHC.Exts qualified as Exts
import GHC.TypeLits (ErrorMessage(..), KnownSymbol, Symbol, TypeError, symbolVal)
import Language.Haskell.TH qualified as TH

import Morley.Util.Label (Label(..))
import Morley.Util.TypeLits (symbolValT')

import Protocol.DAP.Mk (DeriveMkOptions(..))

-- JSON helpers
----------------------------------------------------------------------------

-- | Merge fields in two 'Aeson' objects forming a new object.
mergeObjects :: HasCallStack => Aeson.Value -> Aeson.Value -> Aeson.Value
mergeObjects (Aeson.Object a) (Aeson.Object b) = Aeson.Object (a <> b)
mergeObjects (Aeson.Object _) _ = error "Right part is not an Object"
mergeObjects _ _ = error "Left part is not an Object"

-- | Our schema is: use the same record field names as in the DAP spec.
--
-- If the name collides with Haskell keyword, add a `_` suffix.
dapAesonOptions :: Aeson.Options
dapAesonOptions = Aeson.defaultOptions
  { Aeson.fieldLabelModifier =
      List.dropWhileEnd (== '_')
  , Aeson.omitNothingFields = True
  }

-- | TypeScript's @any@ type.
-- It's basically 'Aeson.Value', but we need `Buildable` instance for it.
newtype AnyType = AnyType { unAnyValue :: Aeson.Value }
  deriving newtype (Eq, Show, ToJSON, FromJSON)

instance Buildable AnyType where
  build (AnyType v) = case v of
    Aeson.Object km ->
      blockMapF' (build . Aeson.Key.toText) (build . AnyType) $ Exts.toList km
    Aeson.Array vec -> build $ map AnyType $ toList vec
    Aeson.String txt -> "`" <> build txt <> "`"
    Aeson.Number sci -> build @Text $ Debug.show sci
    Aeson.Bool b -> build b
    Aeson.Null -> "null"

-- TH
----------------------------------------------------------------------------

-- | Takes instances declarations and filters them out so that:
--
-- * Instance declarations that duplicate existing instances (e.g. that are
-- defined manually eariler) are removed.
-- * Instances for some basic types (@()@ in particular) are removed.
--
-- This currently doesn't work with MultiParamTypeClass instances.
dropExtraInstances :: TH.DecsQ -> TH.DecsQ
dropExtraInstances mkDecls = mkDecls >>= filterM (fmap not . isExtra)
  where
    isExtra :: TH.Dec -> TH.Q Bool
    isExtra dec = isJust <$> runMaybeT do
      TH.InstanceD _ _ overallTy _ <- pure dec
      TH.ConT clas `TH.AppT` ty <- pure overallTy
      asum
        [ -- A manually defined instance already exists
          do
            _:_ <- lift $ TH.reifyInstances clas [ty]
            pass
        , -- The instance is for @()@ - we better define such instance separately
          guard (ty == TH.ConT ''())
        ]

-- TypeScript-like enums
----------------------------------------------------------------------------

-- | Test if the type is mentioned in the given list.
type family IsElem (x :: a) (xs :: [a]) :: Bool where
  IsElem _ '[] = 'False
  IsElem x (x ': _) = 'True
  IsElem x (_ ': xs) = IsElem x xs

-- | 'IsElem' is the form of constraint, produces a type error when 'IsElem' is false.
type family CheckIsElem x xs :: Constraint where
  CheckIsElem x xs =
    If (IsElem x xs) (() :: Constraint)
      ( TypeError
        ( 'Text "Value " ':<>: 'ShowType x ':<>: 'Text " is not allowed here"
          ':$$:
          'Text "Allowed values: " ':<>: 'ShowType xs
        )
      )

class DemoteSymbols (ts :: [Symbol]) where
  demoteSymbols :: [Text]
instance DemoteSymbols '[] where
  demoteSymbols = []
instance (KnownSymbol t, DemoteSymbols ts) => DemoteSymbols (t ': ts) where
  demoteSymbols = toText (symbolVal (Proxy @t)) : demoteSymbols @ts

-- | Closed enum is a string that allows a fixed number of values.
--
-- The type argument designates the available options.
-- This allows for sort of anonymous enums and lets avoid declaring datatypes.
--
-- >>> let myType :: ClosedEnum ["humanReadable", "machineReadable"]
-- >>> let myType = #machineReadable
--
-- NB: not every string is a valid label, in corner cases use 'fromLabel':
-- @fromLabel \@"human readable"@.
newtype ClosedEnum (ts :: [Symbol]) = UnsafeClosedEnum { unClosedEnum :: Text }
  deriving stock (Show, Eq, Ord)
  deriving newtype (Buildable, ToText, ToJSON)

mkClosedEnum :: forall ts. DemoteSymbols ts => Text -> Maybe (ClosedEnum ts)
mkClosedEnum txt = guard (txt `elem` demoteSymbols @ts) $> UnsafeClosedEnum txt

instance DemoteSymbols ts => FromJSON (ClosedEnum ts) where
  parseJSON = Aeson.withText "enum" \txt ->
    mkClosedEnum txt
      & maybe (fail $ "Disallowed value: " <> toString txt) pure

instance (CheckIsElem s ts, KnownSymbol s) => IsLabel s (ClosedEnum ts) where
  fromLabel = UnsafeClosedEnum . toText $ symbolValT' @s

{- | A simple implementation of pattern match on enum value.

@
fromMyEnum :: ClosedEnum ["abc", "cde"] -> Word
fromMyEnum =
  #abc ?-> 1 $
  #cde ?-> 2 $
  emptied
@
-}
(?->) :: Label t -> r -> (ClosedEnum ts -> r) -> ClosedEnum (t : ts) -> r
(?->) (Label :: Label t) r0 cont (UnsafeClosedEnum txt)
  | symbolValT' @t == txt = r0
  | otherwise = cont (UnsafeClosedEnum txt)
infix 3 ?->

-- | Empty enum is isomoprhic to any type since it's impossible too.
--
-- Use this as the end of chain in pattern match.
emptied :: HasCallStack => ClosedEnum '[] -> a
emptied (UnsafeClosedEnum t) =
  error $ "pattern matched on invalid enum with '" <> t <> "'"

_closedEnumEx1 :: ClosedEnum ["abc", "cde"]
_closedEnumEx1 = #abc

_closedEnumEx2 :: ClosedEnum '["with space"]
_closedEnumEx2 = fromLabel @"with space"

_closedEnumMatchEx1 :: ClosedEnum ["abc", "cde"] -> Word
_closedEnumMatchEx1 =
  #abc ?-> 1 $
  #cde ?-> 2 $
  emptied

-- | Open enum is a string that has several recommended values, but generally
-- arbitrary values are allowed.
--
-- * The first type argument stands for a unique identifier of the given enum type.
-- * The second type argument designates the recommended options.
--
-- We propose 3 ways of constructing values of this type:
--
-- 1. Using label works for recommended values:
--    @#abc :: OpenEnum "myEnum" ["abc", "def"]@.
-- 2. Arbitrary values can be constructed from just string literals:
--    @"qwe" :: OpenEnum "myEnum" ["abc", "def"]@.
-- 3. To add your own recommended value, define a respective constant bound to
--    the enum identifier:
--    @qweOption :: OpenEnum "myEnum" ts; myVal = "qwe"@.
newtype OpenEnum (id :: k) (ts :: [Symbol]) = OpenEnum { unOpenEnum :: Text }
  deriving stock (Show, Eq, Ord)
  deriving newtype (Buildable, ToText, IsString, ToJSON, FromJSON)

instance (CheckIsElem s ts, KnownSymbol s) => IsLabel s (OpenEnum id ts) where
  fromLabel = OpenEnum $ symbolValT' @s

_openEnumEx1 :: OpenEnum "myEnum" ["abc", "cde"]
_openEnumEx1 = #abc

_openEnumEx2 :: OpenEnum "myEnum" ["abc", "cde"]
_openEnumEx2 = "qwe"

-- Misc
----------------------------------------------------------------------------

dapDeriveMkOptions :: DeriveMkOptions
dapDeriveMkOptions = DeriveMkOptions
  { dmoMapFieldName = Aeson.fieldLabelModifier dapAesonOptions
  }

atomicUpdateIORef :: MonadIO m => IORef a -> State a b -> m b
atomicUpdateIORef ref st = atomicModifyIORef' ref (swap . runState st)
