-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Machinery for declaring events.
module Protocol.DAP.EventBase
  ( IsEvent (..)
  , eventName
  , PartialEventMessage (..)
  , eventToPartialMessage
  , completeEventMessage

    -- * Class
  , MonadEventSubmit (..)
  , submitEvent

  , EventSubmitPure (..)
  , runEventSubmitPure

  , EventSubmitIO (..)
  , runEventSubmitIO

    -- * Parsing
  , wantEvent
  ) where

import Prelude hiding (id, seq)

import Control.Monad.Morph (MFunctor(..))
import Control.Monad.Reader (mapReaderT)
import Control.Monad.Writer (MonadWriter(..), WriterT(..))
import Data.Aeson (FromJSON, ToJSON(..))
import Data.Aeson qualified as Aeson
import Data.DList qualified as DL
import GHC.TypeLits (KnownSymbol, Symbol, symbolVal)
import UnliftIO (MonadUnliftIO)

import Protocol.DAP.Log
import Protocol.DAP.Message

class (ToJSON e, KnownSymbol (EventName e)) => IsEvent e where
  -- | Name of event, what appears in @event@ field of event message.
  type EventName e :: Symbol

-- | Demote 'EventName'.
eventName :: forall e. IsEvent e => Text
eventName = toText $ symbolVal (Proxy @(EventName e))

-- | A whole event message, but without sequence number.
--
-- This is an intermediate event representation, helpful to stay pure.
data PartialEventMessage = PartialEventMessage
  { event :: Text
  , body :: Aeson.Value
  } deriving stock (Show)

-- | Lift an event body to a message.
eventToPartialMessage :: IsEvent e => e -> PartialEventMessage
eventToPartialMessage (ev :: e) = PartialEventMessage
  { event = eventName @e
  , body = Aeson.toJSON ev
  }

-- | Append sequence number to a partial message.
completeEventMessage :: PartialEventMessage -> SeqId 'MessageToClient -> EventMessage
completeEventMessage PartialEventMessage{..} seq = EventMessage{..}

-- Events sending monad
----------------------------------------------------------------------------

-- | Submitting events.
class Monad m => MonadEventSubmit m where
  submitEventMessage :: PartialEventMessage -> m ()

-- | Submit an event.
submitEvent :: (MonadEventSubmit m, IsEvent e) => e -> m ()
submitEvent = submitEventMessage . eventToPartialMessage

instance MonadEventSubmit m => MonadEventSubmit (StateT s m) where
  submitEventMessage = lift . submitEventMessage

instance MonadEventSubmit m => MonadEventSubmit (ReaderT s m) where
  submitEventMessage = lift . submitEventMessage

instance (MonadEventSubmit m, Monoid w) => MonadEventSubmit (WriterT w m) where
  submitEventMessage = lift . submitEventMessage

instance (MonadEventSubmit m) => MonadEventSubmit (ExceptT e m) where
  submitEventMessage = lift . submitEventMessage

-- | A pure implementation of 'MonadEventSubmit' that uses 'WriterT' under
-- the hood.
--
-- This means that events won't be submitted immediately, rather first collected
-- and then submitted all at once.
--
-- In theory, this might be useful with non-IO monad stacks like an STM-based
-- one.
--
-- See also 'EventSubmitIO'.
newtype EventSubmitPure m a = EventSubmitPure
  { unEventSubmitPure :: WriterT (DL.DList PartialEventMessage) m a
    -- ↑ We keep @Endo EventMessage@ as a poor-man DList
  } deriving newtype (Functor, Applicative, Monad, MonadIO,
                      MonadReader r, MonadState s, MFunctor,
                      MonadThrow, MonadCatch, MonadMask)

runEventSubmitPure :: Functor m => EventSubmitPure m a -> m (a, [PartialEventMessage])
runEventSubmitPure (EventSubmitPure (WriterT action)) =
    second DL.toList <$> action

instance MonadTrans EventSubmitPure where
  lift = EventSubmitPure . lift

instance Monad m => MonadEventSubmit (EventSubmitPure m) where
  submitEventMessage = EventSubmitPure . tell . DL.singleton

instance MonadLog m => MonadLog (EventSubmitPure m) where
  log = lift . log

-- | An IO-based implementation of 'MonadEventSubmit'.
--
-- This is supplied with a submitting function, that is invoked synchronously
-- on 'submitEvent' calls.
newtype EventSubmitIO m a = EventSubmitIO
  { unEventSubmitIO :: ReaderT (PartialEventMessage -> IO ()) m a
  } deriving newtype (Functor, Applicative, Monad, MonadIO, MonadUnliftIO,
                      MonadState s, MonadWriter w, MFunctor,
                      MonadThrow, MonadCatch, MonadMask)

runEventSubmitIO :: (PartialEventMessage -> IO ()) -> EventSubmitIO m a -> m a
runEventSubmitIO submitter (EventSubmitIO action) =
  runReaderT action submitter

instance MonadReader r m => MonadReader r (EventSubmitIO m) where
  ask = lift ask
  local f = EventSubmitIO . mapReaderT (local f) . unEventSubmitIO

instance MonadTrans EventSubmitIO where
  lift = EventSubmitIO . lift

instance MonadIO m => MonadEventSubmit (EventSubmitIO m) where
  submitEventMessage msg = EventSubmitIO $ ReaderT \submit -> liftIO $ submit msg

instance MonadLog m => MonadLog (EventSubmitIO m) where
  log = lift . log

-- Parsing
----------------------------------------------------------------------------

-- | Extract an encoded event back. Returns @Nothing@ on event type mismatch.
wantEvent :: forall e. (IsEvent e, FromJSON e) => MessageToClient -> Maybe e
wantEvent msg = do
  EventMessageToClient evMsg <- pure msg
  guard (eventName @e == evMsg.event)
  Aeson.Success res <- pure $ Aeson.fromJSON evMsg.body
  return res
