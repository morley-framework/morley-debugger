-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Helper types.
--
-- Here types from the [Types section of the spec](https://microsoft.github.io/debug-adapter-protocol/specification#Types)
-- are listed. Exceptions:
--
-- * 'Message' type is defined in the other place.
module Protocol.DAP.Types where

import Prelude hiding (filter, id, length, negate)

import Data.Aeson.TH (deriveFromJSON, deriveToJSON)
import Data.Default (Default(..))
import "fmt" Fmt (Buildable(..), genericF)
import Language.Haskell.TH qualified as TH
import Named (arg, argF, (:!), (:?))

import Protocol.DAP.HelperTypes
import Protocol.DAP.Mk (Mk(..), deriveMkWith)
import Protocol.DAP.Util

-- | Information about the capabilities of a debug adapter.
data Capabilities = Capabilities
  { {- |
      The debug adapter supports the @configurationDone@ request.
    -}
    supportsConfigurationDoneRequest :: Maybe Bool

    {- |
      The debug adapter supports function breakpoints.
    -}
  , supportsFunctionBreakpoints :: Maybe Bool

    {- |
      The debug adapter supports conditional breakpoints.
    -}
  , supportsConditionalBreakpoints :: Maybe Bool

    {- |
      The debug adapter supports breakpoints that break execution after a
      specified number of hits.
    -}
  , supportsHitConditionalBreakpoints :: Maybe Bool

    {- |
      The debug adapter supports a (side effect free) @evaluate@ request for data
      hovers.
    -}
  , supportsEvaluateForHovers :: Maybe Bool

    {- |
      Available exception filter options for the @setExceptionBreakpoints@
      request.
    -}
  , exceptionBreakpointFilters :: Maybe [ExceptionBreakpointsFilter]

    {- |
      The debug adapter supports stepping back via the @stepBack@ and
      @reverseContinue@ requests.
    -}
  , supportsStepBack :: Maybe Bool

    {- |
      The debug adapter supports setting a variable to a value.
    -}
  , supportsSetVariable :: Maybe Bool

    {- |
      The debug adapter supports restarting a frame.
    -}
  , supportsRestartFrame :: Maybe Bool

    {- |
      The debug adapter supports the @gotoTargets@ request.
    -}
  , supportsGotoTargetsRequest :: Maybe Bool

    {- |
      The debug adapter supports the @stepInTargets@ request.
    -}
  , supportsStepInTargetsRequest :: Maybe Bool

    {- |
      The debug adapter supports the @completions@ request.
    -}
  , supportsCompletionsRequest :: Maybe Bool

    {- |
      The set of characters that should trigger completion in a REPL. If not
      specified, the UI should assume the @.@ character.
    -}
  , completionTriggerCharacters :: Maybe [Text]

    {- |
      The debug adapter supports the @modules@ request.
    -}
  , supportsModulesRequest :: Maybe Bool

    {- |
      The set of additional module information exposed by the debug adapter.
    -}
  , additionalModuleColumns :: Maybe [ColumnDescriptor]

    {- |
      Checksum algorithms supported by the debug adapter.
    -}
  , supportedChecksumAlgorithms :: Maybe [ChecksumAlgorithm]

    {- |
      The debug adapter supports the @restart@ request. In this case a client
      should not implement @restart@ by terminating and relaunching the adapter
      but by calling the @restart@ request.
    -}
  , supportsRestartRequest :: Maybe Bool

    {- |
      The debug adapter supports @exceptionOptions@ on the
      @setExceptionBreakpoints@ request.
    -}
  , supportsExceptionOptions :: Maybe Bool

    {- |
      The debug adapter supports a @format@ attribute on the @stackTrace@,
      @variables@, and @evaluate@ requests.
    -}
  , supportsValueFormattingOptions :: Maybe Bool

    {- |
      The debug adapter supports the @exceptionInfo@ request.
    -}
  , supportsExceptionInfoRequest :: Maybe Bool

    {- |
      The debug adapter supports the @terminateDebuggee@ attribute on the
      @disconnect@ request.
    -}
  , supportTerminateDebuggee :: Maybe Bool

    {- |
      The debug adapter supports the @suspendDebuggee@ attribute on the
      @disconnect@ request.
    -}
  , supportSuspendDebuggee :: Maybe Bool

    {- |
      The debug adapter supports the delayed loading of parts of the stack, which
      requires that both the @startFrame@ and @levels@ arguments and the
      @totalFrames@ result of the @stackTrace@ request are supported.
    -}
  , supportsDelayedStackTraceLoading :: Maybe Bool

    {- |
      The debug adapter supports the @loadedSources@ request.
    -}
  , supportsLoadedSourcesRequest :: Maybe Bool

    {- |
      The debug adapter supports log points by interpreting the @logMessage@
      attribute of the @SourceBreakpoint@.
    -}
  , supportsLogPoints :: Maybe Bool

    {- |
      The debug adapter supports the @terminateThreads@ request.
    -}
  , supportsTerminateThreadsRequest :: Maybe Bool

    {- |
      The debug adapter supports the @setExpression@ request.
    -}
  , supportsSetExpression :: Maybe Bool

    {- |
      The debug adapter supports the @terminate@ request.
    -}
  , supportsTerminateRequest :: Maybe Bool

    {- |
      The debug adapter supports data breakpoints.
    -}
  , supportsDataBreakpoints :: Maybe Bool

    {- |
      The debug adapter supports the @readMemory@ request.
    -}
  , supportsReadMemoryRequest :: Maybe Bool

    {- |
      The debug adapter supports the @writeMemory@ request.
    -}
  , supportsWriteMemoryRequest :: Maybe Bool

    {- |
      The debug adapter supports the @disassemble@ request.
    -}
  , supportsDisassembleRequest :: Maybe Bool

    {- |
      The debug adapter supports the @cancel@ request.
    -}
  , supportsCancelRequest :: Maybe Bool

    {- |
      The debug adapter supports the @breakpointLocations@ request.
    -}
  , supportsBreakpointLocationsRequest :: Maybe Bool

    {- |
      The debug adapter supports the @clipboard@ context value in the @evaluate@
      request.
    -}
  , supportsClipboardContext :: Maybe Bool

    {- |
      The debug adapter supports stepping granularities (argument @granularity@)
      for the stepping requests.
    -}
  , supportsSteppingGranularity :: Maybe Bool

    {- |
      The debug adapter supports adding breakpoints based on instruction
      references.
    -}
  , supportsInstructionBreakpoints :: Maybe Bool

    {- |
      The debug adapter supports @filterOptions@ as an argument on the
      @setExceptionBreakpoints@ request.
    -}
  , supportsExceptionFilterOptions :: Maybe Bool

    {- |
      The debug adapter supports the @singleThread@ property on the execution
      requests (@continue@, @next@, @stepIn@, @stepOut@, @reverseContinue@,
      @stepBack@).
    -}
  , supportsSingleThreadExecutionRequests :: Maybe Bool
  }

-- | Capabilities with everything unset.
emptyCapabilities :: Capabilities
emptyCapabilities = Capabilities
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

instance Default Capabilities where
  def = emptyCapabilities

-- | An ExceptionBreakpointsFilter is shown in the UI as an filter option for
-- configuring how exceptions are dealt with.
data ExceptionBreakpointsFilter = ExceptionBreakpointsFilter
  { {- |
      The internal ID of the filter option. This value is passed to the
      @setExceptionBreakpoints@ request.
    -}
    filter :: ExceptionFilterId

    {- |
      The name of the filter option. This is shown in the UI.
    -}
  , label :: Text

    {- |
      A help text providing additional information about the exception filter.
      This string is typically shown as a hover and can be translated.
    -}
  , description :: Maybe Text

    {- |
      Initial value of the filter option. If not specified a value false is
      assumed.
    -}
  , default_ :: Maybe Bool

    {- |
      Controls whether a condition can be specified for this filter option. If
      false or missing, a condition can not be set.
    -}
  , supportsCondition :: Maybe Bool

    {- |
      A help text providing information about the condition. This string is shown
      as the placeholder text for a text box and can be translated.
    -}
  , conditionDescription :: Maybe Text
  }


-- | A Module object represents a row in the modules view.
--
-- The id attribute identifies a module in the modules view and is used in
-- a module event for identifying a module for adding, updating or deleting.
--
-- The name attribute is used to minimally render the module in the UI.
--
-- Additional attributes can be added to the module. They show up in the module
-- view if they have a corresponding ColumnDescriptor.
--
-- To avoid an unnecessary proliferation of additional attributes with similar
-- semantics but different names, we recommend to re-use attributes from the
-- ‘recommended’ list below first, and only introduce new attributes
-- if nothing appropriate could be found.
data Module = Module
  { {- |
      Unique identifier for the module.
    -}
    id :: ModuleId

    {- |
      A name of the module.
    -}
  , name :: Text

    {- |
      Logical full path to the module. The exact definition is implementation
      defined, but usually this would be a full path to the on-disk file for the
      module.
    -}
  , path :: Maybe Text

    {- |
      True if the module is optimized.
    -}
  , isOptimized :: Maybe Bool

    {- |
      True if the module is considered 'user code' by a debugger that supports
      'Just My Code'.
    -}
  , isUserCode :: Maybe Bool

    {- |
      Version of Module.
    -}
  , version :: Maybe Text

    {- |
      User-understandable description of if symbols were found for the module
      (ex: 'Symbols Loaded', 'Symbols not found', etc.)
    -}
  , symbolStatus :: Maybe Text

    {- |
      Logical full path to the symbol file. The exact definition is
      implementation defined.
    -}
  , symbolFilePath :: Maybe Text

    {- |
      Module created or modified, encoded as a RFC 3339 timestamp.
    -}
  , dateTimeStamp :: Maybe Text

    {- |
      Address range covered by this module.
    -}
  , addressRange :: Maybe Text
  }


-- | A ColumnDescriptor specifies what module attribute to show in a column of
-- the modules view, how to format it, and what the column’s label should be.
--
-- It is only used if the underlying UI actually supports this level of customization.
data ColumnDescriptor = ColumnDescriptor
  { {- |
      Name of the attribute rendered in this column.
    -}
    attributeName :: Text

    {- |
      Header UI label of column.
    -}
  , label :: Text

    {- |
      Format to use for the rendered values in this column. TBD how the format
      strings looks like.
    -}
  , format :: Maybe Text

    {- |
      Datatype of values in this column. Defaults to @string@ if not specified.
    -}
  , type_ :: Maybe (ClosedEnum ["string", "number", "bool", "unixTimestampUTC"])

    {- |
      Width of this column in characters (hint only).
    -}
  , width :: Maybe Word
  }


-- | The ModulesViewDescriptor is the container for all declarative
-- configuration options of a module view.
--
-- For now it only specifies the columns to be shown in the modules view.
data ModulesViewDescriptor = ModulesViewDescriptor
  { columns :: [ColumnDescriptor]
  }


-- | A Thread
data Thread = Thread
  { {- |
      Unique identifier for the thread.
    -}
    id :: ThreadId

    {- |
      The name of the thread.
    -}
  , name :: Text
  }


-- | A Source is a descriptor for source code.
-- It is returned from the debug adapter as part of a StackFrame and it
-- is used by clients when specifying breakpoints.
data Source = Source
  { {- |
      The short name of the source. Every source returned from the debug adapter
      has a name.

      When sending a source to the debug adapter this name is optional.
    -}
    name :: Maybe Text

    {- |
      The path of the source to be shown in the UI.

      It is only used to locate and load the content of the source if no
      'sourceReference' is specified (or its value is 0).
    -}
  , path :: Maybe FilePath

    {- |
      If the value > 0 the contents of the source must be retrieved through the
      @source@ request (even if a path is specified).

      Since a @sourceReference@ is only valid for a session, it can not be used
      to persist a source.

      The value should be less than or equal to 2147483647 (2^31-1).
    -}
  , sourceReference :: Maybe SourceId

    {- |
      A hint for how to present the source in the UI.

      A value of @deemphasize@ can be used to indicate that the source is not
      available or that it is skipped on stepping.
    -}
  , presentationHint :: Maybe (ClosedEnum ["normal",  "emphasize", "deemphasize"])

    {- |
      The origin of this source. For example, 'internal module', 'inlined content
      from source map', etc.
    -}
  , origin :: Maybe Text

    {- |
      A list of sources that are related to this source. These may be the source
      that generated this source.
    -}
  , sources :: Maybe [Source]

    {- |
      Additional data that a debug adapter might want to loop through the client.

      The client should leave the data intact and persist it across sessions. The
      client should not interpret the data.
    -}
  , adapterData :: Maybe AnyType

    {- |
      The checksums associated with this file.
    -}
  , checksums :: Maybe [Checksum]
  }

-- | A Stackframe contains the source location.
data StackFrame = StackFrame
  { {- |
      An identifier for the stack frame. It must be unique across all threads.

      This id can be used to retrieve the scopes of the frame with the @scopes@
      request or to restart the execution of a stack frame.
    -}
    id :: StackFrameId

    {- |
      The name of the stack frame, typically a method name.
    -}
  , name :: Text

    {- |
      The source of the frame.
    -}
  , source :: Maybe Source

    {- |
      The line within the source of the frame. If the source attribute is missing
      or doesn't exist, @line@ is 0 and should be ignored by the client.
    -}
  , line :: Pos

    {- |
      Start position of the range covered by the stack frame. It is measured in
      UTF-16 code units and the client capability @columnsStartAt1@ determines
      whether it is 0- or 1-based. If attribute @source@ is missing or doesn't
      exist, @column@ is 0 and should be ignored by the client.
    -}
  , column :: Pos

    {- |
      The end line of the range covered by the stack frame.
    -}
  , endLine :: Maybe Pos

    {- |
      End position of the range covered by the stack frame. It is measured in
      UTF-16 code units and the client capability @columnsStartAt1@ determines
      whether it is 0- or 1-based.
    -}
  , endColumn :: Maybe Pos

    {- |
      Indicates whether this frame can be restarted with the @restart@ request.

      Clients should only use this if the debug adapter supports the @restart@
      request and the corresponding capability @supportsRestartRequest@ is true.

      If a debug adapter has this capability, then @canRestart@ defaults to
      @True@ if the property is absent.
    -}
  , canRestart :: Maybe Bool

    {- |
      A memory reference for the current instruction pointer in this frame.
    -}
  , instructionPointerReference :: Maybe Text

    {- |
      The module associated with this frame, if any.
    -}
  , moduleId :: Maybe ModuleId

    {- |
      A hint for how to present this frame in the UI.

      A value of @label@ can be used to indicate that the frame is an artificial
      frame that is used as a visual label or separator. A value of @subtle@ can
      be used to change the appearance of a frame in a @subtle@ way.
    -}
  , presentationHint :: Maybe (ClosedEnum ["normal", "label", "subtle"])
  }


-- | A Scope is a named container for variables. Optionally a scope can map
-- to a source or a range within a source.
data Scope = Scope
  { {- |
      Name of the scope such as @Arguments@, @Locals@, or @Registers@. This
      string is shown in the UI as is and can be translated.
    -}
    name :: Text

    {- |
      A hint for how to present this scope in the UI. If this attribute is
      missing, the scope is shown with a generic UI.

      Values:

      * @arguments@: Scope contains method arguments.
      * @locals@: Scope contains local variables.
      * @registers@: Scope contains registers. Only a single @registers@ scope
      should be returned from a @scopes@ request.
      * etc.
    -}
  , presentationHint :: Maybe $ OpenEnum "scopePresentationHint"
      ["arguments", "locals", "registers"]

    {- |
      The variables of this scope can be retrieved by passing the value of
      @variablesReference@ to the @variables@ request as long as execution
      remains suspended. See @Lifetime of Object References@ in the Overview
      section for details.
    -}
  , variablesReference :: VariableId

    {- |
      The number of named variables in this scope.
      The client can use this information to present the variables in a paged UI
      and fetch them in chunks.
    -}
  , namedVariables :: Maybe Word

    {- |
      The number of indexed variables in this scope.
      The client can use this information to present the variables in a paged UI
      and fetch them in chunks.
    -}
  , indexedVariables :: Maybe Word

    {- |
      If true, the number of variables in this scope is large or expensive to
      retrieve.
    -}
  , expensive :: Bool

    {- |
      The source for this scope.
    -}
  , source :: Maybe Source

    {- |
      The start line of the range covered by this scope.
    -}
  , line :: Maybe Pos

    {- |
      Start position of the range covered by the scope. It is measured in UTF-16
      code units and the client capability @columnsStartAt1@ determines whether
      it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      The end line of the range covered by this scope.
    -}
  , endLine :: Maybe Pos

    {- |
      End position of the range covered by the scope. It is measured in UTF-16
      code units and the client capability @columnsStartAt1@ determines whether
      it is 0- or 1-based.
    -}
  , endColumn :: Maybe Pos
  }

instance Mk Scope where
  type MkSig Scope
    =  (:!) "name" Text
    -> (:?) "presentationHint"
        (OpenEnum "scopePresentationHint" ["arguments", "locals", "registers"])
    -> (:!) "variablesReference" VariableId
    -> (:?) "namedVariables" Word
    -> (:?) "indexedVariables" Word
    -> (:?) "expensive" Bool
    -> (:?) "source" Source
    -> (:?) "line" Pos
    -> (:?) "column" Pos
    -> (:?) "endLine" Pos
    -> (:?) "endColumn" Pos
    -> Scope
  mk
    (arg #name -> name)
    (argF #presentationHint -> presentationHint)
    (arg #variablesReference -> variablesReference)
    (argF #namedVariables -> namedVariables)
    (argF #indexedVariables -> indexedVariables)
    (argF #expensive -> expensive)
    (argF #source -> source)
    (argF #line -> line)
    (argF #column -> column)
    (argF #endLine -> endLine)
    (argF #endColumn -> endColumn)
    = Scope
        { expensive = expensive ?: False
        , ..
        }

-- | A Variable is a name/value pair.
--
-- The type attribute is shown if space permits or when hovering over
-- the variable’s name.
--
-- The kind attribute is used to render additional properties of the variable,
-- e.g. different icons can be used to indicate that a variable is public or
-- private.
--
-- If the value is structured (has children), a handle is provided to retrieve
-- the children with the variables request.
--
-- If the number of named or indexed children is large, the numbers should be
-- returned via the namedVariables and indexedVariables attributes.
--
-- The client can use this information to present the children in a paged UI
-- and fetch them in chunks.
data Variable = Variable
  { {- |
      The variable's name.
    -}
    name :: Text

    {- |
      The variable's value.

      This can be a multi-line text, e.g. for a function the body of a function.
      For structured variables (which do not have a simple value), it is
      recommended to provide a one-line representation of the structured object.

      This helps to identify the structured object in the collapsed state when
      its children are not yet visible.

      An empty string can be used if no value should be shown in the UI.
    -}
  , value :: Text

    {- |
      The type of the variable's value. Typically shown in the UI when hovering
      over the value.

      This attribute should only be returned by a debug adapter if the
      corresponding capability @supportsVariableType@ is true.
    -}
  , type_ :: Maybe Text

    {- |
      Properties of a variable that can be used to determine how to render the
      variable in the UI.
    -}
  , presentationHint :: Maybe VariablePresentationHint

    {- |
      The evaluatable name of this variable which can be passed to the @evaluate@
      request to fetch the variable's value.
    -}
  , evaluateName :: Maybe Text

    {- |
      If @variablesReference@ is > 0, the variable is structured and its children
      can be retrieved by passing @variablesReference@ to the @variables@ request
      as long as execution remains suspended. See @Lifetime of Object References@
      in the Overview section for details.
    -}
  , variablesReference :: VariableId

    {- |
      The number of named child variables.

      The client can use this information to present the children in a paged UI
      and fetch them in chunks.
    -}
  , namedVariables :: Maybe Word

    {- |
      The number of indexed child variables.

      The client can use this information to present the children in a paged UI
      and fetch them in chunks.
    -}
  , indexedVariables :: Maybe Word

    {- |
      The memory reference for the variable if the variable represents executable
      code, such as a function pointer.

      This attribute is only required if the corresponding capability
      @supportsMemoryReferences@ is true.
    -}
  , memoryReference :: Maybe Text

    {- |
      Field specific to VSCode, suggests a key for getting menu options for
      this variable.
    -}
  , __vscodeVariableMenuContext :: Maybe Text
  }

instance Mk Variable where
  type MkSig Variable
    =  "name" :! Text
    -> "value" :! Text
    -> "type" :? Text
    -> "presentationHint" :? VariablePresentationHint
    -> "evaluateName" :? Text
    -> "variablesReference" :? VariableId
    -> "namedVariables" :? Word
    -> "indexedVariables" :? Word
    -> "memoryReference" :? Text
    -> "__vscodeVariableMenuContext" :? Text
    -> Variable
  mk
    (arg #name -> name)
    (arg #value -> value)
    (argF #type -> type_)
    (argF #presentationHint -> presentationHint)
    (argF #evaluateName -> evaluateName)
    (argF #variablesReference -> variablesReference)
    (argF #namedVariables -> namedVariables)
    (argF #indexedVariables -> indexedVariables)
    (argF #memoryReference -> memoryReference)
    (argF #__vscodeVariableMenuContext -> __vscodeVariableMenuContext)
    = Variable
      { variablesReference = variablesReference ?: NoVariableId
      , ..
      }

-- | Properties of a variable that can be used to determine how to render
-- the variable in the UI.
data VariablePresentationHint = VariablePresentationHint
  { {- |
      The kind of variable. Before introducing additional values, try to use the
      listed values.

      Values:

      * @property@: Indicates that the object is a property.
      * @method@: Indicates that the object is a method.
      * @class@: Indicates that the object is a class.
      * @data@: Indicates that the object is data.
      * event@: Indicates that the object is an event.
      * @baseClass@: Indicates that the object is a base class.
      * @innerClass@: Indicates that the object is an inner class.
      * @interface@: Indicates that the object is an interface.
      * @mostDerivedClass@: Indicates that the object is the most derived class.
      * @virtual@: Indicates that the object is virtual, that means it is a
      synthetic object introduced by the adapter for rendering purposes, e.g. an
      index range for large arrays.
      * @dataBreakpoint@: Deprecated: Indicates that a data breakpoint is
      registered for the object. The @hasDataBreakpoint@ attribute should
      generally be used instead.
      etc.
    -}
    kind :: Maybe $ OpenEnum "variablePresentationHintKind"
      [ "property", "method", "class", "data", "event", "baseClass"
      , "innerClass", "interface", "mostDerivedClass", "virtual"
      , "dataBreakpoint"
      ]

    {- |
      Set of attributes represented as an array of strings. Before introducing
      additional values, try to use the listed values.

      Values:

      * @static@: Indicates that the object is static.
      * @constant@: Indicates that the object is a constant.
      * @readOnly@: Indicates that the object is read only.
      * @rawString@: Indicates that the object is a raw string.
      * @hasObjectId@: Indicates that the object can have an Object ID created for
      it.
      * @canHaveObjectId@: Indicates that the object has an Object ID associated
      with it.
      * @hasSideEffects@: Indicates that the evaluation had side effects.
      * @hasDataBreakpoint@: Indicates that the object has its value tracked by a
      data breakpoint.
      etc.
    -}
  , attributes :: Maybe [OpenEnum "variablePresentationHintAttributes"
      [ "static", "constant", "readOnly", "rawString", "hasObjectId"
      , "canHaveObjectId", "hasSideEffects", "hasDataBreakpoint"
      ]]

    {- |
      Visibility of variable. Before introducing additional values, try to use
      the listed values.
    -}
  , visibility :: Maybe $ OpenEnum "variablePresentationHintVisibility"
      ["public", "private", "protected", "internal", "final"]

    {- |
      If true, clients can present the variable with a UI that supports a
      specific gesture to trigger its evaluation.

      This mechanism can be used for properties that require executing code when
      retrieving their value and where the code execution can be expensive and/or
      produce side-effects. A typical example are properties based on a getter
      function.

      Please note that in addition to the @lazy@ flag, the variable's
      @variablesReference@ is expected to refer to a variable that will provide
      the value through another @variable@ request.
    -}
  , lazy :: Maybe Bool
  }


-- | Properties of a breakpoint location returned from the breakpointLocations
-- request.
data BreakpointLocation = BreakpointLocation
  { {- |
      Start line of breakpoint location.
    -}
    line :: Pos

    {- |
      The start position of a breakpoint location. Position is measured in UTF-16
      code units and the client capability @columnsStartAt1@ determines whether
      it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      The end line of breakpoint location if the location covers a range.
    -}
  , endLine :: Maybe Pos

    {- |
      The end position of a breakpoint location (if the location covers a range).
      Position is measured in UTF-16 code units and the client capability
      @columnsStartAt1@ determines whether it is 0- or 1-based.
    -}
  , endColumn :: Maybe Pos
  }


-- | Properties of a breakpoint or logpoint passed to the setBreakpoints request.
data SourceBreakpoint = SourceBreakpoint
  { {- |
      The source line of the breakpoint or logpoint.
    -}
    line :: Pos

    {- |
      Start position within source line of the breakpoint or logpoint. It is
      measured in UTF-16 code units and the client capability @columnsStartAt1@
      determines whether it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      The expression for conditional breakpoints.

      It is only honored by a debug adapter if the corresponding capability
      @supportsConditionalBreakpoints@ is true.
    -}
  , condition :: Maybe Text

    {- |
      The expression that controls how many hits of the breakpoint are ignored.
      The debug adapter is expected to interpret the expression as needed.

      The attribute is only honored by a debug adapter if the corresponding
      capability @supportsHitConditionalBreakpoints@ is true.

      If both this property and @condition@ are specified, @hitCondition@ should
      be evaluated only if the @condition@ is met, and the debug adapter should
      stop only if both conditions are met.
    -}
  , hitCondition :: Maybe Text

    {- |
      If this attribute exists and is non-empty, the debug adapter must not
      @break@ (stop)
      but log the message instead. Expressions within `{}` are interpolated.
      The attribute is only honored by a debug adapter if the corresponding
      capability @supportsLogPoints@ is true.

      If either @hitCondition@ or @condition@ is specified, then the message
      should only be logged if those conditions are met.
    -}
  , logMessage :: Maybe Text
  }


-- | Properties of a breakpoint passed to the setFunctionBreakpoints request.
data FunctionBreakpoint = FunctionBreakpoint
  { {- |
      The name of the function.
    -}
    name :: Text

    {- |
      An expression for conditional breakpoints.

      It is only honored by a debug adapter if the corresponding capability
      @supportsConditionalBreakpoints@ is true.
    -}
  , condition :: Maybe Text

    {- |
      An expression that controls how many hits of the breakpoint are ignored.
      The debug adapter is expected to interpret the expression as needed.

      The attribute is only honored by a debug adapter if the corresponding
      capability @supportsHitConditionalBreakpoints@ is true.
    -}
  , hitCondition :: Maybe Text
  }


-- | This enumeration defines all possible access types for data breakpoints.
type DataBreakpointAccessType = ClosedEnum ["read", "write", "readWrite"]


-- | Properties of a data breakpoint passed to the setDataBreakpoints request.
data DataBreakpoint = DataBreakpoint
  { {- |
      An id representing the data. This id is returned from the
      @dataBreakpointInfo@ request.
    -}
    dataId :: Text

    {- |
      The access type of the data.
    -}
  , accessType :: Maybe DataBreakpointAccessType

    {- |
      An expression for conditional breakpoints.
    -}
  , condition :: Maybe Text

    {- |
      An expression that controls how many hits of the breakpoint are ignored.
      The debug adapter is expected to interpret the expression as needed.
    -}
  , hitCondition :: Maybe Text
  }


-- | Properties of a breakpoint passed to the setInstructionBreakpoints request
data InstructionBreakpoint = InstructionBreakpoint
  { {- |
      The instruction reference of the breakpoint.

      This should be a memory or instruction pointer reference from an
      @EvaluateResponse@, @Variable@, @StackFrame@, @GotoTarget@, or
      @Breakpoint@.
    -}
    instructionReference :: Text

    {- |
      The offset from the instruction reference.

      This can be negative.
    -}
  , offset :: Maybe Int

    {- |
      An expression for conditional breakpoints.

      It is only honored by a debug adapter if the corresponding capability
      @supportsConditionalBreakpoints@ is true.
    -}
  , condition :: Maybe Text

    {- |
      An expression that controls how many hits of the breakpoint are ignored.
      The debug adapter is expected to interpret the expression as needed.

      The attribute is only honored by a debug adapter if the corresponding
      capability @supportsHitConditionalBreakpoints@ is true.
    -}
  , hitCondition :: Maybe Text
  }


-- | Information about a breakpoint created in setBreakpoints,
-- setFunctionBreakpoints, setInstructionBreakpoints, or setDataBreakpoints
-- requests.
data Breakpoint = Breakpoint
  { {- |
      The identifier for the breakpoint. It is needed if breakpoint events are
      used to update or remove breakpoints.
    -}
    id :: Maybe BreakpointId

    {- |
      If true, the breakpoint could be set (but not necessarily at the desired
      location).
    -}
  , verified :: Bool

    {- |
      A message about the state of the breakpoint.

      This is shown to the user and can be used to explain why a breakpoint could
      not be verified.
    -}
  , message :: Maybe Text

    {- |
      The source where the breakpoint is located.
    -}
  , source :: Maybe Source

    {- |
      The start line of the actual range covered by the breakpoint.
    -}
  , line :: Maybe Pos

    {- |
      Start position of the source range covered by the breakpoint. It is
      measured in UTF-16 code units and the client capability @columnsStartAt1@
      determines whether it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      The end line of the actual range covered by the breakpoint.
    -}
  , endLine :: Maybe Pos

    {- |
      End position of the source range covered by the breakpoint. It is measured
      in UTF-16 code units and the client capability @columnsStartAt1@ determines
      whether it is 0- or 1-based.

      If no end line is given, then the end column is assumed to be in the start
      line.
    -}
  , endColumn :: Maybe Pos

    {- |
      A memory reference to where the breakpoint is set.
    -}
  , instructionReference :: Maybe Text

    {- |
      The offset from the instruction reference.
      This can be negative.
    -}
  , offset :: Maybe Int
  }


-- | The granularity of one ‘step’ in the stepping requests @next@, @stepIn@,
-- @stepOut@, and @stepBack@.
--
-- Values:
--
-- * @statement@: The step should allow the program to run until the current
--   statement has finished executing. The meaning of a statement is determined
--   by the adapter and it may be considered equivalent to a line. For example
--   ‘for(int i = 0; i < 10; i++)’ could be considered to have 3 statements ‘int
--   i = 0’, ‘i < 10’, and ‘i++’.
-- * @line@: The step should allow the program to run until the current source
--   line has executed.
-- * @instruction@: The step should allow one instruction to execute (e.g. one
--   x86 instruction).
type SteppingGranularity =
  OpenEnum "steppingGranularity" ["statement", "line", "instruction"]
-- using open enum since the built-in granularities are not always enough


-- | A StepInTarget can be used in the stepIn request and determines into which
-- single target the stepIn request should step.
data StepInTarget = StepInTarget
  { {- |
      Unique identifier for a step-in target.
    -}
    id :: StepInTargetId

    {- |
      The name of the step-in target (shown in the UI).
    -}
  , label :: Text

    {- |
      The line of the step-in target.
    -}
  , line :: Maybe Pos

    {- |
      Start position of the range covered by the step in target. It is measured
      in UTF-16 code units and the client capability @columnsStartAt1@ determines
      whether it is 0- or 1-based.
    -}
  , column :: Maybe Pos

    {- |
      The end line of the range covered by the step-in target.
    -}
  , endLine :: Maybe Pos

    {- |
      End position of the range covered by the step in target. It is measured in
      UTF-16 code units and the client capability @columnsStartAt1@ determines
      whether it is 0- or 1-based.
    -}
  , endColumn :: Maybe Pos
  }


-- | A GotoTarget describes a code location that can be used as a target in the
-- goto request.
--
-- The possible goto targets can be determined via the gotoTargets request.
data GotoTarget = GotoTarget
  { {- |
      Unique identifier for a goto target. This is used in the @goto@ request.
    -}
    id :: GotoTargetId

    {- |
      The name of the goto target (shown in the UI).
    -}
  , label :: Text

    {- |
      The line of the goto target.
    -}
  , line :: Pos

    {- |
      The column of the goto target.
    -}
  , column :: Maybe Pos

    {- |
      The end line of the range covered by the goto target.
    -}
  , endLine :: Maybe Pos

    {- |
      The end column of the range covered by the goto target.
    -}
  , endColumn :: Maybe Pos

    {- |
      A memory reference for the instruction pointer value represented by this
      target.
    -}
  , instructionPointerReference :: Maybe Text
  }


-- | CompletionItems are the suggestions returned from the completions request.
data CompletionItem = CompletionItem
  { {- |
      The label of this completion item. By default this is also the text that is
      inserted when selecting this completion.
    -}
    label :: Text

    {- |
      If text is returned and not an empty string, then it is inserted instead of
      the label.
    -}
  , text :: Maybe Text

    {- |
      A string that should be used when comparing this item with other items. If
      not returned or an empty string, the @label@ is used instead.
    -}
  , sortText :: Maybe Text

    {- |
      A human-readable string with additional information about this item, like
      type or symbol information.
    -}
  , detail :: Maybe Text

    {- |
      The item's type. Typically the client uses this information to render the
      item in the UI with an icon.
    -}
  , type_ :: Maybe CompletionItemType

    {- |
      Start position (within the @text@ attribute of the @completions@ request)
      where the completion text is added. The position is measured in UTF-16 code
      units and the client capability @columnsStartAt1@ determines whether it is
      0- or 1-based. If the start position is omitted the text is added at the
      location specified by the @column@ attribute of the @completions@ request.
    -}
  , start :: Maybe Word

    {- |
      Length determines how many characters are overwritten by the completion
      text and it is measured in UTF-16 code units. If missing the value 0 is
      assumed which results in the completion text being inserted.
    -}
  , length :: Maybe Word

    {- |
      Determines the start of the new selection after the text has been inserted
      (or replaced). @selectionStart@ is measured in UTF-16 code units and must
      be in the range 0 and length of the completion text. If omitted the
      selection starts at the end of the completion text.
    -}
  , selectionStart :: Maybe Word

    {- |
      Determines the length of the new selection after the text has been inserted
      (or replaced) and it is measured in UTF-16 code units. The selection can
      not extend beyond the bounds of the completion text. If omitted the length
      is assumed to be 0.
    -}
  , selectionLength :: Maybe Word
  }


-- | Some predefined types for the CompletionItem.
-- Please note that not all clients have specific icons for all of them.
type CompletionItemType = ClosedEnum
   [ "method", "function", "constructor", "field"
   , "variable", "class", "interface", "module", "property", "unit"
   , "value", "enum", "keyword", "snippet", "text", "color", "file"
   , "reference", "customcolor"
   ]

-- | Names of checksum algorithms that may be supported by a debug adapter.
type ChecksumAlgorithm = ClosedEnum ["MD5", "SHA1", "SHA256", "timestamp"]

-- | The checksum of an item calculated by the specified algorithm.
data Checksum = Checksum
  { {- |
      The algorithm used to calculate this checksum.
    -}
    algorithm :: ChecksumAlgorithm

    {- |
      Value of the checksum, encoded as a hexadecimal value.
    -}
  , checksum :: Text
  }


-- | Provides formatting information for a value.
data ValueFormat = ValueFormat
  { {- |
      Display the value in hex.
    -}
    hex :: Maybe Bool
  }


-- | Provides formatting information for a stack frame.
data StackFrameFormat = StackFrameFormat
  { {- |
      Display the value in hex.
    -}
    hex :: Maybe Bool

    {- |
      Displays parameters for the stack frame.
    -}
  , parameters :: Maybe Bool

    {- |
      Displays the types of parameters for the stack frame.
    -}
  , parameterTypes :: Maybe Bool

    {- |
      Displays the names of parameters for the stack frame.
    -}
  , parameterNames :: Maybe Bool

    {- |
      Displays the values of parameters for the stack frame.
    -}
  , parameterValues :: Maybe Bool

    {- |
      Displays the line number of the stack frame.
    -}
  , line :: Maybe Bool

    {- |
      Displays the module of the stack frame.
    -}
  , module_ :: Maybe Bool

    {- |
      Includes all stack frames, including those the debug adapter might
      otherwise hide.
    -}
  , includeAll :: Maybe Bool
  }


-- | An ExceptionFilterOptions is used to specify an exception filter together
-- with a condition for the setExceptionBreakpoints request.
data ExceptionFilterOptions = ExceptionFilterOptions
  { {- |
      ID of an exception filter returned by the @exceptionBreakpointFilters@
      capability.
    -}
    filterId :: ExceptionFilterId

    {- |
      An expression for conditional exceptions.
      The exception breaks into the debugger if the result of the condition is
      true.
    -}
  , condition :: Maybe Text
  }


-- | An ExceptionOptions assigns configuration options to a set of exceptions.
data ExceptionOptions = ExceptionOptions
  { {- |
      A path that selects a single or multiple exceptions in a tree. If @path@ is
      missing, the whole tree is selected.

      By convention the first segment of the path is a category that is used to
      group exceptions in the UI.
    -}
    path :: Maybe [ExceptionPathSegment]

    {- |
      Condition when a thrown exception should result in a break.
    -}
  , breakMode :: ExceptionBreakMode
  }


-- | This enumeration defines all possible conditions when a thrown exception
-- should result in a break.
--
-- * @never@: never breaks,
-- * @always@: always breaks,
-- * @unhandled@: breaks when exception unhandled,
-- * @userUnhandled@: breaks if the exception is not handled by user code.
type ExceptionBreakMode = ClosedEnum
  ["never", "always", "unhandled", "userUnhandled"]


-- | An ExceptionPathSegment represents a segment in a path that is used
-- to match leafs or nodes in a tree of exceptions.
--
-- If a segment consists of more than one name, it matches the names provided
-- if negate is false or missing, or it matches anything except the names
-- provided if negate is true.
data ExceptionPathSegment = ExceptionPathSegment
  { {- |
      If false or missing this segment matches the names provided, otherwise it
      matches anything except the names provided.
    -}
    negate :: Maybe Bool

    {- |
      Depending on the value of @negate@ the names that should match or not
      match.
    -}
  , names :: [Text]
  }


-- | Detailed information about an exception that has occurred.
data ExceptionDetails = ExceptionDetails
  { {- |
      Message contained in the exception.
    -}
    message :: Maybe Text

    {- |
      Short type name of the exception object.
    -}
  , typeName :: Maybe Text

    {- |
      Fully-qualified type name of the exception object.
    -}
  , fullTypeName :: Maybe Text

    {- |
      An expression that can be evaluated in the current scope to obtain the
      exception object.
    -}
  , evaluateName :: Maybe Text

    {- |
      Stack trace at the time the exception was thrown.
    -}
  , stackTrace :: Maybe Text

    {- |
      Details of the exception contained by this exception, if any.
    -}
  , innerException :: Maybe [ExceptionDetails]
  }


-- | Represents a single disassembled instruction.
data DisassembledInstruction = DisassembledInstruction
  { {- |
      The address of the instruction. Treated as a hex value if prefixed with
      @0x@, or as a decimal value otherwise.
    -}
    address :: Text

    {- |
      Raw bytes representing the instruction and its operands, in an
      implementation-defined format.
    -}
  , instructionBytes :: Maybe Text

    {- |
      Text representing the instruction and its operands, in an
      implementation-defined format.
    -}
  , instruction :: Text

    {- |
      Name of the symbol that corresponds with the location of this instruction,
      if any.
    -}
  , symbol :: Maybe Text

    {- |
      Source location that corresponds to this instruction, if any.
      Should always be set (if available) on the first instruction returned,
      but can be omitted afterwards if this instruction maps to the same source
      file as the previous instruction.
    -}
  , location :: Maybe Source

    {- |
      The line within the source location that corresponds to this instruction,
      if any.
    -}
  , line :: Maybe Pos

    {- |
      The column within the line that corresponds to this instruction, if any.
    -}
  , column :: Maybe Pos

    {- |
      The end line of the range that corresponds to this instruction, if any.
    -}
  , endLine :: Maybe Pos

    {- |
      The end column of the range that corresponds to this instruction, if any.
    -}
  , endColumn :: Maybe Pos
  }

-- | Logical areas that can be invalidated by the invalidated event. Values:
--
-- * ‘all’: All previously fetched data has become invalid and needs to be
--   refetched.
-- * ‘stacks’: Previously fetched stack related data has become invalid and
--   needs to be refetched.
-- * ‘threads’: Previously fetched thread related data has become invalid and
--   needs to be refetched.
-- * ‘variables’: Previously fetched variable data has become invalid and needs
--   to be refetched. etc.
type InvalidatedAreas = OpenEnum "invalidatedAreas"
  ["all", "stacks", "threads", "variables"]

-- TH Instances
----------------------------------------------------------------------------

-- To avoid boilerplate we generate the instances automatically with TH for all
-- datatypes.
--
-- Note that you still can define instances manually next to your types,
-- the TH generator below will notice that.

$( do
  let
    allDatatypes =
      [ ''Breakpoint
      , ''BreakpointLocation
      , ''Capabilities
      , ''Checksum
      , ''ColumnDescriptor
      , ''CompletionItem
      , ''DataBreakpoint
      , ''DisassembledInstruction
      , ''ExceptionBreakpointsFilter
      , ''ExceptionDetails
      , ''ExceptionFilterOptions
      , ''ExceptionOptions
      , ''ExceptionPathSegment
      , ''FunctionBreakpoint
      , ''GotoTarget
      , ''InstructionBreakpoint
      , ''Module
      , ''ModulesViewDescriptor
      , ''Scope
      , ''Source
      , ''SourceBreakpoint
      , ''StackFrame
      , ''StackFrameFormat
      , ''StepInTarget
      , ''Thread
      , ''ValueFormat
      , ''Variable
      , ''VariablePresentationHint
      ]

  dropExtraInstances $ concatForM allDatatypes \datatyp -> do
    concat <$> sequence
      [ -- A couple of basic instances
        [d| deriving stock instance Generic $(TH.conT datatyp) |]
      , [d| deriving stock instance Eq $(TH.conT datatyp) |]
      , [d| deriving stock instance Show $(TH.conT datatyp) |]
      , [d| instance Buildable $(TH.conT datatyp) where build = genericF |]
      , deriveMkWith dapDeriveMkOptions datatyp

        -- JSON instances
      , deriveToJSON dapAesonOptions datatyp
      , deriveFromJSON dapAesonOptions datatyp
      ]
  )
