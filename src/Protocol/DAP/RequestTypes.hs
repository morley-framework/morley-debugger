-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | All types from the [Requests section of the DAP spec](https://microsoft.github.io/debug-adapter-protocol/specification#Requests)
-- plus 'CancelRequest'.
module Protocol.DAP.RequestTypes where

import Prelude hiding (filter, lines)

import Control.Lens (ix)
import Data.Aeson.TH (deriveJSON)
import Data.Char qualified as Char
import Language.Haskell.TH qualified as TH
import Named (arg, argF, (:!), (:?))

import Protocol.DAP.HelperTypes
import Protocol.DAP.Message
import Protocol.DAP.Mk (Mk(..), deriveMkWith)
import Protocol.DAP.RequestBase
import Protocol.DAP.Types
import Protocol.DAP.Util

-- | @Cancel@ Request
--
-- The cancel request is used by the client in two situations:
--
-- * to indicate that it is no longer interested in the result produced by a
--   specific request issued earlier
-- * to cancel a progress sequence. Clients should only call this request if the
--   corresponding capability `supportsCancelRequest` is true.
--
-- This request has a hint characteristic: a debug adapter can only be expected
-- to make a ‘best effort’ in honoring this request but there are no guarantees.
--
-- The cancel request may return an error if it could not cancel an operation
-- but a client should refrain from presenting this error to end users.
--
-- The request that got cancelled still needs to send a response back. This can
-- either be a normal result (success attribute true) or an error response
-- (success attribute false and the message set to cancelled).
--
-- Returning partial results from a cancelled request is possible but please
-- note that a client has no generic way for detecting that a response is
-- partial or not.
--
-- The progress that got cancelled still needs to send a progressEnd event back.
--
-- A client should not assume that progress just got cancelled after sending the
-- cancel request.
data CancelRequest = CancelRequest
  { {- |
      The ID (attribute `seq`) of the request to cancel. If missing no request is
      cancelled.

      Both a `requestId` and a `progressId` can be specified in one request.
    -}
    requestId :: Maybe (SeqId 'MessageToAdapter)

    {- |
      The ID (attribute `progressId`) of the progress to cancel. If missing no
      progress is cancelled.

      Both a `requestId` and a `progressId` can be specified in one request.
    -}
  , progressId :: Maybe ProgressId
  }

-- | Response to @cancel@ request. This is just an acknowledgement, so no body
-- field is required.
type CancelResponse = ()


-- | @Initialize@ Request
--
-- The initialize request is sent as the first request from the client to
-- the debug adapter in order to configure it with client capabilities and
-- to retrieve capabilities from the debug adapter.
--
-- Until the debug adapter has responded with an initialize response,
-- the client must not send any additional requests or events to the debug
-- adapter.
--
-- In addition the debug adapter is not allowed to send any requests or events
-- to the client until it has responded with an initialize response.
--
-- The initialize request may only be sent once.
data InitializeRequest = InitializeRequest
  { {- |
      The ID of the client using this adapter.
    -}
    clientID :: Maybe ClientId

    {- |
      The human-readable name of the client using this adapter.
    -}
  , clientName :: Maybe Text

    {- |
      The ID of the debug adapter.
    -}
  , adapterID :: AdapterId

    {- |
      The ISO-639 locale of the client using this adapter, e.g. en-US or de-CH.
    -}
  , locale :: Maybe Text

    {- |
      If true all line numbers are 1-based (default).
    -}
  , linesStartAt1 :: Maybe Bool

    {- |
      If true all column numbers are 1-based (default).
    -}
  , columnsStartAt1 :: Maybe Bool

    {- |
      Determines in what format paths are specified. The default is `path`,
      which is the native format.
    -}
  , pathFormat :: Maybe (OpenEnum "pathFormat" ["path", "uri"])

    {- |
      Client supports the `type` attribute for variables.
    -}
  , supportsVariableType :: Maybe Bool

    {- |
      Client supports the paging of variables.
    -}
  , supportsVariablePaging :: Maybe Bool

    {- |
      Client supports the `runInTerminal` request.
    -}
  , supportsRunInTerminalRequest :: Maybe Bool

    {- |
      Client supports memory references.
    -}
  , supportsMemoryReferences :: Maybe Bool

    {- |
      Client supports progress reporting.
    -}
  , supportsProgressReporting :: Maybe Bool

    {- |
      Client supports the `invalidated` event.
    -}
  , supportsInvalidatedEvent :: Maybe Bool

    {- |
      Client supports the `memory` event.
    -}
  , supportsMemoryEvent :: Maybe Bool

    {- |
      Client supports the `argsCanBeInterpretedByShell` attribute on the
      `runInTerminal` request.
    -}
  , supportsArgsCanBeInterpretedByShell :: Maybe Bool

    {- |
      Client supports the `startDebugging` request.
    -}
  , supportsStartDebuggingRequest :: Maybe Bool
  }

-- | Response to @initialize@ request.
type InitializeResponse = Maybe Capabilities


-- | @ConfigurationDone@ Request
--
-- This request indicates that the client has finished initialization of
-- the debug adapter.
--
-- So it is the last request in the sequence of configuration requests
-- (which was started by the initialized event).
--
-- Clients should only call this request if the corresponding capability
-- `supportsConfigurationDoneRequest` is true.
data ConfigurationDoneRequest = ConfigurationDoneRequest

-- | Response to @configurationDone@ request. This is just an acknowledgement,
-- so no body field is required.
type ConfigurationDoneResponse = ()


-- | @Launch@ Request
--
-- This launch request is sent from the client to the debug adapter to start
-- the debuggee with or without debugging (if noDebug is true).
--
-- Since launching is debugger/runtime specific, the arguments for this request
-- are not part of this specification.
--
-- Additional attributes are implementation specific.
data LaunchRequest = LaunchRequest
  { {- |
      If true, the launch request should launch the program without enabling
      debugging.
    -}
    noDebug :: Maybe Bool

    {- |
      Arbitrary data from the previous, restarted session.
      The data is sent as the `restart` attribute of the `terminated` event.
      The client should leave the data intact.
    -}
  , __restart :: Maybe AnyType
  }

-- | Response to @launch@ request. This is just an acknowledgement, so no body field is required.
type LaunchResponse = ()


-- | @Attach@ Request
--
-- The attach request is sent from the client to the debug adapter to attach
-- to a debuggee that is already running.
--
-- Since attaching is debugger/runtime specific, the arguments for this
-- request are not part of this specification.
--
-- Additional attributes are implementation specific.
data AttachRequest = AttachRequest
  { {- |
      Arbitrary data from the previous, restarted session.
      The data is sent as the `restart` attribute of the `terminated` event.
      The client should leave the data intact.
    -}
    __restart :: Maybe AnyType
  }

-- | Response to @attach@ request. This is just an acknowledgement, so no body field is required.
type AttachResponse = ()


-- | @Restart@ Request
--
-- Restarts a debug session. Clients should only call this request if the
-- corresponding capability `supportsRestartRequest` is true.
--
-- If the capability is missing or has the value false, a typical client
-- emulates restart by terminating the debug adapter first and then launching it
-- anew.
--
-- This type contains the latest version of the `launch` or `attach`
-- configuration.
data RestartRequest
  = MkLaunchRequest LaunchRequest
  | MkAttachRequest AttachRequest

-- | Response to @restart@ request. This is just an acknowledgement, so no body
-- field is required.
type RestartResponse = ()


-- | @Disconnect@ Request
--
-- The disconnect request asks the debug adapter to disconnect from the debuggee
-- (thus ending the debug session) and then to shut down itself (the debug
-- adapter).
--
-- In addition, the debug adapter must terminate the debuggee if it was started
-- with the launch request. If an attach request was used to connect to the
-- debuggee, then the debug adapter must not terminate the debuggee.
--
-- This implicit behavior of when to terminate the debuggee can be overridden
-- with the terminateDebuggee argument (which is only supported by a debug
-- adapter if the corresponding capability `supportTerminateDebuggee` is true).
data DisconnectRequest = DisconnectRequest
  { {- |
      A value of true indicates that this `disconnect` request is part of a
      restart sequence.
    -}
    restart :: Maybe Bool

    {- |
      Indicates whether the debuggee should be terminated when the debugger is
      disconnected.
      If unspecified, the debug adapter is free to do whatever it thinks is best.
      The attribute is only honored by a debug adapter if the corresponding
      capability `supportTerminateDebuggee` is true.
    -}
  , terminateDebuggee :: Maybe Bool

    {- |
      Indicates whether the debuggee should stay suspended when the debugger is
      disconnected.
      If unspecified, the debuggee should resume execution.
      The attribute is only honored by a debug adapter if the corresponding
      capability `supportSuspendDebuggee` is true.
    -}
  , suspendDebuggee :: Maybe Bool
  }

-- | Response to @disconnect@ request. This is just an acknowledgement, so no
-- body field is required.
type DisconnectResponse = ()


-- | @Terminate@ Request
--
-- The terminate request is sent from the client to the debug adapter in order
-- to shut down the debuggee gracefully. Clients should only call this request
-- if the capability @supportsTerminateRequest@ is true.
--
-- Typically a debug adapter implements terminate by sending a software signal
-- which the debuggee intercepts in order to clean things up properly before
-- terminating itself.
--
-- Please note that this request does not directly affect the state of the debug
-- session: if the debuggee decides to veto the graceful shutdown for any reason
-- by not terminating itself, then the debug session just continues.
--
-- Clients can surface the terminate request as an explicit command or they can
-- integrate it into a two stage Stop command that first sends terminate to
-- request a graceful shutdown, and if that fails uses disconnect for a forceful
-- shutdown.
data TerminateRequest = TerminateRequest
  { {- |
      A value of true indicates that this `terminate` request is part of a
      restart sequence.
    -}
    restart :: Maybe Bool
  }

-- | Response to @terminate@ request. This is just an acknowledgement, so no
-- body field is required.
type TerminateResponse = ()


-- | @BreakpointLocations@ Request
--
-- The @breakpointLocations@ request returns all possible locations for source
-- breakpoints in a given range.
--
-- Clients should only call this request if the corresponding capability
-- `supportsBreakpointLocationsRequest` is true.
data BreakpointLocationsRequest = BreakpointLocationsRequest
  { {- |
      The source location of the breakpoints; either `source.path` or
      `source.reference` must be specified.
    -}
    source :: Source

    {- |
      Start line of range to search possible breakpoint locations in. If only the
      line is specified, the request returns all possible locations in that line.
    -}
  , line :: Pos

    {- |
      Start position within `line` to search possible breakpoint locations in. It
      is measured in UTF-16 code units and the client capability
      `columnsStartAt1` determines whether it is 0- or 1-based. If no column is
      given, the first position in the start line is assumed.
    -}
  , column :: Maybe Pos

    {- |
      End line of range to search possible breakpoint locations in. If no end
      line is given, then the end line is assumed to be the start line.
    -}
  , endLine :: Maybe Pos

    {- |
      End position within `endLine` to search possible breakpoint locations in.
      It is measured in UTF-16 code units and the client capability
      `columnsStartAt1` determines whether it is 0- or 1-based. If no end column
      is given, the last position in the end line is assumed.
    -}
  , endColumn :: Maybe Pos
  }

-- | Response to @breakpointLocations@ request.
--
-- Contains possible locations for source breakpoints.
data BreakpointLocationsResponse = BreakpointLocationsResponse
  { {- |
      Sorted set of possible breakpoint locations.
    -}
    breakpoints :: [BreakpointLocation]
  }


-- | @SetBreakpoints@ Request
--
-- Sets multiple breakpoints for a single source and clears all previous
-- breakpoints in that source.
--
-- To clear all breakpoint for a source, specify an empty array.
--
-- When a breakpoint is hit, a stopped event (with reason breakpoint) is
-- generated.
data SetBreakpointsRequest = SetBreakpointsRequest
  { {- |
      The source location of the breakpoints; either `source.path` or
      `source.sourceReference` must be specified.
    -}
    source :: Source

    {- |
      The code locations of the breakpoints.
    -}
  , breakpoints :: Maybe [SourceBreakpoint]

    {- |
      Deprecated: The code locations of the breakpoints.
    -}
  , lines :: Maybe [Pos]

    {- |
      A value of true indicates that the underlying source has been modified
      which results in new breakpoint locations.
    -}
  , sourceModified :: Maybe Bool
  }

-- | Response to @setBreakpoints@ request.
--
-- Returned is information about each breakpoint created by this request.
--
-- This includes the actual code location and whether the breakpoint could be
-- verified.
--
-- The breakpoints returned are in the same order as the elements of the
-- breakpoints (or the deprecated lines) array in the arguments.
data SetBreakpointsResponse = SetBreakpointsResponse
  { {- |
      Information about the breakpoints.
      The array elements are in the same order as the elements of the
      `breakpoints` (or the deprecated `lines`) array in the arguments.
    -}
    breakpoints :: [Breakpoint]

  }


-- | @SetFunctionBreakpoints@ Request
--
-- Replaces all existing function breakpoints with new function breakpoints.
--
-- To clear all function breakpoints, specify an empty array.
--
-- When a function breakpoint is hit, a stopped event (with reason function
-- breakpoint) is generated.
--
-- Clients should only call this request if the corresponding capability
-- supportsFunctionBreakpoints is true.
data SetFunctionBreakpointsRequest = SetFunctionBreakpointsRequest
  { {- |
      The function names of the breakpoints.
    -}
    breakpoints :: [FunctionBreakpoint]
  }

-- | Response to @setFunctionBreakpoints@ request.
--
-- Returned is information about each breakpoint created by this request.
data SetFunctionBreakpointsResponse = SetFunctionBreakpointsResponse
  { {- |
      Information about the breakpoints. The array elements correspond to the
      elements of the `breakpoints` array.
     -}
    breakpoints :: [Breakpoint]
  }


-- | @SetExceptionBreakpoints@ Request
--
-- The request configures the debugger’s response to thrown exceptions.
--
-- If an exception is configured to break, a stopped event is fired (with reason
-- exception).
--
-- Clients should only call this request if the corresponding capability
-- @exceptionBreakpointFilters@ returns one or more filters.
data SetExceptionBreakpointsRequest = SetExceptionBreakpointsRequest
  { {- |
      Set of exception filters specified by their ID. The set of all possible
      exception filters is defined by the `exceptionBreakpointFilters`
      capability. The `filter` and `filterOptions` sets are additive.
    -}
    filters :: [ExceptionFilterId]

    {- |
      Set of exception filters and their options. The set of all possible
      exception filters is defined by the `exceptionBreakpointFilters`
      capability. This attribute is only honored by a debug adapter if the
      corresponding capability `supportsExceptionFilterOptions` is true. The
      `filter` and `filterOptions` sets are additive.
    -}
  , filterOptions :: Maybe [ExceptionFilterOptions]

    {- |
      Configuration options for selected exceptions.
      The attribute is only honored by a debug adapter if the corresponding
      capability `supportsExceptionOptions` is true.
    -}
  , exceptionOptions :: Maybe [ExceptionOptions]
  }

-- | Response to @setExceptionBreakpoints@ request.
--
-- The response contains an array of Breakpoint objects with information about
-- each exception breakpoint or filter. The Breakpoint objects are in the same
-- order as the elements of the @filters@, @filterOptions@, @exceptionOptions@
-- arrays given as arguments. If both filters and filterOptions are given, the
-- returned array must start with filters information first, followed by
-- @filterOptions@ information.
--
-- The verified property of a 'Breakpoint' object signals whether the exception
-- breakpoint or filter could be successfully created and whether the condition
-- or hit count expressions are valid. In case of an error the message property
-- explains the problem. The id property can be used to introduce a unique ID
-- for the exception breakpoint or filter so that it can be updated subsequently
-- by sending breakpoint events.
--
-- For backward compatibility both the breakpoints array and the enclosing body
-- are optional. If these elements are missing a client is not able to show
-- problems for individual exception breakpoints or filters.
type SetExceptionBreakpointsResponse = Maybe SetExceptionBreakpointsResponseBody

data SetExceptionBreakpointsResponseBody = SetExceptionBreakpointsResponse
  { {- |
      Information about the exception breakpoints or filters.
      The breakpoints returned are in the same order as the elements of the
      @filters@, @filterOptions@, @exceptionOptions@ arrays in the arguments.

      If both @filters@ and @filterOptions@ are given, the returned array must
      start with @filters@ information first, followed by @filterOptions@
      information.
    -}
    breakpoints :: Maybe [Breakpoint]
  }


-- | @DataBreakpointInfo@ Request
--
-- Obtains information on a possible data breakpoint that could be set on an
-- expression or variable.
--
-- Clients should only call this request if the corresponding capability
-- `supportsDataBreakpoints` is true.
data DataBreakpointInfoRequest = DataBreakpointInfoRequest
  { {- |
      Reference to the variable container if the data breakpoint is requested for
      a child of the container. The `variablesReference` must have been obtained
      in the current suspended state. See @Lifetime of Object References@ in the
      Overview section for details.
    -}
    variablesReference :: Maybe VariableId

    {- |
      The name of the variable's child to obtain data breakpoint information for.
      If `variablesReference` isn't specified, this can be an expression.
    -}
  , name :: Text

    {- |
      When `name` is an expression, evaluate it in the scope of this stack frame.
      If not specified, the expression is evaluated in the global scope. When
      `variablesReference` is specified, this property has no effect.
    -}
  , frameId :: Maybe StackFrameId
  }

-- | Response to @dataBreakpointInfo@ request.
data DataBreakpointInfoResponse = DataBreakpointInfoResponse
  { {- |
      An identifier for the data on which a data breakpoint can be registered
      with the `setDataBreakpoints` request or @Nothing@ if no data breakpoint
      is available.
      -}
    dataId :: Maybe Text

    {- |
      UI Text that describes on what data the breakpoint is set on or why a
      data breakpoint is not available.
      -}
  , description :: Text

    {- |
      Attribute lists the available access types for a potential data
      breakpoint. A UI client could surface this information.
      -}
  , accessTypes :: Maybe [DataBreakpointAccessType]

    {- |
      Attribute indicates that a potential data breakpoint could be persisted
      across sessions.
      -}
  , canPersist :: Maybe Bool
  }


-- | @SetDataBreakpoints@ Request
--
-- Replaces all existing data breakpoints with new data breakpoints.
--
-- To clear all data breakpoints, specify an empty array.
--
-- When a data breakpoint is hit, a stopped event (with reason data breakpoint)
-- is generated.
--
-- Clients should only call this request if the corresponding capability
-- 'supportsDataBreakpoints' is true.
data SetDataBreakpointsRequest = SetDataBreakpointsRequest
  { {- |
      The contents of this array replaces all existing data breakpoints. An empty
      array clears all data breakpoints.
    -}
    breakpoints :: [DataBreakpoint]
  }

-- | Response to @setDataBreakpoints@ request.
--
-- Returned is information about each breakpoint created by this request.
data SetDataBreakpointsResponse = SetDataBreakpointsResponse
  { {- |
      Information about the data breakpoints. The array elements correspond to
      the elements of the input argument `breakpoints` array.
    -}
    breakpoints :: [Breakpoint]
  }


-- | @SetInstructionBreakpoints@ Request
--
-- Replaces all existing instruction breakpoints. Typically, instruction
-- breakpoints would be set from a disassembly window.
--
-- To clear all instruction breakpoints, specify an empty array.
--
-- When an instruction breakpoint is hit, a stopped event (with reason
-- instruction breakpoint) is generated.
--
-- Clients should only call this request if the corresponding capability
-- `supportsInstructionBreakpoints` is true.
data SetInstructionBreakpointsRequest = SetInstructionBreakpointsRequest
  { {- |
      The instruction references of the breakpoints
    -}
    breakpoints :: [InstructionBreakpoint]
  }

-- | Response to @setInstructionBreakpoints@ request
data SetInstructionBreakpointsResponse = SetInstructionBreakpointsResponse
  { {- |
      Information about the breakpoints. The array elements correspond to the
      elements of the `breakpoints` array.
     -}
    breakpoints :: [Breakpoint]
  }


-- | @Continue@ Request
--
-- The request resumes execution of all threads. If the debug adapter supports
-- single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true resumes only the specified thread. If not all threads were resumed,
-- the `allThreadsContinued` attribute of the response should be set to false.
data ContinueRequest = ContinueRequest
  { {- |
      Specifies the active thread. If the debug adapter supports single thread
      execution (see `supportsSingleThreadExecutionRequests`) and the argument
      `singleThread` is true, only the thread with this ID is resumed.
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, execution is resumed only for the thread with given
      `threadId`.
    -}
  , singleThread :: Maybe Bool
  }

-- | Response to @continue@ request.
data ContinueResponse = ContinueResponse
  { {- |
      The value true (or a missing property) signals to the client that all
      threads have been resumed. The value false indicates that not all threads
      were resumed.
     -}
    allThreadsContinued :: Maybe Bool
  }


-- | @Next@ Request
--
-- The request executes one step (in the given granularity) for the specified
-- thread and allows all other threads to run freely by resuming them.
--
-- If the debug adapter supports single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true prevents other suspended threads from resuming.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason step) after the step has completed.
data NextRequest = NextRequest
  { {- |
      Specifies the thread for which to resume execution for one step (of the
      given granularity).
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, all other suspended threads are not resumed.
    -}
  , singleThread :: Maybe Bool

    {- |
      Stepping granularity. If no granularity is specified, a granularity of
      `statement` is assumed.
    -}
  , granularity :: Maybe SteppingGranularity
  }

-- | Response to @next@ request. This is just an acknowledgement, so no body
-- field is required.
type NextResponse = ()


-- | @StepIn@ Request
--
-- The request resumes the given thread to step into a function/method and
-- allows all other threads to run freely by resuming them.
--
-- If the debug adapter supports single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true prevents other suspended threads from resuming.
--
-- If the request cannot step into a target, stepIn behaves like the next
-- request.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason step) after the step has completed.
--
-- If there are multiple function/method calls (or other targets) on the source
-- line,
--
-- the argument targetId can be used to control into which target the stepIn
-- should occur.
--
-- The list of possible targets for a given source line can be retrieved via the
-- `StepInTargetsRequest`.
data StepInRequest = StepInRequest
  { {- |
      Specifies the thread for which to resume execution for one step-into (of
      the given granularity).
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, all other suspended threads are not resumed.
    -}
  , singleThread :: Maybe Bool

    {- |
      Id of the target to step into.
    -}
  , targetId :: Maybe StepInTargetId

    {- |
      Stepping granularity. If no granularity is specified, a granularity of
      `statement` is assumed.
    -}
  , granularity :: Maybe SteppingGranularity
  }

-- | Response to @stepIn@ request. This is just an acknowledgement, so no body
-- field is required.
type StepInResponse = ()


-- | @StepOut@ Request
--
-- The request resumes the given thread to step out (return) from a
-- function/method and allows all other threads to run freely by resuming them.
--
-- If the debug adapter supports single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true prevents other suspended threads from resuming.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason step) after the step has completed.
data StepOutRequest = StepOutRequest
  { {- |
      Specifies the thread for which to resume execution for one step-out (of the
      given granularity).
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, all other suspended threads are not resumed.
    -}
  , singleThread :: Maybe Bool

    {- |
      Stepping granularity. If no granularity is specified, a granularity of
      `statement` is assumed.
    -}
  , granularity :: Maybe SteppingGranularity
  }

-- | Response to @stepOut@ request. This is just an acknowledgement, so no body
-- field is required.
type StepOutResponse = ()


-- | @StepBack@ Request
--
-- The request executes one backward step (in the given granularity) for the
-- specified thread and allows all other threads to run backward freely by
-- resuming them.
--
-- If the debug adapter supports single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true prevents other suspended threads from resuming.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason step) after the step has completed.
--
-- Clients should only call this request if the corresponding capability
-- `supportsStepBack` is true.
data StepBackRequest = StepBackRequest
  { {- |
      Specifies the thread for which to resume execution for one step backwards
      (of the given granularity).
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, all other suspended threads are not resumed.
    -}
  , singleThread :: Maybe Bool

    {- |
      Stepping granularity to step. If no granularity is specified, a granularity
      of `statement` is assumed.
    -}
  , granularity :: Maybe SteppingGranularity
  }

-- | Response to @stepBack@ request. This is just an acknowledgement, so no body field is required.
type StepBackResponse = ()


-- | @ReverseContinue@ Request
--
-- The request resumes backward execution of all threads. If the debug adapter
-- supports single thread execution (see capability
-- `supportsSingleThreadExecutionRequests`), setting the singleThread argument
-- to true resumes only the specified thread. If not all threads were resumed,
-- the allThreadsContinued attribute of the response should be set to false.
--
-- Clients should only call this request if the corresponding capability
-- `supportsStepBack` is true.
data ReverseContinueRequest = ReverseContinueRequest
  { {- |
      Specifies the active thread. If the debug adapter supports single thread
      execution (see `supportsSingleThreadExecutionRequests`) and the
      `singleThread` argument is true, only the thread with this ID is resumed.
    -}
    threadId :: ThreadId

    {- |
      If this flag is true, backward execution is resumed only for the thread
      with given `threadId`.
    -}
  , singleThread :: Maybe Bool
  }

-- | Response to @reverseContinue@ request. This is just an acknowledgement, so
-- no body field is required.
type ReverseContinueResponse = ()


-- | @RestartFrame@ Request
--
-- The request restarts execution of the specified stack frame.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason restart) after the restart has completed.
--
-- Clients should only call this request if the corresponding capability
-- `supportsRestartFrame` is true.
data RestartFrameRequest = RestartFrameRequest
  { {- |
      Restart the stack frame identified by `frameId`. The `frameId` must have
      been obtained in the current suspended state. See @Lifetime of Object
      References@ in the Overview section for details.
    -}
    frameId :: StackFrameId
  }

-- | Response to @restartFrame@ request. This is just an acknowledgement, so no
-- body field is required.
type RestartFrameResponse = ()


-- | @Goto@ Request
--
-- The request sets the location where the debuggee will continue to run.
--
-- This makes it possible to skip the execution of code or to execute code
-- again.
--
-- The code between the current location and the goto target is not executed but
-- skipped.
--
-- The debug adapter first sends the response and then a stopped event with
-- reason goto.
--
-- Clients should only call this request if the corresponding capability
-- `supportsGotoTargetsRequest` is true (because only then goto targets exist
-- that can be passed as arguments).
data GotoRequest = GotoRequest
  { {- |
      Set the goto target for this thread.
    -}
    threadId :: ThreadId

    {- |
      The location where the debuggee will continue to run.
    -}
  , targetId :: GotoTargetId
  }

-- | Response to @goto@ request. This is just an acknowledgement, so no body field
-- is required.
type GotoResponse = ()


-- | @Pause@ Request
--
-- The request suspends the debuggee.
--
-- The debug adapter first sends the response and then a stopped event (with
-- reason pause) after the thread has been paused successfully.
data PauseRequest = PauseRequest
  { {- |
      Pause execution for this thread.
    -}
    threadId :: ThreadId
  }

-- | Response to @pause@ request. This is just an acknowledgement, so no body
-- field is required.
type PauseResponse = ()


-- | @StackTrace@ Request
--
-- The request returns a stacktrace from the current execution state of a given
-- thread.
--
-- A client can request all stack frames by omitting the startFrame and levels
-- arguments. For performance-conscious clients and if the corresponding
-- capability `supportsDelayedStackTraceLoading` is true, stack frames can be
-- retrieved in a piecemeal way with the startFrame and levels arguments. The
-- response of the stackTrace request may contain a 'totalFrames' property that
-- hints at the total number of frames in the stack. If a client needs this
-- total number upfront, it can issue a request for a single (first) frame and
-- depending on the value of @totalFrames@ decide how to proceed. In any case a
-- client should be prepared to receive fewer frames than requested, which is an
-- indication that the end of the stack has been reached.
data StackTraceRequest = StackTraceRequest
  { {- |
      Retrieve the stacktrace for this thread.
    -}
    threadId :: ThreadId

    {- |
      The index of the first frame to return; if omitted frames start at 0.
    -}
  , startFrame :: Maybe Word

    {- |
      The maximum number of frames to return. If levels is not specified or 0,
      all frames are returned.
    -}
  , levels :: Maybe Word

    {- |
      Specifies details on how to format the stack frames.
      The attribute is only honored by a debug adapter if the corresponding
      capability `supportsValueFormattingOptions` is true.
    -}
  , format :: Maybe StackFrameFormat
  }

-- | Response to @stackTrace@ request.
data StackTraceResponse = StackTraceResponse
  { {- |
      The frames of the stack frame. If the array has length zero, there are no
      stack frames available.
      This means that there is no location information available.
     -}
    stackFrames :: [StackFrame]

    {- |
      The total number of frames available in the stack. If omitted or if
      `totalFrames` is larger than the available frames, a client is expected
      to request frames until a request returns less frames than requested
      (which indicates the end of the stack). Returning monotonically
      increasing `totalFrames` values for subsequent requests can be used to
      enforce paging in the client.
     -}
  , totalFrames :: Maybe Word
  }


-- | @Scopes@ Request
--
-- The request returns the variable scopes for a given stack frame ID.
data ScopesRequest = ScopesRequest
  { {- |
      Retrieve the scopes for the stack frame identified by `frameId`. The
      `frameId` must have been obtained in the current suspended state. See
      @Lifetime of Object References@ in the Overview section for details.
    -}
    frameId :: StackFrameId
  }

-- | Response to @scopes@ request.
data ScopesResponse = ScopesResponse
  { {- |
      The scopes of the stack frame. If the array has length zero, there are no
      scopes available.
     -}
    scopes :: [Scope]
  }


-- | @Variables@ Request
--
-- Retrieves all child variables for the given variable reference.
--
-- A filter can be used to limit the fetched children to either named or indexed
-- children.
data VariablesRequest = VariablesRequest
  { {- |
      The variable for which to retrieve its children. The `variablesReference`
      must have been obtained in the current suspended state. See @Lifetime of
      Object References@ in the Overview section for details.
    -}
    variablesReference :: VariableId

    {- |
      Filter to limit the child variables to either named or indexed. If omitted,
      both types are fetched.
    -}
  , filter :: Maybe (ClosedEnum ["indexed", "named"])

    {- |
      The index of the first variable to return; if omitted children start at 0.
    -}
  , start :: Maybe Word

    {- |
      The number of variables to return. If count is missing or 0, all variables
      are returned.
    -}
  , count :: Maybe Word

    {- |
      Specifies details on how to format the 'Variable' values.

      The attribute is only honored by a debug adapter if the corresponding
      capability `supportsValueFormattingOptions` is true.
    -}
  , format :: Maybe ValueFormat
  }

-- | Response to @variables@ request.
data VariablesResponse = VariablesResponse
  { {- |
      All (or a range) of variables for the given variable reference.
     -}
    variables :: [Variable]
  }


-- | @SetVariable@ Request
--
-- Set the variable with the given name in the variable container to a new
-- value. Clients should only call this request if the corresponding capability
-- `supportsSetVariable` is true.
--
-- If a debug adapter implements both `SetVariableRequest` and
-- `SetExpressionRequest`, a client will only use `SetExpressionRequest` if the
-- variable has an `evaluateName` property.
data SetVariableRequest = SetVariableRequest
  { {- |
      The reference of the variable container. The `variablesReference` must
      have been obtained in the current suspended state. See @Lifetime of Object
      References@ in the Overview section for details.
    -}
    variablesReference :: VariableId

    {- |
      The name of the variable in the container.
    -}
  , name :: Text

    {- |
      The value of the variable.
    -}
  , value :: Text

    {- |
      Specifies details on how to format the response value.
    -}
  , format :: Maybe ValueFormat
  }

-- | Response to @setVariable@ request.
data SetVariableResponse = SetVariableResponse
  { {- |
      The new value of the variable.
     -}
    value :: Text

    {- |
      The type of the new value. Typically shown in the UI when hovering over
      the value.
     -}
  , type_ :: Maybe Text

    {- |
      If `variablesReference` is > 0, the new value is structured and its
      children can be retrieved by passing `variablesReference` to the
      `variables` request as long as execution remains suspended. See @Lifetime
      of Object References@ in the Overview section for details.
     -}
  , variablesReference :: Maybe VariableId

    {- |
      The number of named child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , namedVariables :: Maybe Word

    {- |
      The number of indexed child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , indexedVariables :: Maybe Word
}


-- | @Source@ Request
--
-- The request retrieves the source code for a given source reference.
data SourceRequest = SourceRequest
  { {- |
      Specifies the source content to load. Either `source.path` or
      `source.sourceReference` must be specified.
    -}
    source :: Maybe Source

    {- |
      The reference to the source. This is the same as `source.sourceReference`.
      This is provided for backward compatibility since old clients do not
      understand the `source` attribute.
    -}
  , sourceReference :: SourceId
  }

-- | Response to @source@ request.
data SourceResponse = SourceResponse
  { {- |
      Content of the source reference.
     -}
    content :: Text

    {- |
      Content type (MIME type) of the source.
     -}
  , mimeType :: Maybe Text
  }


-- | @Threads@ Request
--
-- The request retrieves a list of all threads.
data ThreadsRequest = ThreadsRequest

-- | Response to @threads@ request.
data ThreadsResponse = ThreadsResponse
  { {- |
      All threads.
     -}
    threads :: [Thread]
  }


-- | @TerminateThreads@ Request
--
-- The request terminates the threads with the given ids.
--
-- Clients should only call this request if the corresponding capability
-- `supportsTerminateThreadsRequest` is true.
data TerminateThreadsRequest = TerminateThreadsRequest
  { {- |
      Ids of threads to be terminated.
    -}
    threadIds :: Maybe [ThreadId]
  }

-- | Response to @terminateThreads@ request. This is just an acknowledgement, no
-- body field is required.
type TerminateThreadsResponse = ()


-- | @Modules@ Request
--
-- Modules can be retrieved from the debug adapter with this request which can
-- either return all modules or a range of modules to support paging.
--
-- Clients should only call this request if the corresponding capability
-- `supportsModulesRequest` is true.
data ModulesRequest = ModulesRequest
  { {- |
      The index of the first module to return; if omitted modules start at 0.
    -}
    startModule :: Maybe Word

    {- |
      The number of modules to return. If `moduleCount` is not specified or 0,
      all modules are returned.
    -}
  , moduleCount :: Maybe Word
  }

-- | Response to @modules@ request.
data ModulesResponse = ModulesResponse
  { {- |
      All modules or range of modules.
     -}
    modules :: [Module]

    {- |
      The total number of modules available.
     -}
  , totalModules :: Maybe Word
  }


-- | @LoadedSources@ Request
--
-- Retrieves the set of all sources currently loaded by the debugged process.
--
-- Clients should only call this request if the corresponding capability
-- `supportsLoadedSourcesRequest` is true.
data LoadedSourcesRequest = LoadedSourcesRequest

-- | Response to @loadedSources@ request.
data LoadedSourcesResponse = LoadedSourcesResponse
  { {- |
      Set of loaded sources.
     -}
    sources :: [Source]
  }


-- | @Evaluate@ Request
--
-- Evaluates the given expression in the context of the topmost stack frame.
--
-- The expression has access to any variables and arguments that are in scope.
data EvaluateRequest = EvaluateRequest
  { {- |
      The expression to evaluate.
    -}
    expression :: Text

    {- |
      Evaluate the expression in the scope of this stack frame. If not specified,
      the expression is evaluated in the global scope.
    -}
  , frameId :: Maybe StackFrameId

    {- |
      The context in which the evaluate request is used.

      Values:

      * @watch@: evaluate is called from a watch view context.
      * @repl@: evaluate is called from a REPL context.
      * @hover@: evaluate is called to generate the debug hover contents.
      This value should only be used if the corresponding capability
      `supportsEvaluateForHovers` is true.
      * @clipboard@: evaluate is called to generate clipboard contents.
      This value should only be used if the corresponding capability
      * @supportsClipboardContext@ is true.
      * @variables@: evaluate is called from a variables view context.
      etc.
    -}
  , context :: Maybe $ OpenEnum "evaluateRequestContext"
      ["watch", "repl", "hover", "clipboard", "variables"]

    {- |
      Specifies details on how to format the result.
      The attribute is only honored by a debug adapter if the corresponding
      capability `supportsValueFormattingOptions` is true.
    -}
  , format :: Maybe ValueFormat
  }

-- | Response to @evaluate@ request.
data EvaluateResponse = EvaluateResponse
  { {- |
      The result of the evaluate request.
     -}
    result :: Text

    {- |
      The type of the evaluate result.

      This attribute should only be returned by a debug adapter if the
      corresponding capability `supportsVariableType` is true.
     -}
  , type_ :: Maybe Text

    {- |
      Properties of an evaluate result that can be used to determine how to
      render the result in the UI.
     -}
  , presentationHint :: Maybe VariablePresentationHint

    {- |
      If `variablesReference` is > 0, the evaluate result is structured and its
      children can be retrieved by passing `variablesReference` to the
      `variables` request as long as execution remains suspended. See @Lifetime
      of Object References@ in the Overview section for details.
     -}
  , variablesReference :: VariableId

    {- |
      The number of named child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , namedVariables :: Maybe Word

    {- |
      The number of indexed child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , indexedVariables :: Maybe Word

    {- |
      A memory reference to a location appropriate for this result.
      For pointer type eval results, this is generally a reference to the
      memory address contained in the pointer.

      This attribute should be returned by a debug adapter if corresponding
      capability `supportsMemoryReferences` is true.
     -}
  , memoryReference :: Maybe Text
  }

instance Mk EvaluateResponse where
  type MkSig EvaluateResponse
    =  "result" :! Text
    -> "type" :? Text
    -> "presentationHint" :? VariablePresentationHint
    -> "variablesReference" :? VariableId
    -> "namedVariables" :? Word
    -> "indexedVariables" :? Word
    -> "memoryReference" :? Text
    -> EvaluateResponse
  mk
    (arg #result -> result)
    (argF #type -> type_)
    (argF #presentationHint -> presentationHint)
    (argF #variablesReference -> variablesReference)
    (argF #namedVariables -> namedVariables)
    (argF #indexedVariables -> indexedVariables)
    (argF #memoryReference -> memoryReference)
    = EvaluateResponse
      { variablesReference = variablesReference ?: NoVariableId
      , ..
      }

-- | @SetExpression@ Request
--
-- Evaluates the given value expression and assigns it to the expression which
-- must be a modifiable l-value.
--
-- The expressions have access to any variables and arguments that are in scope
-- of the specified frame.
--
-- Clients should only call this request if the corresponding capability
-- `supportsSetExpression` is true.
--
-- If a debug adapter implements both `SetExpressionRequest` and
-- `SetVariableRequest`, a client uses `SetExpressionRequest` if the variable
-- has an 'evaluateName' property.
data SetExpressionRequest = SetExpressionRequest
  { {- |
      The l-value expression to assign to.
    -}
    expression :: Text

    {- |
      The value expression to assign to the l-value expression.
    -}
  , value :: Text

    {- |
      Evaluate the expressions in the scope of this stack frame. If not
      specified, the expressions are evaluated in the global scope.
    -}
  , frameId :: Maybe StackFrameId

    {- |
      Specifies how the resulting value should be formatted.
    -}
  , format :: Maybe ValueFormat
  }

-- | Response to @setExpression@ request.
data SetExpressionResponse = SetExpressionResponse
  { {- |
      The new value of the expression.
     -}
    value :: Text

    {- |
      The type of the value.

      This attribute should only be returned by a debug adapter if the
      corresponding capability `supportsVariableType` is true.
     -}
  , type_ :: Maybe Text

    {- |
      Properties of a value that can be used to determine how to render the
      result in the UI.
     -}
  , presentationHint :: Maybe VariablePresentationHint

    {- |
      If `variablesReference` is > 0, the evaluate result is structured and its
      children can be retrieved by passing `variablesReference` to the
      `variables` request as long as execution remains suspended. See @Lifetime
      of Object References@ in the Overview section for details.
     -}
  , variablesReference :: Maybe VariableId

    {- |
      The number of named child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , namedVariables :: Maybe Word

    {- |
      The number of indexed child variables.

      The client can use this information to present the variables in a paged
      UI and fetch them in chunks.

      The value should be less than or equal to 2147483647 (2^31-1).
     -}
  , indexedVariables :: Maybe Word
  }


-- | @StepInTargets@ Request
--
-- This request retrieves the possible step-in targets for the specified stack
-- frame.
--
-- These targets can be used in the stepIn request.
--
-- Clients should only call this request if the corresponding capability
-- `supportsStepInTargetsRequest` is true.
data StepInTargetsRequest = StepInTargetsRequest
  { {- |
      The stack frame for which to retrieve the possible step-in targets.
    -}
    frameId :: StackFrameId
  }

-- | Response to @stepInTargets@ request.
data StepInTargetsResponse = StepInTargetsResponse
  { {- |
      The possible step-in targets of the specified source location.
    -}
    targets :: [StepInTarget]
  }


-- | @GotoTargets@ Request
--
-- This request retrieves the possible goto targets for the specified source
-- location.
--
-- These targets can be used in the goto request.
--
-- Clients should only call this request if the corresponding capability
-- `supportsGotoTargetsRequest` is true.
data GotoTargetsRequest = GotoTargetsRequest
  { {- |
      The source location for which the goto targets are determined.
    -}
    source :: Source

    {- |
      The line location for which the goto targets are determined.
    -}
  , line :: Pos

    {- |
      The position within `line` for which the goto targets are determined. It is
      measured in UTF-16 code units and the client capability `columnsStartAt1`
      determines whether it is 0- or 1-based.
    -}
  , column :: Maybe Pos
  }

-- | Response to @gotoTargets@ request.
data GotoTargetsResponse = GotoTargetsResponse
  { {- |
      The possible goto targets of the specified location.
    -}
    targets :: [GotoTarget]
  }


-- | @Completions@ Request
--
-- Returns a list of possible completions for a given caret position and text.
--
-- Clients should only call this request if the corresponding capability
-- `supportsCompletionsRequest` is true.
data CompletionsRequest = CompletionsRequest
  { {- |
      Returns completions in the scope of this stack frame. If not specified, the
      completions are returned for the global scope.
    -}
    frameId :: Maybe StackFrameId

    {- |
      One or more source lines. Typically this is the text users have typed into
      the debug console before they asked for completion.
    -}
  , text :: Text

    {- |
      The position within `text` for which to determine the completion proposals.
      It is measured in UTF-16 code units and the client capability
      `columnsStartAt1` determines whether it is 0- or 1-based.
    -}
  , column :: Pos

    {- |
      A line for which to determine the completion proposals. If missing the
      first line of the text is assumed.
    -}
  , line :: Maybe Pos
  }

-- | Response to @completions@ request.
data CompletionsResponse = CompletionsResponse
  { {- |
      The possible completions for .
     -}
    targets :: [CompletionItem]
  }


-- | @ExceptionInfo@ Request
--
-- Retrieves the details of the exception that caused this event to be raised.
--
-- Clients should only call this request if the corresponding capability
-- `supportsExceptionInfoRequest` is true.
data ExceptionInfoRequest = ExceptionInfoRequest
  { {- |
      Thread for which exception information should be retrieved.
    -}
    threadId :: ThreadId
  }

-- | Response to @exceptionInfo@ request.
data ExceptionInfoResponse = ExceptionInfoResponse
  { {- |
      ID of the exception that was thrown.
    -}
    exceptionId :: ExceptionId

    {- |
      Descriptive text for the exception.
    -}
  , description :: Maybe Text

    {- |
      Mode that caused the exception notification to be raised.
    -}
  , breakMode :: ExceptionBreakMode

    {- |
      Detailed information about the exception.
    -}
  , details :: Maybe ExceptionDetails
  }


-- | @ReadMemory@ Request
--
-- Reads bytes from memory at the provided location.
--
-- Clients should only call this request if the corresponding capability
-- `supportsReadMemoryRequest` is true.
data ReadMemoryRequest = ReadMemoryRequest
  { {- |
      Memory reference to the base location from which data should be read.
    -}
    memoryReference :: Text

    {- |
      Offset (in bytes) to be applied to the reference location before reading
      data. Can be negative.
    -}
  , offset :: Maybe Int

    {- |
      Number of bytes to read at the specified location and offset.
    -}
  , count :: Word
  }

-- | Response to @readMemory@ request.
type ReadMemoryResponse = Maybe ReadMemoryResponseBody

data ReadMemoryResponseBody = ReadMemoryResponse
  { {- |
      The address of the first byte of data returned.

      Treated as a hex value if prefixed with `0x`, or as a decimal value
      otherwise.
    -}
    address :: Text

    {- |
      The number of unreadable bytes encountered after the last successfully
      read byte.

      This can be used to determine the number of bytes that should be skipped
      before a subsequent `readMemory` request succeeds.
     -}
  , unreadableBytes :: Maybe Word

    {- |
      The bytes read from memory, encoded using base64. If the decoded length
      of `data` is less than the requested `count` in the original `readMemory`
      request, and `unreadableBytes` is zero or omitted, then the client should
      assume it's reached the end of readable memory.
     -}
  , data_ :: Maybe Text
  }


-- | @WriteMemory@ Request
--
-- Writes bytes to memory at the provided location.
--
-- Clients should only call this request if the corresponding capability
-- `supportsWriteMemoryRequest` is true.
data WriteMemoryRequest = WriteMemoryRequest
  { {- |
      Memory reference to the base location to which data should be written.
    -}
    memoryReference :: Text

    {- |
      Offset (in bytes) to be applied to the reference location before writing
      data. Can be negative.
    -}
  , offset :: Maybe Int

    {- |
      Property to control partial writes. If true, the debug adapter should
      attempt to write memory even if the entire memory region is not writable.
      In such a case the debug adapter should stop after hitting the first byte
      of memory that cannot be written and return the number of bytes written in
      the response via the `offset` and `bytesWritten` properties.
      If false or missing, a debug adapter should attempt to verify the region is
      writable before writing, and fail the response if it is not.
    -}
  , allowPartial :: Maybe Bool

    {- |
      Bytes to write, encoded using base64.
    -}
  , data_ :: Text
  }

-- | Response to @writeMemory@ request.
type WriteMemoryResponse = Maybe WriteMemoryResponseBody

data WriteMemoryResponseBody = WriteMemoryResponse
  { {- |
      Property that should be returned when `allowPartial` is true to indicate
      the offset of the first byte of data successfully written. Can be
      negative.
     -}
    offset :: Maybe Int

    {- |
      Property that should be returned when `allowPartial` is true to indicate
      the number of bytes starting from address that were successfully written.
     -}
  , bytesWritten :: Maybe Word
  }


-- | @Disassemble@ Request
--
-- Disassembles code stored at the provided location.
--
-- Clients should only call this request if the corresponding capability
-- `supportsDisassembleRequest` is true.
data DisassembleRequest = DisassembleRequest
  { {- |
      Memory reference to the base location containing the instructions to
      disassemble.
    -}
    memoryReference :: Text

    {- |
      Offset (in bytes) to be applied to the reference location before
      disassembling. Can be negative.
    -}
  , offset :: Maybe Int

    {- |
      Offset (in instructions) to be applied after the byte offset (if any)
      before disassembling. Can be negative.
    -}
  , instructionOffset :: Maybe Int

    {- |
      Number of instructions to disassemble starting at the specified location
      and offset.

      An adapter must return exactly this number of instructions - any
      unavailable instructions should be replaced with an implementation-defined
      'invalid instruction' value.
    -}
  , instructionCount :: Word

    {- |
      If true, the adapter should attempt to resolve memory addresses and other
      values to symbolic names.
    -}
  , resolveSymbols :: Maybe Bool
  }

-- | Response to @disassemble@ request.
type DisassembleResponse = Maybe DisassembleResponseBody

data DisassembleResponseBody = DisassembleResponse
  { {- |
      The list of disassembled instructions.
     -}
    instructions :: [DisassembledInstruction]
  }


-- TH Instances
----------------------------------------------------------------------------

-- To avoid boilerplate we generate the instances automatically with TH for all
-- datatypes.
--
-- Note that you still can define instances manually next to your types,
-- the TH generator below will notice that.

$( do
  let
    allCommands =
      [ "Cancel"

      , "Attach"
      , "BreakpointLocations"
      , "Completions"
      , "ConfigurationDone"
      , "Continue"
      , "DataBreakpointInfo"
      , "Disassemble"
      , "Disconnect"
      , "Evaluate"
      , "ExceptionInfo"
      , "Goto"
      , "GotoTargets"
      , "Initialize"
      , "Launch"
      , "LoadedSources"
      , "Modules"
      , "Next"
      , "Pause"
      , "ReadMemory"
      , "Restart"
      , "RestartFrame"
      , "ReverseContinue"
      , "Scopes"
      , "SetBreakpoints"
      , "SetDataBreakpoints"
      , "SetExceptionBreakpoints"
      , "SetExpression"
      , "SetFunctionBreakpoints"
      , "SetInstructionBreakpoints"
      , "SetVariable"
      , "Source"
      , "StackTrace"
      , "StepBack"
      , "StepIn"
      , "StepInTargets"
      , "StepOut"
      , "Terminate"
      , "TerminateThreads"
      , "Threads"
      , "Variables"
      , "WriteMemory"
      ]

  concatForM allCommands \command -> do
    requestTyName <- TH.lookupTypeName (command <> "Request")
      >>= maybe (fail $ "Request type is not declared for " <> command) pure
    responseTyName <- TH.lookupTypeName (command <> "Response")
      >>= maybe (fail $ "Response type is not declared for " <> command) pure
    -- some requests have separate bodies
    mResponseBodyTyName <- TH.lookupTypeName (command <> "ResponseBody")

    let requestTy = TH.conT requestTyName
    let responseTy = TH.conT responseTyName

    let nulifyDeclsWhen decls cond = if cond then pure [] else decls

    isResponseTypeAlias <- TH.reify responseTyName <&> \case
      TH.TyConI TH.TySynD{} -> True
      _ -> False

    dropExtraInstances $ concat <$> sequence
      [ -- IsRequest instance
        [d| instance IsRequest $requestTy where
              type ResponseFor $requestTy = $responseTy
              type CommandFor $requestTy =
                $(TH.litT $ TH.strTyLit $ ix 0 %~ Char.toLower $ command)
          |]

        -- A couple of basic instances
      , [d| deriving stock instance Eq $requestTy |]
      , [d| deriving stock instance Eq $responseTy |]
          `nulifyDeclsWhen` isResponseTypeAlias
      , [d| deriving stock instance Show $requestTy |]
      , [d| deriving stock instance Show $responseTy |]
          `nulifyDeclsWhen` isResponseTypeAlias
      , deriveMkWith dapDeriveMkOptions requestTyName
          `nulifyDeclsWhen` do
            requestTyName == ''RestartRequest
      , deriveMkWith dapDeriveMkOptions responseTyName
          `nulifyDeclsWhen` isResponseTypeAlias

        -- JSON instances
      , deriveJSON dapAesonOptions requestTyName
      , deriveJSON dapAesonOptions responseTyName
          `nulifyDeclsWhen` isResponseTypeAlias
      , case mResponseBodyTyName of
          Just responseBodyTyName -> concat <$> sequence
            [ deriveJSON dapAesonOptions responseBodyTyName
            , deriveMkWith dapDeriveMkOptions responseBodyTyName
            ]
          Nothing -> pure []
      ]
  )
