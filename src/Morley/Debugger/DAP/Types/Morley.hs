-- SPDX-FileCopyrightText: 2021 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Morley-specific requests
module Morley.Debugger.DAP.Types.Morley
  ( morleyHandlers
  , Morley
  , ContractMetadata (..)
  , MorleyLaunchRequest(..)
  , MorleyLaunchRequestBody(..)
  , MorleyContractEnv(..)
  , emptyMorleyContractEnv
  , VotingPowersConfig(..)
  , SimpleVotingPowersInfo(..)
  , MorleyParseValueRequest(..)
  , MorleyGetContractMetadataRequest(..)
  , mkMorleyDebuggerState
  ) where

import Control.Monad.Except (MonadError(throwError), liftEither)
import Data.Aeson (FromJSON(..), ToJSON(..), object, withObject, (.:), (.=))
import Data.Char (isSpace)
import Data.Default (def)
import Data.Map qualified as M
import Data.Set qualified as S
import Data.Text qualified as T
import Data.Text.IO.Utf8 qualified as Utf8
import Data.Time.Clock.POSIX (getPOSIXTime)
import "morley-prelude" Fmt (Buildable(..), fmt, pretty, (+|), (|+))
import GHC.Natural (naturalToWord)
import System.Directory (doesFileExist)
import System.FilePath (takeFileName, (<.>), (</>))

import Morley.Michelson.ErrorPos (ErrorSrcPos(..))
import Morley.Michelson.Interpret (ContractEnv'(..), RemainingSteps(..))
import Morley.Michelson.Parser qualified as P
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Runtime (ContractState(..), mkVotingPowers, parseExpandContract)
import Morley.Michelson.TypeCheck
  (typeCheckContract, typeCheckParameter, typeCheckStorage, typeCheckValue, typeVerifyParameter,
  typeVerifyStorage)
import Morley.Michelson.Typed
  (Contract, Contract'(..), ContractCode, Instr(..), SingI, SomeContract(..), SomeValue, T(..))
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address (ContractAddress, L1Address, ta)
import Morley.Tezos.Core (ChainId, Mutez, Timestamp(..), dummyChainId, tz, zeroMutez)
import Morley.Tezos.Crypto (KeyHash, parseHash)
import Morley.Util.Constrained (Constrained(..))
import Protocol.DAP (IsRequest(..), ixHandler, respond, respondAndAlso)
import Protocol.DAP qualified as DAP
import Protocol.DAP.Serve.IO (StopAdapter)

import Morley.Debugger.Core
import Morley.Debugger.DAP.Handlers
import Morley.Debugger.DAP.LanguageServer
import Morley.Debugger.DAP.RIO (logMessage, openLogHandle)
import Morley.Debugger.DAP.Types

data Morley

data ContractMetadata = ContractMetadata
  { parameterType :: JsonFromBuildable U.ParameterType
  , storageType   :: JsonFromBuildable U.Ty
  , entrypoints   :: Entrypoints
  , onChainViews  :: OnChainViews
  } deriving stock (Eq, Generic, Show)
    deriving anyclass (Buildable, ToJSON)

data MorleyLaunchRequestBody = MorleyLaunchRequestBody
  { noDebug :: Maybe Bool

    -- | Path to @.tz@ source file.
  , program :: Maybe FilePath

    -- | Name of the entrypoint or on-chain view.
    -- Example: "myEp entrypoint", "myView on-chain view"
  , entrypointOrOnChainView :: Maybe Text

    -- | Storage value for contract.
  , storage :: Maybe Text

    -- | Parameter value for contract.
  , parameter :: Maybe Text

  , contractEnv :: Maybe MorleyContractEnv
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Buildable, FromJSON, ToJSON)

newtype MorleyLaunchRequest = MorleyLaunchRequest
  { unMorleyLaunchRequest :: LazyJson MorleyLaunchRequestBody
  } deriving stock (Eq, Show, Generic)
    deriving newtype (Buildable, FromJSON, ToJSON)

instance IsRequest MorleyLaunchRequest where
  type CommandFor MorleyLaunchRequest = "launch"
  type ResponseFor MorleyLaunchRequest = ()

data MorleyContractEnv = MorleyContractEnv
  { now :: Maybe $ MichelsonJson Timestamp
  , balance :: Maybe $ MichelsonJson Mutez
  , amount :: Maybe $ MichelsonJson Mutez
  , self :: Maybe ContractAddress
  , source :: Maybe L1Address
  , sender :: Maybe L1Address
  , chainId :: Maybe $ MichelsonJson ChainId
  , level :: Maybe $ MichelsonJson Natural
  , votingPowers :: Maybe VotingPowersConfig
  -- | Step limit to avoid running a contract forever.
  , instrLimit :: Maybe $ MichelsonJson Natural
  } deriving stock (Eq, Show, Generic)
    deriving anyclass (Buildable, FromJSON, ToJSON)

emptyMorleyContractEnv :: MorleyContractEnv
emptyMorleyContractEnv =
  MorleyContractEnv
    Nothing Nothing Nothing Nothing Nothing Nothing
    Nothing Nothing Nothing Nothing

data VotingPowersConfig
  = SimpleVotingPowers SimpleVotingPowersInfo
  deriving stock (Eq, Show, Generic)
  deriving anyclass Buildable

instance ToJSON VotingPowersConfig where
  toJSON = \case
    SimpleVotingPowers info -> object
      [ "kind" .= ("simple" :: Text)
      , "contents" .= info.contents
      ]

instance FromJSON VotingPowersConfig where
  parseJSON = withObject "voting powers distribution" \o -> do
    (o .: "kind") >>= \case
      ("simple" :: Text) -> SimpleVotingPowers <$> o .: "contents"
      other -> fail $ "Unknown kind of voting powers: " <> toString other

newtype SimpleVotingPowersInfo = SimpleVotingPowersInfo
  { contents :: Map (MichelsonJson KeyHash) (MichelsonJson Natural)
  } deriving stock (Eq, Show, Generic)
    deriving newtype (Buildable, FromJSON, ToJSON)

data MorleyParseValueRequest = MorleyParseValueRequest
  { value :: Text
  , category :: Text
  , onChainViewType :: Maybe Text
  , entrypointType :: Maybe Text
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Buildable, FromJSON, ToJSON)

data MorleyParseValueResponse
  = MorleyParsedValueSuccessfully
  | MorleyParseValueFailed Text

instance ToJSON MorleyParseValueResponse where
  toJSON v = case v of
    MorleyParsedValueSuccessfully -> toJSON ()
    MorleyParseValueFailed msg -> object [ "errorMessage" .= msg ]

instance IsRequest MorleyParseValueRequest where
  type CommandFor MorleyParseValueRequest = "parseValue"
  type ResponseFor MorleyParseValueRequest = MorleyParseValueResponse


data MorleyGetContractMetadataRequest = MorleyGetContractMetadataRequest
  { file :: FilePath

  -- | Directory where log files will be written
  , logDir :: Maybe String
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Buildable, FromJSON, ToJSON)

instance IsRequest MorleyGetContractMetadataRequest where
  type CommandFor MorleyGetContractMetadataRequest = "getContractMetadata"
  type ResponseFor MorleyGetContractMetadataRequest = ContractMetadata

-- Implementation of Morley-specific requests

instance HasSpecificMessages Morley where
  type LanguageServerStateExt Morley = LanguageServerState
  type InterpretSnapshotExt Morley = InterpretSnapshot
  type RestartFrameArgExt _ = FrameDeleteAmt

  launchHandler = morleyLaunchHandler
  handleStackTraceRequest = handleMorleyStackTrace
  handleScopesRequest = handleMorleyScopes
  handleVariablesRequest = handleMorleyVariables

  reportContractLogs = reportMorleyLogs @Morley
  reportErrorAndStoppedEvent = reportMorleyErrorAndStoppedEvent @Morley

  processStep = defaultProcessStep

morleyCustomHandlers :: DAP.HandlersSet (RIO Morley)
morleyCustomHandlers =
  [ getContractMetadataHandler
  , parseValueHandler
  ]

morleyHandlers :: StopAdapter -> DAP.HandlersSet (RIO Morley)
morleyHandlers stopAdapter =
  allDAPHandlers stopAdapter

  & (<> morleyCustomHandlers)

  -- add previous stack saving
  & ixHandler @DAP.StepInRequest %~ addSetPreviousStack
  & ixHandler @DAP.NextRequest %~ addSetPreviousStack
  & ixHandler @DAP.StepOutRequest %~ addSetPreviousStack
  & ixHandler @DAP.StepBackRequest %~ addSetPreviousStack
  where
    addSetPreviousStack = DAP.mapHandler' (handleMorleySetPreviousStack >>) id

getContractMetadataHandler :: DAP.Handler (RIO Morley)
getContractMetadataHandler = DAP.mkHandler \req@MorleyGetContractMetadataRequest{} ->
  modifyError inheritMessageField do
    let logFileMb = do
          dir <- req.logDir
          Just $ dir </> takeFileName req.file <.> "log"
    whenJust logFileMb openLogHandle

    lServVar <- asks _rcLSState
    lserver <- initLanguageServerState req.file
    do
      let contract = _lsContract lserver
      let U.ParameterType paramType _ = U.contractParameter contract
      let defaultEntry = U.UnsafeEpName ""
      let rootEntry = unsafe $ U.buildEpName "root"
      let onChainsFull = U.contractViews contract
            & U.unViewsSet
            & fmap U.viewArgument
      let entrypoints = U.mkEntrypointsMap U.WithoutImplicitDefaultEp (U.contractParameter contract)
      let entrypointsFull =
            -- If there are neither default nor root entrypoint,
            -- and there are other entrypoints, then we should add default entrypoint
            -- pointing to root type
            if M.notMember defaultEntry entrypoints
               && M.notMember rootEntry entrypoints
               && (  M.size entrypoints > 0
                  || M.size entrypoints == 0 && (not $ null onChainsFull)
                     -- ↑ We want to add a default entrypoint when there is on chains view.
                  ) then
              M.insert defaultEntry paramType entrypoints
            else entrypoints

      atomically $ writeTVar lServVar (Just lserver)
      ContractMetadata
        { parameterType = JsonFromBuildable (U.contractParameter contract)
        , storageType = JsonFromBuildable (U.contractStorage contract)
        , entrypoints = JsonFromBuildable <$> entrypointsFull
        , onChainViews = JsonFromBuildable <$> onChainsFull
        } `respondAndAlso` do
          logMessage $ fmt $ "Getting metadata for a contract " +| req.file |+ " completed"

parseValueHandler :: DAP.Handler (RIO Morley)
parseValueHandler = DAP.mkHandler \req@MorleyParseValueRequest{} -> modifyError inheritMessageField do
  lss <- readTVarIO =<< asks _rcLSState
  let result = do
        contract <- _lsContract <$> maybeToRight "Internal error: no LanguageServer instance" lss
        val <- first (toText . prettyFirstError) $ P.parseExpandValue P.MSUnspecified req.value
        case req.category of
          "parameter" ->
            let U.ParameterType param _ = U.contractParameter contract
             in
              case first P.ParserException . P.parseNoEnv @U.Ty P.type_ "" <$> req.onChainViewType of
                Nothing ->
                  case first P.ParserException . P.parseNoEnv @U.Ty P.type_ "" <$> req.entrypointType of
                    Nothing -> checkParam param val
                    Just (Right type') -> checkParam type' val
                    Just (Left e) -> Left $ toText $ prettyFirstError e
                Just (Right type') -> checkParam type' val
                Just (Left e) -> Left $ toText $ prettyFirstError e
          "storage" ->
            let store = U.contractStorage contract
             in typeCheckingForDebugger $ typeCheckStorage store val
          "address" -> checkValue @'TAddress val
          "contract" -> checkValue @'TAddress val  -- Typecheck contract as address since they have the same representation
          other -> Left $ "Internal error: unknown key prefix: " <> other
  logMessage $ fmt $ "Parsing value " +| req.value |+ " finished: " +| result |+ ""
  case result of
    Left err -> do
      -- Remove "Error occurred on line X char Y."
      respond $ MorleyParseValueFailed $
        T.dropWhileEnd isSpace $ fst $ T.breakOn "Error occurred on" err
    Right{} -> do
      respond MorleyParsedValueSuccessfully
  where
    checkParam :: U.Ty -> U.Value -> Either Text SomeValue
    checkParam = typeCheckingForDebugger ... typeCheckParameter mempty

    checkValue :: forall (t :: T). SingI t => U.Value -> Either Text SomeValue
    checkValue = bimap pretty T.SomeValue . typeCheckingForDebugger . typeCheckValue @t

-- | 'MorleyLaunchRequest' handler
-- This handler creates debugger state by parsing and typechecking of the contract,
-- typechecking of the parameter and storage values, and collecting all debugger snapshots.
-- If an error occurs, we report it to the plugin.
morleyLaunchHandler :: DAP.Handler (RIO Morley)
morleyLaunchHandler = DAP.mkHandler \req@MorleyLaunchRequest{} -> do
  initRes <- initDebuggerSession logMessage req
  lift $ lift do
    logMessage "Launching contract with arguments\n"
    logMessage $ pretty req <> "\n"
  asks _rcDAPState >>= \var -> atomically $ writeTVar var (Just initRes)
  respond ()

-- Name type used to distinguish between entrypoint and view
data NameType
  = EntrypointName U.EpName
  | ViewName U.ViewName

-- TODO [#101]: import from the library with new LTS
modifyError :: MonadError e' m => (e -> e') -> ExceptT e m a -> m a
modifyError f m = runExceptT m >>= either (throwError . f) pure

-- | Load contract, typecheck its code, parameter and values.
--
-- Returns 'DAP.DAPState' or DAP 'DAP.Message' describing the problem.
initDebuggerSession
  :: (MonadIO m, MonadError DAP.Error m)
  => (Text -> m ())
     -- ^ Logger to file
  -> MorleyLaunchRequest
  -> m (DAPSessionState InterpretSnapshot)
initDebuggerSession logger (MorleyLaunchRequest lazyArgs) = modifyError DAP.customError do
  req <-
    liftEither . first (mkErrorMessage "Error in launch.json configuration" . toText) $
    decodeLazy lazyArgs

  program <- checkArgument "program" req.program
  storageT <- checkArgument "storage" req.storage
  paramT <- checkArgument "parameter" req.parameter

  unlessM (liftIO $ doesFileExist program) $
    throwError $ mkErrorMessage "Contract file not found" $ toText program

  let src = P.MSFile program

  uContract <- liftIO (Utf8.readFile program) <&> parseExpandContract src >>=
    either (throwError . mkErrorMessage "Could not parse contract" . pretty) pure

  traverse_ (lift . logger . pretty) $ U.flattenExpandedOp $ U.contractCode uContract

  uParam <- P.parseExpandValue src (toText paramT) &
    either (throwError . mkErrorMessage "Could not parse parameter" . pretty) pure

  uStorage <- P.parseExpandValue src (toText storageT) &
    either (throwError . mkErrorMessage "Could not parse storage" . pretty) pure

  let entrypointOrOnChainView = T.splitOn "," <$> req.entrypointOrOnChainView
  entrypointOrOnChainViewResult <-
    case entrypointOrOnChainView of
      Just [epName, "entrypoint"] ->
        (EntrypointName <$> U.buildEpName epName)
          & either (throwError . mkErrorMessage "Could not parse entrypoint" . toText) pure
      Just [viewName, "on-chain view"] ->
        (ViewName <$> U.mkViewName viewName)
          & either (throwError . mkErrorMessage "Could not parse on-chain view" . pretty) pure
      _ -> pure $ EntrypointName U.DefEpName

  -- This do is purely for scoping, otherwise GHC trips up:
  --     • Couldn't match type ‘a0’ with ‘()’
  --         ‘a0’ is untouchable
  do
    SomeContract (contract@Contract{} :: Contract cp st) <-
      typeCheckContract uContract
      & typeCheckingForDebugger
      & either (throwError . mkErrorMessage "Could not typecheck contract") pure

    lift . logger $ pretty $ T.unContractCode $ cCode contract

    storage <- typeVerifyStorage @st uStorage
      & typeCheckingForDebugger
      & either (throwError . mkErrorMessage "Storage does not typecheck") pure

    let selfState = ContractState
          { csBalance = zeroMutez
          , csContract = contract
          , csStorage = storage
          , csDelegate = Nothing
          }

    contractEnv <-
      initContractEnv selfState (req.contractEnv ?: emptyMorleyContractEnv)

    his <- case entrypointOrOnChainViewResult of
      ViewName n -> do
        onChainView <- T.lookupView n (cViews contract) &
          maybe (throwError $ mkErrorMessage "On-chain view not found" $ pretty n) pure

        case onChainView of
          T.SomeView (typedView@T.View{} :: T.View' instr b st ret) -> do
            arg <- typeVerifyParameter @b mempty uParam
              & typeCheckingForDebugger
              & either (throwError . mkErrorMessage "View argument does not typecheck") pure
            pure $ collectInterpretSnapshotsForView (MSFile program) typedView arg storage contractEnv
      EntrypointName entrypoint -> do
        epcRes <- T.mkEntrypointCall entrypoint (cParamNotes contract) &
          maybe (throwError $ mkErrorMessage "Entrypoint not found" $ pretty entrypoint) pure
        case epcRes of
          T.MkEntrypointCallRes (_ :: T.Notes arg) epc -> do
            arg <- typeVerifyParameter @arg mempty uParam
              & typeCheckingForDebugger
              & either (throwError . mkErrorMessage "Parameter does not typecheck") pure
            pure $ collectInterpretSnapshots (MSFile program) contract epc arg storage contractEnv

    pure $ DAPSessionState
      (mkMorleyDebuggerState (MSFile program) his (cCode contract))
      mempty mempty

-- | Construct initial debugger state for Morley contract.
mkMorleyDebuggerState :: MichelsonSource -> InterpretHistory is -> ContractCode cp st -> DebuggerState is
mkMorleyDebuggerState source snapshots contr =
  let settings = def {T.dsGoToValues = True}
      checkLoc = \case
                    WithLoc cs _ -> S.singleton (toCanonicalLoc $ unErrorSrcPos cs)
                    _ -> mempty
      allLocs = T.dfsFoldInstr settings checkLoc $ T.unContractCode contr
      sources = M.singleton source $ evaluatingState 0 $ mkDebugSource (toList allLocs)
  in DebuggerState
    { _dsSnapshots = playInterpretHistory snapshots
    , _dsSources = sources
    }

initContractEnv
  :: (MonadIO m)
  => ContractState -> MorleyContractEnv -> m ContractEnv
initContractEnv selfState env = do
  ceNow <- liftIO $
    maybe (Timestamp <$> getPOSIXTime) (pure . unMichelsonJson)
    env.now

  let ceBalance = maybe [tz|1|] unMichelsonJson env.balance
  let ceAmount = maybe [tz|0|] unMichelsonJson env.amount

  let ceSelf =
        env.self
        ?: [ta|KT1XQcegsEtio9oGbLUHA8SKX4iZ2rpEXY9b|]
  let ceSource =
        env.source
        ?: Constrained [ta|tz1hTK4RYECTKcjp2dddQuRGUX5Lhse3kPNY|]
  let ceSender =
        env.sender
        ?: Constrained [ta|tz1hTK4RYECTKcjp2dddQuRGUX5Lhse3kPNY|]
  -- ↑ It's good to keep the default addresses in match with default
  -- custom configuration in package.json.

  let ceChainId = maybe dummyChainId unMichelsonJson env.chainId
  let ceLevel = maybe 10000 unMichelsonJson env.level

  ceVotingPowers <- case env.votingPowers of
    Nothing -> pure $ mkVotingPowers
      [ (unsafe $ parseHash "tz1aZcxeRT4DDZZkYcU3vuBaaBRtnxyTmQRr", 100)
      ]
    Just spec -> case spec of
      SimpleVotingPowers (SimpleVotingPowersInfo vps) ->
        pure $ mkVotingPowers $
          bimap unMichelsonJson unMichelsonJson <$> toPairs vps

  let ceErrorSrcPos = def
  let ceMinBlockTime = def
  let ceContracts addr = pure $ selfState <$ guard (addr == ceSelf)

  let ceMaxSteps =
        maybe 10000 (RemainingSteps . fromIntegral @Word @Word64 . naturalToWord . unMichelsonJson)
        env.instrLimit
  let ceOperationHash = Nothing
  return ContractEnv {ceMetaWrapper = id, ..}

checkArgument :: MonadError DAP.ErrorDetails m => Text -> Maybe a -> m a
checkArgument _ (Just a) = return a
checkArgument name Nothing = throwError $
  fromString . toString $ "Required configuration option \"" <> name <> "\" not found in launch.json."
