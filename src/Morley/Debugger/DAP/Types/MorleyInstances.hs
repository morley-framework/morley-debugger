-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Morley-specific requests
module Morley.Debugger.DAP.Types.MorleyInstances () where

import Morley.Debugger.DAP.Types.Morley
import Protocol.DAP.Mk (deriveMk)

-- These are extracted away because overwise they break record-dot-preprocessor.
-- With built-in support of RecordDotSyntax in GHC we may bring these declarations
-- back in module where the types listed below are declared.
concatMapM deriveMk
  [ ''ContractMetadata
  , ''MorleyLaunchRequestBody
  , ''MorleyContractEnv
  , ''MorleyParseValueRequest
  , ''MorleyGetContractMetadataRequest
  ]
