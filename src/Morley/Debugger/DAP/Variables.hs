-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE DuplicateRecordFields #-}

module Morley.Debugger.DAP.Variables
  ( createVariables
  , createVariables'

    -- * Utilities
  , VarsBuilder (..)
  , runVarsBuilder
  , insertVars
  ) where

import Control.Lens (ix, (%=), (<<%=))
import Data.Map.Strict qualified as M
import Data.Singletons (demote)
import "morley-prelude" Fmt (pretty)
import Named (defaults, paramF, (!))

import Morley.Michelson.Interpret (StkEl(..))
import Morley.Michelson.Text (mkMTextCut)
import Morley.Michelson.Typed
import Morley.Michelson.Untyped.Entrypoints (isDefEpName)

import Morley.Debugger.Core
import Protocol.DAP (Variable, VariableId)
import Protocol.DAP qualified as DAP

-- | For a given stack generate its representation as a tree of 'DAP.Variable's.
--
-- This creates a map @variable references -> [variable]@, and returns the index
-- of the root variables group. The bottom-most elment is the first element of the list with
-- the lowest cell .
createVariables :: ([SomeStackElem], Maybe (Int, Text)) -> (M.Map VariableId [DAP.Variable], VariableId)
createVariables = runVarsBuilder . createVariables'

-- | Version of 'createVariables' that returns 'VarsBuilder'.
--
-- Use it if single 'createVariables' call is insufficient and you need to add more
-- variables.
createVariables' :: ([SomeStackElem], Maybe (Int, Text)) -> VarsBuilder VariableId
createVariables' (st, rename) = do
  let cells = ["Cell " <> show i | i <- reverse [(0 :: Int) .. (length st - 1)]]
  let names = case rename of
        Nothing -> cells
        Just (idx, name) -> cells & ix idx .~ name
  topVars <- zipWithM
    (\(SomeStackElem (StkEl v)) name -> buildVariable v name)
    st
    names
  insertVars topVars

-- | Monad that allows registering groups of variables under specific
-- indices.
newtype VarsBuilder a = VarsBuilder
  { runVariableBuilder :: State (Int, M.Map VariableId [DAP.Variable]) a
  } deriving newtype (Functor, Applicative, Monad)

-- | Run the variables builder, producing the registered variables groups.
--
-- Accounting for the DAP interface, you have to run this at most once per
-- single snapshot visiting;
-- running this per scope or per stack frame would produce duplicated indices
-- across the different scopes or stack frames respectively and this is not
-- allowed.
runVarsBuilder :: VarsBuilder a -> (M.Map VariableId [DAP.Variable], a)
runVarsBuilder = first snd . swap . usingState (1, mempty) . runVariableBuilder

-- | Register a group of variables.
--
-- The resulting index is to be put to some @variablesReference@ field.
insertVars :: [Variable] -> VarsBuilder DAP.VariableId
insertVars vars = VarsBuilder do
  -- <<%= modifies state and returns previous value
  (DAP.VariableId -> nextIdx) <- _1 <<%= (+1)
  _2 %= M.insert nextIdx vars
  return nextIdx

buildVariable :: forall t. Value t -> Text -> VarsBuilder Variable
buildVariable v name = do
  let
    varText = pretty $ debugBuild DpmNormal v
    evaluatedText = pretty $ debugBuild DpmEvaluated v
    varType = withValueTypeSanity v $ pretty $ demote @t
    var = DAP.mk @DAP.Variable
      ! #name name
      ! #value varText
      ! #type varType
      ! paramF #__vscodeVariableMenuContext case v of
          VAddress  {} -> Just "address"
          VContract {} -> Just "contract"
          _            -> Nothing
      ! #evaluateName evaluatedText
      ! defaults

  subVars <- buildSubVars v

  case subVars of
    [] -> return var
    _ -> do
      idx <- insertVars subVars
      return $ updateVarIndex var idx

updateVarIndex :: Variable -> VariableId -> Variable
updateVarIndex DAP.Variable{..} idx = DAP.Variable { DAP.variablesReference = idx, .. }

buildSubVars :: Value t -> VarsBuilder [Variable]
buildSubVars = \case
  VOption Nothing -> return []
  VOption (Just v) -> (:[]) <$> buildVariable v "Some"
  VList lst ->
    zipWithM (\v i -> buildVariable v i) lst (show <$> [1 :: Int ..])
  VSet s ->
    zipWithM (\v i -> buildVariable v i) (toList s) (show <$> [1 :: Int ..])
  VPair (l, r) -> do
    lVar <- buildVariable l "first"
    rVar <- buildVariable r "second"
    return [lVar, rVar]
  VOr (Left v) -> (:[]) <$> buildVariable v "Left"
  VOr (Right v) -> (:[]) <$> buildVariable v "Right"
  VMap m ->
    mapM (\(k, v) -> buildVariable v $ pretty $
      debugBuild DpmNormal k) $ toPairs m
  VBigMap _id m ->
    mapM (\(k, v) -> buildVariable v $ pretty $
      debugBuild DpmNormal k) $ toPairs m
  VAddress (EpAddress addr ep) -> case isDefEpName ep of
    True -> return []
    False -> do
      addrVar <- buildVariable (VAddress $ EpAddress addr DefEpName) "address"
      epVar <- buildVariable (VString $ mkMTextCut $ pretty ep) "entrypoint"
      return [addrVar, epVar]
  -- Other value types do not have nested structure
  _ -> return []
