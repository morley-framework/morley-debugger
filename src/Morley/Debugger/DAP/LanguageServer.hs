-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | This module contains the functionality of the language server:
-- parse contracts, typecheck values, list contract entrypoints.
-- This functionality is used before the contract is running
-- to provide some additional features for the debugger
-- and better user experience.
module Morley.Debugger.DAP.LanguageServer
  ( LanguageServerState (..)
  , initLanguageServerState
  , prettyFirstError
  , Entrypoints
  , OnChainViews
  , JsonFromBuildable (..)
  )
where

import Control.Monad.Except (MonadError(..))
import Data.Aeson qualified as A
import Data.Text.IO.Utf8 qualified as Utf8
import "morley-prelude" Fmt (Buildable(..), pretty)
import Morley.Debugger.DAP.Types (mkErrorMessage)
import Morley.Michelson.Parser.Error (ParserException(..))
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Runtime (parseExpandContract)
import Morley.Michelson.Untyped qualified as U
import Protocol.DAP qualified as DAP
import System.Directory (doesFileExist)
import Text.Megaparsec.Error (ParseErrorBundle(bundleErrors), parseErrorTextPretty)

-- | Contains some information about the currently open contract.
data LanguageServerState = LanguageServerState
  { _lsContract :: U.Contract
  , _lsFile :: FilePath
  }

initLanguageServerState
  :: (MonadIO m, MonadError DAP.Error m)
  => FilePath -> m LanguageServerState
initLanguageServerState program = do
  let _lsFile = program

  unlessM (liftIO $ doesFileExist program) $
    throwError . DAP.customError $ mkErrorMessage "Contract file not found" $ toText program

  _lsContract <-
    liftIO (Utf8.readFile program) <&> parseExpandContract (MSFile program)
      >>= \case
      Left err -> throwError . DAP.customError $
        mkErrorMessage ("Could not parse contract " <> toText program) (pretty err)
      Right x -> pure x

  return LanguageServerState {..}

newtype JsonFromBuildable a = JsonFromBuildable a
  deriving newtype (Eq, Show)

type Entrypoints = Map U.EpName (JsonFromBuildable U.Ty)

type OnChainViews = Map U.ViewName (JsonFromBuildable U.Ty)

instance Buildable a => A.ToJSON (JsonFromBuildable a) where
  toJSON (JsonFromBuildable t) = A.String (pretty t)

instance Buildable a => Buildable (JsonFromBuildable a) where
  build (JsonFromBuildable a) = "JsonFromBuildable: " <> build a

-- | Pretty printer to show first error in the bundle
-- without location information.
-- It's supposed to be used in the morley-debugger
-- to show parse error for parameter and storage value.
prettyFirstError :: ParserException -> String
prettyFirstError (ParserException errorBundle) = case bundleErrors errorBundle of
  er :| _ -> parseErrorTextPretty er
