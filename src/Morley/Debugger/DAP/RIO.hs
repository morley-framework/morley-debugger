-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Morley.Debugger.DAP.RIO
  ( RioContext (..)
  , RIO
  , openLogHandle
  , logMessage
  , logMessageHandle
  , closeLogHandle
  ) where

import Data.ByteString.Char8 qualified as BS
import "morley-prelude" Fmt (pretty)
import System.IO (BufferMode(..), hSetBuffering)

import Protocol.DAP.Log qualified as DAP

import Morley.Debugger.DAP.Types (MonadRIO, RIO, RioContext(..))

{-# ANN module ("HLint: ignore Use 'WriteMode' from Universum" :: Text) #-}

openLogHandle :: MonadRIO ext m => String -> m ()
openLogHandle logFile = do
  closeLogHandle -- Just in case
  h <- openFile logFile WriteMode
  varHandle <- asks _rcHandle
  atomically $ writeTVar varHandle (Just h)
  liftIO $ hSetBuffering h LineBuffering

-- | Log message to log file if it exists
logMessage :: MonadRIO ext m => Text -> m ()
logMessage msg =
  asks _rcHandle >>= readTVarIO >>=
    liftIO . flip logMessageHandle msg

logMessageHandle :: Maybe Handle -> Text -> IO ()
logMessageHandle hMb msg = whenJust hMb $ \h -> BS.hPutStrLn h (encodeUtf8 msg)

-- | Close logger handle
closeLogHandle :: MonadRIO ext m => m ()
closeLogHandle = do
  handleVar <- asks _rcHandle
  handle <- readTVarIO handleVar
  atomically $ writeTVar handleVar Nothing
  whenJust handle hClose

instance {-# OVERLAPPING #-} DAP.MonadLog (RIO ext) where
  log = logMessage . pretty
