-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Types and utilities used for functioning of the DAP protocol and handling
-- of communication with VS Code.

module Morley.Debugger.DAP.Types
  ( mkErrorMessage
  , inheritMessageField
  , MichelsonJson(..)
  , LazyJson(..)
  , mkLazyJson
  , decodeLazy
  , StoppedReason(..)

  , HasSpecificMessages (..)
  , HasCustomStepGranularity
  , HasCustomRestartFrameArg
  , StopEventDesc(..)

  , DAPSessionState (..)
  , dsDebuggerState
  , dsVariables
  , dsPreviousStack

  , RioContext (..)
  , newRioContext
  , RIO
  , MonadRIO
  , resetDAPState
  , resetLanguageState

  , MonadHandler
  , MonadAfterResponse
  , StepCommand' (..)
  , StepCommand
  , stepCommandDirection
  , defaultProcessStep
  , cStackFrameRestartArg
  ) where

import Control.Concurrent.STM.TChan (newTChanIO)
import Control.Lens (Prism, makeLenses, prism)
import Control.Monad.Except (MonadError)
import Data.Aeson qualified as A
import Data.Aeson.Types qualified as A (Parser, toJSONKeyText)
import Data.Aeson.Types qualified as AI
import Data.Scientific qualified as Sci
import Data.Singletons (SingI)
import Data.Type.Bool (Not)
import Data.Type.Equality (type (==))
import "fmt" Fmt qualified as Fmt.Fmt
import "morley-prelude" Fmt (Buildable(..), pretty)

import Morley.Debugger.Core.Breakpoint
  (BreakpointSelector(..), continueUntilBreakpoint, reverseContinue)
import Morley.Debugger.Core.Getters
import Morley.Debugger.Core.Navigate
  (DebuggerState, Direction(..), FrameDeleteAmt, HistoryReplay, MovementResult, NavigableSnapshot,
  NavigableSnapshotWithMethods, PausedReason(..), TraversedSnapshots, move, moveOutsideMethod,
  moveSkippingMethods, moveToStartFrame)
import Morley.Debugger.Core.Snapshots (DebuggerFailure, FinalStack(..))
import Morley.Michelson.Interpret (MichelsonFailureWithStack)
import Morley.Michelson.Runtime.Import (readValue)
import Morley.Michelson.Typed (IsoValue, fromVal)
import Morley.Michelson.Untyped qualified as U
import Protocol.DAP qualified as DAP
import Protocol.DAP.Util (AnyType(..))

-- Helpers
----------------------------------------------------------------------------

mkErrorMessage
  :: Text -- ^ Message prefix
  -> Text -- ^ Error message text
  -> DAP.ErrorDetails
mkErrorMessage prefix msg = Fmt.Fmt.pretty (prefix <> ": " <> msg)

-- | Fill the @message@ field of response with @format@ field from
-- 'DAP.ErrorDetails'.
--
-- Note that this operation is unsafe - @format@ field may contain placeholders
-- which @message@ field should not have.
--
-- Grep for @[error handling for custom DAP messages]@ in the TypeScript code
-- for details on why this function is needed.
inheritMessageField :: DAP.Error -> DAP.Error
inheritMessageField err@DAP.Error{..} =
  DAP.Error{ DAP.message = do b <- err.body; pure b.format, .. }

-- | Modifies JSON instance to parse a value from Michelson representation.
newtype MichelsonJson a = MichelsonJson { unMichelsonJson :: a }
  deriving stock (Show, Eq, Ord)
  deriving newtype Buildable

parseMichelsonValue :: IsoValue a => Text -> A.Parser (MichelsonJson a)
parseMichelsonValue txt = do
  val <- either (fail . pretty) pure $
    readValue "debugger input" txt
    `orAlsoTry`
    readValue "debugger input" ("\"" <> txt <> "\"")
    -- ↑ more convenient for addresses and strings

  return $ MichelsonJson (fromVal val)
  where
    orAlsoTry :: Either e x -> Either e x -> Either e x
    orAlsoTry r1 r2 =
      case r1 of
        Right x -> Right x
        Left e -> case r2 of
          Right x -> Right x
          Left _ -> Left e

instance IsoValue a => A.FromJSON (MichelsonJson a) where
  parseJSON v = do
    txt <- case v of
      A.String txt ->
        pure txt
      A.Number num |
       Sci.isInteger num -> pure $ show (round num :: Integer)
      _ -> fail "Expected string or integer value"
    parseMichelsonValue txt

instance IsoValue a => A.FromJSONKey (MichelsonJson a) where
  fromJSONKey = A.FromJSONKeyTextParser parseMichelsonValue

instance A.ToJSON (MichelsonJson a) where
  toJSON = error "not implemented"
  -- ↑ has to be provided due to how deriveSum works

instance IsoValue a => A.ToJSONKey (MichelsonJson a)

-- | Encoding/decoding a JSON lazily.
--
-- This can be used to e.g. provide custom error message.
newtype LazyJson a = LazyJson { unLazyJson :: A.Value }
  deriving stock (Show, Eq, Generic)
  deriving newtype (A.FromJSON, A.ToJSON)

mkLazyJson :: A.ToJSON a => a -> LazyJson a
mkLazyJson = LazyJson . A.toJSON

decodeLazy :: A.FromJSON a => LazyJson a -> Either String a
decodeLazy (LazyJson v) =
  -- We use some internal capabilities of aeson, but this should become
  -- unnecessary after <https://github.com/haskell/aeson/issues/852> is resolved.
  case AI.ifromJSON v of
    AI.IError path err -> Left $ AI.formatError path err
    AI.ISuccess x -> Right x

-- | Used to indicate why a stopped event should be emitted.
data StoppedReason p
  = ExceptionMet (MichelsonFailureWithStack DebuggerFailure)
  -- ^ Interpreter has met a Michelson exception and must stop.
  | Paused (PausedReason p) Text
  -- ^ Interpreter stopped due to an ordinary reason, should as step or breakpoint.
  | TerminatedOkMet FinalStack
  -- ^ Interpreter has met the @terminated ok@ event.
  | PastFinish
  -- ^ Interpreter has tried to advance past the end.
  | ReachedStart
  -- ^ Interpreter has reached the start when stepping backwards.

----------------------------------------------------------------------------
-- Messages
----------------------------------------------------------------------------

instance A.FromJSONKey U.EpName where
    fromJSONKey = A.FromJSONKeyText $ \case
      "default" -> U.UnsafeEpName ""
      s -> U.UnsafeEpName s

instance A.ToJSONKey U.EpName where
  toJSONKey = A.toJSONKeyText (\case
    U.UnsafeEpName "" -> "default"
    U.UnsafeEpName e -> e)

-- | Description of stop event, this should elaborate on why have we stopped.
--
-- It will appear in the @CALL STACK@ pane at the top-right corner.
--
-- This should be given in past tense, preferably start from lower-case letter,
-- be short and of fixed length to fit into UI.
--
-- Good: @faced statement@, @entered method@.
-- Bad: @facing statement@, @statement began@, @entered method <method_name>@.
--
-- This should not provide information about whether have we started / completed
-- execution, it will be attached elsewhere (via 'StoppedReason').
--
-- Good: @faced statement@
-- Bad: @started, faced statement@; @faced first statement@.
--
-- This is not a datatype, but you can create reusable values using pattern
-- synonyms, e.g. if you need to check stop events in tests.
newtype StopEventDesc = StopEventDesc { unStopEventDesc :: Text }
  deriving stock (Show, Eq)

-- | The class that allows the debugger to handle other types of requests that
-- are specific for each language. See 'LaunchRequest', 'ExtraRequest',
-- 'ExtraEvent' and 'ExtraResponse'.
class ( NavigableSnapshot (InterpretSnapshotExt ext)
      , Buildable (InterpretSnapshotExt ext)
      , Buildable (StopEventExt ext)
      , SingI (HasCustomStepGranularity ext)
      , SingI (HasCustomRestartFrameArg ext)
      ) =>
      HasSpecificMessages ext where
  -- | Extra constructors for extraneous requests.
  type LanguageServerStateExt ext
  -- | Interpreter snapshot type.
  type InterpretSnapshotExt ext
  -- | The type that describes why have we stopped at the current position.
  --
  -- Its 'Buildable' instance will be used to provide elaborated details in UI
  -- (in @Call Stack@ pane, the label bumping on hover at the top-right corner).
  --
  -- Unlike 'StopEventDesc', this can contain arbitrary details,
  -- e.g. method name or rendered exception can be mentioned here.
  -- But this should not provide information about whether have we
  -- started / completed execution, similarly to 'StopEventDesc'.
  type StopEventExt ext
  type StopEventExt _ = Void
  -- | Desired granularity of a step.
  --
  -- DAP already hardcodes a set of allowed granularities, but those seem to be
  -- often not enough.
  type StepGranularityExt ext
  type StepGranularityExt _ = DefaultStepGranularity

  type RestartFrameArgExt ext
  type RestartFrameArgExt _ = DefaultRestartFrameArg

  type PausedReasonExt ext
  type PausedReasonExt _ = Void

  -- | The handler for the debugger launch.
  --
  -- The point is that you can declare an extension of 'DAP.LaunchRequest' type
  -- with extra fields and handle this type here.
  launchHandler :: DAP.Handler (RIO ext)

  -- | Helper function to report any logs that may occur during contract execution.
  --
  -- These are /not/ logs produced internally by the debug adapter, but rather
  -- the output of a 'trace'-like function in a Tezos contract.
  reportContractLogs
    :: MonadAfterResponse ext m
    => DAPSessionState (InterpretSnapshotExt ext)
    -> TraversedSnapshots (InterpretSnapshotExt ext)
    -> m ()

  -- | Helper function to stop events including the exception.
  reportErrorAndStoppedEvent
    :: MonadAfterResponse ext m
    => DAPSessionState (InterpretSnapshotExt ext)
    -> StoppedReason (PausedReasonExt ext)
    -> m ()

  -- | Elaboration on why have we stopped at the current snapshot.
  getStopEventInfo
    :: Proxy ext
    -> MonadReader (DebuggerState (InterpretSnapshotExt ext)) m
    => m (Maybe StopEventDesc, Maybe (StopEventExt ext))
  getStopEventInfo _ = pure (Nothing, Nothing)

  handleStackTraceRequest :: DAP.HandlerBody DAP.StackTraceRequest (RIO ext)

  handleScopesRequest :: DAP.HandlerBody DAP.ScopesRequest (RIO ext)
  handleVariablesRequest :: DAP.HandlerBody DAP.VariablesRequest (RIO ext)

  -- | For some of @Step@-like request - how to parse the provided (optional)
  -- granularity field in a stepping request.
  parseStepGranularity :: Maybe Text -> Either Text (StepGranularityExt ext)
  default parseStepGranularity
    :: (StepGranularityExt ext ~ ())
    => Maybe Text -> Either Text (StepGranularityExt ext)
  parseStepGranularity = \case
    Nothing -> Right ()
    Just gran -> Left $
      "No custom value is allowed, but '" <> pretty gran <> "' was passed"

  -- | Perform a step.
  processStep
    :: (HistoryReplay (InterpretSnapshotExt ext) m)
    => StepCommand (StepGranularityExt ext)
    -> m (MovementResult (PausedReason (PausedReasonExt ext)))

  -- | This will wrap the handlers execution and can be used, for instance,
  -- to provide exceptions handling logic.
  --
  -- You are provided with some basic information about the request.
  --
  -- Ideally this function should never throw exceptions, rather they have to
  -- be reported to the client (see 'Morley.Debugger.DAP.Handlers.reportFailure' for example).
  -- Even if the error is fatal, the backend should not crash. For a graceful
  -- exit, send an error response and then a terminate event, the client will
  -- stop this debug adapter on itself.
  -- (if this information turns out to be wrong or you add helpers for the
  -- mentioned scenarios, please update this documentation)
  handlersWrapper :: () -> RIO ext () -> RIO ext ()
  handlersWrapper _ = id

type DefaultStepGranularity = ()
type HasCustomStepGranularity ext =
  Not (StepGranularityExt ext == DefaultStepGranularity)

type DefaultRestartFrameArg = NoStackFrameRestartArg
type HasCustomRestartFrameArg ext =
  Not (RestartFrameArgExt ext == DefaultRestartFrameArg)

data DAPSessionState is = DAPSessionState
  { _dsDebuggerState :: DebuggerState is
  , _dsPreviousStack :: [SomeStackElem] -- Needed to calculate stackDiff
  , _dsVariables :: Map DAP.VariableId [DAP.Variable]
  }
  deriving stock (Generic)

data RioContext ext = RioContext
  { _rcHandle :: TVar (Maybe Handle)
  -- ^ Log file handle

  , _rcLSState :: TVar (Maybe (LanguageServerStateExt ext))
  -- ^ Current language server state

  , _rcDAPState :: TVar (Maybe (DAPSessionState (InterpretSnapshotExt ext)))
  -- ^ Current debugger session state
  }

newRioContext :: IO (RioContext ext)
newRioContext = do
  _rcHandle <- newTVarIO Nothing
  _rcLSState <- newTVarIO Nothing
  _rcDAPState <- newTVarIO Nothing
  _rcOutputChannel <- liftIO newTChanIO
  return RioContext{..}

type RIO ext = ReaderT (RioContext ext) IO

type MonadRIO ext m = (MonadIO m, MonadReader (RioContext ext) m)

type MonadHandler ext m =
  ( MonadAfterResponse ext m
  , MonadError DAP.Error m
  )

type MonadAfterResponse ext m =
  ( MonadReader (RioContext ext) m
  , DAP.MonadEventSubmit m
  , MonadIO m
  )

-- | Command to make one of the steps specified by DAP.
data StepCommand' restartFrameArg granularity
  = CNext Direction granularity
    -- ^ aka Step Over
  | CStepIn granularity
  | CStepOut granularity
  | CContinue Direction
  | CRestartFrame restartFrameArg
  deriving stock (Functor, Foldable, Traversable)

type StepCommand = StepCommand' FrameDeleteAmt

resetDAPState :: MonadRIO ext m => m ()
resetDAPState =
  asks _rcDAPState >>= \var -> atomically $ writeTVar var Nothing

resetLanguageState :: MonadRIO ext m => m ()
resetLanguageState =
  asks _rcLSState >>= \var -> atomically $ writeTVar var Nothing

-- | This datatype can be used as type argument to 'StepCommand\'' to forbid 'CRestartFrame' command.
data NoStackFrameRestartArg

-- | A prism to access or modify the argument of the 'CRestartFrame' command.
--
-- May be helpful to switch to a different argument of 'CRestartFrame'.
-- Example: if a handler does not support 'CRestartFrame', add handling for it:
--
-- @@@
-- processStep :: StepCommand' Int gran -> m ()
-- processStep cmd = case cStackFrameRestartArg `matching` cmd of
--   -- Here we got @StepCommand' NoStackFrameRestartArg gran@
--   Right cmd' -> processStepWithoutRestartSupport cmd'
--   -- And here we got the argument of 'CRestartFrame'
--   Left arg -> moveToStartFrame arg
-- @@@
cStackFrameRestartArg :: Prism (StepCommand' arg1 gran) (StepCommand' arg2 gran) arg1 arg2
cStackFrameRestartArg = prism
  do \arg -> CRestartFrame arg
  do \(step :: StepCommand' arg gran) ->
        case step of
          CNext dir gran -> Left (CNext dir gran)
          CStepIn gran -> Left (CStepIn gran)
          CStepOut gran -> Left (CStepOut gran)
          CContinue dir -> Left (CContinue dir)
          CRestartFrame arg -> Right arg


stepCommandDirection :: StepCommand g -> Direction
stepCommandDirection = \case
  CNext dir _ -> dir
  CStepIn _ -> Forward
  CStepOut _ -> Forward
  CContinue dir -> dir
  CRestartFrame _ -> Backward

defaultProcessStep
  :: (HistoryReplay is m, NavigableSnapshotWithMethods is)
  => StepCommand DefaultStepGranularity -> m (MovementResult (PausedReason p))
defaultProcessStep = \case
  CNext dir () -> const PlainPaused <<$>> moveSkippingMethods dir
  CStepIn () -> const PlainPaused <<$>> move Forward
  CStepOut () -> const PlainPaused <<$>> moveOutsideMethod
  CContinue Forward -> continueUntilBreakpoint NextBreak
  CContinue Backward -> reverseContinue NextBreak
  CRestartFrame amt -> const PlainPaused <<$>> moveToStartFrame amt

makeLenses ''DAPSessionState

-- Buidable instances
----------------------------------------------------------------------------

instance Buildable (LazyJson a) where
  build (LazyJson v) = "Lazy json: " <> build (AnyType v)
