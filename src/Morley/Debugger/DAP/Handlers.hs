-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

-- | Handlers of DAP protocol requests to response
-- to VS plugin.
-- The flow can be found here: https://microsoft.github.io/debug-adapter-protocol/overview
-- The TS-side types can be found here: https://microsoft.github.io/debug-adapter-protocol/specification

module Morley.Debugger.DAP.Handlers
  ( VSCodeSrcLoc (..)
  , toVSCodeLoc
  , fromVSCodeLoc
  , safenHandler
  , allDAPHandlers
  , updateDAPSessionState
  , readDAPSessionState
  , updateSomeDebuggerState
  , readSomeDebuggerState
  , threadId
  , handleStackTraceDefault
  , fromMichelsonSource

    -- * General handlers
  , initializeHandler
  , configurationDoneHandler
  , terminateHandler
  , disconnectHandler
  , stateRequestHandlers
  , stepHandlers
  , setBreakpointsHandler
  , evaluateRequestHandlerDummy

    -- * Morley-specific
  , supportedCapabilities
  , handleMorleyScopes
  , handleMorleyVariables
  , handleMorleyStackTrace
  , handleMorleySetPreviousStack
  , reportMorleyLogs
  , reportMorleyErrorAndStoppedEvent
  ) where

import Prelude hiding (id)
import Unsafe qualified

import Control.Lens (Magnify(magnify), ix, zoom, (.=))
import Control.Monad.Except (MonadError, throwError)
import Data.Singletons (demote)
import Data.Vinyl (Rec(..))
import "morley-prelude" Fmt (Doc, blockListF, build, fmt, pretty)
import Named (defaults, (!))
import System.FilePath (takeFileName)

import Data.Default (def)
import Morley.Debugger.Core
import Morley.Debugger.DAP.RIO (closeLogHandle, logMessage)
import Morley.Debugger.DAP.Types
import Morley.Debugger.DAP.Variables (createVariables)
import Morley.Debugger.Protocol.Instances ()
import Morley.Michelson.Interpret
  (MichelsonFailureWithStack, StkEl(..), buildMorleyLogs, unMorleyLogs)
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Printer.Util (RenderDoc(..), doesntNeedParens)
import Morley.Michelson.Typed qualified as T
import Protocol.DAP (mkHandler, respond, respondAndAlso, submitEvent)
import Protocol.DAP qualified as DAP
import Protocol.DAP.Log (MonadLog, log)
import Protocol.DAP.Serve.IO (StopAdapter(..))

-- | Source locations as per VSCode.
data VSCodeSrcLoc = VSCodeSrcLoc
  { vsLine :: Word
    -- ^ 1-indexed line number.
  , vsCol :: Word
    -- ^ 1-indexed column number.
  }

toVSCodeLoc :: SrcLoc -> VSCodeSrcLoc
toVSCodeLoc (SrcLoc l c) = VSCodeSrcLoc (l + 1) (c + 1)

-- This is partial, so no 'IsSourceLoc' instance.
fromVSCodeLoc :: VSCodeSrcLoc -> Maybe SrcLoc
fromVSCodeLoc (VSCodeSrcLoc l c) =
  let safePred a = guard (a > 0) $> (a - 1)
  in SrcLoc <$> safePred l <*> safePred c

-- | The ID of the thread in execution. There is only one thread and thus this
-- will always be 1.
threadId :: DAP.ThreadId
threadId = DAP.ThreadId 1

-- | Supported by the debugger capabilities.
supportedCapabilities
  :: forall ext. (HasSpecificMessages ext)
  => DAP.Capabilities
supportedCapabilities = def
  { DAP.supportsConfigurationDoneRequest = Just True
  , DAP.supportsTerminateRequest = Just True
  , DAP.supportsStepBack = Just True
  , DAP.supportsSteppingGranularity =
      Just $ demote @(HasCustomStepGranularity ext)
  , DAP.supportsRestartFrame =
      Just $ demote @(HasCustomRestartFrameArg ext)
  }

-- Note: with `MonadSTM`, when applied to some 'IO'-based monad, we will call
-- 'atomically' implicitly over each action, meaning that multiple actions
-- won't be all atomic together.
-- But this is fine since we don't care about thread-safety - thanks to
-- the queues, we already perform sequentially.

updateSomeDebuggerState
  :: (MonadReader (RioContext ext) m, MonadError DAP.Error m, MonadIO m)
  => Text
  -> (RioContext ext -> TVar (Maybe s))
  -> StateT s m a
  -> m a
updateSomeDebuggerState desc getter action = do
  var <- asks getter
  oldState <- readTVarIO var >>= \case
    Nothing -> throwError $ DAP.inappropriateDapState desc
    Just x -> pure x
  (res, newState) <- usingStateT oldState action
  atomically $ writeTVar var (Just newState)
  return res

readSomeDebuggerState
  :: (MonadReader (RioContext ext) m, MonadError DAP.Error m, MonadIO m)
  => Text
  -> (RioContext ext -> TVar (Maybe v))
  -> m v
readSomeDebuggerState desc getter = do
  var <- asks getter
  readTVarIO var >>= \case
    Nothing -> throwError $ DAP.inappropriateDapState desc
    Just x -> pure x

updateDAPSessionState
  :: (MonadReader (RioContext ext) m, MonadError DAP.Error m, MonadIO m)
  => StateT (DAPSessionState (InterpretSnapshotExt ext)) m a
  -> m a
updateDAPSessionState =
  updateSomeDebuggerState "DAP session state is uninitialized" _rcDAPState

readDAPSessionState
  :: (MonadReader (RioContext ext) m, MonadError DAP.Error m, MonadIO m)
  => m (DAPSessionState (InterpretSnapshotExt ext))
readDAPSessionState =
  readSomeDebuggerState "DAP session state is uninitialized" _rcDAPState

-- | Catch all unexpected exceptions in a handler.
safenHandler :: (MonadCatch m, MonadLog m) => DAP.Handler m -> DAP.Handler m
safenHandler = DAP.mapHandler'
  do handleAny \e -> throwError $ DAP.Error
      { message = Just "Internal error in debug adapter occured"
      , body = Just "Unexpected error in handler: {err}"
          { DAP.id = 10123
          , DAP.variables = one ("err", pretty $ displayException e)
          , DAP.showUser = False
          }
      }
  do handleAny \e ->
      log . fmt $
        "Unexpected exception in handler after sending response: " <>
        (build $ displayException e)

allDAPHandlers :: HasSpecificMessages ext => StopAdapter -> [DAP.Handler (RIO ext)]
allDAPHandlers stopAdapter = concat
  [ [ initializeHandler
    , launchHandler
    , configurationDoneHandler
    , terminateHandler
    , disconnectHandler stopAdapter
    ]
  , stateRequestHandlers
  , stepHandlers
  , [setBreakpointsHandler]
  , otherHandlers
  ]

-- | 'InitializeRequest' handler.
-- Response describes which functionality the debug adapter supports.
-- For now we support the following:
-- * ConfigurationDone request
-- * Terminate request
-- * StepBack request to traverse contract code in backward direction
initializeHandler
  :: forall ext. HasSpecificMessages ext
  => DAP.Handler (RIO ext)
initializeHandler = mkHandler \DAP.InitializeRequest{} -> do
  Just (supportedCapabilities @ext) `respondAndAlso`
    submitEvent DAP.InitializedEvent

-- | 'SetBreakpointsRequest' handler.
-- Set breakpoints at the position specified in the request
-- or next available instruction if the position is on empty line,
-- or doesn't correspond to an instruction.
--
-- If user wants to set a breakpoint after the last instruction,
-- we will just return specified position
-- but don't actually set a breakpoint somewhere.
setBreakpointsHandler :: DAP.Handler (RIO ext)
setBreakpointsHandler = mkHandler \req@DAP.SetBreakpointsRequest{} -> updateDAPSessionState do
  DAPSessionState{..} <- get
  let
    pathSource = MSFile $ req.source.path ?: error "No filepath in source"
    toSrcPos srcBreakpoint = fmap
      (pointSourceLocation pathSource) $
      fromVSCodeLoc (VSCodeSrcLoc srcBreakpoint.line (srcBreakpoint.column ?: 1))
  let (breakpoints, newDebSt) = usingState _dsDebuggerState $ do
        let sourceBreaks = req.breakpoints ?: []
        resetBreakpoints pathSource
        forM sourceBreaks $ \sourceBreak -> do
          let dummyPos = (Nothing, Nothing, Nothing)
          (l, c, bId) <- flip (maybe (pure dummyPos)) (toSrcPos sourceBreak) $
            \(SourceLocation _ initPos _) -> switchBreakpoint pathSource initPos >>= \case
              -- Ideally, we could response with success: 'false', or even better,
              -- response with 'ErrorResponse', to notify about invalid place for a breakpoint
              -- but such malformed breakpoint will be displayed on VS Code UI anyway (weird),
              -- as well as being sent every time in 'SetBreakpoints' and preventing new valid
              -- breakpoints to be set. So let's just pretend we set a breakpoint,
              -- but actually it won't be ever triggered.
              Nothing -> pure (Just $ sourceBreak.line, sourceBreak.column, Nothing)
              Just (toVSCodeLoc -> VSCodeSrcLoc l c, bId) -> pure (Just l, Just c, bId)
          pure $
            DAP.Breakpoint
            { DAP.id = bId
            , DAP.verified = True
            , DAP.source   = Just req.source
            , DAP.line     = l
            , DAP.column   = c
            , DAP.endLine = Nothing
            , DAP.endColumn = Nothing
            , DAP.message = Nothing
            , DAP.instructionReference = Nothing
            , DAP.offset = Nothing
            }

  dsDebuggerState .= newDebSt
  respond DAP.SetBreakpointsResponse
    { DAP.breakpoints = breakpoints
    }

-- | 'ConfigurationDoneRequest' handler.
-- Response with meta information before actual execution of the contract.
-- All preparatory steps have to be done before this step (including setting breakpoints).
configurationDoneHandler :: MonadIO m => DAP.Handler m
configurationDoneHandler = mkHandler \DAP.ConfigurationDoneRequest -> do
  () `respondAndAlso` do
    -- We always stop our contract immediately after launch, i.e. before the first instruction.
    submitEvent DAP.StoppedEvent
      { DAP.reason = "entry"
      , DAP.description = Nothing
      , DAP.threadId = Just threadId
      , DAP.allThreadsStopped = Just True
      , DAP.text = Just "Entering the contract"
      , DAP.preserveFocusHint = Nothing
      , DAP.hitBreakpointIds = Nothing
      }

-- | Handlers for basic requests related to fetching the current state:
-- stack trace, variables, e.t.c.
stateRequestHandlers
  :: forall ext. (HasSpecificMessages ext) => [DAP.Handler (RIO ext)]
stateRequestHandlers =
  [ threadsRequestHandler
  , mkHandler handleStackTraceRequest
  , mkHandler handleScopesRequest
  , mkHandler handleVariablesRequest
  ]

-- | 'ThreadsRequest' handler.
-- We have only one thread in our contract, so the response is dummy.
threadsRequestHandler :: Monad m => DAP.Handler m
threadsRequestHandler = mkHandler \DAP.ThreadsRequest -> do
  -- We have only one thread in our debugger, so if VSCode asks, we always report it.
  respond DAP.ThreadsResponse
    { DAP.threads = [DAP.Thread threadId "contract"] }

handleMorleySetPreviousStack
  :: (InterpretSnapshotExt ext ~ InterpretSnapshot, MonadHandler ext m)
  => m ()
handleMorleySetPreviousStack =
  updateDAPSessionState do
    -- We store the previous stack for diff.
    DAPSessionState{..} <- get
    dsPreviousStack .= stackElemsFromDebuggerState _dsDebuggerState

-- | 'StackTraceRequest' handler.
-- Response contains name, source and position of the stack frames. New stack
-- frames may be created in nested instructions such as DIP.
handleMorleyStackTrace
  :: (HasSpecificMessages ext, InterpretSnapshotExt ext ~ InterpretSnapshot)
  => DAP.HandlerBody DAP.StackTraceRequest (RIO ext)
handleMorleyStackTrace = handleStackTraceDefault
  \snap ->
    flip mapMaybe (zip [1..] $ issLastStackFrames snap) \(i, StackFrame{..}) -> do
      SourceLocation file startPos endPos <- _sfPosition
      let
        instructionPointerReference = Nothing
        moduleId = Nothing
        presentationHint = Nothing
        VSCodeSrcLoc startRow startCol = toVSCodeLoc startPos
        VSCodeSrcLoc endRow endCol = toVSCodeLoc endPos
        id = DAP.StackFrameId i
        name = _sfName
        source = Just $ fromMichelsonSource file
        canRestart = Just True
      pure $ case pickSnapshotEdgeStatus snap of
        SnapshotAtEnd (SnapshotEndedWithOk _) ->
          -- When reaching the last instruction, we want the marker to highlight the whole line.
          -- @endRow@ is used since we want to highlight the last line.
          let
              line = endRow
              column = 0
              endLine = Just endRow
              endColumn = Just 0
          in DAP.StackFrame { .. }
        _ ->
          let line = startRow
              column = startCol
              endLine = Just endRow
              endColumn = Just endCol
          in DAP.StackFrame { .. }

-- | Default generic implementation for stack trace handler.
--
-- This tries to look at the next snapshot (or picks the current one if it is
-- the last one) and takes the stack trace from that snapshot using the provided
-- getter.
handleStackTraceDefault
  :: (HasSpecificMessages ext)
  => (InterpretSnapshotExt ext -> [DAP.StackFrame])
     -- ^ Fetch the call stack at the given snapshot.
     --
     -- The first argument is the (only) executed source file.
     -- TODO [#56]: strip this argument
  -> DAP.HandlerBody DAP.StackTraceRequest (RIO ext)
handleStackTraceDefault toDAPFrames _req@DAP.StackTraceRequest{} = do
  DAPSessionState{..} <- readDAPSessionState
  -- A stop in the debugger should mean position /before/ the next instruction, so
  -- we try to look at the next snapshot and report its location.
  --
  -- Current stack will correspond to the state before its execution.
  case runReader ((,) <$> nextSnapshot <*> curSnapshot) _dsDebuggerState of
    (Nothing, cur@(pickSnapshotEdgeStatus -> SnapshotAtEnd (SnapshotEndedWithFail _))) ->
      -- If we ended up with failure instruction we should report
      -- not empty stackframe, but current one and current stack
      -- to make exit more graceful.
      handleSnapshot cur
    (Nothing, _) -> do
      respond DAP.StackTraceResponse
        { DAP.stackFrames = []
        , DAP.totalFrames = Just 0
        }
    (Just snapshot, _) -> handleSnapshot snapshot
  where
    handleSnapshot nextSnap = do
      let frames = toDAPFrames nextSnap

      respond DAP.StackTraceResponse
        { DAP.stackFrames = frames
        , DAP.totalFrames = Just $ Unsafe.fromIntegral @Int @Word $ length frames
        }


-- | 'ScopesRequest' handler.
-- We don't have actual variable, instead we represent stack cells as variables.
-- In this handler we create a 'DAP.Variable' for every cell building its textual representation.
handleMorleyScopes
  :: (InterpretSnapshotExt ext ~ InterpretSnapshot)
  => DAP.HandlerBody DAP.ScopesRequest (RIO ext)
handleMorleyScopes _req@DAP.ScopesRequest{} = do
  DAPSessionState{..} <- readDAPSessionState
  let stack = first stackToElems $ issFullStack $ tapeFocus $ _dsSnapshots _dsDebuggerState
  let (vars, varGroupIdx) = createVariables stack
  -- Lazily calculate tree of variables for the current scope and store it
  -- for subsequent "variables" request.
  updateDAPSessionState $ dsVariables .= vars
  let stackScope = DAP.mk @DAP.Scope
        ! #name "stack"
        ! #variablesReference varGroupIdx
        ! defaults
  let stackDiffScope = DAP.mk @DAP.Scope
        ! #name "stack diff"
        ! #variablesReference (DAP.VariableId 1000)
        ! defaults
  respond DAP.ScopesResponse
    { DAP.scopes = [stackScope, stackDiffScope] }

-- | 'VariablesRequest' handler.
-- Response with 'DAP.Variable' by its id reference.
handleMorleyVariables
  :: (InterpretSnapshotExt ext ~ InterpretSnapshot)
  => DAP.HandlerBody DAP.VariablesRequest (RIO ext)
handleMorleyVariables req@DAP.VariablesRequest{} = do
  let ref = req.variablesReference
  vars <- _dsVariables <$> readDAPSessionState
  case vars ^? ix ref of
    Nothing -> do
      let buildDiffVar (i, stackDiffType) =
            let
              (label, val) = case stackDiffType of
                StackDiffAdd v -> ("Added", pretty (fmt @Text $ debugBuild DpmNormal v))
                StackDiffRemove v -> ("Removed", pretty (fmt @Text $ debugBuild DpmNormal v))
                StackDiffModify o n ->
                  ( "Modified"
                  , pretty ((fmt @Text $ debugBuild DpmNormal o)
                      <> " -> " <> (fmt @Text $ debugBuild DpmNormal n)
                    )
                  )
            in DAP.mk @DAP.Variable
              ! #name (label <> " #" <> pretty i)
              ! #value val
              ! defaults

      DAPSessionState{..} <- readDAPSessionState

      let stackDiffs = getStackDiff _dsPreviousStack
            (stackElemsFromDebuggerState _dsDebuggerState)

      if null _dsPreviousStack then
        -- We don't want to display stack diff on the first initial request.
        respond $ DAP.VariablesResponse []
      else if (ref == diffScopeRef) then do
        respond $ DAP.VariablesResponse
          (buildDiffVar <$> reverse stackDiffs)
      else
        throwError "Internal error: unknown variables reference"
    Just vs ->
      respond $ DAP.VariablesResponse vs

stepHandlers
  :: HasSpecificMessages ext
  => [DAP.Handler (RIO ext)]
stepHandlers =
  [ nextHandler
  , stepInHandler
  , stepBackHandler
  , stepOutHandler
  , continueHandler
  , reverseContinueHandler
  , restartFrameRequestHandler
  ]

-- | 'NextRequest' handler.
-- Make step forward and report stopped event.
nextHandler
  :: HasSpecificMessages ext
  => DAP.Handler (RIO ext)
nextHandler = mkHandler \req@DAP.NextRequest{} -> do
  handleStepGeneral (CNext Forward req.granularity)

-- | 'StepBackRequest' handler.
-- Make one step back and report stopped event.
stepBackHandler
  :: HasSpecificMessages ext
  => DAP.Handler (RIO ext)
stepBackHandler = mkHandler \req@DAP.StepBackRequest{} -> do
  handleStepGeneral (CNext Backward req.granularity)

-- | 'StepInRequest' handler.
-- Make a step forward jumping into lambda being exec and report stopped event.
stepInHandler
  :: HasSpecificMessages ext
  => DAP.Handler (RIO ext)
stepInHandler = mkHandler \req@DAP.StepInRequest{} -> do
  handleStepGeneral (CStepIn req.granularity)

-- | 'StepOutRequest' handler.
-- Skip the remainder of the current function block and report stopped event.
stepOutHandler
  :: (HasSpecificMessages ext)
  => DAP.Handler (RIO ext)
stepOutHandler = mkHandler \req@DAP.StepOutRequest{} -> do
  handleStepGeneral (CStepOut req.granularity)

-- | Stub implementation for 'EvaluateRequest' handler.
evaluateRequestHandlerDummy :: DAP.Handler (RIO ext)
evaluateRequestHandlerDummy = mkHandler \req@DAP.EvaluateRequest{} -> do
  respond $ DAP.mk @DAP.EvaluateResponse
    ! #result req.expression
    ! defaults

restartFrameRequestHandler
  :: (HasSpecificMessages ext)
  => DAP.Handler (RIO ext)
restartFrameRequestHandler = mkHandler \req@DAP.RestartFrameRequest{} -> do

  submitEvent "Restart frame request was sent."
    { DAP.category = Just #stdout
    }

  -- The top-most frame id is 1, so we need to substract 1 here to get correct amount to delete.
  let frameDeleteAmt = FrameDeleteAmt $ DAP.unStackFrameId req.frameId - 1
  handleStepGeneral (CRestartFrame frameDeleteAmt)

handleStepGeneral
  :: forall ext m n.
    (HasSpecificMessages ext, MonadHandler ext m, MonadAfterResponse ext n)
  => StepCommand (Maybe DAP.SteppingGranularity)
  -> m (DAP.ResponseAndLaterActions n ())
handleStepGeneral stepCmdRaw = do
  stepCmd <- forM stepCmdRaw \granularityRaw ->
    case parseStepGranularity @ext (toText <$> granularityRaw) of
      Right g -> pure g
      Left err -> throwError . DAP.customError $
        mkErrorMessage "Unexpected stepping granularity" err

  ((moveResult, traversedSnapshots), dapState) <- updateDAPSessionState do
    res <- runWriterT $ zoom dsDebuggerState $ processStep @ext stepCmd
    newSt <- get
    return (res, newSt)

  () `respondAndAlso` do
    reportContractLogs @ext dapState traversedSnapshots
    reportErrorAndStoppedEvent @ext dapState $
      stoppedReasonFromMovement (stepCommandDirection stepCmd) (stopReason stepCmd) moveResult
  where
    stopReason = \case
      CNext _ _ -> "step"
      CStepIn _ -> "step"
      CStepOut _ -> "step"
      CContinue _ -> "breakpoint"
      CRestartFrame _ -> "restart"

-- | 'ContinueRequest' handler.
-- Continue execution until either a breakpoint or end of the contract is reached.
-- Emit stopped event.
continueHandler
  :: (HasSpecificMessages ext)
  => DAP.Handler (RIO ext)
continueHandler = mkHandler \DAP.ContinueRequest{} -> do
  res <- handleStepGeneral (CContinue Forward)
  return $
    res <&> \() -> DAP.ContinueResponse Nothing

-- | 'ReverseContinueRequest' handler.
-- Continue execution in backward direction until either a breakpoint or start of the contract is reached.
-- Emit stopped event.
reverseContinueHandler
  :: HasSpecificMessages ext
  => DAP.Handler (RIO ext)
reverseContinueHandler = mkHandler \DAP.ReverseContinueRequest{} -> do
  handleStepGeneral (CContinue Backward)

-- | 'TerminateRequest' handler.
-- Terminate debugging and reset debugger state.
-- Termination is needed to gracefully clean up and free the debug adapter's resources.
terminateHandler :: DAP.Handler (RIO ext)
terminateHandler = mkHandler \_req@DAP.TerminateRequest{} -> do
  respondAndAlso () do
    submitEvent "Debug session is requested to be terminated."
      { DAP.category = Just #stdout
      }
    submitEvent $ Just DAP.TerminatedEvent{ DAP.restart = Nothing }
    resetDAPState
    logMessage "Terminating the contract\n"

-- | 'DisconnectRequest' handler.
-- Disconnect debug adapter.
disconnectHandler :: StopAdapter -> DAP.Handler (RIO ext)
disconnectHandler stop = mkHandler \_req@DAP.DisconnectRequest{} -> do
  respondAndAlso () do
    logMessage "Debugging session has been disconnected\n"
    closeLogHandle
    -- TODO [#94]: it's quite unclear what actually should we do here
    -- In theory, the request's arguments define whether should we halt or not,
    -- but somehow they are not available in our DAP library yet.
    when False $ liftIO $ doStopAdapter stop

-- | Necessary handlers for which we do not provide any smart logic.
otherHandlers :: DAP.HandlersSet (RIO ext)
otherHandlers =
  [ evaluateRequestHandlerDummy
  , mkHandler \DAP.SetExceptionBreakpointsRequest{} ->
      respond Nothing
  ]

reportMorleyLogs
  :: (MonadAfterResponse ext m, InterpretSnapshotExt ext ~ InterpretSnapshot)
  => DAPSessionState (InterpretSnapshotExt ext)
  -> TraversedSnapshots InterpretSnapshot
  -> m ()
reportMorleyLogs dapState snapshots = do
  let logs = unMorleyLogs . buildMorleyLogs . issLogs <$> tsAfterInstrs snapshots
  traverse_ outputLog $ concat logs
  where
    outputLog x = do
      let pos = getLastExecutedPosition (dapState ^. dsDebuggerState)
            & fromMaybe (error "Unexpectedly no last position")
      let srcLoc = toVSCodeLoc $ _slStart pos

      submitEvent (fmt $ pretty x <> "\n")
        { DAP.category = Just #console
          -- TODO [#65]: extract these 3 fields to one datatype
        , DAP.source = Just $ fromMichelsonSource $ _slPath pos
        , DAP.line   = Just $ vsLine srcLoc
        , DAP.column = Just $ vsCol srcLoc
        }

-- | Helper function to turn a reason and `MovementResult` into the appropriate
-- reason that the movement stopped. Note the `String` parameter is used iff
-- it succeeded.
stoppedReasonFromMovement
  :: HasCallStack
  => Direction -> Text -> MovementResult (PausedReason p) -> StoppedReason p
stoppedReasonFromMovement Forward label = \case
  MovedSuccessfully reason -> Paused reason label
  MovedToException exception -> ExceptionMet exception
  HitBoundary -> PastFinish
  ReachedTerminatedOk finalStack -> TerminatedOkMet finalStack
stoppedReasonFromMovement Backward label = \case
  HitBoundary -> ReachedStart
  MovedSuccessfully reason -> Paused reason label
  _ -> error "Unexpected move result"

-- | This function sends the proper response based on `StoppedReason`.
reportMorleyErrorAndStoppedEvent
  :: forall ext p m.
     ( HasSpecificMessages ext, InterpretSnapshotExt ext ~ InterpretSnapshot
     , MonadAfterResponse ext m
     )
  => DAPSessionState (InterpretSnapshotExt ext) -> StoppedReason p -> m ()
reportMorleyErrorAndStoppedEvent dapState = \case
  ExceptionMet exception -> writeException dapState exception
  Paused reason label -> writeStoppedEvent dapState reason label
  TerminatedOkMet finalStack -> writeTerminatedEvent (Just finalStack)
  PastFinish -> writeTerminatedEvent Nothing
  ReachedStart -> writeStoppedEvent dapState PlainPaused "entry"

-- | Send `StoppedEvent` response with the reason. Used when the debugger is paused or reaching the start.
writeStoppedEvent
  :: forall ext p m.
     (HasSpecificMessages ext, InterpretSnapshotExt ext ~ InterpretSnapshot, MonadAfterResponse ext m)
  => DAPSessionState (InterpretSnapshotExt ext) -> PausedReason p -> Text -> m ()
writeStoppedEvent dapState reason label = do
  let hitBreakpointIds = case reason of
        BreakpointPaused ids -> Just ids
        _ -> Nothing

  (mDesc, mLongDesc) <- usingReaderT dapState $ magnify dsDebuggerState $
    getStopEventInfo @ext Proxy
  let fullLabel = case mDesc of
        Nothing -> label
        Just (StopEventDesc desc) -> label <> ", " <> desc
  submitEvent DAP.StoppedEvent
    { DAP.reason = fromString $ toString fullLabel
      -- ↑ By putting moderately large text we slightly violate DAP spec,
      -- but it seems to be worth it
    , DAP.description = Nothing
    , DAP.threadId = Just threadId
    , DAP.allThreadsStopped = Just True
    , DAP.text = Just $ maybe "" pretty mLongDesc
    , DAP.hitBreakpointIds = hitBreakpointIds
    , DAP.preserveFocusHint = Nothing
    }

-- | Type of terminated event that could occur.
data TerminatedEventType
  = TerminateSucceed Doc Doc
  -- ^ Terminated after reaching the end of the contract.
  | TerminatedSucceedViewCall Doc
  -- ^ Terminated after a successful VIEW call.
  | TerminateFailure
  -- ^ Terminated due to reaching a failwith instruction.

-- | Send `TerminatedEvent` response and output the storage and operations to debug console.
-- Termination event doesn't mean exit but rather that debugger has reached a
-- terminal point (the end of contract).
writeTerminatedEvent
  :: forall ext m.
    (MonadAfterResponse ext m)
  => Maybe FinalStack -> m ()
writeTerminatedEvent mFinalStack = do
  let terminateType = stackToTerminateEventType mFinalStack
      (category, output) = terminateEventTypeToOutput terminateType
  submitEvent (fmt output)
    { DAP.category = Just category
    }
  submitEvent $ Just DAP.TerminatedEvent{ restart = Nothing }
    where
      stackToTerminateEventType :: Maybe FinalStack -> TerminatedEventType
      stackToTerminateEventType (Just finalStack) = case finalStack of
        ContractFinalStack (StkEl (T.VPair (T.VList ops, (r :: T.Value r))) :& RNil) ->
          TerminateSucceed (blockListF ops) $ renderDoc doesntNeedParens r
        ViewFinalStack (StkEl (r :: T.Value r) :& RNil) ->
          TerminatedSucceedViewCall $ renderDoc doesntNeedParens r
      stackToTerminateEventType Nothing = TerminateFailure

      terminateEventTypeToOutput = \case
        TerminateSucceed opsText storeText -> (,) #stdout $
          build $ unlines
            [ "Execution completed successfully."
            , ""
            ,"Operations:"
            , fmt opsText
            , "Storage:"
            , fmt storeText
            ]
        TerminatedSucceedViewCall valText -> (,) #stdout $
          build $ unlines
            [ "Execution completed successfully."
            , ""
            , "Result of executing view:"
            , fmt valText
            ]
        TerminateFailure -> (#stderr, "Exit due to an exception.")

-- | Send the exception response.
-- Used when an error has occurred at certain position of the file.
writeException
  :: forall ext m. (InterpretSnapshotExt ext ~ InterpretSnapshot, MonadAfterResponse ext m)
  => DAPSessionState (InterpretSnapshotExt ext) -> MichelsonFailureWithStack DebuggerFailure -> m ()
writeException dapState exception = do
  pos <- usingReaderT (dapState ^. dsDebuggerState) $ unfreezeLocally do
    -- the snapshot where the failure occurs does not contain any location,
    -- so we need to get the location of `failwith` that we faced right before
    _ <- moveRaw Backward
    frozen getLastExecutedPosition
      <&> fromMaybe (error "Unexpectedly no last position")
  let srcLoc = toVSCodeLoc $ _slStart pos
  let msg = pretty exception
  submitEvent DAP.StoppedEvent
    { DAP.reason = #exception
    , DAP.threadId = Just threadId
    , DAP.allThreadsStopped = Just True
    , DAP.description = Just "Paused on exception"
    , DAP.text = Just msg
    , DAP.preserveFocusHint = Nothing
    , DAP.hitBreakpointIds = Nothing
    }
  submitEvent (fmt $ pretty msg <> "\n")
    { DAP.category = Just #stderr
    , DAP.source = Just $ fromMichelsonSource $ _slPath pos
    , DAP.line   = Just $ vsLine srcLoc
    , DAP.column = Just $ vsCol srcLoc
    }

fromMichelsonSource :: MichelsonSource -> DAP.Source
fromMichelsonSource = \case
  MSFile filepath -> DAP.mk @DAP.Source
    ! #name (toText $ takeFileName filepath)
    ! #path filepath
    ! defaults
  other -> DAP.mk @DAP.Source
    ! #name (pretty other)
    ! defaults
