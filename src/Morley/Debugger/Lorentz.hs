-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Integration of debugger core with lorentz.
module Morley.Debugger.Lorentz
  ( lCollectInterpretSnapshots
  ) where

import Lorentz hiding (defaultContract)
import Morley.Michelson.Typed (defaultContract)
import Morley.Michelson.Typed.Scope (ParameterScope, StorageScope)

import Morley.Debugger.Core

-- | Lorentz version of 'collectInterpretSnapshots'.
lCollectInterpretSnapshots
  :: forall cp st arg.
     (IsoValue arg, IsoValue st, ParameterScope (ToT cp), StorageScope (ToT st))
  => ContractCode cp st
  -> EntrypointCall cp arg
  -> arg
  -> st
  -> ContractEnv
  -> InterpretHistory InterpretSnapshot
lCollectInterpretSnapshots (unContractCode -> contract_) epc param store env =
  collectInterpretSnapshots lorentzSource
    (defaultContract $ compileLorentzWithOptions intactCompilationOptions contract_)
    epc (toVal param) (toVal store) env
