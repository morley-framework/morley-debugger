-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

module Morley.Debugger.Protocol.Instances
  () where

import "fmt" Fmt qualified as Fmt.Fmt
import "morley-prelude" Fmt (Buildable(..), Doc, FromDoc(..), pretty)
import Protocol.DAP as DAP
import Protocol.DAP.Util as DAP

fromFmt :: Fmt.Fmt.Buildable a => a -> Doc
fromFmt = build . Fmt.Fmt.build

instance Buildable (DAP.RequestMessage dir) where
  build = fromFmt

instance Buildable (DAP.ResponseMessage dir) where
  build = fromFmt

instance Buildable DAP.EventMessage where
  build = fromFmt

instance Buildable DAP.ErrorDetails where
  build = fromFmt

instance Buildable DAP.Error where
  build = fromFmt

instance Buildable DAP.MessageToClient where
  build = fromFmt

instance Buildable DAP.Capabilities where
  build = fromFmt

instance Buildable DAP.ColumnDescriptor where
  build = fromFmt

instance Buildable DAP.Source where
  build = fromFmt

instance Buildable DAP.Breakpoint where
  build = fromFmt

instance Buildable DAP.ExceptionBreakpointsFilter where
  build = fromFmt

instance Buildable DAP.FunctionBreakpoint where
  build = fromFmt

instance Buildable DAP.Thread where
  build = fromFmt

instance Buildable DAP.StackFrame where
  build = fromFmt

instance Buildable DAP.Scope where
  build = fromFmt

instance Buildable DAP.Variable where
  build = fromFmt

instance Buildable DAP.BreakpointId where
  build = fromFmt

instance Buildable DAP.AnyType where
  build = fromFmt

instance Buildable (DAP.ClosedEnum l) where
  build = fromFmt

instance Buildable (DAP.OpenEnum desc l) where
  build = fromFmt

instance FromDoc OutputEvent where
  fmt msg = OutputEvent
    { category = Nothing
    , output = pretty msg
    , group = Nothing
    , variablesReference = Nothing
    , source = Nothing
    , line = Nothing
    , column = Nothing
    , data_ = Nothing
    }
