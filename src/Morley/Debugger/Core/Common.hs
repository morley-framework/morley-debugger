-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Debugger commons.

module Morley.Debugger.Core.Common
  ( debuggerTcOptions
  , typeCheckingForDebugger
  , SrcLoc (..)
  , IsSourceLoc (..)
  , convertSourceLoc
  ) where

import Control.Lens (Iso', from, iso)
import "morley-prelude" Fmt (Buildable(..), pretty)

import Morley.Michelson.ErrorPos (Pos(..), SrcPos(..))
import Morley.Michelson.Printer.Util (RenderDoc)
import Morley.Michelson.TypeCheck (TypeCheckOptions(..), TypeCheckResult, typeCheckingWith)

debuggerTcOptions :: TypeCheckOptions
debuggerTcOptions = TypeCheckOptions
  { tcVerbose = False
  , tcStrict = False
  }

-- | Run typechecking suitable for debugger.
typeCheckingForDebugger :: (RenderDoc op, Buildable op) => TypeCheckResult op a -> Either Text a
typeCheckingForDebugger = first pretty . typeCheckingWith debuggerTcOptions

-- | Location within a file.
--
-- This type serves as a canonical representation of location, and with
-- 'IsSourceLoc' serves as a common ground for all the other used representations.
data SrcLoc = SrcLoc
  { slLine :: Word
    -- ^ 0-indexed line number.
  , slColumn :: Word
    -- ^ 0-indexed column number.
  } deriving stock (Show, Eq, Ord, Generic)
    deriving anyclass (NFData)

instance Buildable SrcLoc where
  build (SrcLoc l c) = build l <> ":" <> build c

-- | Allows canonicalizing source locations.
--
-- Different libraries use slightly different source locations.
-- E.g. Morley uses 0-indexed lines and columns enumeration, while VSCode thinks
-- that lines and columns are counted from 1.
--
-- To avoid bugs, we suggest using different types for semantically different
-- source locations, and this class handles the conversion between such types.
class IsSourceLoc a where
  toCanonicalLoc :: a -> SrcLoc
  toCanonicalLoc = view canonicalizingLoc

  fromCanonicalLoc :: SrcLoc -> a
  fromCanonicalLoc = view (from canonicalizingLoc)

  canonicalizingLoc :: Iso' a SrcLoc
  canonicalizingLoc = iso toCanonicalLoc fromCanonicalLoc

  {-# MINIMAL toCanonicalLoc, fromCanonicalLoc | canonicalizingLoc #-}

instance IsSourceLoc SrcLoc where
  toCanonicalLoc = id
  fromCanonicalLoc = id
  canonicalizingLoc = id

-- | Convert between arbitrary source position formats.
convertSourceLoc :: forall b a. (IsSourceLoc a, IsSourceLoc b) => a -> b
convertSourceLoc = fromCanonicalLoc . toCanonicalLoc

instance IsSourceLoc SrcPos where
  toCanonicalLoc (SrcPos (Pos l) (Pos c)) = SrcLoc l c
  fromCanonicalLoc (SrcLoc l c) = SrcPos (Pos l) (Pos c)
