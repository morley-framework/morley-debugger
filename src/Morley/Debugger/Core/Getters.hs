-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Some functions to operate with snapshots data.
--
-- They are moved to a separate module because some of them are
-- time/memory consuming, it's fine to perform them once on user action, but
-- we would better not store their result in debugger state.
module Morley.Debugger.Core.Getters
  ( SomeStackElem (..)
  , stackToElems
  , stackElemsFromSnapshot
  , stackElemsFromDebuggerState
  ) where

import Prelude hiding (Const(..))

import Data.Vinyl.Functor (Const(..))
import Data.Vinyl.Recursive (recordToList, rmap)
import "morley-prelude" Fmt (Buildable(..))

import Morley.Debugger.Core.Navigate (DebuggerState(..), tapeFocus)
import Morley.Debugger.Core.Print
import Morley.Debugger.Core.Snapshots
import Morley.Michelson.Interpret (NoStkElMeta(..), StkEl(..))
import Morley.Michelson.Typed

-- | Stack element.
data SomeStackElem = forall t. SomeStackElem (StkEl NoStkElMeta t)

deriving stock instance Show SomeStackElem

instance Buildable (DebugPrint SomeStackElem) where
  build (DebugPrint mode (SomeStackElem v)) = build (DebugPrint mode v)

instance Eq SomeStackElem where
  SomeStackElem (seValue -> v1) == SomeStackElem (seValue -> v2) = v1 `eqValueExt` v2

-- | Turn stack into printable list of elements, from stack top to bottom
stackToElems :: SomeStack -> [SomeStackElem]
stackToElems (SomeStack stack) =
  recordToList $ rmap (\e -> Const $ SomeStackElem e) stack

-- | Turn a snapshot to printable list of elements
stackElemsFromSnapshot :: InterpretSnapshot -> [SomeStackElem]
stackElemsFromSnapshot = stackToElems . fst . issFullStack

-- | Turn the current snapshot in debugger state to printable list of elements
stackElemsFromDebuggerState :: DebuggerState InterpretSnapshot -> [SomeStackElem]
stackElemsFromDebuggerState = stackElemsFromSnapshot . tapeFocus . _dsSnapshots
