-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Debugger custom printing.
module Morley.Debugger.Core.Print
  ( DebugPrintMode (..)
  , DebugPrint (..)
  , debugBuild
  , DebugBuildable
  ) where

import Data.Map qualified as M
import "morley-prelude" Fmt (Buildable(..), Doc)

import Morley.Michelson.Interpret (StkEl(..))
import Morley.Michelson.Printer (printTypedValue)
import Morley.Michelson.Text (writeMText)
import Morley.Michelson.Typed
import Morley.Tezos.Address
import Morley.Tezos.Core
import Morley.Tezos.Crypto
import Morley.Util.Text

-- | Different ways of printing stuff.
data DebugPrintMode
  = DpmNormal
    -- ^ Print full info.
    -- Use this to allow user copy-paste ready Michelson values.
  | DpmEvaluated
    -- ^ The same as `DpmNormal` but expand lambda
  -- TODO [#95]: add this when it comes to UI
  -- | DpmShort
  --   -- ^ Print only information relevant for debugging, preferrably
  --   -- human-readable.
  --   -- Rule of thumb: display as few information as needed to distinguish two
  --   -- different values in the most cases.

-- | Modifies print capabilities to fit debugger needs.
--
-- Differences include
-- 1. Printing operations;
-- 2. Printing shortened versions of values.
data DebugPrint a = DebugPrint DebugPrintMode a

-- | Constraint for showing something for debugger.
type DebugBuildable a = Buildable (DebugPrint a)

-- | Show something for debugger.
debugBuild :: DebugBuildable a => DebugPrintMode -> a -> Doc
debugBuild mode a = build $ DebugPrint mode a

dBuildImpl :: (DebugPrintMode -> a -> Doc) -> (DebugPrint a -> Doc)
dBuildImpl f (DebugPrint mode a) = f mode a

instance Buildable (DebugPrint Mutez) where
  build (DebugPrint _ m) = build m

instance Buildable (DebugPrint KeyHash) where
  build = dBuildImpl $ \_ -> build

instance Buildable (DebugPrint PublicKey) where
  build = dBuildImpl $ \_ -> build

instance Buildable (DebugPrint Address) where
  build = dBuildImpl $ \_ -> build

instance Buildable (DebugPrint EpAddress) where
  build = dBuildImpl $ \_ -> build

instance Buildable (DebugPrint Signature) where
  build = dBuildImpl $ \_ -> build

instance Buildable (DebugPrint ByteString) where
  build = dBuildImpl $ \_ -> build . untypeValue . VBytes

instance Buildable (DebugPrint Timestamp) where
  build = dBuildImpl $ \_ t ->
      if t < [timestampQuote|1900-01-01T00:00:00Z|]
      then build @Integer (timestampToSeconds t) <> "s"
      else build t

instance Buildable (DebugPrint Bls12381Fr) where
  build = dBuildImpl $ \_ -> build . untypeValue . VBls12381Fr

instance Buildable (DebugPrint Bls12381G1) where
  build = dBuildImpl $ \_ -> build . untypeValue . VBls12381G1

instance Buildable (DebugPrint Bls12381G2) where
  build = dBuildImpl $ \_ -> build . untypeValue . VBls12381G2

instance Buildable (DebugPrint Operation) where
  build (DebugPrint mode op) = case op of
    OpTransferTokens TransferTokens{..} ->
      "<transfer " <> debugBuild mode ttAmount <> " with arg " <>
      debugBuild mode ttTransferArgument <> " to " <>
      buildVContract ttContract <> ">"
    OpSetDelegate (SetDelegate mbDelegate _counter) ->
      "<set delegate to " <> maybe "none" (debugBuild mode) mbDelegate <> ">"
    OpCreateContract CreateContract{..} ->
      "<create contract balance=" <> debugBuild mode ccBalance <> " \
      \ storage=" <> debugBuild mode ccStorageVal <> ">"
    OpEmit Emit{..} ->
      "<emit contract event with tag " <> build emTag <>
      "and payload " <> debugBuild mode emValue <> ">"

instance Buildable (DebugPrint Chest) where
  build = dBuildImpl $ \_ -> build . toVal

instance Buildable (DebugPrint ChestKey) where
  build = dBuildImpl $ \_ -> build . toVal

-- This instance differs from @instance RenderDoc T.Value@:
-- 1. It doesn't require any constraints on @t@;
-- 2. Prints operations;
-- 3. Renders only relevant parts of values.
instance Buildable (DebugPrint (Value t)) where
  build (DebugPrint mode val) = buildValue mode False val

instance Buildable (DebugPrint (StkEl meta t)) where
  build (DebugPrint mode val) = buildStkEl mode False val

-- Bool argument stands for need in parentheses
buildStkEl :: DebugPrintMode -> Bool -> StkEl meta t -> Doc
buildStkEl mode pn = buildValue mode pn . seValue

-- Bool argument stands for need in parentheses
buildValue :: forall t. DebugPrintMode -> Bool -> Value t -> Doc
buildValue mode pn = \case
  VInt i               -> build i
  VNat n               -> build n
  VString s            -> "\"" <> build (writeMText s) <> "\""
  VBytes b             -> debugBuild mode b
  VMutez m             -> build m
  VBool b              -> build b
  VKeyHash k           -> debugBuild mode k
  VTimestamp t         -> debugBuild mode t
  VAddress a           -> debugBuild mode a
  VKey k               -> debugBuild mode k
  VUnit                -> "()"
  VSignature s         -> debugBuild mode s
  VChainId c           -> build c
  VLam l               -> case mode of
    DpmNormal    -> "<code>"
    DpmEvaluated ->
      build (untypeDemoteT @t)
      <> " " <> build (printTypedValue False (VLam l))

  VOp op               -> debugBuild mode op
  VBls12381Fr c        -> debugBuild mode c
  VBls12381G1 c        -> debugBuild mode c
  VBls12381G2 c        -> debugBuild mode c
  VChest c             -> debugBuild mode c
  VChestKey c          -> debugBuild mode c

  VContract addr sepc -> debugBuild mode $ EpAddress' addr (sepcName sepc)
  VTicket ticketer dat amount ->
    withValueTypeSanity dat $ addParens pn $
      "Ticket " <> debugBuild mode dat <> " / " <>
      build amount <> " tokens from " <> debugBuild mode ticketer

  VOption ov -> case ov of
    Nothing -> "None"
    Just v -> addParens pn $ "Some " <> buildValue mode True v
  VList vs ->
    surround "[" "]" $
      mconcat $ intersperse ", " (buildValue mode False <$> vs)
  VSet vs ->
    surround "{" "}" $
      mconcat $ intersperse ", " (buildValue mode False <$> toList vs)
  VMap vs ->
    surround "{" "}" $
      mconcat $ intersperse ", " (buildElt mode <$> M.toList vs)
  VBigMap _id vs ->
    surround "{" "}" $
      mconcat $ intersperse ", " (buildElt mode <$> M.toList vs)

  VPair (l, r) -> mconcat
    [ "(" , buildValue mode False l
    , ", ", buildValue mode False r
    , ")"
    ]
  VOr ve -> case ve of
    Left  v -> buildOrValue mode pn "Left "  v
    Right v -> buildOrValue mode pn "Right " v

buildOrValue :: DebugPrintMode -> Bool -> Doc -> Value a -> Doc
buildOrValue mode pn fieldName v = addParens pn $
  fieldName <> buildValue mode True v

buildElt :: DebugPrintMode -> (Value k, Value v) -> Doc
buildElt mode (k, v) =
  buildValue mode False k <> " -> " <> buildValue mode False v

addParens :: Bool -> Doc -> Doc
addParens = \case
  True -> \x -> "(" <> x <> ")"
  False -> id
