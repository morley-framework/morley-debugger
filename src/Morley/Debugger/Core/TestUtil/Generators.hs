-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Test generators for debugger types.
module Morley.Debugger.Core.TestUtil.Generators
  ( genDirection
  ) where

import Hedgehog (Gen)
import Hedgehog.Gen qualified as Gen

import Morley.Debugger.Core.Navigate

genDirection :: Gen Direction
genDirection = Gen.frequency [(7, pure Forward), (3, pure Backward)]
