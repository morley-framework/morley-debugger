-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Test the property that some navigation steps are mutually reversible.
module Morley.Debugger.Core.TestUtil.Reversible
  ( jumpRandomly
  , OneStepReversedSettings (..)
  , defaultOneStepReversedSettings
  , oneStepReversed
  ) where

import Data.Default (Default(..))
import Hedgehog (PropertyT, annotate, annotateShow, discard, forAll, (===))
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Morley.Debugger.Core.Navigate
import Morley.Debugger.Core.TestUtil.Generators
import Morley.Debugger.DAP.Types

-- | Performs given step until facing the boundary.
--
-- Returns the number of steps made.
-- Second call to this function with the same argument will return 0.
doStepTillEnd :: HistoryReplay is m => m (MovementResult a) -> m Int
doStepTillEnd step = go 0
  where
    go n =
      step >>= \case
        HitBoundary -> pure n
        _ -> go (succ n)

-- | Jump to the arbitrary position of the execution history
-- that is achievable via starting at the beginning of the history and
-- applying the given step multiple times.
--
-- So, for instance, if your step does not enter functions in the code,
-- this call would bring you to an arbitrary position in `main` body.
--
-- The initial position does not matter.
--
-- Note: in practice, placing 'PropertyT' as the top layer of the monadic stack
-- causes troubles. So here and in other functions we assume @navigation@ layer
-- to be at top, 'PropertyT' be below it, and we explicitly accept a `PropertyT'
-- lifting function (usually should be just a sequence of `lift`s).
jumpRandomly
  :: HistoryReplay is m
  => (forall a. PropertyT IO a -> m a)
  -> (forall n. HistoryReplay is n => n (MovementResult b))
  -> m ()
jumpRandomly liftProp step = do
  -- Go to beginning, then do some steps forward, picking number of steps
  -- with uniform distribution
  _ <- moveTill Backward false
  futureStepsNum <- frozen $ unfreezeLocallyW $
    doStepTillEnd step
  stepsToDo <- liftProp . forAll $
    Gen.integral (Range.constant 0 futureStepsNum)
  liftProp . annotate $
    "Stopped at step " <> show stepsToDo <> " / " <> show futureStepsNum
  replicateM_ stepsToDo step

-- | Default settings for 'oneStepReversed' test.
data OneStepReversedSettings r m = OneStepReversedSettings
  { osrsDiscardAtStartWhenHolds :: FrozenPredicate r m ()
    -- ^ Whether should the test be discarded, checked after the initial
    -- random position is taken.
  , osrsDiscardAfterStepForwardWhenHolds :: FrozenPredicate r m ()
    -- ^ Whether should the test be discarded, checked after the step forward.
  , osrsDiscardAfterStepBackwardWhenHolds :: FrozenPredicate r m ()
    -- ^ Whether should the test be discarded, checked after the step backward
    -- (final step).
  }

instance Monad m => Default (OneStepReversedSettings r m) where
  def = OneStepReversedSettings
    { osrsDiscardAtStartWhenHolds = false
    , osrsDiscardAfterStepForwardWhenHolds = false
    , osrsDiscardAfterStepBackwardWhenHolds = false
    }

defaultOneStepReversedSettings :: Monad m => OneStepReversedSettings r m
defaultOneStepReversedSettings = def

-- | Test that 'StepBack' command is reverse to 'Next'.
--
-- This works in 3 steps:
--
-- 1. Jump at arbitrary position of the history.
--    This is done via rewinding to the start and doing some steps ahead.
-- 2. Select a direction and do a step in that direction.
-- 3. Do a step in the opposite direction and check whether we appeared
--    at the same position as after step (1).
--
-- The algorithm provides some extension points via 'OneStepReversedSettings'.
oneStepReversed
  :: forall granularity restartFrameArg is m b.
     (HistoryReplay is m, NavigableSnapshotWithMethods is)
  => (forall a. PropertyT IO a -> m a)
     -- ^ PropertyT lifting function
  -> OneStepReversedSettings (DebuggerState is) m
  -> (forall n. HistoryReplay is n =>
       StepCommand' restartFrameArg granularity -> n (MovementResult b)
     )
     -- ^ Action to perform a step, usually just 'processStep'
  -> granularity
     -- ^ Granularity used in stepping
  -> m ()
oneStepReversed liftProp settings doProcessStep granularity = do
  let
    processStep'
      :: HistoryReplay is n
      => StepCommand' restartFrameArg () -> n (MovementResult ())
    processStep' step = fmap void $ doProcessStep (step $> granularity)

    discardIfHolds :: FrozenPredicate (DebuggerState is) m () -> m ()
    discardIfHolds p = frozen (runFrozenPredicate p) >>= \case
      Nothing -> pass
      Just () -> liftProp discard

  jumpRandomly liftProp (processStep' (CStepIn ()))
  discardIfHolds $ osrsDiscardAtStartWhenHolds settings

  startPos <- frozen getExecutedPosition
  liftProp $ annotateShow startPos

  startMethodLevel <- frozen getCurMethodBlockLevel

  dir <- liftProp $ forAll genDirection
  -- Do a step over...
  do
    moveRes <- processStep' (CNext dir ())
    unless (moveRes == MovedSuccessfully ()) $
      -- We started at the end of the tape, this case is not interesting
      liftProp discard

  discardIfHolds $ osrsDiscardAfterStepForwardWhenHolds settings

  endMethodLevel <- frozen getCurMethodBlockLevel
  unless (startMethodLevel == endMethodLevel) $
    -- Exited current method, step back won't return to the old position
    liftProp discard

  liftProp . annotateShow =<< frozen getExecutedPosition

  -- ...and then a step in the opposite direction
  _ <- processStep' (CNext (reverseDirection dir) ())

  discardIfHolds $ osrsDiscardAfterStepBackwardWhenHolds settings

  -- Check this all resulted in no-op
  endPos <- frozen getExecutedPosition
  liftProp $ startPos === endPos
