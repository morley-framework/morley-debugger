-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Types and utilities for getting diff between two stacks.
module Morley.Debugger.Core.StackDiff
  ( StackDiffType (..)
  , StackDiffWithIndex
  , diffScopeRef
  , getStackDiff
  ) where

import Protocol.DAP qualified as DAP

import Morley.Debugger.Core.Getters

data StackDiffType
  = StackDiffAdd SomeStackElem
  | StackDiffRemove SomeStackElem
  | StackDiffModify
      SomeStackElem -- old value
      SomeStackElem -- new value

-- | A stack diff with the index of the its cell.
type StackDiffWithIndex = (Int, StackDiffType)

-- | A ref needed to identify the stack diff scope.
-- Choosing a high value to avoid colliding with a ref in variables.
diffScopeRef :: DAP.VariableId
diffScopeRef = DAP.VariableId 1000

-- | Get the diff between two stacks.
getStackDiff
  :: [SomeStackElem]
  -> [SomeStackElem]
  -> [StackDiffWithIndex]
getStackDiff lastStackArg currentStackArg =
  let
    -- Reverse the element so that bottom element of the stack become the first element of the list.
    lastStack :: [Maybe SomeStackElem] = Just <$> reverse lastStackArg
    currentStack :: [Maybe SomeStackElem] = Just <$> reverse currentStackArg

    -- Define diffSize
    diffSize = length currentStack - length lastStack

    -- Make both stacks the same length so we can zip it.
    (currentStackUpdatedSize, lastStackUpdatedSize)
      | (diffSize > 0)
        = (currentStack, lastStack <> (replicate diffSize Nothing))
      | (diffSize == 0)
        = (currentStack, lastStack)
      | otherwise
        = (currentStack <> (replicate (- diffSize) Nothing), lastStack)

    -- Compare the current and previous stacks, return the diff as the result.
    -- Example (sorted from bottom of the stack to the top):
    -- - old stack: [Just (2, 3), Just 1, Just 2]
    -- - new stack: [Just 2, Nothing, Nothing]
    -- This will result in:
    -- - removed: "1, 2"
    -- - modified: "(2, 3) -> 2"
  in
    zip (zip [0..] currentStackUpdatedSize) lastStackUpdatedSize
      & foldl'
          (\stackDiff ((index, curElemMaybe), previElemMaybe) ->
            case (curElemMaybe, previElemMaybe) of
              (Just curElem, Just prevElem) -> if curElem == prevElem
                then stackDiff
                else stackDiff <> [(index, StackDiffModify prevElem curElem)]
              (Nothing, Just prevElem) ->
                stackDiff <> [(index, StackDiffRemove prevElem)]
              (Just curElem, Nothing) ->
                stackDiff <> [(index, StackDiffAdd curElem)]
              _ -> stackDiff
          ) []
