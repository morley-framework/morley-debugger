-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Building a series of snapshots of contract interpretation.
module Morley.Debugger.Core.Snapshots
  ( SnapshotNo (..)
  , SomeStack (..)
  , InterpretStatus (..)
  , InterpretRunningArg (..)
  , InterpretTerminatedOkArg (..)
  , FinalStack(..)
  , DebuggerFailure (..)
  , InterpretFailedArg (..)
  , StackFrame (..)
  , CollectingEvalOp (..)
  , ContractEnv
  , sfName
  , sfOuterStack
  , SourceLocation
  , SourceLocation' (..)
  , pointSourceLocation
  , slPath
  , slStart
  , slEnd
  , InterpretSnapshot (..)
  , issStack
  , issFullStack
  , InterpretHistory (..)
  , filterInterpretHistory
  , mapMaybeInterpretHistory
  , collectInterpretSnapshots
  , collectInterpretSnapshotsForView
  , lorentzSource
  ) where

import Debug qualified
import Prelude hiding (Ordering(..))

import Control.Lens (_head, makeLenses, (%=), (+=), (-=), (.=), (<<%=), (<<.=))
import Control.Monad.Except (MonadError(throwError))
import Control.Monad.RWS.Strict (RWST, listen)
import Control.Monad.Writer.Class (MonadWriter)
import Data.Conduit (ConduitT, (.|))
import Data.Conduit qualified as C
import Data.Conduit.Lift qualified as CL
import Data.GADT.Compare (defaultEq)
import Data.Vinyl (Rec(..), rmap, (<+>))
import Data.Vinyl.Recursive (rfoldMap)
import "morley-prelude" Fmt (Buildable(..), Doc, pretty)
import GHC.Stack qualified
import Text.Show qualified
import Unsafe qualified

import Morley.Michelson.ErrorPos
import Morley.Michelson.Interpret hiding (ContractEnv)
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Runtime.Dummy (dummyBigMapCounter, dummyGlobalCounter)
import Morley.Michelson.Typed
import Morley.Util.PeanoNatural (fromPeanoNatural)

import Morley.Debugger.Core.Common

-- | Number of snapshot in interpreter execution.
--
-- The snapshot with no 0 should correspond to the state upon interpretation
-- start.
newtype SnapshotNo = SnapshotNo Word
  deriving stock (Show, Generic)
  deriving anyclass Buildable

initSnapshotNo :: SnapshotNo
initSnapshotNo = SnapshotNo 0

incSnapshotNo :: SnapshotNo -> SnapshotNo
incSnapshotNo (SnapshotNo i) = SnapshotNo (i + 1)

data InterpretRunningArg = InterpretRunningArg
  { iraStack :: SomeStack
  , iraLastLocation :: SourceLocation
    -- ^ Location of last executed instruction.
  , iraPrevStack :: SomeStack
  } deriving stock Generic
    deriving anyclass Buildable

data InterpretTerminatedOkArg = InterpretTerminatedOkArg
  { itoaPrevStack :: SomeStack
  , itoaFinalStack :: FinalStack
  } deriving stock Generic

instance Buildable InterpretTerminatedOkArg where
  build (InterpretTerminatedOkArg prevStack _) = build prevStack

data FinalStack where
  ContractFinalStack :: StorageScope st => Rec (StkEl NoStkElMeta) (ContractOut st) -> FinalStack
  ViewFinalStack :: ViewableScope ret => Rec (StkEl NoStkElMeta) '[ret] -> FinalStack

instance Show FinalStack where
  show (ContractFinalStack (x :& RNil)) = Debug.show x
  show (ViewFinalStack (x :& RNil)) = Debug.show x

instance Eq FinalStack where
  ContractFinalStack (StkEl x :& RNil) == ContractFinalStack (StkEl y :& RNil) = defaultEq x y
  ViewFinalStack (StkEl x :& RNil) == ViewFinalStack (StkEl y :& RNil) = defaultEq x y
  _ == _ = False

data DebuggerFailure
  = DebuggerInfiniteLoop
    -- ^ Only one case of morley-debugger failure for now, add a new one below when needed.
  deriving stock (Show, Generic, Eq)

instance Buildable DebuggerFailure where
  build = \case
    DebuggerInfiniteLoop -> "Gas limit exceeded on contract execution. This might be due to an infinite loop"

data InterpretFailedArg = InterpretFailedArg
  { ifaError :: MichelsonFailureWithStack DebuggerFailure
  , ifaPrevStack :: SomeStack
  } deriving stock Generic
    deriving anyclass Buildable

-- | A meta info for debugger which reports the current execution status.
data InterpretStatus
  = InterpretRunning InterpretRunningArg
    -- ^ Some intermediate state.
  | InterpretTerminatedOk InterpretTerminatedOkArg
    -- ^ Last instruction has been executed, contract execution was successful.
  | InterpretFailed InterpretFailedArg
    -- ^ Just executed @FAILWITH@.
  deriving stock Generic
  deriving anyclass Buildable

-- | Stack of some type.
--
-- We tend to use this type rather than list of some values because
-- it's easier to achieve sharing this way.
data SomeStack = forall s. SomeStack (Rec (StkEl NoStkElMeta) s)

instance Show SomeStack where
  show (SomeStack v) = case v of
    RNil -> "[]"
    x :& xs -> Debug.show x <> " :& " <> Debug.show (SomeStack xs)

deriving stock instance Generic (StkEl meta l)

instance Buildable (meta l) => Buildable (StkEl meta l)

instance Buildable (NoStkElMeta l) where
  build NoStkElMeta = "no metadata"

instance Buildable SomeStack where
  build s = "[ " <> (buildInner s) <> " ]"
    where
      buildInner :: SomeStack -> Doc
      buildInner (SomeStack v) = case v of
        RNil -> ""
        x :& xs -> pretty x <> " , " <> pretty (SomeStack xs)

instance Semigroup SomeStack where
  SomeStack a <> SomeStack b = SomeStack (a <+> b)

instance Monoid SomeStack where
  mempty = SomeStack RNil


-- | Instances needed for @MichelsonSource@ to be used in @SourceLocation@
deriving stock instance Ord GHC.Stack.SrcLoc
deriving stock instance Ord MichelsonSource
deriving stock instance Generic MichelsonSource
deriving anyclass instance NFData MichelsonSource

lorentzSource :: MichelsonSource
lorentzSource = MSName "<lorentz>"

-- | Represents the location range of some source language.
data SourceLocation' srcPos = SourceLocation
  { _slPath   :: MichelsonSource
  -- ^ The file corresponding to this location.
  , _slStart :: srcPos
  -- ^ The start position (line, column).
  , _slEnd :: srcPos
  -- ^ The end position (line, column).
  }
  deriving stock (Eq, Ord, Show, Functor, Generic)
  deriving anyclass (NFData)

type SourceLocation = SourceLocation' SrcLoc

instance Buildable SourceLocation where
  build (SourceLocation src startPos endPos) =
    build src <> ":" <> build startPos <> "," <> build endPos

makeLenses ''SourceLocation'

-- | Source location referring to a point (rather than a range).
pointSourceLocation :: MichelsonSource -> srcPos -> SourceLocation' srcPos
pointSourceLocation path pos = SourceLocation path pos pos

-- | Stack frame
data StackFrame = StackFrame
  { _sfName       :: Text
  -- ^ Contains the name of the stack frame to be displayed on the call stack.
  , _sfPosition   :: Maybe SourceLocation
  -- ^ Contains the location in the code where the stack frame was created.
  , _sfOuterStack :: SomeStack
  -- ^ Contains the stack elements which would be discarded by the stack frame
  -- so that they can be displayed by the debugger.
  --
  -- For a @DIP@ instruction, for example, it will contain the head of the stack.
  --
  -- The stack will not be saved for an @EXEC@ instruction.
  } deriving stock (Generic)
    deriving anyclass Buildable

-- | Indicates whether a snapshot was produced or not by an instruction that
-- renames the topmost stack element, such as ITER or EXEC.
data NamedInstr
  = NamedInstr Text
  -- ^ Instructions such as @ITER@, @IF_NONE@ or any other containing nested
  -- instructions are considered a named instruction. Contains the instruction
  -- name that should be displayed by the debugger.
  | UnnamedInstr
  -- ^ Indicates that the instruction should not rename the topmost stack.
  deriving stock (Eq, Show, Generic)
  deriving anyclass Buildable

pattern SomeName, LeftName, RightName, ConsName, IterName, LoopLeftName, MapName, ExecName :: Text
pattern SomeName = "Some"
pattern LeftName = "Left"
pattern RightName = "Right"
pattern ConsName = "Cons"
pattern IterName = "Iter"
pattern LoopLeftName = "LoopLeft"
pattern MapName = "Map"
pattern ExecName = "Exec"


-- | A snapshot of interpreter state at some execution point.
data InterpretSnapshot = InterpretSnapshot
  { issState :: InterpreterState
  -- ^ Interpreter state.
  , issStatus :: InterpretStatus
  -- ^ Meta info about at which stage of execution we currently are.
  , issLastStackFrames :: [StackFrame]
  -- ^ Stack frames at the /previous/ execution point. @DIP@ and @EXEC@ are considered.
  , issLastMethodBlockLevel :: Int
  -- ^ Number of nested method calls we were within.
  --
  -- Here under @method@ we mean only true functions like 'EXEC' and loops,
  -- that should be accounted in @Step out@.
  --
  -- 0 stands for the top-level.
  -- We keep this separately from stack frames because they designate a completely
  -- different things.
  , issNamedInstr :: NamedInstr
  -- ^ Indicates whether this snapshot should rename the topmost stack element
  -- or not.
  , issSnapNo :: SnapshotNo
  -- ^ Sequence number.
  , issLogs :: MorleyLogsBuilder
  -- ^ Logs produced so far. It's a builder for sharing what has been produced
  -- so far across snapshots.
  } deriving stock (Generic)
    deriving anyclass Buildable

instance Buildable InterpreterState

instance Buildable MorleyLogsBuilder where
  build = build . buildMorleyLogs

-- | The current stack (or at least what would you probably expect it to be).
issStack :: InterpretSnapshot -> SomeStack
issStack InterpretSnapshot{..} = case issStatus of
  InterpretRunning InterpretRunningArg{..} -> iraStack
  InterpretTerminatedOk InterpretTerminatedOkArg{..} -> itoaPrevStack
  InterpretFailed InterpretFailedArg{..} ->
    -- Cut off the argument we failed with
    case ifaPrevStack of
      SomeStack RNil -> SomeStack RNil
      SomeStack (_ :& st) -> SomeStack st

-- | The stack after the last executed instruction, including stack elements
-- that would not be shown by @issStack@ such as the ones that would normally
-- be discarded during a @DIP@ instruction.
--
-- The frames that were discarded for an @EXEC@ instruction will not be shown.
--
-- This function may return a name to be used by the topmost element (considering
-- instructions such as @DIP@s), in the case when the snapshot contains a complex
-- instruction. See @NamedInstr@.
issFullStack :: InterpretSnapshot -> (SomeStack, Maybe (Int, Text))
issFullStack sps = case issNamedInstr sps of
  NamedInstr ExecName -> (issStack sps, Just (0, ExecName))
  NamedInstr name -> (fullStack, Just (renameIndex, name))
  UnnamedInstr -> (fullStack, Nothing)
  where
    framesAfterExec :: [StackFrame]
    framesAfterExec = takeWhile ((/= "LAMBDA") . _sfName) (issLastStackFrames sps)

    fullStack :: SomeStack
    renameIndex :: Int
    (fullStack, renameIndex) = foldl'
      (\(acc, idx) stk -> (_sfOuterStack stk <> acc, idx + stkLength (_sfOuterStack stk)))
      (issStack sps, 0)
      framesAfterExec

    stkLength :: SomeStack -> Int
    stkLength (SomeStack s) = getSum $ rfoldMap (const (Sum 1)) s

-- | A series of snapshots of a single contract execution.
--
-- This uses 'NonEmpty' because it must contain at least one snapshot,
-- corresponding to the state after the last instruction execution.
--
-- In practice this should share the data between individual snapshots
-- (e.g. common parts of stack and all the recent morley logs), thus
-- the amount of memory required to keep the whole history should be @O(steps)@.
--
-- Also, this list can potentially be (almost) infinite, so it is supposed to be
-- produced in a lazy way.
newtype InterpretHistory is = InterpretHistory
  { unInterpretHistory :: NonEmpty is
  } deriving stock (Show, Eq, Functor, Foldable, Traversable, Generic)
    deriving anyclass Buildable

-- | Filter interpretation history, leaving only the snapshots matching
-- the predicate.
--
-- This returns @Nothing@ when zero snapshots are left.
filterInterpretHistory
  :: (is -> Bool)
  -> InterpretHistory is -> Maybe (InterpretHistory is)
filterInterpretHistory p = mapMaybeInterpretHistory (\s -> guard (p s) $> s)

-- | Update interpretation history, leaving only the snapshots that
-- map to @Just@.
--
-- This returns @Nothing@ when zero snapshots are left.
mapMaybeInterpretHistory
  :: (is1 -> Maybe is2)
  -> InterpretHistory is1 -> Maybe (InterpretHistory is2)
mapMaybeInterpretHistory f (InterpretHistory te) =
  InterpretHistory <$> (nonEmpty $ mapMaybe f (toList te))

-- | 'InterpretSnapshot' with last fields not initialized yet.
type RawInterpretSnapshot = SnapshotNo -> SomeStack -> InterpretSnapshot

-- | Datatype describing whether there is an "considerable" instruction
-- in the set of instructions. We treat instruction as considerable if it's
-- a real (not meta) instruction or it produced a snapshot.
data ConsiderableInstr
  = NoInstr
  | RealInstr
  | SnapshotInstr
  deriving stock (Eq, Show, Enum)

instance Semigroup ConsiderableInstr where
  (<>) a b = toEnum $ max (fromEnum a) (fromEnum b)

-- | Auxiliary state used in @runInstrCollect@.
data CollectorState = CollectorState
  { _csInterpreterState :: InterpreterState
  -- ^ State of the interpreter
  , _csLastStackFrames      :: [StackFrame]
  -- ^ Stack frames pointing to the last executed instruction.
  , _csLastMethodBlockLevel  :: Int
  -- ^ Number of nested method calls we were within.
  , _csLastMetLoc       :: Maybe SourceLocation
  -- ^ If we meet @WithLoc@ constructor, we remember position.
  -- It might be taken later for instruction which should be recorded in snapshots.
  , _csConsiderableInstr :: ConsiderableInstr
  -- ^ If we met some considerable instruction.
  , _csInsideComplexInstr :: Maybe (SomeStack -> RawInterpretSnapshot)
  -- ^ Snapshot constructor corresponding
  -- to the outer complex instructions like IF, LOOP, etc
  }

makeLenses ''CollectorState
makeLenses ''StackFrame

-- | 'ContractEnv' for 'CollectingEvalOp'
type ContractEnv = ContractEnv' CollectingEvalOp

-- | Our monadic stack, allows running interpretation and making snapshot
-- records.
newtype CollectingEvalOp a = CollectingEvalOp
  -- Including ConduitT to build snapshots sequence lazily.
  -- Normally ConduitT lies on top of the stack, but here we put it under
  -- ExceptT to make it record things even when a failure occurs.
  { unCollectingEvalOp :: ExceptT (MichelsonFailureWithStack DebuggerFailure)
      (ConduitT () RawInterpretSnapshot
      (RWST ContractEnv MorleyLogsBuilder CollectorState Identity)) a
  }
  deriving newtype
    ( MonadError (MichelsonFailureWithStack DebuggerFailure)
    , MonadState CollectorState
    , MonadWriter MorleyLogsBuilder
    , MonadReader ContractEnv
    , Monad
    , Applicative
    , Functor
    )

instance InterpreterStateMonad CollectingEvalOp where
  stateInterpreterState = state . csInterpreterState

-- | Snapshots collector to be embedded into executor.
-- Conduit-source. Yield to 'fillRawSnapshots'.
runInstrCollect :: MichelsonSource -> InstrRunner NoStkElMeta CollectingEvalOp
runInstrCollect issPath instr stack = do
  -- We uses @isRemainingSteps@ to track how many time @runInstrCollect@ is run.
  -- Fail when it exceeds the limit.
  rs <- isRemainingSteps <$> getInterpreterState
  if rs == 0 then
    throwError $
      MichelsonFailureWithStack
        (MichelsonExt DebuggerInfiniteLoop)
        ((ErrorSrcPos (SrcPos (Pos 0) (Pos 0))))
  else
    modifyInterpreterState (\s -> s {isRemainingSteps = rs - 1})

  lastLocationM <- use csLastMetLoc
  issLastMethodBlockLevel <- use csLastMethodBlockLevel
  case instr of
    -- If we meet @WithLoc@ rewrite location with new one
    WithLoc (ErrorSrcPos (toCanonicalLoc -> sourcePos)) _ -> do
      csLastMetLoc .= Just (pointSourceLocation issPath sourcePos)
      -- If we just entered in the body of the complex instruction and we reached
      -- WithLoc, we emit a snapshot with current stack.
      whenJustM (csInsideComplexInstr <<.= Nothing) $
        CollectingEvalOp . lift . C.yield . ($ SomeStack stack)
    _ -> pass

  when (isRealInstr instr) $
    csConsiderableInstr %= (<> RealInstr)

  case lastLocationM of
    Just lastLocation | isSubjectToSnapshot instr -> do
      -- Reset last known location and index
      csLastMetLoc .= Nothing
      -- Replace a stack frame index with the current index
      csLastStackFrames . _head . sfPosition .= lastLocationM

      if isComplexInstr instr || isLikelyIfMacro instr then do
        -- If we meet complex instruction like DIP, IF, etc
        -- we would create an incomplete future snapshot
        -- to emit as soon as we enter its body.
        issState <- use csInterpreterState
        issLastStackFrames <- use csLastStackFrames
        let issNamedInstr = namedInstr
        -- We don't want logs in the snapshot when we have a complex instruction,
        -- as it would cause us to report duplicate logs when reporting them.
        -- Instead, we add logs to snapshots when they are more likely to be a
        -- primitive (below).
        let issLogs = mempty
        let futureComplexSnapshot nextStack issSnapNo prevStack =
              InterpretSnapshot
              -- Appending topmost elements for a case when @instr@ is DIP alike
              { issStatus = InterpretRunning InterpretRunningArg
                  { iraStack = outerStack <> nextStack
                  , iraLastLocation = lastLocation
                  , iraPrevStack = prevStack
                  }
              , .. }
        csConsiderableInstr .= SnapshotInstr
        -- We set this future snapshot to be emitted
        -- as soon as we enter the body of the current complex instruction
        csInsideComplexInstr .= Just futureComplexSnapshot
        (newStack, _, _) <- collectInnerConsiderable lastLocationM
        pure newStack
      else do
        (newStack, considerable, issLogs) <- collectInnerConsiderable lastLocationM
        -- The idea here, that @Nested@ can be used in two cases:
        -- 1. auxiliary for other instructions, like APPLY and macro
        -- 2. instructions in Michelson like {instr1; instr2;..}
        -- In former case we would like to record only outer @Nested@ presence (with its location).
        -- In latter case, we record inner instructions among with their locations, and
        -- we don't want to produce a snapshot for @Nested@ instruction itself.
        --
        -- So to distinguish one case from another, we check whether there were records
        -- among internal instructions of the current @Nested@.
        -- As we treat instruction without a position as auxiliary, it's pretty fair.
        unless (isNested instr && considerable /= RealInstr) $ do
          issState <- use csInterpreterState
          issLastStackFrames <- use csLastStackFrames
          let issNamedInstr = UnnamedInstr
          CollectingEvalOp . lift . C.yield $ \issSnapNo prevStack ->
            InterpretSnapshot
              { issStatus = InterpretRunning InterpretRunningArg
                { iraStack = SomeStack newStack
                , iraLastLocation = lastLocation
                , iraPrevStack = prevStack
                }
              , .. }
          csConsiderableInstr .= SnapshotInstr
        -- Here we could put the previous stack, but in other 'yield' places
        -- we can't do that so easily anyway, so let's pretend that old stack
        -- is unavailable here as well
        return newStack
    _ -> runInstrImpl (runInstrCollect issPath) instr stack
  where
    outerStack :: SomeStack
    outerStack = takeValuesByDepth instr (SomeStack stack)

    collectInnerConsiderable lastLocation = do
      -- Add a stack frame if necessary before the inner code execution
      pushFrameIfNeeded instr outerStack lastLocation
      when (isMethod instr) $ csLastMethodBlockLevel += 1
      considerablePrev <- csConsiderableInstr <<.= NoInstr
      (newStack, logs) <- listen $ runInstrImpl (runInstrCollect issPath) instr stack
      considerableInner <- csConsiderableInstr <<%= (<> considerablePrev)
      when (isMethod instr) $ csLastMethodBlockLevel -= 1
      dropFrameIfNeeded instr
      return (newStack, considerableInner, logs)

    isSubjectToSnapshot :: Instr a b -> Bool
    isSubjectToSnapshot i = isRealInstr i || isNested i

    withMeta :: (Instr a b -> r) -> Instr a b -> r
    withMeta f = \case
      Meta _ i -> withMeta f i
      i -> f i

    -- Some macros such as IF_SOME or IF_RIGHT are created inside a Nested
    -- instruction. This function lets you access through a Nested, if present.
    withNested :: (Instr a b -> r) -> Instr a b -> r
    withNested f = \case
      Nested i -> withNested f i
      i -> f i

    stackFrameName :: Instr a b -> Maybe Text
    stackFrameName = withMeta \case
      DIP    _ -> Just "DIP"
      DIPN n _ -> Just $ "DIP " <> show (fromPeanoNatural n)
      EXEC     -> Just "LAMBDA"
      VIEW    _ -> Just "VIEW"
      _        -> Nothing

    takeValuesByDepth :: Instr a b -> SomeStack -> SomeStack
    takeValuesByDepth (stackFrameDepth -> depth) = go depth
      where
        go 0 _ = mempty
        go n (SomeStack (x :& xs)) = SomeStack (x :& RNil) <> go (n - 1) (SomeStack xs)
        go _ stk = stk

    stackFrameDepth :: Instr a b -> Natural
    stackFrameDepth = withMeta \case
      DIP    _ -> 1
      DIPN n _ -> fromPeanoNatural n
      _        -> 0

    isMethod :: Instr a b -> Bool
    isMethod i = or
      [ isLoop i
      , case i of
          EXEC{} -> True
          VIEW{} -> True
          Ext (TEST_ASSERT{}) -> True
          _ -> False
      ]

    -- Some macros have a distinct structure such that they all are created
    -- inside a Nested instruction and some of their inner instructions don't
    -- contain WithLoc instructions. This function checks for such cases by
    -- checking the AST of these problematic macros.
    --
    -- Ideally, we should have a way to distinguish macros and ordinary
    -- instructions, such as creating some Macro instruction, for example. The
    -- debugger logic could be simplified since many of these functions are
    -- checking for the execution of macros.
    isLikelyIfMacro :: Instr a b -> Bool
    isLikelyIfMacro = withMeta \case
      Nested (withMeta id -> Seq (withMeta id -> COMPARE) (withMeta id -> Seq (isEqualityInstr -> True) (isIf -> True))) -> True
      Nested (withMeta id -> Seq (isEqualityInstr -> True) (isIf -> True)) -> True
      Nested (withMeta id -> IF_NONE _ _) -> True
      Nested (withMeta id -> IF_LEFT _ _) -> True
      _ -> False

    isIf :: Instr a b -> Bool
    isIf = withMeta \case
      IF _ _ -> True
      _ -> False

    isEqualityInstr :: Instr a b -> Bool
    isEqualityInstr = withMeta \case
      EQ -> True
      NEQ -> True
      LT -> True
      GT -> True
      LE -> True
      GE -> True
      _ -> False

    pushFrameIfNeeded i vals loc = case stackFrameName i of
      Just frName -> csLastStackFrames %= (StackFrame frName loc vals : )
      Nothing -> pass

    dropFrameIfNeeded i = case stackFrameName i of
      Just _ -> csLastStackFrames %= Unsafe.tail
      Nothing -> pass

    isRealInstr :: Instr a b -> Bool
    isRealInstr = withMeta \case
      DocGroup{} -> False
      Seq{} -> False
      Nop -> False
      WithLoc{} -> False
      Meta{} -> False
      Nested{} -> False
      _ -> True

    namedInstr :: NamedInstr
    namedInstr = withNested (withMeta (flip go stack)) instr
      where
        go :: Instr a b -> Rec (StkEl meta) a -> NamedInstr
        go (IF_NONE _ _) (StkEl (VOption (Just _)) :& _) = NamedInstr SomeName
        go (IF_LEFT _ _) (StkEl (VOr (Left _)) :& _) = NamedInstr LeftName
        go (IF_LEFT _ _) (StkEl (VOr (Right _)) :& _) = NamedInstr RightName
        go (IF_CONS _ _) (StkEl (VList (_ : _)) :& _) = NamedInstr ConsName
        go (ITER      _) (StkEl (iterOpDetachOne -> (Just _, _)) :& _) = NamedInstr IterName
        go (LOOP_LEFT _) (StkEl (VOr (Left _)) :& _) = NamedInstr LoopLeftName
        go (MAP       _) _ = NamedInstr MapName
        go (EXEC       ) _ = NamedInstr ExecName
        go _             _ = UnnamedInstr

    isComplexInstr :: Instr a b -> Bool
    isComplexInstr = withNested $ withMeta \case
      IF_NONE{}           -> True
      IF_LEFT{}           -> True
      IF_CONS{}           -> True
      IF{}                -> True
      ITER{}              -> True
      LOOP{}              -> True
      LOOP_LEFT{}         -> True
      MAP{}               -> True
      DIP{}               -> True
      DIPN{}              -> True
      EXEC{}              -> True
      VIEW{}              -> True
      Ext (TEST_ASSERT{}) -> True
      _                   -> False

    isNested :: Instr a b -> Bool
    isNested = withMeta \case
      Nested{} -> True
      _        -> False

    isLoop :: Instr a b -> Bool
    isLoop = withMeta \case
      ITER{} -> True
      LOOP{} -> True
      LOOP_LEFT{} -> True
      MAP{} -> True
      _ -> False

-- | Complete raw snapshots, enumerating them and giving each a reference to the
-- previous stack.
-- Intermediate conduit, awaits 'RawInterpretSnapshot' from 'runInstrCollect',
-- supply it with a snapshot index among with a previous stack, and yield after that.
fillRawSnapshots
  :: Monad m
  => SomeStack
  -> ConduitT RawInterpretSnapshot InterpretSnapshot m ()
fillRawSnapshots = loop initSnapshotNo
  where
  loop no prevStack =
    whenJustM C.await $ \ris -> do
      let is = ris no prevStack
      C.yield is
      loop (incSnapshotNo no) (issStack is)

runCollectInterpretSnapshots
  :: MichelsonSource
  -> CollectingEvalOp FinalStack
  -> ContractEnv
  -> CollectorState
  -> SomeStack
  -> InterpretHistory InterpretSnapshot
runCollectInterpretSnapshots sourcePath act env initSt initStack =
  InterpretHistory $
  -- This is safe because we yield at least two snapshots
  (Unsafe.fromJust . nonEmpty) $
  runIdentity $
  C.sourceToList . (.| fillRawSnapshots initStack) $ do
    C.yield \issSnapNo _ ->
      InterpretSnapshot
        { issStatus = InterpretRunning $ InterpretRunningArg
          { iraStack = initStack
          , iraLastLocation = pointSourceLocation sourcePath (SrcLoc 0 0)
          , iraPrevStack = mempty
          }
        , issState = _csInterpreterState initSt
        , issLastStackFrames = _csLastStackFrames initSt
        , issLastMethodBlockLevel = 0
        , issNamedInstr = UnnamedInstr
        , issLogs = mempty
        , ..
        }
    (outcome, endState, _) <-
      CL.runRWSC env initSt $ runExceptT $ unCollectingEvalOp act
    let mkEndStatus prevStack =
          either
            (\err -> InterpretFailed $ InterpretFailedArg err prevStack)
            (InterpretTerminatedOk . InterpretTerminatedOkArg prevStack)
            outcome
    C.yield \issSnapNo prevStack ->
      InterpretSnapshot
        { issStatus = mkEndStatus prevStack
        , issState = _csInterpreterState endState
        , issLastStackFrames = _csLastStackFrames endState
        , issLastMethodBlockLevel = 0
        , issNamedInstr = UnnamedInstr
        , issLogs = mempty
        , ..
        }

-- | Execute contract similarly to 'interpret' function, but in result
-- produce an entire execution history.
collectInterpretSnapshots
  :: forall cp st arg. StorageScope st
  => MichelsonSource
  -> Contract cp st
  -> EntrypointCallT cp arg
  -> Value arg
  -> Value st
  -> ContractEnv
  -> InterpretHistory InterpretSnapshot
collectInterpretSnapshots path Contract{..} epc param initStore env =
  runCollectInterpretSnapshots
    path
    collectedOperations
    env
    collSt
    (SomeStack initStack)
  where
    initStack = rmap (MkStkEl NoStkElMeta) $ mkInitStack (liftCallArg epc param) initStore
    initStackFrame = [StackFrame "code" Nothing mempty]
    initSt =
      InterpreterState (ceMaxSteps env)
        dummyGlobalCounter dummyBigMapCounter
    collSt = CollectorState initSt initStackFrame 0 Nothing NoInstr Nothing
    collectedOperations = fmap ContractFinalStack $
      runInstrCollect path (unContractCode cCode) initStack

collectInterpretSnapshotsForView
  :: forall st arg ret. ViewableScope ret
  => MichelsonSource
  -> View arg st ret
  -> Value arg
  -> Value st
  -> ContractEnv
  -> InterpretHistory InterpretSnapshot
collectInterpretSnapshotsForView path onChainView param initStore env =
  runCollectInterpretSnapshots
    path
    collectedOperations
    env
    collSt
    (SomeStack initStack)
  where
    initStack = MkStkEl NoStkElMeta (VPair (param, initStore)) :& RNil
    initStackFrame = [StackFrame "view" Nothing mempty]
    initSt =
      InterpreterState (ceMaxSteps env)
        dummyGlobalCounter dummyBigMapCounter
    collSt = CollectorState initSt initStackFrame 0 Nothing NoInstr Nothing
    collectedOperations = fmap ViewFinalStack $
      runInstrCollect path (vCode onChainView) initStack
