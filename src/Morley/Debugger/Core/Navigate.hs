-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Debugger state, navigation within execution history.
module Morley.Debugger.Core.Navigate
  ( mkTapeL
  , tapeFocus
  , tapeNext
  , tapePrev
  , tapeRemainingAtRight

  , OldNew (..)

    -- Commonly useful helpers
  , MonadWriter
  , Writer
  , WriterT
  , runWriter
  , runWriterT
  , evalWriter
  , evalWriterT

  , DebugSource (..)
  , mkDebugSource
  , dsBreakpoints
  , dsInstrLocations
  , dsSnapshots
  , dsSources

  , SourceLocation' (..)
  , SourceLocation
  , groupSourceLocations

  , DebuggerState (..)
  , playInterpretHistory
  , getFutureSnapshotsNum

  , Frozen (..)
  , frozen
  , FrozenPredicate (..)
  , runFrozenPredicate
  , UnfrozenLocally (..)
  , unfreezeLocally
  , unfreezeLocallyW

  , SnapshotEdgeStatus (..)
  , SnapshotEndedWith (..)
  , NavigableSnapshot (..)
  , NavigableSnapshotWithMethods (..)
  , FrameDeleteAmt (..)

  , curSnapshot
  , nextSnapshot
  , isBreakpoint
  , isAtBreakpoint
  , goesAfter
  , goesBefore
  , matchesSrcType
  , Direction (..)
  , reverseDirection
  , MovementResult (..)
  , moveSucceeded
  , TraversedSnapshots (..)
  , tsBeforeInstrs
  , tsAfterInstrs
  , tsAllVisited
  , HistoryReplay
  , HistoryReplayM
  , move
  , moveRaw
  , moveTill
  , moveWhileNot

  , moveOutsideMethod
  , moveSkippingMethods
  , moveToStartFrame
  , BreakpointId (..)
  , PausedReason (..)
  , boolToMaybe
  ) where

import Control.Lens (at, ix, makeLenses, non, (.=), (?~))
import Control.Monad.Writer (MonadWriter, Writer, WriterT, runWriter, runWriterT, tell)
import Data.DList (DList(..))
import Data.DList qualified as DDL
import Data.Map qualified as M
import "morley-prelude" Fmt (Buildable(..))

import Morley.Debugger.Core.Common (SrcLoc(..))
import Morley.Debugger.Core.Snapshots
import Morley.Debugger.Protocol.Instances ()
import Morley.Michelson.Interpret (MichelsonFailureWithStack)
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Util.TypeLits

import Protocol.DAP (BreakpointId(..))

-- Utilities
----------------------------------------------------------------------------

-- | List with a focus on an element, aka zipper.
--
-- Had to reimplement our own.
--
-- * 'list-zipper' package has too strict cabal deps to work with our lts;
-- * @ListZipper@ provides interface where at some given moment there can be no
--    focused element, this is too inconvenient for us.
data Tape a = Tape [a] a [a]
  deriving stock (Show, Generic)
  deriving anyclass Buildable

-- | Make up a tape with initial focus at the left end.
mkTapeL :: NonEmpty a -> Tape a
mkTapeL (x :| xs) = Tape [] x xs

tapeFocus :: Tape a -> a
tapeFocus (Tape _ x _) = x

tapeNext :: Tape a -> Maybe (Tape a)
tapeNext = \case
  Tape _ _ [] -> Nothing
  Tape l x (r : rs) -> Just $ Tape (x : l) r rs

tapePrev :: Tape a -> Maybe (Tape a)
tapePrev = \case
  Tape [] _ _ -> Nothing
  Tape (l : ls) x rs -> Just $ Tape ls l (x : rs)

-- | Gets the number of the future entries.
--
-- This will halt if the tape is infinite.
tapeRemainingAtRight :: Tape a -> Int
tapeRemainingAtRight (Tape _ _ r) = length r

-- | Stores values before and after some change.
data OldNew a = OldNew
  { pickOld, pickNew :: a }
  deriving stock (Show, Eq)

evalWriter :: Writer w a -> a
evalWriter = fst . runWriter

evalWriterT :: Functor m => WriterT w m a -> m a
evalWriterT = fmap fst . runWriterT

-- Snapshots navigation
----------------------------------------------------------------------------

-- | All the debug information about a single source.
data DebugSource = DebugSource
  { _dsBreakpoints    :: Map SrcLoc BreakpointId
  -- ^ Map of all the breakpoints' location and IDs.
  , _dsInstrLocations :: Map SrcLoc Int
  -- ^ List of all instructions' locations, enumerated.
  -- (enumeration is global, not on per-source basis)
  } deriving stock (Eq, Ord, Show, Generic)
    deriving anyclass Buildable

-- | Construct a 'DebugSource'.
--
-- This exploits a global counter for creating breakpoints, you must
-- preserve the state when calling 'mkDebugSource' multiple times.
mkDebugSource
  :: [SrcLoc]
     -- ^ All potentially interesting location.
     -- Breakpoints can be created at these locations.
  -> State Int DebugSource
mkDebugSource allLocs = do
  enumeratedLocs <- fmap mconcat $ forM allLocs $ \loc -> state
    \counter -> (one (loc, counter + 1), counter + 1)
  pure DebugSource
    { _dsBreakpoints = mempty
    , _dsInstrLocations = enumeratedLocs
    }

-- | Group source locations by source.
groupSourceLocations :: Ord srcLoc => [SourceLocation' srcLoc] -> Map MichelsonSource (Set (srcLoc, srcLoc))
groupSourceLocations =
  let add (SourceLocation src startPos endPos) = at src . non mempty . at (startPos, endPos) ?~ ()
  in foldl' (flip add) mempty

-- | Debugger state.
data DebuggerState is = DebuggerState
  { _dsSnapshots    :: Tape is
  -- ^ The overall execution history + focus at a specific stage.
  , _dsSources      :: Map MichelsonSource DebugSource
  -- ^ A map containing all source location relating to instruction locations
  -- and breakpoint locations.
  } deriving stock Generic
    deriving anyclass Buildable

-- | Put interpret history on a tape and start playing it from its beginning.
--
-- This is a helper for constructing 'DebuggerState'.
playInterpretHistory :: InterpretHistory is -> Tape is
playInterpretHistory (InterpretHistory twoElemList) = mkTapeL twoElemList

makeLenses ''DebugSource
makeLenses ''DebuggerState

-- | Turns state from 'MonadState' read-only.
newtype Frozen r m a = Frozen (ReaderT r m a)
  deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader r)


newtype FrozenPredicate r m a = FrozenPredicate { unFrozenPredicate :: MaybeT (Frozen r m) a }
  deriving newtype (Functor, Applicative, Monad, Alternative)

instance (Monad m, a ~ ()) => Boolean (FrozenPredicate r m a) where
  (&&) = (>>)
  (||) = (<|>)
  not (FrozenPredicate (MaybeT a)) =
    FrozenPredicate . MaybeT $ maybe pass (const Nothing) <$> a

instance (Monad m, a ~ ()) => BooleanMonoid (FrozenPredicate r m a) where
  true = pass
  false = empty

runFrozenPredicate :: FrozenPredicate r m a -> Frozen r m (Maybe a)
runFrozenPredicate = runMaybeT . unFrozenPredicate

instance ( Monad m
         , TypeError ('Text "Working with state is not allowed here")
         ) => MonadState s (Frozen s m) where
  state = error "impossible"

-- | Run an action with read-only access to state.
--
-- This lifts 'Reader'-like monad into a 'State'-like one.
frozen :: MonadState s m => Frozen s m a -> m a
frozen (Frozen act) = runReaderT act =<< get

-- | Turns env from 'MonadReader' into state, but changes to state will
-- be drop in the end of monadic computation.
--
-- An important property of this monad - it hides the access to the original
-- reader. Without this, it is easy to write a buggy code:
--
-- @
-- readThing = unfreezeLocally $ moveRaw Forward *> curSnapshot
-- @
--
-- In this example, it is usually wanted that @curSnapshot@ accesses the
-- snapshot after movement, but in fact movement is not accounted as it
-- affected @State@ monad and @curSnapshot@ operates on reader.
newtype UnfrozenLocally s m a = UnfrozenLocally (StateT s m a)
  deriving newtype (Functor, Applicative, Monad, MonadIO, MonadState s, MonadWriter w)

instance ( Monad m
         , TypeError ('Text "Working with reader is not allowed here")
         ) => MonadReader r (UnfrozenLocally r m) where
  reader = error "impossible"
  local = error "impossible"

-- | Turn 'State'-like monad into 'Reader'-like one; state changes won't go
-- outside of this call.
unfreezeLocally :: MonadReader r m => UnfrozenLocally r m a -> m a
unfreezeLocally (UnfrozenLocally act) = ask >>= evalStateT act

-- | Like 'unfreezeLocally', plus allows 'Writer' operations, with the produced
-- output being discarded in the end.
unfreezeLocallyW
  :: (MonadReader r m, Monoid w)
  => UnfrozenLocally r (WriterT w m) a -> m a
unfreezeLocallyW = fmap fst . runWriterT . unfreezeLocally

-- | Get the snapshot at the current position.
curSnapshot
  :: MonadReader (DebuggerState is) m
  => m is
curSnapshot = tapeFocus <$> view dsSnapshots

-- | Get the snapshot at the next position.
nextSnapshot
  :: MonadReader (DebuggerState is) m
  => m (Maybe is)
nextSnapshot = do
  tape <- view dsSnapshots
  pure $ tapeFocus <$> tapeNext tape

-- | How many snapshots remain to play.
--
-- Formally, this is the number of @moveRaw@ calls we can make forward while
-- still getting 'MovedSuccessfully'.
--
-- NOTE: this will force the entire contract computation!
-- Use in tests only.
getFutureSnapshotsNum
  :: (MonadReader (DebuggerState is) m)
  => m Int
getFutureSnapshotsNum =
  tapeRemainingAtRight <$> view dsSnapshots

-- | Status of execution at specific snapshot.
data SnapshotEdgeStatus
  = SnapshotIntermediate
    -- ^ Intermediate snapshot.
  | SnapshotAtEnd SnapshotEndedWith
    -- ^ Last snapshot, with result of execution.

data SnapshotEndedWith
  = SnapshotEndedWithOk FinalStack
  | SnapshotEndedWithFail (MichelsonFailureWithStack DebuggerFailure)

-- | Class that allows for some method to navigate over interpreter history.
class NavigableSnapshot is where
  -- | Get index of the instruction that is currently being executed.
  --
  -- Here and further, under "currently being executed" we will mean
  -- the place in code indicated by the respective marker in UI.
  --
  -- For some implementations this may require the access to the adjacent
  -- snapshots on the tape, thus requiring @MonadReader DebuggerState@.
  getExecutedPosition :: MonadReader (DebuggerState is) m => m (Maybe SourceLocation)

  -- | Get index of the previously executed instruction (if any).
  --
  -- This is called right after doing the step forward and should point
  -- to the @currently executed instruction@ as per the previous state.
  -- This may return the same value as 'getExecutedPosition'.
  --
  -- For some implementations this may require the access to the adjacent
  -- snapshots on the tape, thus requiring @MonadReader DebuggerState@.
  getLastExecutedPosition :: MonadReader (DebuggerState is) m => m (Maybe SourceLocation)

  -- | Get edge status of the snapshot.
  pickSnapshotEdgeStatus :: is -> SnapshotEdgeStatus

-- | Class that allows navigation that accounts for method calls and thus
-- allows supporting Step Over & Step Out.
class NavigableSnapshot is => NavigableSnapshotWithMethods is where
  -- | Get the current method block level - the length of the calls stack.
  -- Used in @StepOut@ implementation.
  getCurMethodBlockLevel :: MonadReader (DebuggerState is) m => m Int

  -- | Get the number of stack frames, useful for moving to a specific stack frame.
  -- Used in @RestartFrameRequest@ implementation.
  getStackFramesAmt :: MonadReader (DebuggerState is) m => m Int
  getStackFramesAmt =
    -- In some case, the method block level is the same as the stack frame amounts.
    getCurMethodBlockLevel


instance NavigableSnapshot InterpretSnapshot where
  getLastExecutedPosition =
    (\is ->
        case issStatus is of
          InterpretRunning InterpretRunningArg{..} -> Just iraLastLocation
          _ -> Nothing
    ) <$> curSnapshot
  getExecutedPosition = unfreezeLocallyW do
    move Forward >>= \case
      HitBoundary -> return Nothing
      _ -> frozen getLastExecutedPosition
  pickSnapshotEdgeStatus InterpretSnapshot{..} = case issStatus of
    InterpretRunning _ -> SnapshotIntermediate
    InterpretTerminatedOk InterpretTerminatedOkArg{..} -> SnapshotAtEnd $ SnapshotEndedWithOk itoaFinalStack
    InterpretFailed InterpretFailedArg{..} -> SnapshotAtEnd $ SnapshotEndedWithFail ifaError

instance NavigableSnapshotWithMethods InterpretSnapshot where
  getCurMethodBlockLevel = unfreezeLocallyW do
    move Forward *> (issLastMethodBlockLevel <$> frozen curSnapshot)
  getStackFramesAmt = unfreezeLocallyW do
    move Forward *> (length . issLastStackFrames <$> frozen curSnapshot)

-- | Check if the passed position has one of the breakpoint from the list of breakpoint
isBreakpoint :: MonadReader (DebuggerState is) m => SourceLocation -> m [BreakpointId]
isBreakpoint (SourceLocation source startPos endPos) = do
  sources <- view dsSources
  pure $ case sources ^? ix source of
    Nothing -> []
    Just ds -> mapMaybe isPosInRange . M.toList $ _dsBreakpoints ds
  where
      isPosInRange :: (SrcLoc, BreakpointId) -> Maybe BreakpointId
      isPosInRange (sourcePos, bId) =
        if startPos <= sourcePos && sourcePos <= endPos then Just bId
        else Nothing

isAtBreakpoint
  :: (MonadState (DebuggerState is) m, NavigableSnapshot is)
  => FrozenPredicate (DebuggerState is) m (NonEmpty BreakpointId)
isAtBreakpoint = FrozenPredicate do
  Just pos <- getExecutedPosition
  Just bps <- nonEmpty <$> isBreakpoint pos
  return bps

goesAfter
  :: (MonadState (DebuggerState is) m, NavigableSnapshot is)
  => SrcLoc -> FrozenPredicate (DebuggerState is) m ()
goesAfter loc = FrozenPredicate do
  Just (SourceLocation _ _ endPos) <- getExecutedPosition
  guard (endPos >= loc)

goesBefore
  :: (MonadState (DebuggerState is) m, NavigableSnapshot is)
  => SrcLoc -> FrozenPredicate (DebuggerState is) m ()
goesBefore loc = FrozenPredicate do
  Just (SourceLocation _ startPos _) <- getExecutedPosition
  guard (startPos <= loc)

matchesSrcType
  :: (MonadState (DebuggerState is) m, NavigableSnapshot is)
  => MichelsonSource -> FrozenPredicate (DebuggerState is) m ()
matchesSrcType srcType = FrozenPredicate do
  Just (SourceLocation givenSrcType _ _) <- getExecutedPosition
  guard (givenSrcType == srcType)

-- | Direction of movement over execution history.
data Direction = Backward | Forward
  deriving stock (Show, Eq)

reverseDirection :: Direction -> Direction
reverseDirection = \case
  Forward -> Backward
  Backward -> Forward

-- | Result of internal state after moving snapshots.
data MovementResult p
  = HitBoundary
  -- ^ The tape has previously reached either the beginning or end of
  -- the history, so no movement was performed.
  | ReachedTerminatedOk FinalStack
  -- ^ The tape has reached the last element of the history which is @InterpretTerminatedOk@
  | MovedToException (MichelsonFailureWithStack DebuggerFailure)
  -- ^ The tape has moved but reached a Michelson exception.
  | MovedSuccessfully p
  -- ^ The tape has successfully moved to a normal state.
  deriving stock (Show, Eq, Functor)

-- | Returns @True@ when @MovementResult@ is @MovedSuccessfully@, or @False@
-- otherwise.
moveSucceeded :: MovementResult p -> Bool
moveSucceeded = \case
  MovedSuccessfully _ -> True
  _ -> False

data PausedReason p
  = BreakpointPaused (NonEmpty BreakpointId)
  | PlainPaused
  | OtherPaused p
  deriving stock (Show, Eq, Generic, Functor)

boolToMaybe :: Bool -> Maybe ()
boolToMaybe = \case
  True -> Just ()
  False -> Nothing

-- | For 'move'-like functions - which snapshots have we visited.
newtype TraversedSnapshots is = TraversedSnapshots
  { tsVisited :: DList (OldNew is)
    -- ^ Snapshots that we visited, one entry in list per movement over
    -- executed instruction.
  } deriving newtype (Semigroup, Monoid)

-- | Get snapshots before each executed instruction.
-- The snapshot we finish at is thus excluded.
tsBeforeInstrs :: TraversedSnapshots is -> [is]
tsBeforeInstrs = map pickOld . DDL.toList . tsVisited

-- | Get snapshots after each executed instruction.
-- The snapshot we started from is thus excluded.
tsAfterInstrs :: TraversedSnapshots is -> [is]
tsAfterInstrs = map pickNew . DDL.toList .tsVisited

-- | Get all the snapshots we have touched.
tsAllVisited :: TraversedSnapshots is -> [is]
tsAllVisited (TraversedSnapshots dl) = case dl of
  DDL.Cons s ss -> pickOld s : pickNew s : map pickNew ss
  _ -> []

-- | For __one__ step, record the visited snapshot.
--
-- This is an internal function.
recordingSnapshot
  :: (MonadState (DebuggerState is) m, MonadWriter (TraversedSnapshots is) m)
  => m a -> m a
recordingSnapshot action = do
  pickOld <- frozen curSnapshot
  res <- action
  pickNew <- frozen curSnapshot
  tell $ TraversedSnapshots $ DDL.singleton $ OldNew{..}
  return res

-- | Version of 'move' that does not record traversed snapshots.
moveRaw
  :: (MonadState (DebuggerState is) m, NavigableSnapshot is)
  => Direction -> m (MovementResult ())
moveRaw dir = do
  sps <- use dsSnapshots
  let tapeMove = case dir of { Backward -> tapePrev; Forward -> tapeNext }
  case tapeMove sps of
    Nothing -> pure HitBoundary
    Just sps' -> (dsSnapshots .= sps') $> case pickSnapshotEdgeStatus $ tapeFocus sps' of
      SnapshotAtEnd (SnapshotEndedWithOk finalStack) -> ReachedTerminatedOk finalStack
      SnapshotAtEnd (SnapshotEndedWithFail exception) -> case dir of
        Forward -> MovedToException exception
        Backward -> MovedSuccessfully () -- do not add exception if we're going backwards
      _ -> MovedSuccessfully ()

-- | Described a monad where we can replay the history, moving in different
-- directions through the snapshots, and record what we have visited.
type HistoryReplay is m =
  ( NavigableSnapshot is
  , MonadState (DebuggerState is) m
  , MonadWriter (TraversedSnapshots is) m
  )

-- | Monad that satisfies 'HistoryReplay' constraint.
type HistoryReplayM is m =
  StateT (DebuggerState is) $ WriterT (TraversedSnapshots is) m

-- | Go 1 instruction forward/backward.
--
-- Returns @MovedSuccessfully@ if we successfully moved and reached a normal state.
-- Returns @HitBoundary@ if we have already reached the beginning or the end
-- and no state change was applied.
-- Returns @MovedToException@ if we moved but reached a failure state because
-- instruction execution threw an exception.
-- If you don't care about whether state has changed, then explicitly ignore the
-- result of the call.
-- Note that exceptions are not reported when going backwards.
move
  :: (HistoryReplay is m)
  => Direction -> m (MovementResult ())
move dir = recordingSnapshot $ moveRaw dir

-- | Move over execution history, halting at the snapshot where predicate turns
-- @Just pausedReason@.
--
-- This performs at least one step if we are not yet at beginning/end.
-- If we reach the end of the execution history, we stop there.
-- Returns @MovedSuccessfully PausedReason@ if we advanced at least once and finally
-- found a snapshot where predicate holds.
-- Also returns the visited snapshots.
moveTill
  :: (HistoryReplay is m)
  => Direction -> FrozenPredicate (DebuggerState is) m p -> m (MovementResult p)
moveTill dir predicateM = do
  result <- move dir
  case result of
    MovedSuccessfully _ -> do
      predicate <- frozen $ runFrozenPredicate predicateM
      case predicate of
        Just pausedReason -> pure $ MovedSuccessfully pausedReason
        Nothing -> moveTill dir predicateM
    HitBoundary -> pure HitBoundary
    ReachedTerminatedOk finalStack -> pure $ ReachedTerminatedOk finalStack
    MovedToException err -> pure $ MovedToException err

-- | Like 'moveTill' but check predicate beforehand. Can make zero steps.
-- Returns @MovedSuccessfully PausedReason@ if we found a snapshot where predicate holds.
moveWhileNot
  :: (HistoryReplay is m)
  => Direction -> FrozenPredicate (DebuggerState is) m p -> m (MovementResult p)
moveWhileNot dir predicateM = do
  predicate <- frozen $ runFrozenPredicate predicateM
  case predicate of
    Just pausedReason -> pure $ MovedSuccessfully pausedReason
    Nothing -> move dir >>= \case
      MovedSuccessfully _ -> moveWhileNot dir predicateM
      HitBoundary -> pure HitBoundary
      ReachedTerminatedOk finalStack -> pure $ ReachedTerminatedOk finalStack
      MovedToException err -> pure $ MovedToException err

-- | Move forward until reaching the end of the currently executed method.
moveOutsideMethod
  :: (HistoryReplay is m, NavigableSnapshotWithMethods is)
  => m (MovementResult ())
moveOutsideMethod = do
  targetMethodNestingLevel <- pred <$> frozen getCurMethodBlockLevel
  moveTill Forward $ FrozenPredicate do
    curMethodNestingLevel <- getCurMethodBlockLevel
    guard (curMethodNestingLevel <= targetMethodNestingLevel)

-- | An amount of frames to be removed, used in @moveToStartFrame@.
newtype FrameDeleteAmt = FrameDeleteAmt Int

-- | Move to a the top-most frame, after removing according to the amount.
moveToStartFrame
  :: (HistoryReplay is m, NavigableSnapshotWithMethods is)
  => FrameDeleteAmt -> m (MovementResult ())
moveToStartFrame (FrameDeleteAmt amt) = do
  startStackFrameAmt <- frozen getStackFramesAmt
  result <- moveWhileNot Backward $ FrozenPredicate do
    curStackFrameAmt <- getStackFramesAmt
    guard $ startStackFrameAmt - curStackFrameAmt == (amt + 1)

  -- Step into a method since @moveWhileNot@ will put the cursor 1 step behind the method.
  -- When frame id is equal to the most bottom frame (@code@), we won't do the step.
  if (startStackFrameAmt /= amt + 1) then
    move Forward
  else pure result

-- | Move in the direction, and skip any method call.
moveSkippingMethods
  :: (HistoryReplay is m, NavigableSnapshotWithMethods is)
  => Direction -> m (MovementResult ())
moveSkippingMethods dir = do
  methodNestingLevelBeforeStep <- frozen getCurMethodBlockLevel
  startPos <- frozen getExecutedPosition
  res <- moveTill dir $ FrozenPredicate do
    curMethodNestingLevel <- getCurMethodBlockLevel
    guard (curMethodNestingLevel <= methodNestingLevelBeforeStep)

  -- It may be that we exit the method call returning at the same position that
  -- we started from (example: 'LOOP' call), in such case it would be convenient
  -- if we make one step more.
  endPos <- frozen getExecutedPosition
  case (startPos == endPos, res) of
    (True, MovedSuccessfully _) -> move dir
    _ -> return res
