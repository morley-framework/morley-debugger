-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | Breakpoints functionality
module Morley.Debugger.Core.Breakpoint
  ( switchBreakpoint
  , BreakpointSelector (..)
  , breakpointSelectorFromMaybe
  , continueUntilBreakpoint
  , reverseContinue
  , resetBreakpoints
  ) where

import Control.Lens (ix, (%=), (.=))
import Data.Map qualified as M

import Morley.Debugger.Core.Common
import Morley.Debugger.Core.Navigate
import Morley.Michelson.Parser.Types (MichelsonSource)

-- | Switch breakpoint: set it up if it doesn't exist yet, and vice versa.
-- The idea is to set up a breakpoint on the next closest valid instruction,
-- so if a user set it up on the empty line, actual breakpoint will be set up
-- on the following instruction.
switchBreakpoint :: MonadState (DebuggerState is) m => MichelsonSource -> SrcLoc -> m (Maybe (SrcLoc, Maybe BreakpointId))
switchBreakpoint source pos = do
  DebuggerState{..} <- get
  case _dsSources ^? ix source of
    Just DebugSource{..} -> case M.lookupGE pos _dsInstrLocations of
      Just (nextInstrPos, posId) ->
        case M.lookup nextInstrPos _dsBreakpoints of
          Just _ -> do
            dsSources . ix source . dsBreakpoints %= M.delete nextInstrPos
            pure $ Just (nextInstrPos, Nothing)
          Nothing -> do
            let newBreakpointId = BreakpointId posId
            dsSources . ix source . dsBreakpoints %= M.insert nextInstrPos newBreakpointId
            pure $ Just (nextInstrPos, Just newBreakpointId)
      Nothing ->
        pure Nothing
    Nothing ->
      pure Nothing

-- | Datatype describes how to choose a breakpoint we should stop at.
data BreakpointSelector
  = NextBreak
  -- ^ Just take the next breakpoint in order of execution:
  -- * if we pass this constructor in 'continueUntilBreakpoint', then next breakpoint in forward order
  -- * if we pass this constructor in 'reverseContinue', then next breakpoint in backward order
  | NextBreakAfter SourceLocation
  -- ^ Stop at a breakpoint which is set not before that the specified position in order of execution.

breakpointSelectorFromMaybe :: Maybe SourceLocation -> BreakpointSelector
breakpointSelectorFromMaybe Nothing = NextBreak
breakpointSelectorFromMaybe (Just pos) = NextBreakAfter pos

-- | Continue to pause on the breakpoint where
-- 'getExecutedPosition' is in accordance with passed breakpoint selector.
-- If continuation stumbled upon a breakpoint, @MovedSuccessfully@ will be returned.
continueUntilBreakpoint
  :: (HistoryReplay is m)
  => BreakpointSelector -> m (MovementResult (PausedReason p))
continueUntilBreakpoint = \case
  NextBreak -> do
    moveTill Forward (BreakpointPaused <$> isAtBreakpoint)
  NextBreakAfter (SourceLocation loc startPos _) -> do
    moveTill Forward
      (  (PlainPaused <$ goesAfter startPos)
      *> (PlainPaused <$ matchesSrcType loc)
      *> (BreakpointPaused <$> isAtBreakpoint)
      )

-- | Reverse to pause on the breakpoint where
-- 'getExecutedPosition' is in accordance with passed breakpoint selector.
-- If reverse continuation stumbled upon a breakpoint, @MovedSuccessfully@ will be returned.
reverseContinue
  :: (HistoryReplay is m)
  => BreakpointSelector -> m (MovementResult (PausedReason p))
reverseContinue = \case
  NextBreak -> do
    moveTill Backward (BreakpointPaused <$> isAtBreakpoint)
  NextBreakAfter (SourceLocation loc startPos _) -> do
    moveTill Backward
      (  (PlainPaused <$ goesBefore startPos)
      *> (PlainPaused <$ matchesSrcType loc)
      *> (BreakpointPaused <$> isAtBreakpoint)
      )

-- | Reset all breakpoints of a given file.
resetBreakpoints :: MonadState (DebuggerState is) m => MichelsonSource -> m ()
resetBreakpoints source = dsSources . ix source . dsBreakpoints .= mempty
