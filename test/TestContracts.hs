-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestContracts
  ( simpleContract
  , loopContract
  , infiniteContract
  , failingContract
  , dipIfContract
  ) where

import Lorentz (iAnyCode, pattern I, (#), (:->))
import Lorentz qualified as L
import Morley.Michelson.ErrorPos (ErrorSrcPos(..), srcPos)
import Morley.Michelson.Typed

loc :: Word -> (s :-> t) -> (s :-> t)
loc l i = I $ WithLoc (ErrorSrcPos $ srcPos l 0) (iAnyCode i)

simpleContract :: L.ContractCode () Natural
simpleContract = L.mkContractCode $
  loc 1 L.cdr #
  loc 2 (L.push @Natural 1) #
  loc 3 L.add #
  loc 4 L.nil #
  loc 5 L.pair

loopContract :: L.ContractCode [Natural] Natural
loopContract = L.mkContractCode $
  loc 1 L.car #
  loc 2 (L.push @Natural 0) #
  loc 3 L.swap #
  loc 4 (L.iter (
    -- s = s + 3 * ai
    loc 5 (L.push @Natural 3) #
    loc 6 L.mul #
    loc 7 L.add
  )) #
  loc 8 L.nil #
  loc 9 L.pair

infiniteContract :: L.ContractCode () ()
infiniteContract = L.mkContractCode $
  loc 1 L.drop #
  loc 2 (L.push True) #
  loc 3 (L.loop
    (loc 4 (L.push True))) #
  loc 5 L.unit #
  loc 6 L.nil #
  loc 7 L.pair

failingContract :: L.ContractCode L.MText ()
failingContract = L.mkContractCode $
  L.car # L.failWith

dipIfContract :: L.ContractCode Bool ()
dipIfContract = L.mkContractCode $
  loc 1 L.car #
  loc 2 L.dup #
  loc 3 (L.dip (
         loc 4 (L.push @Natural 1) #
         loc 5 L.drop #
         loc 6
           (L.if_
             (loc 7 $ L.push @Natural 1)
             (loc 8 $ L.push @Natural 0)))) #
  loc 9 L.drop #
  loc 10 L.drop #
  L.unit # L.nil # L.pair
