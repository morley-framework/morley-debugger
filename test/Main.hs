-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Test.Tasty (defaultMainWithIngredients)

import Test.Cleveland.Ingredients (ourIngredients)

import Tree (tests)

main :: IO ()
main = tests >>= defaultMainWithIngredients ourIngredients
