-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

module TestDAP
  ( initializeRequest
  , getContractMetadataRequest
  , launchRequest
  , configurationDoneRequest
  , terminateRequest
  , continueRequest
  , setBreakPointsRequest
  , mkSource
  , sourceBreakpoint
  , stepInRequest
  , stackTraceRequest
  , stackFrame
  , scopesRequest
  , getTheScope
  , variablesRequest
  , variablesRequestWithRef
  , fillVariableEvaluateName
  , evaluateRequest
  ) where

import Named (defaults, paramF, (!))

import Morley.Debugger.Core (InterpretSnapshot, SourceLocation'(..), getLastExecutedPosition)
import Morley.Debugger.DAP.Handlers (fromMichelsonSource, threadId)
import Morley.Debugger.DAP.Types
import Morley.Debugger.DAP.Types.Morley
import Morley.Debugger.DAP.Types.MorleyInstances ()
import Protocol.DAP (mk)
import Protocol.DAP qualified as DAP

initializeRequest :: DAP.InitializeRequest
initializeRequest = mk @DAP.InitializeRequest
  ! #linesStartAt1 True
  ! #columnsStartAt1 True
  ! #adapterID (DAP.AdapterId "Adapter")
  ! defaults

getContractMetadataRequest :: FilePath -> MorleyGetContractMetadataRequest
getContractMetadataRequest file = MorleyGetContractMetadataRequest
  { file = file
  , logDir = Nothing
  }

launchRequest :: FilePath -> Text -> Text -> MorleyLaunchRequest
launchRequest program storage parameter = MorleyLaunchRequest . mkLazyJson $
  mk @MorleyLaunchRequestBody
    ! #noDebug False
    ! #program program
    ! #storage storage
    ! #parameter parameter
    ! defaults

configurationDoneRequest :: DAP.ConfigurationDoneRequest
configurationDoneRequest = DAP.ConfigurationDoneRequest

terminateRequest :: DAP.TerminateRequest
terminateRequest = DAP.TerminateRequest
  { restart = Nothing
  }

continueRequest :: DAP.ContinueRequest
continueRequest = DAP.ContinueRequest
  { threadId = threadId
  , singleThread = Nothing
  }

_reverseContinueRequest :: DAP.ReverseContinueRequest
_reverseContinueRequest = DAP.ReverseContinueRequest
  { threadId = threadId
  , singleThread = Nothing
  }

setBreakPointsRequest :: DAP.Source -> [DAP.SourceBreakpoint] -> DAP.SetBreakpointsRequest
setBreakPointsRequest source breakpoints = mk @DAP.SetBreakpointsRequest
  ! #source source
  ! #breakpoints breakpoints
  ! defaults

mkSource :: HasCallStack => DAPSessionState InterpretSnapshot -> DAP.Source
mkSource st =
  let srcLoc = getLastExecutedPosition (_dsDebuggerState st)
        ?: error "No last exec position"
  in fromMichelsonSource (_slPath srcLoc)

sourceBreakpoint :: Word -> Maybe Word -> DAP.SourceBreakpoint
sourceBreakpoint line column = mk @DAP.SourceBreakpoint
  ! #line line
  ! paramF #column column
  ! defaults

stepInRequest :: DAP.StepInRequest
stepInRequest = mk @DAP.StepInRequest
  ! #threadId threadId
  ! defaults

stackTraceRequest :: DAP.StackTraceRequest
stackTraceRequest = DAP.StackTraceRequest
  { DAP.threadId = threadId
  , startFrame = Just 0
  , levels = Just 1
  , format = Nothing
  }

stackFrame :: Int -> DAP.Source -> Text -> Word -> Word -> DAP.StackFrame
stackFrame id' source' name line column = DAP.StackFrame
  { DAP.id = DAP.StackFrameId id'
  , name
  , source = Just source'
  , line
  , column
  , endLine = Just line
  , endColumn = Just column
  , canRestart = Just True
  , instructionPointerReference = Nothing
  , moduleId = Nothing
  , presentationHint = Nothing
  }

scopesRequest :: DAP.ScopesRequest
scopesRequest = DAP.ScopesRequest
  { DAP.frameId = DAP.StackFrameId 1
  }

-- | Get the first scope.
--
-- Currently we assume that only one scope exists, and if in the future
-- this changes, we might need to select the right scope manually here.
getTheScope :: HasCallStack => DAP.ScopesResponse -> DAP.Scope
getTheScope resp =
  case resp.scopes of
    scope : _ -> scope
    _ -> error "Expected scope, but no scopes were returned"

variablesRequest :: DAP.Scope -> DAP.VariablesRequest
variablesRequest scope = mk @DAP.VariablesRequest
  ! #variablesReference scope.variablesReference
  ! defaults

variablesRequestWithRef :: DAP.VariableId -> DAP.VariablesRequest
variablesRequestWithRef ref = mk @DAP.VariablesRequest
  ! #variablesReference ref
  ! defaults

fillVariableEvaluateName :: DAP.Variable -> DAP.Variable
fillVariableEvaluateName var@DAP.Variable{..} =
  DAP.Variable { DAP.evaluateName = Just var.value, .. }

evaluateRequest :: Text -> DAP.EvaluateRequest
evaluateRequest value = DAP.EvaluateRequest
  { expression = value
  , frameId = Just (DAP.StackFrameId 1)
  , context = Just #variables
  , format = Nothing
  }
