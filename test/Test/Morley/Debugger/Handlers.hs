-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Test.Morley.Debugger.Handlers
  ( test_mockRequests
  , test_exhaustiveness
  ) where

import Control.Concurrent.STM
  (TChan, isEmptyTChan, newTChanIO, peekTChan, stateTVar, tryReadTChan, writeTChan)
import Data.Aeson (ToJSON)
import "morley-prelude" Fmt (pretty)
import Named (defaults, (!))
import System.FilePath (takeFileName, (</>))
import System.IO.Temp (withSystemTempFile)
import Test.HUnit (Assertion, assertEqual, assertFailure)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, testCaseSteps, (@?=))

import Morley.Debugger.Core.Snapshots
import Morley.Debugger.DAP.Handlers (supportedCapabilities)
import Morley.Debugger.DAP.Types
import Morley.Debugger.DAP.Types.Morley
import Protocol.DAP qualified as DAP
import Protocol.DAP.EventBase qualified as DAP
import Protocol.DAP.Mk (mk)
import Protocol.DAP.RequestBase qualified as DAP
import Protocol.DAP.Serve.IO qualified as DAP
import Protocol.DAP.TestUtils (ExhaustivenessError(..), areHandlersExhaustive)
import TestDAP

contractDir :: FilePath
contractDir = "contracts"

contractFile :: FilePath -> FilePath
contractFile name = contractDir </> name

{-# ANN test_mockRequests ("HLint: ignore Reduce duplication" :: Text) #-}

test_mockRequests :: [TestTree]
test_mockRequests =
  [ testRunner "failwith_message2.tz" "Unit" "Pair True \"Failure\""
    \step h reqC stVar ch -> do
      step "Skip to end yields one error"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      stoppedEvent <- assertRead ch
      expectError "Reached FAILWITH instruction with '\"Failure\"' at line 10 char 18." stoppedEvent
      _outputEvent <- assertRead ch

      assertHandle h reqC stVar ch terminateRequest
      _terminatedRes <- assertRead ch
      pass

  , testRunner "compare_sum_with_10.tz" "1" "2"
    \step h reqC stVar ch -> do
      step "Skip to end yields no error"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner "optimized_format.tz" "Unit" "Unit"
    \step h reqC stVar ch -> do
      st <- assertState stVar
      let src = mkSource st
          dummyName = "dummy-contract"
          dummySrc = DAP.mk @DAP.Source
            ! #name dummyName
            ! #path (toString dummyName)
            ! defaults

      step "Setting breakpoints for two distinct sources"
      assertHandle h reqC stVar ch $ setBreakPointsRequest src [sourceBreakpoint 13 (Just 4)]
      assertHandle h reqC stVar ch $ setBreakPointsRequest dummySrc [sourceBreakpoint 11 (Just 2)]
      _bpSrcRes <- assertRead ch
      _bpDummyRes <- assertRead ch
      expectEmpty ch

      step "Continue until source's breakpoint"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      _stopEvt <- assertRead ch
      expectEmpty ch

      assertHandle h reqC stVar ch stackTraceRequest
      stRes <- assertRead ch
      expectStackTrace [stackFrame 1 src "code" 13 5] stRes
      expectEmpty ch

      step "Continue to end"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner "lambdas.tz" "False" "Pair 5 2"
    \step h reqC stVar ch -> do
      st <- assertState stVar
      let src = mkSource st

      step "Add breakpoint inside LAMBDA"
      assertHandle h reqC stVar ch $ setBreakPointsRequest src [sourceBreakpoint 11 Nothing]
      _bpRes <- assertRead ch
      expectEmpty ch

      step "Continue to breakpoint"
      assertHandle h reqC stVar ch $ continueRequest
      _contRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Request stack trace"
      assertHandle h reqC stVar ch $ stackTraceRequest
      stRes <- assertRead ch
      let expectedStkFrames =
            [ stackFrame 1 src "LAMBDA" 11 14
            , stackFrame 2 src "DIP" 28 17
            , stackFrame 3 src "code" 28 5
            ]
      expectStackTrace expectedStkFrames stRes
      expectEmpty ch

      step "Request scopes"
      assertHandle h reqC stVar ch $ scopesRequest
      scopesRes <- extractScope =<< assertRead ch
      expectEmpty ch

      step "Request variables"
      assertHandle h reqC stVar ch $ variablesRequest (getTheScope scopesRes)
      varRes <- assertRead ch
      let expectedVars =
            [ mk @DAP.Variable
                ! #name "Exec"
                ! #value "(5, 2)"
                ! #type "pair int int"
                ! #variablesReference (DAP.VariableId 1)
                ! defaults
                & fillVariableEvaluateName
            ]
      expectVariables expectedVars varRes
      expectEmpty ch

      step "Continue to end"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner ("tezos_examples" </> "mini_scenarios" </> "vote_for_delegate.tz") "Pair (Pair \"tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU\" None) (Pair \"tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU\" None)" "None"
    \step h reqC stVar ch -> do
      st <- assertState stVar
      let src = mkSource st

      step "Spam Step Next until we get to IFCMPEQ"
      replicateM_ 5 do
        assertHandle h reqC stVar ch stepInRequest
        _stepRes <- assertRead ch
        _stopRes <- assertRead ch
        pass
      expectEmpty ch

      step "Assert that we stopped at the correct position"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes1 <- assertRead ch
      let expectedStkFrames1 = [stackFrame 1 src "code" 11 8]
      expectStackTrace expectedStkFrames1 stRes1
      expectEmpty ch

      step "Spam Step Next until we get to the other IFCMPEQ"
      replicateM_ 4 do
        assertHandle h reqC stVar ch stepInRequest
        _stepRes <- assertRead ch
        _stopRes <- assertRead ch
        pass
      expectEmpty ch

      step "Assert that we stopped at the correct position again"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes2 <- assertRead ch
      let expectedStkFrames2 = [stackFrame 1 src "code" 14 12]
      expectStackTrace expectedStkFrames2 stRes2
      expectEmpty ch

      step "Terminate execution"
      assertHandle h reqC stVar ch terminateRequest
      _terminatedRes <- assertRead ch
      pass

  , testRunner "apply.tz" "Unit" "Unit"
    \step h reqC stVar ch -> do
      -- Non-regression for #25.
      st <- assertState stVar
      let src = mkSource st

      step "Add breakpoint inside LAMBDA"
      assertHandle h reqC stVar ch $ setBreakPointsRequest src [sourceBreakpoint 9 Nothing]
      _bpRes <- assertRead ch
      expectEmpty ch

      step "Continue to breakpoint"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Request stack trace"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes <- assertRead ch
      let expectedStkFrames =
            [ stackFrame 1 src "LAMBDA" 9 5
            , stackFrame 2 src "code" 15 15
            ]
      expectStackTrace expectedStkFrames stRes
      expectEmpty ch

      step "Request scopes"
      assertHandle h reqC stVar ch scopesRequest
      scopesRes <- extractScope =<< assertRead ch
      expectEmpty ch

      step "Request variables"
      assertHandle h reqC stVar ch $ variablesRequest (getTheScope scopesRes)
      varRes <- assertRead ch
      let expectedVars =
            [ mk @DAP.Variable
                ! #name "Exec"
                ! #value "(1, (2, (3, 4)))"
                ! #evaluateName "(1, (2, (3, 4)))"
                ! #type "pair int int int int"
                ! #variablesReference (DAP.VariableId 3)
                ! defaults
                & fillVariableEvaluateName
            ]
      expectVariables expectedVars varRes
      expectEmpty ch

      step "Continue to end"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner ("tezos_examples" </> "attic" </> "cps_fact.tz") "42" "42"
    \step h reqC stVar ch -> do
      st <- assertState stVar
      let src = mkSource st

      -- See #499 for context (changed to 15 due to a fixed bug):
      step "Spam Step Next fifteen times"
      replicateM_ 15 do
        assertHandle h reqC stVar ch stepInRequest
        _stepRes <- assertRead ch
        _stopRes <- assertRead ch
        pass
      expectEmpty ch

      step "Request stack trace"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes1 <- assertRead ch
      let expectedStkFrames1 = [stackFrame 1 src "code" 15 12]
      expectStackTrace expectedStkFrames1 stRes1
      expectEmpty ch

      step "Request scopes"
      assertHandle h reqC stVar ch scopesRequest
      scRes <- extractScope =<< assertRead ch
      expectEmpty ch

      step "Request variables and assert that we have parameter 'Some 41' and storage '42'"
      assertHandle h reqC stVar ch $ variablesRequest (getTheScope scRes)
      varRes1 <- assertRead ch
      let expectedVars1 =
            [ mk @DAP.Variable
                ! #name "Cell 1"
                ! #value "Some 41"
                ! #type "option nat"
                ! #variablesReference (DAP.VariableId 1)
                ! defaults
                & fillVariableEvaluateName
            , mk @DAP.Variable
                ! #name "Cell 0"
                ! #value "1"
                ! #type "nat"
                ! defaults
                & fillVariableEvaluateName
            ]
      expectVariables expectedVars1 varRes1
      expectEmpty ch

      step "Step Next one time"
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Request stack trace"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes2 <- assertRead ch
      let expectedStkFrames2 = [stackFrame 1 src "code" 17 16]
      expectStackTrace expectedStkFrames2 stRes2
      expectEmpty ch

      step "Request scopes"
      assertHandle h reqC stVar ch scopesRequest
      scopesRes2 <- extractScope =<< assertRead ch
      expectEmpty ch

      step "Request variables and assert that we have parameter '41' and storage '42'"
      assertHandle h reqC stVar ch $ variablesRequest (getTheScope scopesRes2)
      varRes2 <- assertRead ch
      let expectedVars2 =
            [ mk @DAP.Variable
                ! #name "Some"
                ! #value "41"
                ! #type "nat"
                ! #variablesReference (DAP.VariableId 0)
                ! defaults
                & fillVariableEvaluateName
            , mk @DAP.Variable
                ! #name "Cell 0"
                ! #value "1"
                ! #type "nat"
                ! defaults
                & fillVariableEvaluateName
            ]
      expectVariables expectedVars2 varRes2
      expectEmpty ch

      step "Continue to end"
      assertHandle h reqC stVar ch $ continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner "dips.tz" "Unit" "Unit"
    \step h reqC stVar ch -> do
      st <- assertState stVar
      let src = mkSource st

      step "Jump to the first DIP"
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Check that we are in the correct position"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes1 <- assertRead ch
      expectStackTrace [stackFrame 1 src "code" 10 8] stRes1
      expectEmpty ch

      step "Go inside the first DIP"
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Check that we are in the correct position"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes2 <- assertRead ch
      expectStackTrace [stackFrame 1 src "DIP" 10 13, stackFrame 2 src "code" 10 8] stRes2
      expectEmpty ch

      step "Go to the next instruction in the first DIP (ADD)"
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Check that we are in the correct position"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes3 <- assertRead ch
      expectStackTrace [stackFrame 1 src "DIP" 10 25, stackFrame 2 src "code" 10 8] stRes3
      expectEmpty ch

      step "Go inside the second DIP"
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      assertHandle h reqC stVar ch stepInRequest
      _stepRes <- assertRead ch
      _stopRes <- assertRead ch
      expectEmpty ch

      step "Check that we are in the correct position"
      assertHandle h reqC stVar ch stackTraceRequest
      stRes4 <- assertRead ch
      expectStackTrace [stackFrame 1 src "DIP" 11 13, stackFrame 2 src "code" 11 8] stRes4
      expectEmpty ch

      step "Evaluate request"
      assertHandle h reqC stVar ch $ evaluateRequest "100"
      stRes5 :: DAP.EvaluateResponse <- assertRead ch >>= \msg ->
        case DAP.wantResponseFor @DAP.EvaluateRequest msg of
          Nothing -> assertFailure $ "Expected EvaluateRequest but got: " <> pretty msg
          Just resp -> pure resp
      let expectedRes = DAP.mk @DAP.EvaluateResponse
            ! #result "100"
            ! defaults
      assertEqual "Expected evaluate response" stRes5 expectedRes

      step "Continue to end"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass

  , testRunner "address_with_ep.tz" "Unit" "Unit"
    \step h reqC stVar ch -> do
      step "Step twice until the address variable"
      replicateM_ 2 do
        assertHandle h reqC stVar ch stepInRequest
        _stepRes <- assertRead ch
        _stopRes <- assertRead ch
        pass
      expectEmpty ch

      step "Request scopes"
      assertHandle h reqC stVar ch scopesRequest
      scopesRes <- extractScope =<< assertRead ch
      expectEmpty ch

      step "Request variables"
      assertHandle h reqC stVar ch $ variablesRequest (getTheScope scopesRes)
      varRes <- assertRead ch
      vars <- getVariables varRes

      let addrRef = case vars of
            var:_ -> var.variablesReference
            _ -> error "Expect to have address variable, but there is none."
      assertHandle h reqC stVar ch $ variablesRequestWithRef addrRef
      varRes2 <- assertRead ch

      let expectedVars =
            [ mk @DAP.Variable
                ! #name "address"
                ! #value "tz1gfArv665EUkSg2ojMBzcbfwuPxAvqPvjo"
                ! #type "address"
                ! #__vscodeVariableMenuContext "address"
                ! defaults
                & fillVariableEvaluateName
            , mk @DAP.Variable
                ! #name "entrypoint"
                ! #value "\"foo\""
                ! #type "string"
                ! defaults
                & fillVariableEvaluateName
            ]
      expectVariables expectedVars varRes2
      expectEmpty ch

      step "Continue to end"
      assertHandle h reqC stVar ch continueRequest
      _contRes <- assertRead ch
      pass
  ]
  where
    testRunner
      :: FilePath
      -> Text
      -> Text
      -> ((String -> IO ()) -> Handle -> TVar (DAP.SeqId 'DAP.MessageToAdapter) -> TVar (Maybe (DAPSessionState InterpretSnapshot)) -> TChan DAP.MessageToClient -> Assertion)
      -> TestTree
    testRunner program storage parameter test =
      testCaseSteps testName \step -> withSystemTempFile (takeFileName program) \_ h -> do
        -- Initialization boilerplate:
        let launchReq = launchRequest (contractFile program) storage parameter
        ch <- newTChanIO
        stVar <- newTVarIO Nothing
        reqC <- newTVarIO DAP.initSeqId

        step "Sending get contract metadata request"
        assertHandle h reqC stVar ch $ getContractMetadataRequest program
        _metadataRes <- assertRead ch

        step "Sending initialize request"
        assertHandle h reqC stVar ch initializeRequest
        _initRes <- assertRead ch
        _initEvt <- assertRead ch

        step "Sending launch request"
        assertHandle h reqC stVar ch launchReq
        launchRes <- assertRead ch
        case launchRes of
          (DAP.wantResponseFor @DAP.LaunchRequest -> Just _) -> pass
          (DAP.wantError -> Just err) ->
            assertFailure $ "Could not launch debugger: " <> pretty err.body
          _ -> assertFailure $ "Unexpected response: " <> pretty launchRes

        assertHandle h reqC stVar ch configurationDoneRequest
        _configurationDoneRes <- assertRead ch
        _stopRes <- assertRead ch

        -- Actually start the test:
        test step h reqC stVar ch
        _storageOutputEvent <- assertRead ch
        _terminatedEvent <- assertRead ch

        step "Assert that all requests were read"
        expectEmpty ch
      where
        testName =
          "Run " <> contractFile program
          <> " with storage \"" <> toString storage
          <> "\" and parameter \"" <> toString parameter <> "\""

    assertRead :: HasCallStack => TChan a -> IO a
    assertRead ch = do
      msg <- atomically $ tryReadTChan ch
      maybe (assertFailure "Channel should not be empty but is") pure msg

    assertHandle :: (HasCallStack, DAP.IsRequest req, ToJSON req) => Handle -> TVar (DAP.SeqId 'DAP.MessageToAdapter) -> TVar (Maybe (DAPSessionState InterpretSnapshot)) -> TChan DAP.MessageToClient -> req -> Assertion
    assertHandle h reqCounter stVar ch req = do
      lgVar <- newTVarIO Nothing
      hVar <- newTVarIO (Just h)
      stoppedVar <- newTVarIO False
      let stopper = DAP.StopAdapter $ atomically $ writeTVar stoppedVar True
      seqId <- atomically $ stateTVar reqCounter (runState DAP.nextSeqId)
      reqProc <- DAP.compileHandlers (morleyHandlers stopper)
      usingReaderT (RioContext hVar lgVar stVar) $
        DAP.runRequestsProcessor reqProc
          (atomically . writeTChan ch)
          (DAP.wrapRequest seqId req)

    assertState :: HasCallStack => TVar (Maybe (DAPSessionState InterpretSnapshot)) -> IO (DAPSessionState InterpretSnapshot)
    assertState stVar = maybe (assertFailure "DAPSessionState is Nothing but should be Just") pure =<< readTVarIO stVar

    expectEmpty :: HasCallStack => TChan DAP.MessageToClient -> Assertion
    expectEmpty ch = do
      isEmpty <- atomically $ isEmptyTChan ch
      unless isEmpty do
        msg <- atomically $ peekTChan ch
        assertFailure $ "Channel should be empty but isn't. First message: " <> pretty msg

    expectError :: HasCallStack => Text -> DAP.MessageToClient -> Assertion
    expectError msg = \case
      (DAP.wantEvent @DAP.StoppedEvent -> Just ev) ->
        assertEqual "Exception message" (Just msg) ev.text
      event -> assertFailure $ "Expected StoppedEvent with failure, but got " <> pretty event

    expectStackTrace :: HasCallStack => [DAP.StackFrame] -> DAP.MessageToClient -> Assertion
    expectStackTrace frames = \case
      (DAP.wantResponseFor @DAP.StackTraceRequest -> Just stRes) ->
        assertEqual
          "Stack frames"
          frames
          stRes.stackFrames
      otherRes -> assertFailure $ "Expected StackTraceRequest but got: " <> pretty otherRes

    extractScope :: HasCallStack => DAP.MessageToClient -> IO DAP.ScopesResponse
    extractScope = \case
      (DAP.wantResponseFor @DAP.ScopesRequest -> Just stRes) -> return stRes
      otherRes -> assertFailure $ "Expected ScopesRequest but got: " <> pretty otherRes

    expectVariables :: HasCallStack => [DAP.Variable] -> DAP.MessageToClient -> Assertion
    expectVariables vars = \case
      (DAP.wantResponseFor @DAP.VariablesRequest -> Just varRes) ->
        assertEqual
          "Variables"
          vars
          varRes.variables
      otherRes -> assertFailure $ "Expected StackTraceRequest but got: " <> pretty otherRes

    getVariables :: HasCallStack => DAP.MessageToClient -> IO [DAP.Variable]
    getVariables = \case
      (DAP.wantResponseFor @DAP.VariablesRequest -> Just varRes) ->
        pure $ varRes.variables
      otherRes -> assertFailure $ "Expected VariableRequest but got: " <> pretty otherRes

test_exhaustiveness :: TestTree
test_exhaustiveness =
  testCase "Handlers cover all the requests" $
    areHandlersExhaustive
      (supportedCapabilities @Morley)
      (morleyHandlers (DAP.StopAdapter pass))
      -- TODO [#98] should return Right ()
      @?= Left (ExhaustivenessError $ "attach" :| ["pause", "source"])
