-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Morley.Debugger.Print
  ( test_printValues
  ) where

import Data.Map.Strict qualified as Map
import Data.Set qualified as Set
import "morley-prelude" Fmt (fmt)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase, (@?=))

import Lorentz (mt)
import Morley.Michelson.Interpret (NoStkElMeta(..), StkEl(..))
import Morley.Michelson.Typed (Instr(..), RemFail(..), Value, Value'(..))
import Morley.Michelson.Typed qualified as T

import Morley.Debugger.Core.Print

debugBuildDefault :: DebugBuildable a => a -> LText
debugBuildDefault = fmt . debugBuild DpmNormal

mkStkEl :: Value t -> StkEl NoStkElMeta t
mkStkEl = MkStkEl NoStkElMeta

test_printValues :: [TestTree]
test_printValues =
  [ testCase "Print Int" do
      debugBuildDefault (mkStkEl (VInt 42)) @?= "42"

  , testCase "Print Either (Maybe Bool, String) ()" $
      let stkEl = mkStkEl @('T.TOr ('T.TPair ('T.TOption 'T.TBool) 'T.TString) 'T.TUnit)
            (VOr (Left (VPair (VOption (Just (VBool True)), VString [mt|foo|]))))
      in debugBuildDefault stkEl @?= "Left (Some True, \"foo\")"

  , testCase "Print Map Integer (Set Natural)" $
      let stkEl = mkStkEl
            (VMap (Map.fromList
              [ (VInt 42, VSet (Set.fromList [VNat 4, VNat 2]))
              , (VInt 1337, VSet (Set.fromList [VNat 1, VNat 3, VNat 7]))
              ]))
      in debugBuildDefault stkEl @?= "{42 -> {2, 4}, 1337 -> {1, 3, 7}}"

  , testCase "Print [Map String (Either Int Unit)]" $
      let stkEl = mkStkEl @('T.TList ('T.TMap 'T.TString ('T.TOr 'T.TInt 'T.TUnit)))
            (VList [VMap (one (VString [mt|foo|], VOr (Right VUnit))), VMap mempty])
      in debugBuildDefault stkEl @?= "[{\"foo\" -> Right ()}, {}]"

  , testCase "Print [Either Int Unit]" $
      let stkEl = mkStkEl (VList [VOr (Left (VInt 42)), VOr (Right VUnit)])
      in debugBuildDefault stkEl @?= "[Left 42, Right ()]"

  , testCase "Print [lambda bool bool { UNIT; DROP }]" $
      let stkEl = mkStkEl @('T.TLambda 'T.TBool 'T.TBool)
            (VLam $ T.LambdaCode $ RfNormal $ UNIT `Seq` DROP)
      in fmt @LText (debugBuild DpmEvaluated stkEl)
            @?= "lambda bool bool { UNIT; DROP }"
  ]
