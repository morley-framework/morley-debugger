-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Morley.Debugger.Breakpoint
       ( test_Breakpoints
       ) where

import Control.Lens (ix, (%=))
import Data.Map qualified as M
import Test.HUnit (Assertion, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCaseSteps)

import Lorentz qualified as L
import Morley.Michelson.Runtime.Dummy
import Morley.Michelson.Typed

import Morley.Debugger.Core
import Morley.Debugger.DAP.Types.Morley
import Morley.Debugger.Lorentz
import TestContracts

test_Breakpoints :: [TestTree]
test_Breakpoints =
  [ testCaseSteps "Continue: stop at the end and don't move after that" $
    \step -> testRunner (simpleContract, (), 4) $ do
      liftIO $ step "Setting breakpoint at 3rd line"
      let breakAt = SrcLoc 3 0
      switchBreakpoint lorentzSource breakAt
      liftIO $ step "Continue until the breakpoint"
      continueUntilBreakpoint NextBreak
      checkPosition $ Just breakAt
      liftIO $ step "Continue till the end"
      continueUntilBreakpoint NextBreak
      checkPosition Nothing
      liftIO $ step "Continue again"
      continueUntilBreakpoint NextBreak
      checkPosition Nothing

  , testCaseSteps "Continue without breakpoints" $ \_step -> testRunner (simpleContract, (), 5) $ do
      void $ continueUntilBreakpoint NextBreak
      checkPosition Nothing

  , testCaseSteps "Continue and reverse without breakpoints" $ \step -> testRunner (simpleContract, (), 5) $ do
      liftIO $ step "Continue till the end"
      continueUntilBreakpoint NextBreak
      checkPosition Nothing
      liftIO $ step "Reverse till the beginning"
      reverseContinue NextBreak
      -- We are pointing at the next instruction to be executed
      checkPosition $ Just (SrcLoc 1 0)

  , testCaseSteps "Reverse from the end" $ \step -> testRunner (simpleContract, (), 5) $ do
      liftIO $ step "Continue till the end"
      continueUntilBreakpoint NextBreak
      end <- frozen getExecutedPosition
      liftIO $ end @?= Nothing
      liftIO $ step "Set breakpoint at 2nd line"
      let breakAt = SrcLoc 2 0
      switchBreakpoint lorentzSource breakAt
      liftIO $ step "Reverse until the breakpoint"
      reverseContinue NextBreak
      checkPosition $ Just breakAt

  , testCaseSteps "Сontinue followed by next" $ testRunner (simpleContract, (), 5) . continueThenNext

  , testCaseSteps "Continue + next then reverse + step back" $ \step -> testRunner (simpleContract, (), 5) $ do
      continueThenNext step
      liftIO $ step "Reverse and step back"
      reverseContinue NextBreak
      move Backward
      checkPosition $ Just (SrcLoc 2 0)

  , testCaseSteps "Adjacent breakpoints passed with continue" $ \step -> testRunner (simpleContract, (), 5) $ do
      liftIO $ step "Setting breakpoints at 3rd and 4th lines"
      let breakAt1 = SrcLoc 3 0
          breakAt2 = SrcLoc 4 0
      switchBreakpoint lorentzSource breakAt1
      switchBreakpoint lorentzSource breakAt2
      liftIO $ step "Double continue (stop at the 2nd one)"
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      checkPosition $ Just breakAt2

  , testCaseSteps "Interleaving continue and reverses" $ \step -> testRunner (loopContract, [1, 2, 3], 0) $ do
      liftIO $ step "Set breakpoints"
      let break1 = SrcLoc 2 0
          break2 = SrcLoc 5 0
          break3 = SrcLoc 7 0
      switchBreakpoint lorentzSource break1
      switchBreakpoint lorentzSource break2
      switchBreakpoint lorentzSource break3
      liftIO $ step "Two continues and one reverse back"
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      reverseContinue NextBreak
      checkPosition $ Just break1
      liftIO $ step "Three continues and two reverses"
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      reverseContinue NextBreak
      reverseContinue NextBreak
      checkPosition $ Just break2

  , testCaseSteps "Switching breakpoints during the execution" $ \step -> testRunner (loopContract, [1, 2], 0) $ do
      liftIO $ step "Set 1st breakpoint at 7th line (inside loop)"
      let breakAt1 = SrcLoc 7 0
      switchBreakpoint lorentzSource breakAt1
      liftIO $ step "Continue until the 1st breakpoint"
      continueUntilBreakpoint NextBreak
      checkPosition $ Just breakAt1
      liftIO $ step "Set 2nd breakpoint at 5th line (inside loop)"
      let breakAt2 = SrcLoc 5 0
      switchBreakpoint lorentzSource breakAt2
      liftIO $ step "Continue until the 2nd breakpoint"
      continueUntilBreakpoint NextBreak
      checkPosition $ Just breakAt2
      liftIO $ step "Remove 1st breakpoint at 7th line"
      switchBreakpoint lorentzSource breakAt1
      liftIO $ step "Continue till the end"
      continueUntilBreakpoint NextBreak
      checkPosition Nothing

  , testCaseSteps "Continue with unreachable breakpoints" $ \step -> testRunner (loopContract, [], 0) $ do
      liftIO $ step "Set unreachable breakpoint (inside loop) and continue till the end"
      switchBreakpoint lorentzSource (SrcLoc 7 0)
      continueUntilBreakpoint NextBreak
      checkPosition Nothing

  , testCaseSteps "Continue (with specified position)" $ \step -> testRunner (loopContract, [1, 2], 0) $ do
      liftIO $ step "Set three breakpoints: before, inside and after the loop"
      switchBreakpoint lorentzSource (SrcLoc 2 2) -- breakpoint before the loop, should be set at (3, 0)
      switchBreakpoint lorentzSource (SrcLoc 5 3) -- breakpoint inside the loop, should be set at (6, 0)
      switchBreakpoint lorentzSource (SrcLoc 8 0) -- breakpoint after the loop
      liftIO $ step "Continue till the second break"
      continueUntilBreakpoint (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 4 0))
      checkPosition $ Just (SrcLoc 6 0)
      liftIO $ step "Continue exactly till the third break"
      continueUntilBreakpoint (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 8 0))
      checkPosition $ Just (SrcLoc 8 0)

  , testCaseSteps "Continue (with specified position inside a loop)" $ \step -> testRunner (loopContract, [1, 2], 0) $ do
      liftIO $ step "Set two breakpoints inside the loop"
      switchBreakpoint lorentzSource (SrcLoc 5 0)
      switchBreakpoint lorentzSource (SrcLoc 7 0)
      liftIO $ step "Three continues"
      continueUntilBreakpoint (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 4 0))
      checkPosition $ Just (SrcLoc 5 0)
      continueUntilBreakpoint (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 7 0))
      checkPosition $ Just (SrcLoc 7 0)
      continueUntilBreakpoint (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 7 0))
      checkPosition $ Just (SrcLoc 7 0)

  , testCaseSteps "Reverse (with specified position)" $ \step -> testRunner (loopContract, [1, 2], 0) $ do
      liftIO $ step "Set two breakpoints: before and inside the loop"
      switchBreakpoint lorentzSource (SrcLoc 3 0)
      switchBreakpoint lorentzSource (SrcLoc 6 0)
      liftIO $ step "Continue three times and next"
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      continueUntilBreakpoint NextBreak
      move Forward
      liftIO $ step "Reverse two times"
      reverseContinue (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 6 0))
      checkPosition $ Just $ SrcLoc 6 0
      reverseContinue (NextBreakAfter $ pointSourceLocation lorentzSource (SrcLoc 5 0))
      checkPosition $ Just $ SrcLoc 3 0

  , testCaseSteps "isBreakpoint" $ \_ -> testRunner (dipIfContract, False, ()) $ do

      dsSources . ix lorentzSource . dsBreakpoints %= M.insert (SrcLoc 3 3) (BreakpointId 0)

      testBreakpointIs [BreakpointId 0] (SrcLoc 2 4) (SrcLoc 4 2)

      testBreakpointIs [BreakpointId 0] (SrcLoc 2 0) (SrcLoc 3 4)

      testBreakpointIs [BreakpointId 0] (SrcLoc 3 2) (SrcLoc 4 0)

      testBreakpointIs [BreakpointId 0] (SrcLoc 3 3) (SrcLoc 3 3)

      testBreakpointIs [] (SrcLoc 3 4) (SrcLoc 3 5)

      testBreakpointIs [] (SrcLoc 3 0) (SrcLoc 3 2)

      -- Shouldn't happen but when end pos is actually invalid, still return `[]`
      testBreakpointIs [] (SrcLoc 3 4) (SrcLoc 3 3)
      testBreakpointIs [] (SrcLoc 3 4) (SrcLoc 3 0)


  ]
  where
    testRunner
      :: forall cp st . (IsoValue st, IsoValue cp, ParameterScope (ToT cp), ForbidOr (ToT cp), StorageScope (ToT st))
      => (L.ContractCode cp st, cp, st)
      -> HistoryReplayM InterpretSnapshot IO ()
      -> Assertion
    testRunner (ctr, arg, s) =
      let his = lCollectInterpretSnapshots ctr epcPrimitive arg s dummyContractEnv
          ctrCode = ContractCode . L.iAnyCode $ L.unContractCode ctr
      in evalWriterT . evaluatingStateT (mkMorleyDebuggerState lorentzSource his ctrCode)

    continueThenNext step = do
      liftIO $ step "Setting breakpoint and continue until the it"
      let breakAt = SrcLoc 3 0
      switchBreakpoint lorentzSource breakAt
      continueUntilBreakpoint NextBreak
      liftIO $ step "Move next and check the position"
      move Forward
      cur <- frozen getExecutedPosition
      liftIO $ view slStart <$> cur @?= Just (SrcLoc 4 0)

    checkPosition expectedPos = do
      cur <- frozen getExecutedPosition
      liftIO $ view slStart <$> cur @?= expectedPos

    testBreakpointIs expected startLoc endLoc =
      frozen (isBreakpoint $ SourceLocation lorentzSource startLoc endLoc) >>=
          liftIO . (@?= expected)
