-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Morley.Debugger.Utils
  ( test_filterSnapshotsHistory
  , test_groupSourceLocations
  ) where

import Data.Map qualified as Map
import Data.Set qualified as Set
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Morley.Michelson.ErrorPos (Pos(..), SrcPos(..))
import Morley.Michelson.Parser.Types (MichelsonSource(..))

import Morley.Debugger.Core

test_filterSnapshotsHistory :: TestTree
test_filterSnapshotsHistory = testGroup "Tests on filterSnapshotsHistory / mayMaybeSnapshotHistory"
  [ testCase "Laziness" do
      Just (InterpretHistory (_ :| _)) <- pure $ filterInterpretHistory even $
        InterpretHistory $
        (0 :: Int) :| ([1..5] ++ error "Tried to evaluate the history entirely!")
      pass

  , testCase "Too few snapshots" do
      filterInterpretHistory (== 0) (InterpretHistory ((1 :: Int) :| []))
        @?= Nothing

  , testCase "Simple case" do
      filterInterpretHistory even (InterpretHistory ((0 :: Int) :| [1..5]))
        @?= Just (InterpretHistory (0 :| [2, 4]))

  ]

test_groupSourceLocations :: TestTree
test_groupSourceLocations = testGroup "groupSourceLocations"
  [ testCase "simple case" $
      groupSourceLocations
        [ pointSourceLocation (MSFile "a") (SrcPos (Pos 0) (Pos 0))
        , pointSourceLocation (MSFile "b") (SrcPos (Pos 1) (Pos 1))
        , pointSourceLocation (MSFile "a") (SrcPos (Pos 2) (Pos 2))
        ]
      @?= Map.fromList
        [ ( MSFile "a"
          , Set.fromList
            [ (SrcPos (Pos 0) (Pos 0), SrcPos (Pos 0) (Pos 0))
            , (SrcPos (Pos 2) (Pos 2), SrcPos (Pos 2) (Pos 2))
            ]
          )
        , ( MSFile "b"
          , Set.fromList
            [ (SrcPos (Pos 1) (Pos 1), SrcPos (Pos 1) (Pos 1))
            ]
          )
        ]


  ]
