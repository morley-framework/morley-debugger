-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module Test.Morley.Debugger.Core
  ( hprop_Execution_history_is_lazy
  , test_History_is_correct
  , test_Step_out
  , test_Step_over
  , test_Step_back
  ) where

import Unsafe qualified

import "morley-prelude" Fmt (pretty)
import Hedgehog (Property, PropertyT, property)
import Test.HUnit (Assertion, assertBool, assertFailure, (@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCaseSteps)

import Lorentz qualified as L
import Morley.Michelson.ErrorPos (ErrorSrcPos(..), Pos(..), SrcPos(..), srcPos)
import Morley.Michelson.Interpret
import Morley.Michelson.Parser.Types (MichelsonSource(..))
import Morley.Michelson.Runtime.Dummy
import Morley.Michelson.Typed (Contract, EntrypointCallT, Value, Value'(..), epcPrimitive)
import Morley.Michelson.Typed qualified as T
import Test.Cleveland.Michelson (meanTimeUpperBoundProp, sec)
import Test.Cleveland.Michelson.Import (testTreesWithTypedContract)

import Morley.Debugger.Core
import Morley.Debugger.Core.TestUtil.Reversible
import Morley.Debugger.DAP.Handlers
import Morley.Debugger.DAP.Types
import Morley.Debugger.DAP.Types.Morley
import Morley.Debugger.Lorentz
import TestContracts

hprop_Execution_history_is_lazy :: Property
hprop_Execution_history_is_lazy =
  let
    contract :: L.ContractCode () ()
    contract = infiniteContract

    history env = lCollectInterpretSnapshots contract epcPrimitive () () env
    history' env = InterpretHistory $
      (Unsafe.fromJust . nonEmpty) (take 1000 (toList $ unInterpretHistory $ history env)) <>
      error "Went too far in execution history"

    typedContractCode = T.ContractCode . L.iAnyCode $ L.unContractCode contract

    run env =
      evalWriter $ evaluatingStateT (mkMorleyDebuggerState lorentzSource (history' env) typedContractCode) do
        replicateM_ 3 $ move Forward
        frozen curSnapshot

  in meanTimeUpperBoundProp (sec 1) run dummyContractEnv

test_History_is_correct :: [TestTree]
test_History_is_correct =
  [ testCaseSteps "simple contract" $ \step -> do
      historyTestL simpleContract epcPrimitive () 5 do
        liftIO $ step "Start"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 0
            , issStatus = InterpretRunning _
              -- TODO [#93]: compare stacks as well
            } -> pass
          sp -> unexpectedSnapshot sp

        moved1 <- move Forward
        liftIO $ assertBool "'move' succeeded" (moveSucceeded moved1)

        liftIO $ step "After 1 step"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 1
            , issStatus = InterpretRunning _
            } -> pass
          sp -> unexpectedSnapshot sp

        replicateM_ 5 $ move Forward
        liftIO $ step "After more steps"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 6
            , issStatus = InterpretTerminatedOk _
            } -> pass
          sp -> unexpectedSnapshot sp

        moved3 <- move Forward
        liftIO $ assertBool "'move' in the end succeeded" (not $ moveSucceeded moved3)

        replicateM_ 6 $ move Backward
        liftIO $ step "Go to the beginning"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 0
            , issStatus = InterpretRunning _
            } -> pass
          sp -> unexpectedSnapshot sp

        replicateM_ 3 $ move Forward
        liftIO $ step "Go to the middle"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 3
            , issStatus = InterpretRunning _
            } -> pass
          sp -> unexpectedSnapshot sp

  , testCaseSteps "failing contract" $ \step -> do
      historyTestL failingContract epcPrimitive [L.mt|aa|] () do
        replicateM_ 999 $ move Forward
        liftIO $ step "Check end"
        checkSnapshot $ \case
          InterpretSnapshot
            { issStatus = InterpretFailed
              (InterpretFailedArg
                (MichelsonFailureWithStack
                  (MichelsonFailedWith (VString [L.mt|aa|]))
                  (ErrorSrcPos (SrcPos (Pos 0) (Pos 0))))
                _
              )
            } -> pass
          sp -> unexpectedSnapshot sp

  , testCaseSteps "loop contract" $ \step -> do
        historyTestL loopContract epcPrimitive ([1, 2] :: [Natural]) 0 do
          replicateM_ 4 $ move Forward
          liftIO $ step "After 4 steps stop at LOOP"
          checkSnapshotVirtualPos 4 4

          replicateM_ 3 $ move Forward
          liftIO $ step "After 7 steps stop at the last instruction in the loop"
          checkSnapshotVirtualPos 7 7

          replicateM_ 1 $ move Forward
          liftIO $ step "After 8 steps stop at LOOP again"
          checkSnapshotVirtualPos 8 4

  , testCaseSteps "method blocks are well assigned" $ \step -> do
        historyTestL loopContract epcPrimitive ([1, 2] :: [Natural]) 0 do
          checkSnapshot $ \case
            InterpretSnapshot{ issLastMethodBlockLevel = 0 } -> pass
            sp -> unexpectedSnapshot sp

          replicateM_ 4 $ move Forward
          liftIO $ step "Approached loop"

          checkSnapshot $ \case
            InterpretSnapshot{ issLastMethodBlockLevel = 0 } -> pass
            sp -> unexpectedSnapshot sp

          move Forward
          liftIO $ step "Entered loop"
          checkSnapshotVirtualPos 5 5

          checkSnapshot $ \case
            InterpretSnapshot{ issLastMethodBlockLevel = 1 } -> pass
            sp -> unexpectedSnapshot sp

          void $ move Forward

          checkSnapshot $ \case
            InterpretSnapshot{ issLastMethodBlockLevel = 1 } -> pass
            sp -> unexpectedSnapshot sp

  , testCaseSteps "DIP with IF contract" $ \step -> do
        historyTestL dipIfContract epcPrimitive False () do
          replicateM_ 3 $ move Forward
          liftIO $ step "After 3 steps stop at DIP"
          checkSnapshotVirtualPos 3 3

          replicateM_ 3 $ move Forward
          liftIO $ step "After 6 steps stop at IF"
          checkSnapshotVirtualPos 6 6

          replicateM_ 1 $ move Forward
          liftIO $ step "After 7 steps stop at false-branch"
          checkSnapshotVirtualPos 7 8

          replicateM_ 1 $ move Forward
          liftIO $ step "After 8 go out of the DIP and IF"
          checkSnapshotVirtualPos 8 9
  ]

test_Step_out :: IO [TestTree]
test_Step_out =
  testTreesWithTypedContract dipContractPath $ \dipContract ->
  testTreesWithTypedContract lambdaContractPath $ \lambdaContract ->

  pure
  [ testCaseSteps "DIP" $ \step -> do
      historyTest dipContract dipContractPath epcPrimitive (L.toVal ()) (L.toVal ()) do
        replicateM_ 3 $ move Forward
        checkSnapshotCurPos 10 8

        replicateM_ 2 $ move Forward
        liftIO $ step "Jumped inside the first DIP"
        checkSnapshotCurPos 10 25

        _ <- moveOutsideMethod
        liftIO $ step "Step out from DIP"
        checkSnapshot $ \case
          InterpretSnapshot
            { issStatus = InterpretTerminatedOk _
            } -> pass
          snapshot -> unexpectedSnapshot snapshot

  , testCaseSteps "LAMBDA" $ \step -> do
      historyTest lambdaContract lambdaContractPath epcPrimitive (L.toVal ()) (L.toVal ()) do
        replicateM_ 8 $ move Forward
        checkSnapshotCurPos 15 15

        move Forward
        liftIO $ step "Jumped to LAMBDA"
        checkSnapshotCurPos 9 5

        _ <- moveOutsideMethod
        liftIO $ step "Step out from LAMBDA"
        checkSnapshotCurPos 16 3

  , testCaseSteps "While `StepIn` request does a step forward when there is no lambda to step into, \
      \ `StepOut` request shouldn't do a step backward when there is no lambda to step out from. \
      \ In other word, `StepIn` and `StepOut` requests are not supposed to be opposite." $ \step -> do
      historyTest lambdaContract lambdaContractPath epcPrimitive (L.toVal ()) (L.toVal ()) do
        replicateM_ 9 $ move Forward
        liftIO $ step "Jumped to LAMBDA"
        checkSnapshotCurPos 9 5

        _ <- moveOutsideMethod
        liftIO $ step "Step out from LAMBDA"
        checkSnapshotCurPos 16 3

        _ <- moveOutsideMethod
        liftIO $ step "Step out again should terminate and not going back."
        checkSnapshot $ \case
          InterpretSnapshot
            { issStatus = InterpretTerminatedOk _
            } -> pass
          snapshot -> unexpectedSnapshot snapshot

  , testCaseSteps "Restart frame" $ \_ -> do
    historyTest lambdaContract lambdaContractPath epcPrimitive (L.toVal ()) (L.toVal ()) do

      -- Jump into deepest DIP (4 stack frames)
      replicateM_ 13 $ move Forward
      checkSnapshotCurPos 9 31

      -- Restart frame on first `DIP
      moveToStartFrame (FrameDeleteAmt 0)
      checkSnapshotCurPos 9 31

      -- Restart frame on second `DIP`
      moveToStartFrame (FrameDeleteAmt 1)
      checkSnapshotCurPos 9 18

      -- Restart frame on `LAMBDA`
      moveToStartFrame (FrameDeleteAmt 1)
      checkSnapshotCurPos 9 5

      -- Restart frame on `LAMBDA` again, pos should stay the same
      moveToStartFrame (FrameDeleteAmt 0)
      checkSnapshotCurPos 9 5

      -- Restart frame on `code`
      moveToStartFrame (FrameDeleteAmt 1)
      checkSnapshotCurPos 8 3

  ]
  where
    dipContractPath = "contracts/dips.tz"
    lambdaContractPath = "contracts/apply.tz"

test_Step_over :: IO [TestTree]
test_Step_over =
  testTreesWithTypedContract "contracts/failwith_message2.tz" $ \failContract ->

  pure
  [ testCaseSteps "Loop" $ \step -> do
      historyTestL loopContract epcPrimitive ([1..5] :: [Natural]) 5 do
        replicateM_ 3 $ move Forward
        liftIO $ step "Approached Iter"
        checkSnapshotVirtualCurPos 4 4

        _ <- moveSkippingMethods Forward
        liftIO $ step "Executed Iter"
        checkSnapshotVirtualCurPos 25 8

  , testCaseSteps "Dip" $ \step -> do
      -- The point is that we should /not/ skip 'dip' blocks.
      historyTestL loopContract epcPrimitive ([1..5] :: [Natural]) 5 do
        replicateM_ 3 $ moveSkippingMethods Forward
        liftIO $ step "Entered dip"
        checkSnapshotVirtualCurPos 4 4

  , testCaseSteps "Failure" $ \step -> do
      let failContractPath = "contracts/failwith_message2.tz"
      -- At some point I made a wrong implementation so that "Step over"
      -- stepped over 'FAILWITH' instructions without displaying an error, just
      -- quiting execution.
      historyTest failContract failContractPath epcPrimitive (L.toVal (True, [L.mt||])) (L.toVal ()) do
        replicateM_ 5 $ move Forward
        res <- moveSkippingMethods Forward
        liftIO $ step "Executed FAILWITH"
        liftIO $ res @?= MovedToException
          (MichelsonFailureWithStack
            (MichelsonFailedWith (L.toVal [L.mt||]))
            (ErrorSrcPos (srcPos 9 17)))
  ]

test_Step_back :: [TestTree]
test_Step_back =
  [ testGroup "Step back is reverse to Next"
    [ testProperty "Contract with DIPs" $ property $
        historyPropertyTestL dipIfContract epcPrimitive True () do
          oneStepReversed
            (lift . lift)
            defaultOneStepReversedSettings
            (processStep @Morley)
            ()

    , testProperty "Contract with loops" $ property $
        historyPropertyTestL loopContract epcPrimitive ([1..10] :: [Natural]) 5 do
          oneStepReversed
            (lift . lift)
            defaultOneStepReversedSettings
            (processStep @Morley)
            ()

    ]
  ]

historyTest
  :: forall cp arg st. T.StorageScope st
  => Contract cp st
  -> FilePath
  -> EntrypointCallT cp arg
  -> Value arg
  -> Value st
  -> HistoryReplayM InterpretSnapshot IO ()
  -> Assertion
historyTest contr filePath epc arg st =
  let
    his = collectInterpretSnapshots (MSFile filePath) contr epc arg st dummyContractEnv
  in
  evalWriterT . evaluatingStateT (mkMorleyDebuggerState lorentzSource his $ T.cCode contr)

historyTestL
  :: forall cp arg st
   . (T.IsoValue arg, T.IsoValue st, T.ParameterScope (T.ToT cp), T.StorageScope (T.ToT st))
  => L.ContractCode cp st
  -> L.EntrypointCall cp arg
  -> arg
  -> st
  -> HistoryReplayM InterpretSnapshot IO ()
  -> Assertion
historyTestL contr epc arg st =
  let his = lCollectInterpretSnapshots contr epc arg st dummyContractEnv
      typedContrCode = T.ContractCode . L.iAnyCode $ L.unContractCode contr
  in evalWriterT .
     evaluatingStateT
      (mkMorleyDebuggerState lorentzSource his typedContrCode)

historyPropertyTestL
  :: forall cp arg st
   . (T.IsoValue arg, T.IsoValue st, T.ParameterScope (T.ToT cp), T.StorageScope (T.ToT st))
  => L.ContractCode cp st
  -> L.EntrypointCall cp arg
  -> arg
  -> st
  -> HistoryReplayM InterpretSnapshot (PropertyT IO) ()
  -> PropertyT IO ()
historyPropertyTestL contr epc arg st =
  let his = lCollectInterpretSnapshots contr epc arg st dummyContractEnv
      typedContrCode = T.ContractCode . L.iAnyCode $ L.unContractCode contr
  in evalWriterT .
     evaluatingStateT
      (mkMorleyDebuggerState lorentzSource his typedContrCode)

unexpectedSnapshot
  :: HasCallStack => InterpretSnapshot -> Assertion
unexpectedSnapshot sp =
  assertFailure $ "Unexpected snapshot:\n" <> pretty sp

checkSnapshotPos
  :: (HasCallStack, MonadState (DebuggerState InterpretSnapshot) m, MonadIO m)
  => Word -> Word -> m ()
checkSnapshotPos row col = do
  checkSnapshot \case
    InterpretSnapshot
        { issStatus = InterpretRunning
            (InterpretRunningArg _ (SourceLocation _ (toVSCodeLoc -> VSCodeSrcLoc row' col') _) _)
        } | row == row' && col == col' -> pass
          -- ↑ adjusted VSCode coordinates to our internal ones
    sp -> unexpectedSnapshot sp

checkSnapshotCurPos
  :: (HasCallStack, MonadState (DebuggerState InterpretSnapshot) m, MonadIO m)
  => Word -> Word -> m ()
checkSnapshotCurPos row col = frozen $ unfreezeLocally do
  _ <- moveRaw Forward
  checkSnapshotPos row col

-- | Version for our own test contracts written in Lorentz.
checkSnapshotVirtualPos
  :: (HasCallStack, MonadState (DebuggerState InterpretSnapshot) m, MonadIO m)
  => Word -> Word -> m ()
checkSnapshotVirtualPos sn l = do
  checkSnapshot \case
    InterpretSnapshot
      { issSnapNo = SnapshotNo sn1
      , issStatus = InterpretRunning
          (InterpretRunningArg _ (SourceLocation _ (SrcLoc l1 0) _) _)
      } | sn == sn1 && l == l1 -> pass
    sp -> unexpectedSnapshot sp

checkSnapshotVirtualCurPos
  :: (HasCallStack, MonadState (DebuggerState InterpretSnapshot) m, MonadIO m)
  => Word -> Word -> m ()
checkSnapshotVirtualCurPos sn l = frozen $ unfreezeLocally do
  _ <- moveRaw Forward
  checkSnapshotVirtualPos sn l

checkSnapshot
  :: (MonadState (DebuggerState InterpretSnapshot) m, MonadIO m)
  => (InterpretSnapshot -> Assertion)
  -> m ()
checkSnapshot check = frozen curSnapshot >>= liftIO . check
