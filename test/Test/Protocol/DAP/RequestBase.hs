-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Test.Protocol.DAP.RequestBase
  ( test_Handlers_duplication_checker
  , test_Handlers_exhaustiveness_checker
  , test_Communication
  ) where

import Control.Exception (ErrorCall(..))
import Data.Aeson qualified as Aeson
import Data.Aeson.KeyMap qualified as Aeson.KeyMap
import Data.Default (def)
import Data.List (isInfixOf)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (assertBool, assertFailure, testCase, (@?=))

import Control.Concurrent.STM (flushTQueue, newTQueueIO, writeTQueue)
import Protocol.DAP
import Protocol.DAP.Log
import Protocol.DAP.TestUtils

dummyHandler :: forall r m. IsRequestToAdapter r => Handler m
dummyHandler = mkHandler @r \_ -> error "handler invoked"

test_Handlers_duplication_checker :: [TestTree]
test_Handlers_duplication_checker =
  [ testCase "Simple case with no duplication" $
      evaluateWHNF_ $ compileHandlers @(NoLogT IO)
        [ dummyHandler @InitializeRequest
        , dummyHandler @ConfigurationDoneRequest
        ]

  , testCase "Simple case with conflicting handlers" $
      do
        evaluateWHNF_ $ compileHandlers @(NoLogT IO)
          [ dummyHandler @InitializeRequest
          , dummyHandler @ConfigurationDoneRequest
          , dummyHandler @InitializeRequest
          ]
        assertFailure "Unexpectedly no exception thrown"
      `catch` \(ErrorCallWithLocation msg _loc) ->
        assertBool "Unexpected error fired" $
          "conflicting handlers" `isInfixOf` msg

  ]

-- | Test our exhaustiveness checker for DAP handlers.
test_Handlers_exhaustiveness_checker :: [TestTree]
test_Handlers_exhaustiveness_checker =
  [ testCase "Complex case" $
      areHandlersExhaustive
        def
        { supportsConfigurationDoneRequest = Just True
        , supportsRestartFrame = Just True
        , supportsStepBack = Just True
        }
        -- here to the set of mandatory basic handlers
        -- added TerminateRequest handler
        -- and removed SetBreakpointsRequest and StepOutRequest
        --
        -- on the extra handlers,
        -- configurationDone is present,
        -- restartFrame is not, and
        -- stepBack - one handler for it is given and one is not
        [ dummyHandler @InitializeRequest
        , dummyHandler @ConfigurationDoneRequest
        , dummyHandler @LaunchRequest
        , dummyHandler @AttachRequest
        , dummyHandler @DisconnectRequest
        , dummyHandler @TerminateRequest
        , dummyHandler @StepInRequest
        , dummyHandler @NextRequest
        , dummyHandler @ContinueRequest
        , dummyHandler @StepBackRequest
        , dummyHandler @PauseRequest
        , dummyHandler @StackTraceRequest
        , dummyHandler @ScopesRequest
        , dummyHandler @VariablesRequest
        , dummyHandler @SourceRequest
        , dummyHandler @ThreadsRequest
        , dummyHandler @EvaluateRequest
        ]
      @?=
        Left
          (ExhaustivenessError $
            "setBreakpoints" :| ["stepOut", "reverseContinue", "restartFrame"]
          )
  ]

test_Communication :: [TestTree]
test_Communication =
  [ testCase "Sequence numbers and commands are assigned correctly" do
      reqProc <- compileHandlers @(NoLogT IO)
        [ mkHandler \ConfigurationDoneRequest -> respond ()
        , mkHandler \DisconnectRequest{} -> respond ()
        ]
      outputQueue <- newTQueueIO
      let sendReq = runNoLog . runRequestsProcessor reqProc (atomically . writeTQueue outputQueue)

      do
        sendReq RequestMessage
          { seq = SeqId @'MessageToAdapter 100
          , command = "configurationDone"
          , args = Aeson.Array mempty
          }
        outputs <- atomically (flushTQueue outputQueue)
        outputs @?=
          [ ResponseMessageToClient ResponseMessage
            { seq = SeqId @'MessageToClient 1
            , requestSeq = SeqId @'MessageToAdapter 100
            , command = "configurationDone"
            , outcome = Right (Aeson.Array mempty)
            }
          ]

      do
        sendReq RequestMessage
          { seq = SeqId @'MessageToAdapter 150
          , command = "disconnect"
          , args = Aeson.Object mempty
          }
        outputs <- atomically (flushTQueue outputQueue)
        outputs @?=
          [ ResponseMessageToClient ResponseMessage
            { seq = SeqId @'MessageToClient 2
            , requestSeq = SeqId @'MessageToAdapter 150
            , command = "disconnect"
            , outcome = Right (Aeson.Array mempty)
            }
          ]

  , testCase "Request and response bodies are parsed/encoded correctly" do
      reqProc <- compileHandlers @(NoLogT IO)
        [ mkHandler \req@ContinueRequest{} -> liftIO do
            req.threadId @?= ThreadId 5
            req.singleThread @?= Nothing
            respond ContinueResponse{ allThreadsContinued = Just True }
        ]
      outputQueue <- newTQueueIO
      let sendReq = runNoLog . runRequestsProcessor reqProc (atomically . writeTQueue outputQueue)

      do
        sendReq RequestMessage
          { seq = SeqId 1
          , command = "continue"
          , args = Aeson.Object $ Aeson.KeyMap.fromList
              [ ("threadId", Aeson.Number 5)
              ]
          }
        outputs <- atomically (flushTQueue outputQueue)
        outputs @?=
          [ ResponseMessageToClient ResponseMessage
            { seq = SeqId 1
            , requestSeq = SeqId 1
            , command = "continue"
            , outcome = Right . Aeson.Object $ Aeson.KeyMap.fromList
                [ ("allThreadsContinued", Aeson.Bool True)
                ]
            }
          ]



  ]
