-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -Wno-missing-export-lists #-}

module Test.Protocol.DAP.AllTypes
  ( test_Dummy
  ) where

import Data.Aeson (FromJSON(..), ToJSON(..))
import Test.Tasty (TestTree)

import Protocol.DAP.EventTypes
import Protocol.DAP.RequestTypes

-- | Dummy test to satisfy weeder
test_Dummy :: [TestTree]
test_Dummy = []

-- Check that our TH generators cover various cases of types.

_test_ToJSONs :: Identity ()
_test_ToJSONs = do
  let hasToJSON :: forall a m. (ToJSON a, Applicative m) => m ()
      hasToJSON = let _ = toJSON @a in pass

  hasToJSON @CancelResponse
  hasToJSON @ConfigurationDoneResponse
  hasToJSON @ThreadsResponse
  hasToJSON @SetExceptionBreakpointsResponse

  hasToJSON @InitializedEvent
  hasToJSON @TerminatedEvent


_test_FromJSONs :: Identity ()
_test_FromJSONs = do
  let hasFromJSON :: forall a m. (FromJSON a, Applicative m) => m ()
      hasFromJSON = let _ = parseJSON @a in pass

  hasFromJSON @CancelRequest
  hasFromJSON @ConfigurationDoneRequest
  hasFromJSON @RestartRequest
  hasFromJSON @ThreadsRequest
  hasFromJSON @SetExceptionBreakpointsRequest
