## Description

<!--
Describes the nature of your changes. If they are substantial, you should
further subdivide this into a section describing the problem you are solving and
another describing your solution.
-->

## Related issue(s)

<!--
- Short description of how the MR relates to the issue, including an issue link.
For example
- Fixed #999 by adding lenses to exported items

Write 'None' if there are no related issues (which is discouraged).
-->

Resolves #

## :white_check_mark: Checklist for your Merge Request

<!--
Ideally a MR has all of the checkmarks set.

If something in this list is irrelevant to your MR, you should still set this
checkmark indicating that you are sure it is dealt with (be that by irrelevance).

If you don't set a checkmark (e. g. don't add a test for new functionality),
you must be able to justify that.
-->

#### Related changes (conditional)

- Dependencies
    - [ ] We [depend](/stack.yaml) only on package versions that have been released on hackage (i.e., we don't fetch [dependencies from git repositories][git-dep]).
      Failing that, we depend only on commits from the project's `master` branch, and not from any other branch.

- Tests (see [short guidelines](/CONTRIBUTING.md#tests))
  - [ ] If I added new functionality, I added tests covering it.
  - [ ] If I fixed a bug, I added a regression test to prevent the bug from
        silently reappearing again.

- Documentation
  - I checked whether I should update the docs and did so if necessary:
    - [ ] Root [README](/README.md) and other `README.md` files
    - [ ] Haddock
    - [ ] [docs/](/docs/)
  - [ ] I updated changelog files of all affected packages released to Hackage if my changes are externally visible.

#### Stylistic guide (mandatory)

- [ ] My commits comply with [the following policy](https://www.notion.so/serokell/Where-and-how-to-commit-your-work-58f8973a4b3142c8abbd2e6fd5b3a08e).
- [ ] My code complies with the [style guide](/docs/code-style.md).

[git-dep]: https://docs.haskellstack.org/en/stable/yaml_configuration/#extra-deps

#### Implementation concerns

- VSCode plugin
  - [ ] If I changed values in the initial configuration for `launch.json`, I also searched for them in code and updated accordingly. I keep the default configurations and default values for not listed fields in match.
