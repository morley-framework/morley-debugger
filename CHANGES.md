<!-- Unreleased: append new entries here -->

* [!117](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/117)
  `issLastFullStack` field has been renamed to `issFullStack`.
* [!122](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/122)
  LTS-21 update
  * Switched to latest morley
  * Switched to `OverloadedRecordDot` instead of `record-dot-preprocessor`
  * Windows support is disabled as cross-build is broken for GHC 9.4.5
* [!107](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/107)
  Switched to a different DAP Haskell interface. Use `Protocol.DAP` module.
  * Types are now simpler and use less verbose naming. But may require using
    `RecordDotPreprocessor`.
  * Custom request and response types are now declared differently:
    + The types that carry only `seq`, `command`, `body` and maybe other core fields -
      are not necessary anymore, remove them.
    + Rename the `*Body` types by stripping the suffix.
    + In field names remove the common suffix that matches the datatype name
      (you'll need `DuplicatedRecordFields`)
    + Declare `IsRequest` instance for your types, gluing the request, response,
      and the respective command.
    + Use `Text` instead of `String`, appropriate numeric types instead of simple `Int`.
    + Remove `jsonify`, `deriveSum` and other TH calls.

    Example:

    Before:
    ```hs
    data MyRequest = MyRequest
      { seqMyRequest :: Int
      , commandMyRequest :: String
      , bodyMyRequest :: MyRequestBody
      }

    data MyRequestBody = MyRequestBody
      { field1MyRequestBody :: String
      , field2MyRequestBody :: Int
      }
    ```

    Now:
    ```hs
    data MyRequest = MyRequest
      { field1 :: Text
      , field2 :: Word
      }

    instance IsRequest MyRequest where
      type CommandFor MyRequest = "my"
      type ResponseFor MyRequest = ...
    ```
  * Same for events, the migration routine is similar, use `IsEvent` typeclass.
  * Handlers are now run differently.
    + Instead of `handleRequest` use `allDAPHandlers` + `compileHandlers`.
    + Instead of `withDebuggerMain` use `servingRequestsIO` from `Protocol.DAP.IO`.
  * Custom handlers are declared differently.
    + Instead of having a super-datatype for custom requests and responses, you now
      gather a list of `Handler`s.
    + Use `mkHandler` to create a handler.
    + Add the custom handlers to basic handlers before running `compileHandlers`.
    + Use `mapHandler` to update the handler's body after construction.
  * `handlersWrapper` is now gone, use `mapHandler` on your handlers instead.
  * `ShouldStop` is now gone (and shouldn't be used).
  * `writePostAction`, `onStep`, `onTerminate`, `handleSetPreviousStack` are now gone,
    instead update the handler before passing your handlers to 'compileHandlers'
    using `ixHandler` + `embedHandler` or `hoistHandler`.
    For `onTerminate` you can overwrite the handler with a brand new implementation.
  * `handleLaunch` is now replaced with `launchHandler` of `Handler` type.
    `LaunchExt` type family is removed, just specify your request type in the handler.
  * `reportContractLogs` and `reportErrorAndStoppedEvent` now accept `DAPSessionState`
     explicitly (since they don't have error context).
  * Handlers now must use IO-based monad.
    + If you had STM-based handlers, use 'atomically' within the handler's
      implementation.
* [!121](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/121)
  Fixed breakpoints allocation, now all `BreakpointId` should be unique.
  * The contents of `DebugSource` has been changed again. Use `mkDebuggerState` smart constructor for it.
* [!120](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/120)
  Added a generic property test for checking that `Step back` is reverse to `Next`.
  * Also added `unfreezeLocallyW` that, unlike `unfreezeLocally`, can be used with `move`.
* [!114](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/114)
  Update morley &c
* [!118](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/118)
  Fix the hang happening when stepping past `FAILWITH`.
* [!103](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/103)
  Propagate type knowledge of final stack to `writeTerminatedEvent`
  + Added `FinalStack` data representing contract or view final stack.
  + Added `SnapshotEndedWith` data as an argument of `SnapshotEdgeStatus` constructor.
  + Several datas are amended to take `FinalStack` as an argument in their constructors:
    * `InterpretTerminatedOkArg`
    * `SnapshotEndedWith`
    * `MovementResult`
    * `StoppedReason`

0.4.0
=====
* [!105](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/105)
  Update morley to the latest (Mumbainet-ready) release
  + `CollectingEvalOp` is now a newtype
  + `ContractEnv` is defined in `Morley.Debugger.Core.Snapshots` to use
    `CollectingEvalOp`
* [!104](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/104)
  Some minor refactorings.
  + `_dsSource` has been removed. Use the file info returned by `getLastExecutedPosition` instead.
  + Added a related `fromMichelsonSource :: MichelsonSource -> DAP.Source` utility.
* [!101](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/101)
  Applied a few changes that may affect debuggers for other languages.
  + The body of `FrozenPredicate` is now wrapped into `MaybeT`.
  + `&&`, `true`, `and` and other functions work on `FrozenPredicate`.
  + Language server state is now accessible from `Handler` context (see `HandlerEnv`).
  + Added `onStep` extension point to `HasSpecificMessages`.
* [!97](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/97)
  Add some CI scripts from Morley, namely
  + danger scripts
  + `internal-module-check.hs` script
* [!100](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/100)
  + Add `InvalidatedEvent`.
  + Refactored handlers running.
    If you face difficulties with `handlersWrapper`, just make it return `ShouldStop False`
    in case of exceptions.
  + Added `onTerminate` to `HasSpecificMessages`.
* [!93](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/93)
  + Replace reading log directory from `launch.json` with using `context.logUri` method.
  + Write contract log directly in UI, namely to the "Output" lower panel.
* [!89](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/89)
  + Provide breakpoint id in `StoppedEvent`.
* [!87](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/87)
  + Add support for calling a view directly and executing view instruction when the targeted address is self address.
* [!94](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/94)
  + Add a case to handle when the final stack is not a pair, add proper message when terminated due to FAILWITH.
* [!95](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/95)
  + Fix `Open Address in TzKT Explorer` functionality: make it work even if one or more network endpoints are inaccessible.
* [!96](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/96)
  + Update to latest commit of `morley`, which makes the workaround for
  `LOOP` instruction no longer necessary.
* [!85](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/85)
  + Update common config files and some CI scripts in accordance with Morley ones.

0.3.0
=====
* [!92](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/92)
  + Add `NFData` instance for `SrcLoc`.
  + Replace `Terminated` constructor of `StopReason` with two others to separate
    "stopped at terminated ok event" and "tried to pass past finish" cases.
  + Rename `ReachedBoundary` to `HitBoundary`.
* [!90](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/90)
  + Update `morley` and related dependencies to the latest, `lima`-ready, version.
* [!69](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/69)
  + Add the functionality to keep network addresses up-to-date:
    * Read default endpoints from the endpoint config
    * Update endpoint config at each debugger launch in accordance with the remote gitlab config
    * Add the script, that compares network addresses from the endpoint config with the active ones, to CI tests
* [!83](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/83)
  + Show children for address with entrypoint in variables panel.
* [!82](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/82)
  + Replace not valid "Paused on Reached start" message with a valid "Paused on entry" in a call stack pane.
* [!79](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/79)
  + Remove `SourceType` and use `MichelsonSource` instead.
* [!75](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/75)
  + Made `move`-like functions work with `Writer` monad.
  + Rename `moveR` to `move`, `move` to `moveRaw`.
  + Added stepping granularities support, added the respective `processStep` and `parseStepGranularity` functions.
* [!74](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/74)
  + Remove `InterpretStarting` construtor of `InterpretStatus` as it's not informative.
  + Update `InterpretHistory` invariant to carry at least one snapshot but not two.
* [!70](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/70)
  + It is now possible to add a global exceptions handler for DAP handlers.
* [!65](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/65)
  + Move `lastLocation` and `prevStack` fields from `InterpretSnapshot` to `InterpretStatus`.

0.2.1
=====
* [!59](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/59)
  + Bumped dependencies to the latest available version.
  + Updated to a new version of the used LTS and GHC
  + Removed support for the deprecated morley extensions.
  + Removed support for annotations, following `jakarta`'s changes.
* [!61](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/61)
  + Output information after fine termination.
* [!56](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/56)
  + Export helpers from `DAP.Variables` module.
* [!54](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/54)
  + Renamed `reportLogs` method of `HasSpecificMessages` typeclass to
    `reportContractLogs`.
* [!53](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/53)
  + Removed everything related to source mappers, carry source locations instead
    of `InstrNo`s.
  + Removed `curPosition` and `lastSourceLocation` as they now duplicate
    `getExecutedPosition` and `getLastExecutedPosition`.
* [!50](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/50)
  + Added `handleStackTraceDefault` that can be used by debuggers for other
    languages over Michelson.
  + Added `getStopEventDesc` to `HasSpecificMessages`, now one can expose
    events that occur in debugger.
  + Fixed weird `Paused on paused on ...` in `STACK TRACE` pane.
  + Added non-Morley-specific `mkDebuggerState`, renamed the old method to
    `mkMorleyDebuggerState`.
  + Renamed methods of `NavigableSnapshot`:
    * `getNextInstrNo` to `getExecutedInstrNo`;
    * `getLastInstrNo` to `getLastExecutedInstrNo`.
  + Added `Buildable` instances for source-related types.

0.2.0
=====
* [!51](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/51)
  + Replace all occurrences of "debugger adapter" with "debug adapter".
* [!46](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/46)
  + Fix the marker position when reaching the last instruction.
* [!40](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/40)
  + Implement `EvaluateRequest` which allows copying value in the stack.
* [!43](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/43)
  + Mention Open VSX Registry in `morley-debugger-vscode/README.md`.
* [!34](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/34)
  + Bump Stackage LTS version from 17.9 to 18.10.
* [!31](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/31)
  + Implemented different `Step in`, `Step over` and `Step out`.
  + In case when step back moves us beyond the first instruction, do not quit the debugging session.
* [!30](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/30)
  + For some types of parameters / initial storages, try to guess what user inputs.
* [!25](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/25)
  + Read contract environment from config.
  + Add snippets for `launch.json` Michelson debugger config.

0.1.2
=======
* [!26](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/26)
  + Support Edo instructions and types.
* [!21](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/21)
  + Add an option to open an address in TzKT on right click of a value of type `address`
    or `contract`
* [!14](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/14)
  + Fixed an issue that caused the cursor to ignore some Morley extensions, such
    as `STACKTYPE` or typed stack functions.
  + Create a new stack frame when we enter a typed stack function.
  + Use Morley 1.14.0
* [!13](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/13)
  + Added variable annotations to pair value types.
  + Added storage and parameter variable annotations.
* [!22](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/22)
  + Add support for the debugger on Windows
  + Use Morley 1.13.0
  + Bump LTS to 17.3
* [!19](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/19)
  + Type-check parameter and storage values in input boxes

0.1.1
=======
* [!10](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/10)
  + Add entrypoint picker
  + Improve prompts and hints for parameter/storage input fields. Show expected type
* [!9](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/9)
  + Run the debugger adapter in server mode
  + Check on the fly during input that parameter and storage values are parsable
* [!8](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/8)
  + Input boxes are introduced for parameter and storage values
  + Memorization of previously input values is implemented
* [!6](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/6):
  + The plugin is published to openVSX
* [!4](https://gitlab.com/morley-framework/morley-debugger/-/merge_requests/4)
  + Fixed an issue that caused the debugger cursor to incorrectly behave with
    some `IF`-related macros such as `IFCMPEQ` or `IF_SOME`.
  + Fixed an issue that caused the stack when executing an `EXEC` instruction to
    not properly display arguments applied to the `LAMBDA` with `APPLY`.

0.1.0
=======
Initial release.
