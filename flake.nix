# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{
  description = "The morley-debugger flake";

  nixConfig.flake-registry = "https://gitlab.com/morley-framework/morley-infra/-/raw/main/flake-registry.json";

  inputs = {
    morley-infra.url = "gitlab:morley-framework/morley-infra";
    nix-npm-buildpackage.url = "github:serokell/nix-npm-buildpackage";
    openvsx.url = "github:eclipse/openvsx";
    openvsx.flake = false;
    vscode-vsce.url = "github:microsoft/vscode-vsce";
    vscode-vsce.flake = false;
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, morley-infra, nix-npm-buildpackage, ... }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" "x86_64-darwin" ] (system:
      let
        overlay = nixpkgs.lib.composeManyExtensions [
          nix-npm-buildpackage.overlay
          (final: prev: prev.lib.recursiveUpdate prev {
            # silly workaround for https://github.com/input-output-hk/haskell.nix/issues/2008
            haskell-nix.compiler.ghc945 =
              if system == "x86_64-darwin"
              then final.haskell-nix.haskellLib.makeCompilerDeps (
                prev.haskell-nix.compiler.ghc945.overrideAttrs (finalAttrs: prevAttrs:
                  {
                    installPhase = ''
                      export XATTR="$(mktemp -d)/nothing"
                      ${prevAttrs.installPhase}
                    '';
                  }
                )
              )
              else prev.haskell-nix.compiler.ghc945;
          })
        ];

        pkgs = morley-infra.legacyPackages.${system}.extend overlay;

        inherit (morley-infra.utils.${system}) ci-apps;

        # all local packages and their subdirectories
        # we need to know subdirectories for weeder and for cabal check
        local-packages = [
          { name = "morley-debugger"; subdirectory = "."; }
        ];

        # names of all local packages
        local-packages-names = map (p: p.name) local-packages;

        # source with gitignored files filtered out
        projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
          name = "morley-debugger";
          src = ./.;
        };

        # haskell.nix package set
        # parameters:
        # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
        #   This flag basically disables weeder related files production, haddock and enables stripping
        # - optimize -- 'true' to enable '-O1' ghc flag, we intend to use it only in production branch
        hs-pkgs = hs-pkgs-template pkgs;
        hs-pkgs-template = pkgs: { release, static ? true, optimize ? false }:
        let
          haskell-nix = pkgs.haskell-nix;
        in
          haskell-nix.stackProject {
            src = projectSrc;

            # use .cabal files for building because:
            # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
            # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
            ignorePackageYaml = true;

            modules = [
              # common options for all local packages:
              {
                packages = pkgs.lib.genAttrs local-packages-names (packageName: ci-apps.collect-hie release {
                  ghcOptions = with pkgs.lib; (
                    # we use O1 for production binaries in order to improve their performance
                    # for end-users
                    [ (if optimize then "-O1" else "-O0") "-Werror"]
                    # do not erase any 'assert' calls
                    ++ optionals (!release) ["-fno-ignore-asserts"]
                  );
                  dontStrip = !release;  # strip in release mode, reduces closure size
                  doHaddock = !release;  # don't haddock in release mode
                });
              }
            ];
        };

        hs-pkgs-development = hs-pkgs { release = false; };

        hs-pkgs-non-static = hs-pkgs { static = false; release = false; };

        flake = hs-pkgs-development.flake {};

        hs-pkgs-release = (hs-pkgs { release = true; }).flake { crossPlatforms = p: [ p.musl64 ]; };

        # shellFor is borked, but this is an easy workaround
        # (https://github.com/input-output-hk/haskell.nix/issues/1885)
        shellForHack = pkgs: args: pkgs.shellFor (pkgs.args.shell // args);

      in nixpkgs.lib.recursiveUpdate { inherit (flake) packages; } (if system == "x86_64-linux" then {

        devShells = {
          default = (hs-pkgs-non-static.flake {}).devShell;
          hlint = shellForHack hs-pkgs-development {
            tools.hlint = {
              version = "3.5";
              # temporary hack due to lax constraints in `yaml` package
              cabalProjectLocal = ''
                constraints: aeson <2.2
              '';
            };
            nativeBuildInputs = with pkgs; [ danger-gitlab ];
          };
        };

        legacyPackages = pkgs;

        utils = {
          haddock = with pkgs.lib; flatten (attrValues
            (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) { morley-debugger = hs-pkgs-development.morley-debugger.components; }));
          morleyDebuggerDockerImage = { creationDate, tag ? null }:
            pkgs.dockerTools.buildImage {
              name = "morley-debugger";
              contents = (hs-pkgs { release = true; }).morley-debugger.components.exes.morley-debugger-console;
              created = creationDate;
              inherit tag;
            };
          run-internal-modules-check =
            let env = hs-pkgs-development.ghcWithPackages (
                ps: with ps; [ filepath directory ghc-lib-parser ]
              );
            in with pkgs; writeShellScript "run-module-check" ''
              export PATH="${lib.makeBinPath [ env ]}"
              exec ${env.targetPrefix}runghc \
                --ghc-arg=-package --ghc-arg=ghc-lib-parser \
                ${projectSrc}/scripts/internal-module-check.hs ${projectSrc}/code
            '';
        };

        packages = {
          default = self.packages.${system}.all-components;

          all-components = pkgs.linkFarmFromDrvs "all-components"
            (builtins.attrValues (flake).packages);

          morley-debug-adapter = hs-pkgs-release.packages."x86_64-unknown-linux-musl:morley-debugger:exe:morley-debug-adapter";

          morley-debugger-plugin = pkgs.callPackage ./morley-debugger-vscode {
            morley-debug-adapter-static-linux = self.packages.x86_64-linux.morley-debug-adapter;
            morley-debug-adapter-darwin = self.packages.x86_64-darwin.morley-debug-adapter;
          };

          vsce = pkgs.buildYarnPackage {
            src = inputs.vscode-vsce;
            yarnBuild = ''
              yarn
              yarn run compile
            '';
            installPhase = ''
              cp -Lr . $out
              mkdir $out/bin
              ln -s $out/out/vsce $out/bin/vsce
            '';
          };

          ovsx = pkgs.buildYarnPackage {
            src = inputs.openvsx + "/cli";
            installPhase = ''
              cp -Lr . $out
              mkdir $out/bin
              ln -s $out/lib/ovsx $out/bin/ovsx
            '';
          };

          scrape-network-addresses = pkgs.stdenv.mkDerivation rec {
            name = "scrape-network-addresses";
            version = "0.1";
            src = ./scripts/web-scrape-network-addresses.py;
            phases = [ "installPhase" ];
            buildInputs = [ pkgs.makeWrapper ];

            installPhase = let
              python = (pkgs.python39.withPackages(ps: with ps; [ selenium ]));
            in ''
              mkdir -p $out/bin
              echo "python ${src}" >> $out/bin/${name}

              chmod +x $out/bin/${name}
              wrapProgram $out/bin/${name} \
                --prefix PATH : ${python}/bin \
                --prefix PATH : ${pkgs.geckodriver}/bin \
                --prefix PATH : ${pkgs.firefox}/bin
            '';
          };
        };

        checks = {
          run-cabal-check = morley-infra.utils.${system}.run-cabal-check {
            projectSrc = projectSrc;
            inherit local-packages;
          };
          trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;

          reuse-lint = pkgs.build.reuseLint ./.;
        };

        apps = ci-apps.apps {
          hs-pkgs = hs-pkgs-development;
          inherit local-packages projectSrc;
        };
      } else if system == "x86_64-darwin" then {
        packages.morley-debug-adapter =
        let
          relink-script = ./scripts/relink-mac-binary.sh;
        in
          (((hs-pkgs { release = true; }).flake {}).packages."morley-debugger:exe:morley-debug-adapter".overrideAttrs (_: {
            # otherwise, binary is linked with libraries from /nix/store
            postInstall = ''
              mkdir -p $out/lib
              ${relink-script} $out/bin/morley-debug-adapter ../lib
            '';
          }));
      } else {})));
}
