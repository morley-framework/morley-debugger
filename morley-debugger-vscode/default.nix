# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

{ morley-debug-adapter-static-linux, morley-debug-adapter-darwin,
  buildYarnPackage, linkFarm, writeTextFile,
  unzip, zip
}:
let
  morley-debug-adapter-dispatcher = writeTextFile {
    name = "morley-debug-adapter";
    text = ''
      #!/usr/bin/env bash
      "$(dirname "''${BASH_SOURCE[0]}")/$(uname)/bin/morley-debug-adapter" $@
    '';
    executable = true;
  };
  morley-debug-adapter = linkFarm "morley-debug-adapter" [
    { name = "bin/morley-debug-adapter";
      path = morley-debug-adapter-dispatcher;
    }
    { name = "bin/Linux";
      path = morley-debug-adapter-static-linux;
    }
    { name = "bin/Darwin";
      path = morley-debug-adapter-darwin;
    }
  ];
in
buildYarnPackage {
  src = ./.;
  yarnBuild = ''
    mkdir bin
    cp -Lr ${morley-debug-adapter}/* .
    yarn
  '';
  installPhase = ''
    yarn run package
    # https://github.com/microsoft/vscode-vsce/issues/827
    package="$PWD/$(ls *.vsix)"
    mkdir extracted
    ${unzip}/bin/unzip "$package" -d extracted
    find extracted -exec touch -d @$SOURCE_DATE_EPOCH {} \;
    pushd extracted
    ${zip}/bin/zip -r "$package" *
    popd
    mkdir -p $out
    cp "$package" $out
  '';
}
