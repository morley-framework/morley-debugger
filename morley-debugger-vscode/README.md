> **Note: this debugger extension is being deprecated.**
>
> It will no longer be maintained after the activation of protocol "N" on the Tezos mainnet.

# Alpha Morley Debugger VSCode extension

The debugger is available as a
VS Code extension at the [VS Marketplace](https://marketplace.visualstudio.com/items?itemName=serokell-io.michelson-debugger)
and at the [Open VSX Registry](https://open-vsx.org/extension/serokell-io/michelson-debugger).

## Debugger functionality and features

The debugger runs Michelson (`.tz`) contracts.

The debugger supports the following functionalities:
* step-by-step execution, including in reverse order
* breakpoints
* stack during execution
* stack frames

New stack frames are created either when entering `DIP`/`DIP n` instructions,
or a lambda is called by `EXEC`.

The debugger partially supports contracts containing [Sapling](http://tezos.gitlab.io/009/michelson.html#sapling-operations)
types, instructions, and operations
The parsing and typechecking works, but attempting to execute a contract with these will
result in an error.
See the related issues: [morley/#380](https://gitlab.com/morley-framework/morley/-/issues/380) and [morley/#774](https://gitlab.com/morley-framework/morley/-/issues/774).

[Special operations](http://tezos.gitlab.io/009/michelson.html#special-operations)
and some [operations on contracts](http://tezos.gitlab.io/009/michelson.html#operations-on-contracts)
are mocked with dummy values.
Operations produced by instructions neither cause any side effects on the blockchain nor
invoke a contract in case of `TRANSFER_TOKENS` to the contract address.

## Usage

After you've [installed the VS Code extension](#getting-the-extension), you need
to open a folder with a contract in VS Code, and then open the contract itself.

To run a debug session press `F5`. You can also click the Debug icon on the left Activity bar:

![Debug icon](https://gitlab.com/morley-framework/morley-debugger/-/raw/master/morley-debugger-vscode/images/run-debug.png)

and then the `Run and Debug` in the Side bar:

![Run panel](https://gitlab.com/morley-framework/morley-debugger/-/raw/master/morley-debugger-vscode/images/run-panel.png)
or pick `create a launch.json file` to [customize the launch configuration](https://code.visualstudio.com/docs/editor/debugging#_launch-configurations)
and change the debugger's options.

Once your debug session started, if your contract has endpoints you will be prompted to pick one:

![Entrypoints picker](https://gitlab.com/morley-framework/morley-debugger/-/raw/master/morley-debugger-vscode/images/entrypoints.png)

After that you need to input the parameter and storage values.
Their types will be hinted for your convenience:

![Parameter InputBox](https://gitlab.com/morley-framework/morley-debugger/-/raw/master/morley-debugger-vscode/images/parameter.png)

After this is done, execution should start with a cursor on the first instruction in the file.

During the execution of a contract, you have access to four panels on the left side:

![Debugger panels](https://gitlab.com/morley-framework/morley-debugger/-/raw/master/morley-debugger-vscode/images/side-bar.png)

- `Breakpoints` containing active breakpoints with their positions.
- `Variables` containing stack cells.
- `Watch` which is currently empty and not supported yet.
- `Call stack` stack frames, each with a position where it has been entered,
the topmost frame being the current one.


## Further plans

The current version of the debugger is a starting point towards
much more mature ones.

In the next versions, we are going to focus our efforts on the following workstreams:
* developing an extensible debugger protocol for the high-level languages compiled to Michelson (like Ligo and Indigo).

  We are going to come up with, implement, and describe a protocol to make it easier
  for other Michelson-targeting languages to implement specific debuggers.

* extending a contract execution environment.

  This will start with a feature as simple as debugging a Michelson contract
  against user-provided data for instructions like `SENDER`, `SOURCE`, `NOW`, `LEVEL`, etc to make
  debugging process more customizable.

  The next step will move towards connecting the debugger with the real blockchain:
  you'll be able to specify a node to connect for fetching blockchain specific-data,
  with different options to debug a contract that is already on the blockchain.

  After that, the next useful feature will be debugging of invoked external contracts and
  emitting produced operations to the blockchain.

* implementing more convenient features for debugging.
  We'll implement features
  like watching expressions, conditional breakpoints, editing stack cell values during execution, etc.

The focus of the upcoming versions can be seen in the [project milestones](https://gitlab.com/morley-framework/morley-debugger/-/milestones).

## Getting the extension

The simplest way to get the latest extension release is to download it from the
the [VS Marketplace](https://marketplace.visualstudio.com/items?itemName=serokell-io.michelson-debugger)
or the [Open VSX Registry](https://open-vsx.org/extension/serokell-io/michelson-debugger).

Alternatively, it's possible to:
* download the latest version from the repository CI
* build the extension from scratch

These options will be described in depth below.

### Downloading from CI

The latest version of the extension can be downloaded from the project CI.

However, this method only works for Linux-based operating systems,
Windows and macOS users will have to build the adapter executable and replace
the binary in the extension folder (see [the following section](#building-manually)).


To download the extension from CI:
1. Go to the [morley-debugger repo](https://gitlab.com/morley-framework/morley-debugger).
2. Click on the `Pipeline` button of the last successful commit.
3. Click on the `build-morley-debugger-plugin` job.
4. On the right sidebar, download the **Job artifact**.
5. The (unarchived) artifact contains a `.vsix` extension package, which can be
installed following the [instructions here](https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix).

### Building manually

In this section we'll explain how to build and run the extension from scratch.

First of all, you need to have the following things installed:

- [VSCode](https://code.visualstudio.com/)
- [`yarn`](https://classic.yarnpkg.com/)
- Michelson Syntax extension for VSCode, from the [VS Marketplace](https://marketplace.visualstudio.com/items?itemName=baking-bad.michelson) or the [Open VSX Registry](https://open-vsx.org/extension/baking-bad/michelson)
- [The Haskell tool `stack`](https://github.com/commercialhaskell/stack)

After you've made sure you have everything above, you can run `make all` to
build and install the extension, or you can manually:

1. Build and install the debug adapter: `stack install --local-bin-path bin morley-debugger`
2. Run `yarn` and `yarn compile`
3. Run `yarn package`
4. Install `michelson-debugger-<version>.vsix` (replacing `<version>` with the current version)
  following [these instructions](https://code.visualstudio.com/docs/editor/extension-gallery#_install-from-a-vsix).

#### Partial rebuilds and replacements

All VSCode extensions are stored on the disk unpacked (for instance, on Linux
in `~/.vscode/extensions`).

So, you can replace only the executable in the `serokell.michelson-debugger-{version}/bin`
folder if you don't want to rebuild everything from scratch, patch only the adapter
but not JS code, or other similar partial updates

Be aware, however, that this approach is more fragile than the one described above.

### Running in debug mode

This section describes how to run the extension in _development_ mode and debug
the JS part of the code.

_Note: this is based on [these extension debugging instructions](https://vscode-docs.readthedocs.io/en/stable/extensions/debugging-extensions/)
and the `.vscode` JSON options generated by the [`vscode-generator-code`](https://github.com/Microsoft/vscode-generator-code)._

You can run and debug this extension, without compiling and installing the `.vsix`
extension package, by:
1. opening this folder in VSCode
2. running `yarn` to install the dependencies
3. finally pressing `F5`

A new window with the `Extension Development Host` title will open, in which the
`morley-debugger` extension is loaded, where you can make any [usage](#usage) of
it as if you had installed it.

You can now modify the typescript source code of the extension and reload the
window, or use the `Ctrl-Shift-F5` on the first window, to check the compiled changes.

During the debug session, logs will be written to a directory specified by `vscode.ExtensionContext`, `context.logUri` in particular. Usually, it is `~/.vscode-server/data/logs`. There is a splitting into dates and extension launches. In the matching extension launch folder, you could see a `serokell-io.michelson-debugger` directory with contract logs.

In a contract log file you will see something like this:
```haskell
Right (DAPRequest {drSpecific = InitializeRequest (InitializeRequest {seqInitializeRequest = 1, typeInitializeRequest = "request", commandInitializeRequest = "initialize", argumentsInitializeRequest = InitializeRequestArguments {adapterIDInitializeRequestArguments = "michelson", linesStartAt1InitializeRequestArguments = True, columnsStartAt1InitializeRequestArguments = True, pathFormatInitializeRequestArguments = "path"}}), drRaw = Object (fromList [("command",String "initialize"),("arguments",Object (fromList [("clientID",String "vscode"),("supportsVariablePaging",Bool True),("supportsProgressReporting",Bool True),("supportsVariableType",Bool True),("adapterID",String "michelson"),("locale",String "ru"),("columnsStartAt1",Bool True),("supportsRunInTerminalRequest",Bool True),("clientName",String "Visual Studio Code"),("pathFormat",String "path"),("linesStartAt1",Bool True)])),("seq",Number 1.0),("type",String "request")])})

Right (DAPRequest {drSpecific = DisconnectRequest (DisconnectRequest {seqDisconnectRequest = 2, typeDisconnectRequest = "request", commandDisconnectRequest = "disconnect", argumentsDisconnectRequest = Just (DisconnectArguments {restartDisconnectRequestArguments = Just False})}), drRaw = Object (fromList [("command",String "disconnect"),("arguments",Object (fromList [("restart",Bool False)])),("seq",Number 2.0),("type",String "request")])})
```

This means it can successfully parse VSCode messages.
