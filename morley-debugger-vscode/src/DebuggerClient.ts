// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ

import { ProtocolClient } from "vscode-debugadapter-testsupport/lib/protocolClient";
import * as Net from 'net';
import {
    GetContractMetadataArguments,
    GetContractMetadataResponse,
    ParseValueArguments,
    ParseValueResponse
} from "./messages";
import { DebugProtocol } from 'vscode-debugprotocol';
import { OutputChannel } from 'vscode';

export class DebuggerClient extends ProtocolClient {
    socket: Net.Socket
    logOutputChannel: OutputChannel

    constructor(pipeName: string, logOutputChannel: OutputChannel) {
        super();
        this.logOutputChannel = logOutputChannel;

        this.socket = Net.createConnection({ path: pipeName }, () => {
            this.connect(this.socket, this.socket);
        });

        const responsesListener = (bytes: Buffer) => {
            this.logOutputChannel.appendLine(`\nResponse:\n${bytes.toString()}`);
        }
        this.socket.on('data', responsesListener);
    }

    sendMsg(command: 'parseValue', args: ParseValueArguments): Promise<ParseValueResponse>
    sendMsg(command: 'getContractMetadata', args: GetContractMetadataArguments): Promise<GetContractMetadataResponse>

    sendMsg(command: 'parseValue' | 'getContractMetadata', args: any): Promise<DebugProtocol.Response> {
        const response = this.send(command, args)
        return response
    }
}

/* Note [error handling for custom DAP messages]

TODO [#100]

This method of supporting custom DAP messages has several drawbacks.
One of them is error messages handling.

Normally, the client should try to pick `body.error` field as error, and use
`message` if that error is absent. This is what matches the DAP spec, and what
happens for built-in DAP responses as can be seen in the open-source implementation
of VSCode: https://github.com/microsoft/vscode/blob/ec6021d199c5a7432ee64206c0ffc095f652bee1/src/vs/workbench/contrib/debug/browser/rawDebugSession.ts#L760-L785

However, for sending our custom messages in this module we provide a solution
based on `vscode-debugadapter-testsupport` package, that honors only the
`message` field, and that's annoying, that solution looks raw.
https://github.com/microsoft/vscode-debugadapter-node/blob/92976b5883a016174ec02a687e7fd5282d4c84a9/testSupport/src/protocolClient.ts#L76

Note that we cannot simply use the intended `DebugSession.customRequest`
because it becomes available along with `DebugSession`, and it is given to
the extension code only after configuration resolution, while we need custom
requests exactly to make the configuration stage interactive.
However I do not claim that there are no workarounds to this, they are yet
to be considered.

*/
