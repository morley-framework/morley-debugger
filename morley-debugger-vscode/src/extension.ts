// SPDX-FileCopyrightText: 2020 Tocqueville Group
// SPDX-FileCopyrightText: Microsoft Corporation
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ
// SPDX-License-Identifier: LicenseRef-MIT-Microsoft

'use strict';

import * as vscode from 'vscode';
import { join } from 'path';
import { platform } from 'process';
import { DebuggerClient } from './DebuggerClient';
import { RetranslateDebuggerServer } from './RetranslateDebuggerServer'
import { MichelsonConfigurationProvider } from './ConfigurationProvider'
import { createRememberingInputBox, createRememberingQuickPick, openInTzKT } from './ui'
import { ContractMetadata, DebuggedContractSession, Maybe, Ref, emptyDebuggedContractSession } from './base'
import { ParseValueCategory } from './messages';

let michelsonServer: RetranslateDebuggerServer
let michelsonClient: DebuggerClient;

// This variable is used to provide additional information
// about currently launching contract.
// 'createRememberingQuickPick' and 'ConfigurationProvider' write here,
// 'createRememberingInputBox' reads.
let debuggedContract: Ref<DebuggedContractSession> = {ref: emptyDebuggedContractSession};

export function activate(context: vscode.ExtensionContext) {
    const validateInput = (category: ParseValueCategory) => async (value: string): Promise<Maybe<string>> => {
        if (michelsonClient) {
            const entrypointType = debuggedContract.ref.pickedEntrypoint?.argType
            const onChainViewType = debuggedContract.ref.pickedOnChainView?.argType
            return (await michelsonClient.sendMsg('parseValue', { value, category, entrypointType, onChainViewType })).body?.errorMessage
        }
        return undefined
    }

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.michelson-debugger.getContractEntrypointOrOnChainView',
            createRememberingQuickPick(
                debuggedContract,
                "Please pick an entrypoint or a view to run")));

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.michelson-debugger.getContractParameter',
            createRememberingInputBox(
                context,
                validateInput,
                "parameter",
                "Please input the contract parameter",
                "Parameter value",
                debuggedContract)));

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.michelson-debugger.getContractStorage',
            createRememberingInputBox(context,
                validateInput,
                "storage",
                "Please input the contract storage",
                "Storage value",
                debuggedContract)));

    context.subscriptions.push(
        vscode.commands.registerCommand('extension.michelson-debugger.openAddress', openInTzKT))

    // Check if the debug adapter exists on the plugin directory at all.
    // Can be useful for development, unlikely to happen on CI version.
    let adapterPath = join(context.extensionPath, 'bin', `morley-debug-adapter${platform === 'win32' ? '.exe' : ''}`)
    try {
        const logOutputChannel = vscode.window.createOutputChannel("Contract logs");
        michelsonServer = new RetranslateDebuggerServer(adapterPath, [], logOutputChannel);
        michelsonClient = new DebuggerClient(michelsonServer.address(), logOutputChannel);
        logOutputChannel.show(true)
    } catch (e) {
        console.error(e)
        return undefined
    }

    // register a configuration provider for 'michelson' debug type
    const provider = new MichelsonConfigurationProvider(
        debuggedContract,
        async (file: string, logDir: string): Promise<ContractMetadata> => {
            const result = await michelsonClient.sendMsg('getContractMetadata', { file, logDir })
            return result.body
        },
        context);
    context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('michelson', provider));

    let factory = new DebugAdapterServerDescriptorFactory();

    context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('michelson', factory));
    if ('dispose' in factory) {
        context.subscriptions.push(factory);
    }
}

export function deactivate() {
    michelsonServer?.dispose();
}

class DebugAdapterServerDescriptorFactory implements vscode.DebugAdapterDescriptorFactory {
    createDebugAdapterDescriptor(_session: vscode.DebugSession, _executable: vscode.DebugAdapterExecutable | undefined): vscode.ProviderResult<vscode.DebugAdapterDescriptor> {
        return new vscode.DebugAdapterNamedPipeServer(michelsonServer.address());
    }
}
