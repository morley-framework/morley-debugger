// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ

// Useful UI elements which is used to make the plugin more pleasant to use.

import * as vscode from 'vscode';
import { ExtensionContext, QuickPickItem } from 'vscode';
import { DebuggedContractSession, Maybe, Ref, isDefined } from './base'
import { addressExists } from './address'

export type InputBoxType = "parameter" | "storage"

// Create input box which remembers previously
// inputted value in workspace storage.
export const createRememberingInputBox = (
	context: ExtensionContext,
	validateInput: (inputType: InputBoxType) => (value: string) => Promise<Maybe<string>>,
	inputBoxType: InputBoxType,
	placeHolder: string,
	prompt: string,
	debuggedContract: Ref<DebuggedContractSession>
) => (config: any): Thenable<Maybe<string>> => {

	const currentFilePath = vscode.window.activeTextEditor?.document.uri.fsPath
	let currentKey: Maybe<string> = currentFilePath && inputBoxType + '_' + currentFilePath
	let promptExtra: string = ''
	let placeholderExtra: string = ''
	let type: string
	switch (inputBoxType) {
		case "parameter":
			type = debuggedContract.ref.parameterType
			// Consider picked on-chain view or picked entrypoint in the key to remember value depending on an entrypoint
			const onChainView = debuggedContract.ref.pickedOnChainView
			const entrypoint = debuggedContract.ref.pickedEntrypoint
			if (onChainView) {
				currentKey =
					currentFilePath &&
					inputBoxType +
					(entrypoint ? + '_' + entrypoint.name : '') +
					'_' + currentFilePath
				placeholderExtra = " for '" + onChainView.name + "' on-chain view"
				promptExtra = " of type '" + onChainView.argType + "'"
			} else if (entrypoint) {
				// rewrite currentKey with entrypoint specific one
				currentKey =
					currentFilePath &&
					inputBoxType +
					(entrypoint ? + '_' + entrypoint.name : '') +
					'_' + currentFilePath
				placeholderExtra = " for '" + entrypoint.name + "' entrypoint"
				promptExtra = " of type '" + entrypoint.argType + "'"
			} else {
				promptExtra = " of type '" + debuggedContract.ref.parameterType + "'"
			}
			break;

	    case "storage":
			type = debuggedContract.ref.storageType
			promptExtra = " of type '" + debuggedContract.ref.storageType + "'"
			break;
	}
	const previousVal = currentKey && context.workspaceState.get<string>(currentKey)
	const defaultValue =
		isDefined(previousVal)
			? { value: previousVal, selection: oldValueSelection(type, previousVal) }
			: suggestTypeValue(type)
	return vscode.window.showInputBox({
		placeHolder: placeHolder + placeholderExtra,
		prompt: prompt + promptExtra,
		value: defaultValue.value,
		valueSelection: defaultValue.selection,
		validateInput: validateInput(inputBoxType),
		// Keep input box open if focus is moved out
		ignoreFocusOut: true
	}).then(newVal => {
		// Preserve new parameter value to show it next time
		if (isDefined(newVal) && isDefined(currentKey)) {
			context.workspaceState.update(currentKey, newVal)
		}
		return newVal
	})
}

const suggestTypeValue = (type: string): { value: string, selection?: [number, number] } => {
	const startsWith = (prefix: string, str: string): boolean =>
		str.substring(0, prefix.length) === prefix

	if (type === "unit") {
		return { value: "Unit", selection: [4, 4] }
	}
	else if (type === "string") {
		return { value: "\"\"", selection: [1, 1] }
	}
	else if (type === "int" || type === "nat" || type === "mutez") {
		return { value: "0" }
	}
	else if (type === "bytes") {
		// We select 0x, not put the cursor at the end, because the user will likely
		// copypaste a ready value from somewhere else.
		return { value: "0x" }
	}
	else if (type === "timestamp") {
		let defTime = new Date().toISOString()
		return { value: "\"" + defTime + "\"", selection: [1, defTime.length + 1] }
	}
	else if (startsWith("option", type)) {
		return { value: "None" }
	}
	else if (startsWith("list", type) || startsWith("set", type) || startsWith("map", type) || startsWith("big_map", type)) {
		return { value: "{  }", selection: [2, 2] }
	}
	else {
		return { value: "" }
	}
}

const oldValueSelection = (type: string, oldVal: string): Maybe<[number, number]> => {
	const startsWith = (prefix: string, str: string): boolean =>
		str.substring(0, prefix.length) === prefix
	const len = oldVal.length

	if (type === "unit") {
		return [4, 4]
	}
  else if (oldVal.charAt(0) == "\"" && (type === "string" || type === "timestamp" || type === "address")) {
		return [1, len - 1]
	}
	else if (startsWith("list", type) || startsWith("set", type) || startsWith("map", type) || startsWith("big_map", type)) {
		return [1, len - 1]
	}
}

// Create QuickPick which remembers previously
// inputted value in workspace storage.
export const createRememberingQuickPick =
	(debuggedContract: Ref<DebuggedContractSession>,
		placeHolder: string
	) => async (config: any): Promise<Maybe<string>> => {

		const currentFilePath = vscode.window.activeTextEditor?.document.uri.fsPath
		const entrypoints = currentFilePath && debuggedContract.ref.entrypoints
		const entrypointPickerOptions: QuickPickItem[] =
			entrypoints ?
				Object.entries(entrypoints).map(([name, tp]) => {
					return {
						label: name,
						description: tp,
						detail: "entrypoint"
					}
				}) :
				[]
		const onChainViews = currentFilePath && debuggedContract.ref.onChainViews
		const onChainViewPickerOptions: QuickPickItem[] =
			onChainViews ?
				Object.entries(onChainViews).map(([name, tp]) => {
					return {
						label: name,
						description: tp,
						detail: "on-chain view"
					}
				}) :
				[]

		const pickerOptions = [...entrypointPickerOptions, ...onChainViewPickerOptions]

		return vscode.window.showQuickPick<QuickPickItem>(pickerOptions,
			{
				placeHolder,
				ignoreFocusOut: true
			}).then(newVal => {
				// Have to recreate an object to make it immune to changes of 'newVal'
				if (newVal) {
					if (newVal.detail === "entrypoint") {
						debuggedContract.ref.pickedEntrypoint =
					  		{ name: newVal.label, argType: newVal.description }
						if (newVal.label === "default") {
							newVal.label = ""
						}
					} else if (newVal.detail === "on-chain view") {
						debuggedContract.ref.pickedOnChainView =
					  		{ name: newVal.label, argType: newVal.description }
					}
				} else {
					debuggedContract.ref.pickedEntrypoint = undefined
					debuggedContract.ref.pickedOnChainView = undefined
				}
				return newVal ? (newVal.label + "," + newVal.detail) : undefined
			})
	}

export async function openInTzKT(config: any): Promise<void> {
	const value = config.variable.value as string
	// Unwanted annotations may appear, so remove them:
	// Explanation of this RegEx:
	// (?:[@:%][a-zA-Z0-9_.]+ ){0,3}: Match up to three Michelson annotations
	// [^%]+: Match the address itself, ignore % which starts an entrypoint
	// (?:%(.*))?: Match the (possibly empty) entrypoint if it exists
	const regex: RegExp = /^(?:[@:%][a-zA-Z0-9_.]+ ){0,3}([^%]+)(?:%(.*))?$/
	const test = regex.exec(value)
	if (test?.length === 3) {
		// test[0] seems to contain the entire `value`
		const address = test[1]
		const entrypoint = test[2]
		let accounts: string[]
		try {
			accounts = await addressExists(address, entrypoint)
		} catch (e) {
			vscode.window.showErrorMessage(e)
			return undefined
		}

		const openAddress =
			address => vscode.env.openExternal(vscode.Uri.parse(address))

		switch (accounts.length) {
			case 0:
				vscode.window.showErrorMessage(`Address ${address} not found in TzKT`)
				break
			case 1:
				await openAddress(accounts[0])
				break
			default:
				const choices = await vscode.window.showQuickPick(
					accounts,
					{
						canPickMany: true,
						ignoreFocusOut: true,
						placeHolder: 'Network to address',
					})
				choices?.forEach(openAddress)
				break
		}
	} else {
		vscode.window.showErrorMessage(`Value is not a valid address: ${value}`)
	}
}
