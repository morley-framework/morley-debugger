// SPDX-FileCopyrightText: 2021 Oxhead Alpha
// SPDX-License-Identifier: LicenseRef-MIT-OA

// Configuration provider for Michelson debugger

import * as vscode from 'vscode';
import * as fs from 'fs';
import fetch from 'node-fetch';
import stripJsonComments from 'strip-json-comments';

import { Ref, ContractMetadataFetcher, DebuggedContractSession, Maybe, isDefined } from './base';

const extensionId = 'serokell-io.michelson-debugger';

const localEndpointConfigFilename = 'endpoint-config.json';

const remoteEndpointConfigURL = 'https://gitlab.com/api/v4/projects/23139223/repository/files/morley-debugger-vscode%2Fendpoint-config.json/raw?ref=master';

// This class makes some validations, and prepare a config for running:
// changing some fields correspondingly.
export class MichelsonConfigurationProvider implements vscode.DebugConfigurationProvider {
    fetchContractMetadata: ContractMetadataFetcher
    debuggedContract: Ref<DebuggedContractSession>
    context: vscode.ExtensionContext

    constructor(
        debuggedContract: Ref<DebuggedContractSession>,
        fetchContractMetadata: ContractMetadataFetcher,
        context: vscode.ExtensionContext)
    {
        this.fetchContractMetadata = fetchContractMetadata
        this.debuggedContract = debuggedContract
        this.context = context;
    }

    /**
     * Prepare a debug configuration just before a debug session is being launched,
     * e.g. add all missing attributes to the debug configuration.
     */
    async resolveDebugConfiguration(
        folder: vscode.WorkspaceFolder | undefined,
        config: vscode.DebugConfiguration): Promise<vscode.ProviderResult<vscode.DebugConfiguration>> {

        updateEndpointConfig();

        // if launch.json is missing or empty, using the following config instead
        if (Object.keys(config).length === 0) {
            const editor = vscode.window.activeTextEditor;
            if (editor && (editor.document.languageId === 'michelson' || editor.document.languageId === 'morley')) {
                config.name = 'Launch Michelson';
                config.entrypointOrOnChainView = "${command:AskForEntrypointOrOnChainView}"
                config.parameter = "${command:AskForParameter}"
                config.storage = "${command:AskForStorage}"
            }
        }

        // User is allowed to omit this fields, they will be assigned to the default values
        config.type ??= 'michelson'
        config.request ??= 'launch'
        config.stopOnEntry ??= true
        config.program ??= '${file}'

        //Remove entrypoints picker if there are no entrypoints in the current contract
        const currentFilePath = vscode.window.activeTextEditor?.document.uri.fsPath
        if (currentFilePath) {
            const logDir = this.context.logUri.path;
            // Create logDir if needed
            if (logDir) {
                createLogDir(logDir)
            }

            const metaData = await this.fetchContractMetadata(currentFilePath, logDir);
            this.debuggedContract.ref = {...metaData}
            if  ( Object.entries(metaData.entrypoints).length === 0
                && Object.entries(metaData.onChainViews).length === 0
                ) {
                    config.entrypointOrOnChainView = ""
                }
        }

        return config;
    }
}

function createLogDir(logDir: string): void | undefined {
    if (!fs.existsSync(logDir)) {
        fs.mkdir(logDir, {recursive: true} as fs.MakeDirectoryOptions, err => {
            if (err) {
                vscode.window.showErrorMessage("Couldn't create a log folder: " + err)
                    .then(_ => undefined) // abort launch
            }
        })
    }
}

function getEndpointConfigFilepath(): Maybe<string> {
    const extension = vscode.extensions.getExtension(extensionId);
    return extension?.extensionPath.concat(`/${localEndpointConfigFilename}`);
}

export function updateEndpointConfig() {
    const remoteConfigPromise = fetch(remoteEndpointConfigURL, { method: 'GET' }).then(
        response => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.text();
        }
    );

    const endpointConfigFilepath = getEndpointConfigFilepath();

    if (isDefined(endpointConfigFilepath)) {
        remoteConfigPromise
            .then(remoteEndpoints => fs.writeFileSync(endpointConfigFilepath, remoteEndpoints))
            .catch(console.error);
    }
}

export function parseEndpointConfigJson(): Object {
    const configFilepath = getEndpointConfigFilepath();
    let jsonString = '{}';

    if (isDefined(configFilepath)) {
        jsonString = fs.readFileSync(configFilepath).toString();
    }

    return JSON.parse(
        stripJsonComments(jsonString)
    )
}
