// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ

import * as Net from 'net';
import * as cp from 'child_process';
import { Disposable, OutputChannel } from 'vscode';
import * as vscode from 'vscode';
import * as fs from 'fs';
import { randomBytes } from 'crypto';
import { platform } from 'process';
import { join } from 'path';
import { tmpdir } from 'os';
import { prependNewline } from './base';

'use strict';

// Server to forward requests to the debug adapter
// to the process stdin, stdout of the process to
// the client.
export class RetranslateDebuggerServer implements Disposable {
	server: Net.Server
	adapterProcess: cp.ChildProcess
	logOutputChannel: OutputChannel

	public constructor(command: string, args: ReadonlyArray<string>, logOutputChannel: OutputChannel) {
		this.logOutputChannel = logOutputChannel;

		if (!fs.existsSync(command)) {
			this.showError("Couldn't find debug adapter executable")
		}

		let adapterProcess = cp.spawn(command, args);
		if (!adapterProcess || !adapterProcess.pid) {
			this.showError("Couldn't run debug adapter")
		}

		// start listening on a random named pipe path
		const pipeName = randomBytes(10).toString('hex');
		const pipePath = platform === "win32" ? join('\\\\.\\pipe\\', pipeName) : join(tmpdir(), pipeName);

		this.server = Net.createServer(socket => {
			const logSeparator = 'Content-Length';

			const requestsListener = (bytes: Buffer) => {
				// Just forward client request to the adapter
				adapterProcess.stdin.write(bytes);

				const requestLogMessage = prependNewline(logSeparator, bytes.toString());
				this.logOutputChannel.appendLine(`\nRequest:\n${requestLogMessage}`);
			}

			const responsesListener = (bytes: Buffer) => {
				// Just forward the adapter response to the client
				if (socket.writable) {
					socket.write(bytes);
				}
			}

			socket.on('data', requestsListener);
			adapterProcess.stdout.on('data', responsesListener);

			socket.on('end', () => {
				// gracefully remove all listeners, to avoid spamming to the adapter
				socket.removeListener('data', requestsListener);
				adapterProcess.stdout.removeListener('data', responsesListener);
				this.logOutputChannel.appendLine('\nDebugging session has been disconnected.');
			})
		}).listen(pipePath)

		if (!this.server.listening)
			this.showError("Adapter server couldn't start listening on pipe " + pipePath + ". Try again")
	}


	address() {
		return this.server.address() as string;
	}

	port() {
		return (this.server.address() as Net.AddressInfo).port
	}

	dispose() {
		this.adapterProcess.kill()
		this.server.close()
		this.logOutputChannel.dispose();
	}

	private showError(msg: string) {
		vscode.window.showErrorMessage(msg).
			then(_ => {
				return undefined;   // abort launch
			});
		throw new Error(msg)
	}
}
