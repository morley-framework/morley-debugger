// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ

import { DebugProtocol } from 'vscode-debugprotocol/lib/debugProtocol'
import { ContractMetadata } from './base'

export type ParseValueCategory = "parameter" | "storage" | "address" | "contract"

export interface ParseValueArguments {
    value: string
    category: ParseValueCategory
    entrypointType?: string
    onChainViewType?: string
}

export interface ParseValueRequest extends DebugProtocol.Request {
    command: 'parseValue'
    arguments: ParseValueArguments;
}

export interface ParseValueResponse extends DebugProtocol.Response {
    body?: {
        errorMessage?: string
    }
}

export interface GetContractMetadataRequest extends DebugProtocol.Request {
    command: 'getContractMetadata'
    arguments: GetContractMetadataArguments;
}

export interface GetContractMetadataArguments {
    file: string
    logDir?: string
}

export interface GetContractMetadataResponse extends DebugProtocol.Response {
    body: ContractMetadata
}
