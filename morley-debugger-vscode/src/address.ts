// SPDX-FileCopyrightText: 2020 Tocqueville Group
// SPDX-FileCopyrightText: 2021 Oxhead Alpha
// SPDX-License-Identifier: LicenseRef-MIT-OA
// SPDX-License-Identifier: LicenseRef-MIT-Microsoft

import fetch, { Response } from 'node-fetch'

import { Maybe } from './base'
import { parseEndpointConfigJson } from './ConfigurationProvider';

export interface Endpoint {
    api: string
    address: string
}

export async function addressExists(
        address: string,
        entrypoint: Maybe<string> = undefined,
        endpoints: Endpoint[] = getDefaultEndpoints()): Promise<string[]> {
    const accountEp = (ep: Endpoint) => `${ep.api}/accounts/${address}`
    const entrypointEp = (ep: Endpoint) => `${ep.api}/contracts/${address}/entrypoints`

    const responses = await Promise
        .all(endpoints.map(ep => getWithTimeout(accountEp(ep)).then(res => [res, ep] as const)))
    const found = await asyncFilter(res => isNotEmpty(res[0]), responses)
    const withEps = entrypoint
        ? await Promise.all(
            found.map(res => hasEntrypoint(entrypointEp(res[1])).then(hasEp => [res[1], hasEp] as const)))
        : found.map(res => [res[1], false] as const)
    return withEps.map(res => `${res[0].address}/${address}/${res[1] ? 'entrypoints' : ''}`)
}

async function hasEntrypoint(api: string): Promise<boolean> {
    return getWithTimeout(api)
        .then(res => res.json().then(body => res.ok && res.status === 200 && body.length > 0))
        .catch(_ => false)
}

async function getWithTimeout(ep: string, timeout: number = 5000): Promise<Response> {
    const id = setTimeout(() => { throw new Error(`Timed out after ${timeout} ms`) }, timeout)
    return fetch(ep, { method: 'GET' })
        .then(res => { clearTimeout(id); return res })
}

async function asyncFilter<A>(pred: (item: A) => Promise<boolean>, arr: A[]): Promise<A[]> {
    const booleans = await Promise.all(arr.map(pred))
    return arr.filter((_, i) => booleans[i])
}

async function isNotEmpty(res: Response): Promise<boolean> {
    return res ?
        Promise.resolve(res.ok && res.status === 200)
            .then(ok => res.json().then(body => ok && body.type !== 'empty'))
            .catch(_ => false)
        : false
}

function getDefaultEndpoints() {
    return parseEndpointConfigJson() as Endpoint[]
}
