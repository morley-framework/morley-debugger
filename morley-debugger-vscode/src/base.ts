// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ

export type Maybe<T> = T | undefined

// Make from an object reference
export type Ref<T> = {
    ref: T
}

export function isDefined<T>(x: T | undefined | null): x is T {
    return x !== null && x !== undefined
}

export interface Entrypoints {
    [entrypoint: string]: string
}
export interface OnChainViews {
    [view: string]: string
}

export interface ContractMetadata {
    parameterType: string
    storageType: string
    entrypoints: Entrypoints
    onChainViews: OnChainViews
}

export type ContractMetadataFetcher = (file: string, logDir: string) => Promise<ContractMetadata>

export interface DebuggedContractSession extends ContractMetadata {
    pickedEntrypoint?: Maybe<{ name: string, argType?: string }>
    pickedOnChainView?: Maybe<{ name: string, argType?: string }>
    logDir?: string
}

export const emptyDebuggedContractSession: DebuggedContractSession = {
    parameterType: "",
    storageType: "",
    entrypoints: {},
    onChainViews: {}
}

export function prependNewline(separator: string, text: string): string {
    const separatorRegExp = new RegExp(separator, 'g');
    return text.replace(separatorRegExp, `\r\n${separator}`)
}
