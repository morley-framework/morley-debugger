# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

.PHONY: morley-debugger test haddock haddock-no-deps clean all

# Build target from the common utility Makefile
MAKEU = $(MAKE) -f ./make/Makefile

MAKE_PACKAGE = $(MAKEU) PACKAGE=morley-debugger

all: morley-debugger

morley-debugger:
	$(MAKE_PACKAGE) dev
test:
	$(MAKE_PACKAGE) test
test-dumb-term:
	$(MAKE_PACKAGE) test-dumb-term
test-hide-successes:
	$(MAKE_PACKAGE) test-hide-successes
haddock:
	$(MAKE_PACKAGE) haddock
haddock-no-deps:
	$(MAKE_PACKAGE) haddock-no-deps
clean:
	$(MAKE_PACKAGE) clean

stylish:
	find . -name '.stack-work' -prune -o -name '*.hs' -exec stylish-haskell -i '{}' \;
