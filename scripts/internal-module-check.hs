#!/usr/bin/env bash
{-  2>/dev/null
exec /usr/bin/env runghc --ghc-arg=-package --ghc-arg=ghc "$0" "$@"
-}

-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE LambdaCase, EmptyCase, RecordWildCards, BlockArguments, DerivingStrategies
  , ViewPatterns, ImportQualifiedPost, TupleSections, OverloadedStrings, CPP #-}
{-# OPTIONS_GHC -Wall -Werror -Wno-missing-fields #-}

{- | Check that module names and exports satisfy internal module rules described
in the code style guide.

Note this requires ghc-9.0 or 9.2 to run.

Accepts a list of paths to check recursively.

Finds all @*.hs@ files, parses them, and does some simple checks.

If it found style warnings, exits with exit code 2. If it found style errors,
exist with exit code 1. Otherwise, exits with 0.
-}
module Main (main) where

#if __GLASGOW_HASKELL__ >= 902
import GHC.Driver.Config (initParserOpts)
import GHC.Parser.Lexer (ParseResult(..), initParserState, unP)
import GHC.Platform (genericPlatform)
import GHC.Settings
  (FileSettings(..), GhcNameVersion(..), PlatformMisc(..), Settings(..), ToolSettings(..))
#else
import GHC.Parser.Lexer (ParseResult(..), mkPState, unP)
import GHC.Platform (Arch(..), ByteOrder(..), OS(..), PlatformWordSize(..))
import GHC.Settings
  (FileSettings(..), GhcNameVersion(..), Platform(..), PlatformConstants(..), PlatformMini(..),
  PlatformMisc(..), Settings(..), ToolSettings(..))
#endif

import GHC.Data.FastString (mkFastString)
import GHC.Data.StringBuffer (stringToStringBuffer)
import GHC.Driver.Session
  (DynFlags, Language(..), defaultDynFlags, haddockOptions, languageExtensions,
  parseDynamicFilePragma, xopt_set)
import GHC.Hs (GhcPs, HsDecl, HsModule(..), IE(..), ImportDecl(..))
import GHC.LanguageExtensions (Extension(..))
import GHC.Parser (parseModule)
import GHC.Parser.Header (getOptionsFromFile)
import GHC.Types.SrcLoc (Located, mkRealSrcLoc, unLoc)
import GHC.Unit.Module.Name (moduleNameString)

import Data.List (isInfixOf, isSuffixOf)
import Data.Map qualified as Map
import Data.Maybe (catMaybes, fromMaybe, mapMaybe)
import System.Directory (doesDirectoryExist, listDirectory)
import System.Environment (getArgs)
import System.Exit (ExitCode(..), exitWith)
import System.FilePath ((</>))
import System.IO (IOMode(..), hGetContents, hSetEncoding, utf8, withFile)

{-# ANN module ("HLint: ignore" :: String) #-}

main :: IO ()
main = do
  dirs <- getArgs
  results <- mapM checkDir dirs
  case mconcat results of
    ResultOk -> pure ()
    ResultWarn -> exitWith $ ExitFailure 8
    ResultErr -> exitWith $ ExitFailure 16

data Result = ResultOk | ResultWarn | ResultErr
  deriving (Eq, Ord)

instance Semigroup Result where
  (<>) = max

instance Monoid Result where
  mempty = ResultOk

dirWalk :: (FilePath -> IO Result) -> FilePath -> IO Result
dirWalk action root = do
  isDirectory <- doesDirectoryExist root
  if isDirectory
  then do
    ress <- mapM (dirWalk action . (root </>)) =<< listDirectory root
    pure $ mconcat ress
  else if ".hs" `isSuffixOf` root
    then action root
    else pure ResultOk

checkDir :: FilePath -> IO Result
checkDir = dirWalk checkFile

-- NB: The author would very much like to get this from a cabalfile,
-- unfortunately, using Cabal drags in a bunch of additional dependencies, and
-- converting from Cabal's 'KnownExtension' to GHC's 'Extension' turns out to be
-- non-trivial.
defaultExtensions :: [Extension]
defaultExtensions = languageExtensions (Just Haskell2010) <>
  [ AllowAmbiguousTypes
  , ApplicativeDo
  , BangPatterns
  , BlockArguments
  , ConstraintKinds
  , DataKinds
  , DefaultSignatures
  , DeriveAnyClass
  , DeriveDataTypeable
  , DeriveFoldable
  , DeriveFunctor
  , DeriveGeneric
  , DeriveTraversable
  , DerivingStrategies
  , DerivingVia
  , EmptyCase
  , FlexibleContexts
  , FlexibleInstances
  , GADTs
  , GeneralizedNewtypeDeriving
  , ImportQualifiedPost
  , InstanceSigs
  , LambdaCase
  , MultiParamTypeClasses
  , MultiWayIf
  , NegativeLiterals
  , NumDecimals
  , NumericUnderscores
  , OverloadedLabels
  , OverloadedStrings
  , PatternSynonyms
  , PolyKinds
  , QuantifiedConstraints
  , QuasiQuotes
  , RankNTypes
  , RebindableSyntax
  , RecordWildCards
  , RecursiveDo
  , ScopedTypeVariables
  , StandaloneDeriving
  , StandaloneKindSignatures
  , StrictData
  , TemplateHaskell
  , TemplateHaskellQuotes
  , TupleSections
  , TypeApplications
  , TypeFamilies
  , TypeOperators
  , UndecidableInstances
  , UndecidableSuperClasses
  , ViewPatterns
  ]

checkFile :: FilePath -> IO Result
checkFile path = do
  (haddock, unLoc -> HsModule{..}) <- parseFile path
  case moduleNameString . unLoc <$> hsmodName of
    Nothing -> do
      putStrLn $ "Module without name: " <> path
      pure ResultOk
    Just name -> do
      case fmap unLoc . unLoc <$> hsmodExports of
        Nothing -> do
          putStrLn $ "Module without exports: " <> path
          pure ResultOk
        Just exports ->
          if isInternalModule name
          then printMsgs path name $ checkInternal haddock name exports (unLoc <$> hsmodDecls)
          else do
            let imports = unLoc <$> hsmodImports
            printMsgs path name $ checkNonInternal name exports imports

printMsgs :: String -> String -> [Msg] -> IO Result
printMsgs path name = \case
  [] -> pure ResultOk
  xs -> do
    putStrLn $ path <> " (" <> name <> "):\n" <> indentF 4 (blockListF $ buildMsg <$> xs)
    pure $ if hasErrs xs then ResultErr else ResultWarn

buildMsg :: Msg -> String
buildMsg = \case
  Err msg -> nameF "Error" msg
  Warn msg -> nameF "Warning" msg

indentF :: Int -> String -> String
indentF num = unlines . map (replicate num ' ' <> ) . lines

blockListF :: [String] -> String
blockListF = unlines . map ("- " <>)

nameF :: String -> String -> String
nameF pfx msg
  | null $ drop 1 (lines msg)
  = pfx <> ": " <> msg
  | otherwise
  = unlines $ pfx <> ":" : (("  " <>) <$> lines msg)

data Msg = Err String | Warn String

hasErrs :: [Msg] -> Bool
hasErrs = any \case
  Err{} -> True
  _ -> False

checkInternal :: Maybe String -> String -> [IE GhcPs] -> [HsDecl GhcPs] -> [Msg]
checkInternal haddock name exports decls = catMaybes
  $ check (maybe False (elem "not-home" . words) haddock)
      (Err "must have not-home Haddock attribute")
  : if null decls
    then []
    else
      [ check (containsSelfExport name exports) $ Err "must contain a self-export"
      , check (length exports == 1) $ Warn "should not contain any re-exports"
      ]

check :: Bool -> a -> Maybe a
check cond msg
  | cond = Nothing
  | otherwise = Just msg

exportedModules :: [IE GhcPs] -> [String]
exportedModules = mapMaybe \case
  IEModuleContents _ (moduleNameString . unLoc -> nm) -> Just nm
  _ -> Nothing

containsSelfExport :: String -> [IE GhcPs] -> Bool
containsSelfExport name exports = name `elem` exportedModules exports

checkNonInternal :: String -> [IE GhcPs] -> [ImportDecl GhcPs] -> [Msg]
checkNonInternal name exports imports = do
  let aliases = moduleAliases imports
      aliasLookup nm = fromMaybe [nm] $ Map.lookup nm aliases
      internalReexports = filter isInternalModule $ concatMap aliasLookup $ exportedModules exports
  catMaybes
    [ check (not $ containsSelfExport name exports) $ Err "must not contain a self-export"
    , check (null internalReexports) $
        Err $ "must not re-export internal modules:\n" <> unlines internalReexports
    ]

moduleAliases :: [ImportDecl GhcPs] -> Map.Map String [String]
moduleAliases decls = Map.fromListWith (<>) $ flip mapMaybe decls \ImportDecl{..} -> do
  let name = moduleNameString $ unLoc ideclName
      alias = moduleNameString . unLoc <$> ideclAs
  (,[name]) <$> alias

isInternalModule :: String -> Bool
isInternalModule name = ".Internal." `isInfixOf` name || ".Internal" `isSuffixOf` name

parseFile :: String -> IO (Maybe String, Located HsModule)
parseFile filename = do
  flags <- getOptionsFromFile fakeDynFlags filename
  (flags', _, _) <- parseDynamicFilePragma fakeDynFlags flags
  withFile filename ReadMode \inputHandle -> do
    hSetEncoding inputHandle utf8
    content <-  hGetContents inputHandle
    let strbuf = stringToStringBuffer content
        location = mkRealSrcLoc (mkFastString filename) 1 1
        parseState = mkPState flags' strbuf location
#if __GLASGOW_HASKELL__ >= 902
        mkPState =  initParserState . initParserOpts
#endif
    case unP parseModule parseState of
      POk _ r -> pure (haddockOptions flags', r)
      PFailed _ -> error $ "Failed parsing file " <> filename

fakeDynFlags :: DynFlags
fakeDynFlags =
  let flags = defaultDynFlags fakeSettings (error "llvm is lazy")
  in foldr (flip xopt_set) flags defaultExtensions
  where
    fakeSettings = Settings
      { sGhcNameVersion = GhcNameVersion {}
      , sFileSettings = FileSettings {}
      , sTargetPlatform = genericPlatform
      , sToolSettings = ToolSettings {}
      , sPlatformMisc = PlatformMisc {}
      , sRawSettings = []
#if __GLASGOW_HASKELL__ < 902
      , sPlatformConstants = platformConstants
#endif
      }

#if __GLASGOW_HASKELL__ < 902
genericPlatform :: Platform
genericPlatform = Platform
   { platformWordSize                = PW8
   , platformByteOrder               = LittleEndian
   , platformUnregisterised          = False
   , platformHasGnuNonexecStack      = False
   , platformHasIdentDirective       = False
   , platformHasSubsectionsViaSymbols= False
   , platformIsCrossCompiling        = False
   , platformLeadingUnderscore       = False
   , platformTablesNextToCode        = True
   , platformMini = PlatformMini {platformMini_arch=ArchX86_64, platformMini_os=OSLinux}
   }

platformConstants :: PlatformConstants
platformConstants = PlatformConstants{pc_DYNAMIC_BY_DEFAULT=False,pc_WORD_SIZE=8}
#endif
