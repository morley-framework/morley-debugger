#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2021 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script checks if the code in the last commit on the branch is "stylish"
# (as defined by our stylish-haskell config), and pushes a fixup commit if not.
set -e

bot_name="Stylish bot"
# Initializing git variables
our_branch="$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"

git config --local user.email "hi@serokell.io"
git config --local user.name "$bot_name"
git remote remove auth-origin 2> /dev/null || :
git remote add auth-origin "https://oath2:$GITLAB_PUSH_TOKEN@gitlab.com/morley-framework/morley-debugger.git"
git fetch
git checkout -B "$our_branch" --track "origin/$our_branch"

# If last commit was pushed by this bot, we can leave, the files are already fixed
if [ "$(git show -s --format='%an' HEAD)" == "$bot_name" ]; then
    echo "Skipping duplicate stylish check"
    exit 0
fi

# Running stylish-haskell on all the source files
make stylish

echo "Checking if sources are stylish"
# We add everything here to be able to see new files in the `git diff` below
git add --all

# If nothing changed, exit without errors
if [ -z "$(git diff --name-only --staged)" ]; then
    echo "Sources are indeed stylish!"
    exit 0
fi

# If any file has changed push a fixup commit
echo "Sources are insufficiently stylish, pushing a fixup commit"
git commit --fixup HEAD -m "Make stylish"
git push auth-origin "$our_branch"

# Because we don't want to run any other steps in this pipeline.
# All the steps will run on a fixup commit anyway
exit 1
