'''
SPDX-FileCopyrightText: 2022 Oxhead Alpha
SPDX-License-Identifier: LicenseRef-MIT-OA
'''

from dataclasses import dataclass
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from typing import List

endpointConfigPath = './morley-debugger-vscode/endpoint-config.json'
tzKTMainPage = 'https://tzkt.io/'

@dataclass
class NetworkEndpoint:
  api: str
  address: str

  def __key(self):
    return (self.api, self.address)

  def __hash__(self):
    return hash(self.__key())

  def pretty(self):
    return f'{{"api": "{self.api}", "address": "{self.address}"}}'

def listNetworkEndpoints(endpoints: List[NetworkEndpoint]):
  return '\n- '.join(map(lambda p: p.pretty(), endpoints))


def scrapeNetworksFromTzKT(driver: webdriver.Firefox, pageUrl: str) -> List[str]:
  networkElementClassName = 'v-list-item__title'

  driver.get(pageUrl)

  # click on drop-down list of networks
  driver.find_element(By.CLASS_NAME, 'network-select').click()

  # get the list of web elements containing network names
  networkListElement = driver.find_element(By.CLASS_NAME, 'v-select-list')
  networkElements = networkListElement.find_elements(By.CLASS_NAME, networkElementClassName)

  # extract network names from web elements
  networkNames = list(map(lambda s: s.text, networkElements))

  if len(networkNames) == 0:
      raise Exception(
        f'The list of parsed networks is empty. Most likely, \
\'{networkElementClassName}\' class name is incorrect')

  else:
    # Mainnet is not in the same list of network names
    # However, since it always exists, we add it manually
    networkNames.append("Mainnet")
    return networkNames

def networkNameToEndpoint(networkName: str) -> NetworkEndpoint:
  lowerCaseNetworkName = networkName.lower()

  return NetworkEndpoint (
    api=f'https://api.{lowerCaseNetworkName}.tzkt.io/v1',
    address=f'https://{lowerCaseNetworkName}.tzkt.io'
  )

def parseEnpointsFromConfig(configPath: str) -> List[NetworkEndpoint]:
  endpointsJSON = []

  with open(configPath, 'r') as f:
    fileLines = f.read().splitlines()

  # cut the license from json file
  fileLinesWithoutLicense = fileLines[3:]

  endpointsJSONString = ''.join(fileLinesWithoutLicense)
  endpointsJSON = json.loads(endpointsJSONString)

  endpoints = list(map(
    lambda endpointDict:
      NetworkEndpoint(api=endpointDict['api'], address=endpointDict['address']),
    endpointsJSON
  ))

  return endpoints

if __name__ == '__main__':
  # make Firefox run in headless mode
  browserOptions = webdriver.FirefoxOptions()
  browserOptions.add_argument('--headless')

  # initialize Firefox web driver
  driver = webdriver.Firefox(options=browserOptions)

  try:
    activeNetworkNames = scrapeNetworksFromTzKT(driver=driver, pageUrl=tzKTMainPage)
  finally:
    # quit Firefox web driver in any case
    driver.quit()

  activeNetworkEndpoints = set(
    map(networkNameToEndpoint, activeNetworkNames)
  )

  configNetworkEndpoints = set(parseEnpointsFromConfig(endpointConfigPath))

  missingEndpoints = activeNetworkEndpoints.difference(configNetworkEndpoints)
  extraEndpoints = configNetworkEndpoints.difference(activeNetworkEndpoints)

  if activeNetworkEndpoints != configNetworkEndpoints:
    listingElementsMsg = ''

    if len(missingEndpoints) != 0:
      listingElements = listNetworkEndpoints(missingEndpoints)
      listingElementsMsg += f'Elements that are missing in the config:\
\n- {listingElements}\n\n'

    if len(extraEndpoints) != 0:
      listingElements = listNetworkEndpoints(extraEndpoints)
      listingElementsMsg += f'Extra elements in the config:\
\n- {listingElements}\n\n'

    raise Exception(
      f'Network endpoints in \'{endpointConfigPath}\' \
differ from the active ones.\n\n\
{listingElementsMsg}\
Please, update the endpoint config.'
    )
