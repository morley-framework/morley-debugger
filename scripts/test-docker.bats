#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  contract="contracts/add1.tz"
  morley_debugger="./scripts/morley-debugger.sh"
}

@test "invoking morley-debugger with add1.tz" {
  echo exit | $morley_debugger run --contract $contract --storage 1 --parameter 1
}
