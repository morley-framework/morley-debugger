#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

@test "debugging contract with cyrillic comments with morley debugger" {
    printf "status\nnext\nstatus\nnext\nstatus\nexit" | \
        morley-debugger-console run \
                                --contract contracts/add1_with_cyrillic_comments.tz \
                                --storage 1 --parameter 1
}
