<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

This project uses a variation of the [OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow) branching model with two branches. Naming of long-lived branches is different:
* `develop` branch from OneFlow is called `master` in this repository.
* `master` branch from OneFlow is called `production` in this repository.

So code in the `master` branch represents latest development version and code in the `production` branch represents latest stable version.

## Michelson versions

In case different versions of Michelson exist we may have additional long-lived branch.
In this case our policy is the following:
1. `master` branch is compatible with current version running on mainnet.
   However, we may raise warnings when one tries to use something that is remove/deprecated/changed in newer version of Michelson.
2. `release-name` (e. g. `athens`, `babylon`, etc.) branch may be created to support newer version.
* It is protected.
* It does not have to be compatible with mainnet.
* It may be WIP and thus is not guaranteed to be fully compatible with newer version either.
* However, development of features from the new version should go there.
* All other development targets `master` as usual.
* `master` is merged to `release-name` from time to time.
* After this version is available on mainnet the branch is merged to `master` and removed.
